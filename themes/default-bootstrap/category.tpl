{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./errors.tpl"}
{if isset($category)}


	{* <pre>{print_r($category)}</pre> *}


	{if $category->id AND $category->active}
    	{if $scenes || $category->description || $category->id_image}
			<div class="content_scene_cat">
            	 {if $scenes}
                 	<div class="content_scene">
                        <!-- Scenes -->
                        {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                        {if $category->description}
                            <div class="cat_desc rte">
                            {if Tools::strlen($category->description) > 350}
                                <div id="category_description_short">{$description_short}</div>
                                <div id="category_description_full" class="unvisible">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                            {else}
                                <div>{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                    </div>
				{else}
                    <!-- Category image -->
                    {if $category->level_depth <= 2 }
	                    <div class="content_scene_cat_bg"{if $category->id_image} style="background:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}) right center no-repeat; background-size:cover; min-height:{$categorySize.height}px;"{/if}>
	                        {if $category->description}
	                            <div class="cat_desc">
	                            <span class="category-name">
	                                {strip}
	                                    {$category->name|escape:'html':'UTF-8'}
	                                    {if isset($categoryNameComplement)}
	                                        {$categoryNameComplement|escape:'html':'UTF-8'}
	                                    {/if}
	                                {/strip}
	                            </span>
	                            {if Tools::strlen($category->description) > 350}
	                                <div id="category_description_short" class="rte">{$description_short}</div>
	                                <div id="category_description_full" class="unvisible rte">{$category->description}</div>
	                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
	                            {else}
	                                <div class="rte">{$category->description}</div>
	                            {/if}
	                            </div>
	                        {/if}
	                     </div>
                    {/if}
                  {/if}
            </div>
		{/if}
		{* <h1 class="page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if}"><span class="cat-name">{$category->name|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</span>{include file="$tpl_dir./category-count.tpl"} *}
		
		
		
		{* {if isset($smarty.get.allcanada)}
			<span style="font-weight:900!important;">(All Canada)</span></h1>
		{/if}
		
		{if isset($smarty.get.allusa)}
			<span style="font-weight:900!important;">(All USA)</span></h1> 
		{/if}
		
		
		{if !isset($smarty.get.allcanada) && !isset($smarty.get.allusa) }
		<span style="font-weight:900!important;">({$cityinfos.cityName}, {$cityinfos.regionName},  100 miles radius)</span></h1>
		{/if} *}
		<div class="category_name">
			{if $category->id == 1438}
				Products & Accessories
			{else}
			{$category->name|escape:'html':'UTF-8'}
			{/if}
		</div>
		{if $show_geo_filters}
			<div class="product_filters">
				<a rel="nofollow" href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}?allusa=1" class="{if isset($smarty.get.allusa)}active_directory{/if}"> 
				<span>Search USA</span>
				</a>
				<span class="black_circle"></span>
				<a rel="nofollow" href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}?allcanada=1" class="{if isset($smarty.get.allcanada)}active_directory{/if}">
				<span>Search Canada</span>
				</a>
				<span class="black_circle"></span>
				<a rel="nofollow" href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="{if !isset($smarty.get.allcanada) && !isset($smarty.get.allusa)}active_directory{/if}">
				<span>Search Selected City ({$cityinfos.cityName}, {$cityinfos.regionName})</span>
				</a>
			</div>
		{/if}
		{if $top_category_baner !== ''}
		<div class="top_category_banner">
			{if $top_category_baner.type == 2 || $top_category_baner.type== 3}
			<div class="publi">
				<a target="_blank" href="{if strpos($top_category_baner.url,'http') !== false}{$top_category_baner.url}{else}http://{$top_category_baner.url}{/if}">
					<img class="publi-img" src="{getBuildBannerUrl($top_category_baner.banner_img)}" />
				</a>
			</div>
			{elseif $top_category_baner.type == 1} 
			<div class="publi">
				<a target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$top_category_baner.id_camp}">
					<img class="publi-img" src="{getBuildBannerUrl($top_category_baner.banner_img)}" />
				</a>
			</div>
			{/if}
		</div>
		{/if}
		
		{if $category_fetature_products}
			<div id="category-feature-products">
				<span class="featured_title">{l s='Featured'}</span>
				<div>
					{include file="./category-feature-products.tpl" products=$category_fetature_products id='category-feature-products-list'}
				</div>
			</div>
		{/if}
		{if $category->id !== 1438}
		<div class="category_banner_section">
				{hook h='banner'}
		</div>
		{/if}
		{if isset($subcategories) && $subcategories}
			{foreach from=$subcategories item="sub_category"}
				{if !isset($sub_products[$sub_category.id_category])}
					{continue}
				{/if}
				<div class="sub-category-box">
					<span class="sub_cat_name">{$sub_category.name}</span>
					<a class="pull-right show_sub_cat" href="{$link->getCategoryLink($sub_category.id_category)}">{l s='Show all'}</a>
					<div class="sub-products-list">
						{include file="./category-sub-products.tpl" products=$sub_products[$sub_category.id_category]}
					</div>
				</div>
			{/foreach}
		{/if}
		{if $products}
			<div class="content_sortPagiBar clearfix">
            	<div class="sortPagiBar clearfix">
            		{* {include file="./product-sort.tpl"} *}
                	{include file="./nbr-product-page.tpl"}
				</div>
               <div class="top-pagination-content clearfix">
                	{* {include file="./product-compare.tpl"} *}
					{include file="$tpl_dir./pagination.tpl"}
                </div> 
			</div>
			{include file="./product-list.tpl" products=$products}
			<div class="content_sortPagiBar">
				<div class="bottom-pagination-content clearfix">
					{* {include file="./product-compare.tpl" paginationId='bottom'} *}
                    {include file="./pagination.tpl" paginationId='bottom'}
				</div>
			</div>
		{/if}
		{if $bottom_category_baner !== ''}
		<div class="bottom_category_baner">
			{if $bottom_category_baner.type == 2 || $bottom_category_baner.type == 3}
			<div class="publi">
				<a target="_blank" href="{if strpos($bottom_category_baner.url,'http') !== false}{$bottom_category_baner.url}{else}http://{$bottom_category_baner.url}{/if}">
					<img class="publi-img" src="{getBuildBannerUrl($bottom_category_baner.banner_img)}" />
				</a>
			</div>
			{elseif $bottom_category_baner.type == 1} 
			<div class="publi">
				<a target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$bottom_category_baner.id_camp}">
					<img class="publi-img" src="{getBuildBannerUrl($bottom_category_baner.banner_img)}" />
				</a>
			</div>
			{/if}
		</div>
		{/if}
		
		
		
		
	{elseif $category->id}
		<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
	{/if}
{/if} 


{if isset($products) && $products}
	{*define number of products per line in other page for desktop*}
	{assign var='nbItemsPerLine' value=2}
	{assign var='nbItemsPerLineTablet' value=2}
	{assign var='nbItemsPerLineMobile' value=1}
	{*define numbers of product per line in other page for tablet*}
	{assign var='nbLi' value=$products|@count}
	{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
	{math equation="nbLi/nbItemsPerLineTablet" nbLi=$nbLi nbItemsPerLineTablet=$nbItemsPerLineTablet assign=nbLinesTablet}
	<!-- Products list -->
	<ul {if isset($id) && $id} id="{$id}"{else} id="product_list"{/if} class="product_list grid row{if isset($class) && $class} {$class}{/if}">
		{foreach from=$products item=product name=products}
			{* {if $product.review == 1} *}
				{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$nbItemsPerLine assign=totModulo}
				{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineTablet assign=totModuloTablet}
				{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineMobile assign=totModuloMobile}
				{if $totModulo == 0}
					{assign var='totModulo' value=$nbItemsPerLine}
				{/if}
				{if $totModuloTablet == 0}
					{assign var='totModuloTablet' value=$nbItemsPerLineTablet}
				{/if}
				{if $totModuloMobile == 0}
					{assign var='totModuloMobile' value=$nbItemsPerLineMobile}
				{/if}
				<li class="col-xs-12 col-sm-6 col-md-3 {if $smarty.foreach.products.iteration%$nbItemsPerLine == 1} last-in-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLine == 0}{* first-in-line *}{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModulo)} last-line{/if}{if $smarty.foreach.products.iteration%$nbItemsPerLineTablet == 0} last-item-of-tablet-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLineTablet == 1} first-item-of-tablet-line{/if}{if $smarty.foreach.products.iteration%$nbItemsPerLineMobile == 0} last-item-of-mobile-line{elseif $smarty.foreach.products.iteration%$nbItemsPerLineMobile == 1} first-item-of-mobile-line{/if}{if $smarty.foreach.products.iteration > ($smarty.foreach.products.total - $totModuloMobile)} last-mobile-line{/if}">
					<div class="product-container" itemscope itemtype="https://schema.org/Product">
						<div class="left-block">
							<div class="product-image-container">
								{if isset($product.featurecustomer)}
									{if $product.featurecustomer == 1}
										<img style="position: absolute;height: 86px;right: -6px;top: -6px;z-index:999999999" src="{$img_dir}fea.png">
									{/if}
								{/if}
								<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
									<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
								</a>
							</div>
							{if isset($product.is_virtual) && !$product.is_virtual}
								{hook h="displayProductDeliveryTime" product=$product}
							{/if}
							{hook h="displayProductPriceBlock" product=$product type="weight"}
						</div>
						<div class="shop-right-block">
							
							<div itemprop="name">
								{if isset($product.pack_quantity) && $product.pack_quantity}
									{$product.pack_quantity|intval|cat:' x '}
								{/if}
								<a class="shop-product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
									{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
								</a>
								
							</div>

							
							
							{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) 
								 || (isset($product.available_for_order) && $product.available_for_order)))}
								<div class="shop_product_price">
									{* not need show price *}
									{if false && isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
										{hook h="displayProductPriceBlock" product=$product type='before_price'}
										<span class="shop_product_price_block">
											{if !$priceDisplay}
												{convertPrice price=$product.price}
											{else}
												{convertPrice price=$product.price_tax_exc}
											{/if}
										</span>
										{if isset($product.default_attribute_name)}
											<span class="shop_product_size">{$product.default_attribute_name}</span>
										{/if}
										{if isset($product.nb_attributes) && $product.nb_attributes - 1 > 0}
											<a href="{$product.link|escape:'html':'UTF-8'}" id="shop_product_more_sizes"> 
												+{$product.nb_attributes - 1} more sizes
											</a>
										{/if}
										{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											{hook h="displayProductPriceBlock" product=$product type="old_price"}
											<span class="old-price product-price">
												{displayWtPrice p=$product.price_without_reduction}
											</span>
											{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
											{if $product.specific_prices.reduction_type == 'percentage'}
												<span class="price-percent-reduction">
													-{$product.specific_prices.reduction * 100}%
												</span>
											{/if}
										{/if}
										{hook h="displayProductPriceBlock" product=$product type="price"}
										{hook h="displayProductPriceBlock" product=$product type="unit_price"}
										{hook h="displayProductPriceBlock" product=$product type='after_price'}
									{/if}
								</div>
							{/if}		
						</div>
					</div>
				</li>
			{* {/if} *}
		{/foreach}
	</ul>
	<!-- End Products list -->
{/if}


$(document).ready(function(){
   linkable_page_info = new Array("66", "65", "64", "63", "62", "61", "60", "49", "48", "47", "46", "45", "44", "122", "42", "41", "40", "39", "38", "37", "36", "35", "34", "33", "32", "31", "30", "29", "28", "27", "26", "25", "24", "23", "22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "12", "11", "10", "9", "7", "6");
    function isTouchEnabled() {
        return "ontouchstart" in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0
    }

    function addEvent(e, t) {
        var n = $("#" + e);
        var r = $("#" + e + "," + "#" + map_config[e]["namesId"]);
        var i = $("#map").height();
        $("#" + map_config[e]["namesId"]).find("tspan").attr({
            fill: map_config["default"]["namescolor"]
        });
        n.attr({
            fill: map_config[e]["upcolor"],
            stroke: map_config["default"]["bordercolor"]
        });
        r.attr({
            cursor: "default"
        });
        if (map_config[e]["enable"] == true) {
            if (isTouchEnabled()) {
                r.on("touchstart", function(t) {
                    var n = t.originalEvent.touches[0];
                    var r = n.pageX - 20,
                        i = n.pageY + -38;
                    var s = $("[selected]")[0];
                    var o = $("#tip").outerWidth(),
                        u = $("#tip").outerHeight(),
                        r = r + o > $(document).scrollLeft() + $(window).width() ? r - o - 20 * 2 : r;
                    i = i + u > $(document).scrollTop() + $(window).height() ? $(document).scrollTop() + $(window).height() - u - 10 : i;
                    if (s) {
                        s.removeAttribute("style");
                        s.removeAttribute("selected")
                    }
                    $("#" + e).css({
                        fill: map_config[e]["downcolor"]
                    }).attr("selected", "selected");
                    $("#tip").show().html(map_config[e]["name"]);
                    $("#tip").css({
                        left: r,
                        top: i
                    })
                });
                r.on("touchend", function() {
                    $("#msg_title").html(map_config[e]["name"]);
                    $("#msg_data").html(map_config[e]["data"]);
                    $("#tip").hide()
                })
            } else {
                r.attr({
                    cursor: "pointer"
                });
                r.hover(function() {
                    $("#tip").show().html(map_config[e]["name"]);
                    if (!n.attr("selected")) {
                        n.css({
                            fill: map_config[e]["overcolor"]
                        })
                    }
                }, function() {
                    var t = $("#" + e);
                    if (!t.attr("selected")) {
                        t.css({
                            fill: map_config[e]["upcolor"]
                        })
                    }
                    $("#tip").hide()
                });
                r.click(function(t) {
                    var n = $("[selected]")[0];
                    if (n) {
                        n.removeAttribute("style");
                        n.removeAttribute("selected")
                    }
                    $("#" + e).css({
                        fill: map_config[e]["downcolor"]
                    }).attr("selected", "selected");
                    $("#msg_title").html(map_config[e]["name"]);
                    $("#msg_data").html(map_config[e]["data"]);
                });
                r.mousemove(function(e) {
                    var t = e.pageX - 20,
                        n = e.pageY + -38;
                    var r = $("#tip").outerWidth(),
                        i = $("#tip").outerHeight(),
                        t = t + r > $(document).scrollLeft() + $(window).width() ? t - r - 20 * 2 : t;
                    n = n + i > $(document).scrollTop() + $(window).height() ? $(document).scrollTop() + $(window).height() - i - 10 : n;
                    $("#tip").css({
                        left: t,
                        top: n
                    })
                })
            }
        }
    }
    $(function() {
        addEvent("map_1");
        addEvent("map_2");
        addEvent("map_3");
        addEvent("map_4");
        addEvent("map_5");
        addEvent("map_6");
        addEvent("map_7");
        addEvent("map_8");
        addEvent("map_9");
        addEvent("map_10");
        addEvent("map_11");
        addEvent("map_12");
        addEvent("map_13");
        addEvent("map_14");
        addEvent("map_15");
        addEvent("map_16");
        addEvent("map_17");
        addEvent("map_18");
        addEvent("map_19");
        addEvent("map_20");
        addEvent("map_21");
        addEvent("map_22");
        addEvent("map_23");
        addEvent("map_24");
        addEvent("map_25");
        addEvent("map_26");
        addEvent("map_27");
        addEvent("map_28");
        addEvent("map_29");
        addEvent("map_30");
        addEvent("map_31");
        addEvent("map_32");
        addEvent("map_33");
        addEvent("map_34");
        addEvent("map_35");
        addEvent("map_36");
        addEvent("map_37");
        addEvent("map_38");
        addEvent("map_39");
        addEvent("map_40");
        addEvent("map_41");
        addEvent("map_42");
        addEvent("map_43");
        addEvent("map_44");
        addEvent("map_45");
        addEvent("map_46");
        addEvent("map_47");
        addEvent("map_48");
        addEvent("map_49");
        addEvent("map_50");
        addEvent("map_51")
    });
    $(function() {
        if ($("#lakes").find("path").eq(0).attr("fill") != "undefined") {
            $("#lakes").find("path").attr({
                fill: map_config["default"]["lakescolor"]
            }).css({
                stroke: map_config["default"]["bordercolor"]
            })
        }
        $(".tip").css({
            "box-shadow": "1px 2px 4px " + map_config["default"]["namesShadowColor"],
            "-moz-box-shadow": "2px 3px 6px " + map_config["default"]["namesShadowColor"],
            "-webkit-box-shadow": "2px 3px 6px " + map_config["default"]["namesShadowColor"]
        });
        if ($("#shadow").find("path").eq(0).attr("fill") != "undefined") {
            var e = map_config["default"]["shadowOpacity"];
            var e = parseInt(e);
            if (e >= 100) {
                e = 1
            } else if (e <= 0) {
                e = 0
            } else {
                e = e / 100
            }
            $("#shadow").find("path").attr({
                fill: map_config["default"]["shadowcolor"]
            }).css({
                "fill-opacity": e
            })
        }
        $("#msg_title").html(map_config["default"]["msg_title"]);
        $("#msg_data").html(map_config["default"]["msg_data"]);
        $("#msg_title").attr({
            unselectable: "on",
            onselectstart: "return false;"
        }).css({
            "-moz-user-select": "none;",
            cursor: "default"
        })
    })

    var map_config = {
        "default": {
            bordercolor: "#9CA8B6",
            lakescolor: "#66CCFF",
            shadowcolor: "#000000",
            shadowOpacity: "50",
            namescolor: "#666666",
            namesShadowColor: "#666666",
            msg_title: "ADVANCED USA MAP",
            msg_data: 'This map is built in HTML5 code and runs with javascript based on the new technique of Scalable Vector Graphics (<a href="http://en.wikipedia.org/wiki/Scalable_Vector_Graphics">SVG</a>), So it can be scaled to any size without losing its quality.<br/><br/>Also it doesn&#39;t require special plugins to work such as Flash player, So it works on all platforms including iPad and iPhone.<br/><br/>Each state has its own parameters so you can change upcolor, hover color and click color of each state also you can enable/disable the state. You can also add <b>HTML</b> formatted data for each state i.e you can change the font <b><font color="#FF3300">c</font><font color="#FF9900">o</font><font color="#FFFF00">l</font><font color="#00FF00">o</font><font color="#00CCFF">r</font></b>, <font size="+2">size</font>, <i>style</i>, <u>underline</u>....etc<br/><br/>You can add clickable links such as websites or emails like this:<br>E-Mail: <a href="mailto:me@site.com" target="_blank"><u>me@Site.com</u></a><br/>Website: <a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/>You can load image like this:<br/><a href="http://html5interactivemaps.com/"><img src="img/logo.png"></a><br/><br/>The scroll bar appears only with long content, i.e It&#39;s autohide with short text. So you can add information as much as you want.<br/><br/>Responsive;it can fit in its available space without the need to scroll horizontally while preserving its quality.'
        },
        map_1: {
            namesId: "AL",
            name: "ALABAMA",
            data: '<b><u>Main office in Alabama</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, AL 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, AL 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, AL 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, AL 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_2: {
            namesId: "AK",
            name: "ALASKA",
            data: '<b><u>Main office in Alaska</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, AK 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, AK 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, AK 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, AK 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_3: {
            namesId: "AZ",
            name: "ARIZONA",
            data: '<b><u>Main office in Arizona</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, AZ 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, AZ 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, AZ 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, AZ 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_4: {
            namesId: "AR",
            name: "ARKANSAS",
            data: '<b><u>Main office in Arkansas</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, KS 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, KS 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, KS 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, KS 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_5: {
            namesId: "CA",
            name: "CALIFORNIA",
            data: 'California is a state located on the West Coast of the United States. It is the most populous U.S. state, home to 1 out of 8 Americans, and is the third largest state by area (after Alaska and Texas).<br/><img src="img/california.png"><br/><br/>- Nickname: The Golden State<br/>- Capital: Sacramento<br/>- Largest city: Los Angeles<br/>- Area: 163,696 sq mi<br/>- Population: 38,041,430 (2012 est)<br><br/><a href="http://en.wikipedia.org/wiki/California" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_6: {
            namesId: "CO",
            name: "COLORADO",
            data: '<b><u>Main office in Colorado</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, CO 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, CO 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, CO 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, CO 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_7: {
            namesId: "CT",
            name: "CONNECTICUT",
            data: '<b><u>Main office in Connecticut</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, CT 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, CT 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, CT 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, CT 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_8: {
            namesId: "DE",
            name: "DELAWARE",
            data: '<b><u>Main office in Delaware</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, DE 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, DE 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, DE 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, DE 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_9: {
            namesId: "FL",
            name: "FLORIDA",
            data: '<b>Geography:</b><br>Much of the state of Florida is situated on a peninsula between the Gulf of Mexico, the Atlantic Ocean, and the Straits of Florida. Spanning two time zones, It extends to the northwest into a panhandle, extending along the northern Gulf of Mexico.<br><br><b>Climate:</b><br>The climate of Florida is tempered somewhat by the fact that no part of the state is very distant from the ocean. North of Lake Okeechobee, the prevalent climate is humid subtropical, while coastal areas south of the lake (including the Florida Keys) have a true tropical climate.<br><br/><a href="http://en.wikipedia.org/wiki/Florida" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_10: {
            namesId: "GA",
            name: "GEORGIA",
            data: '<b><u>Main office in Georgia</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, GA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, GA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, GA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, GA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_11: {
            namesId: "HI",
            name: "HAWAII",
            data: '<u>Hawaii is the only state of the United States that:</u><br/><br/>- is not geographically located in North America.<br/>- is completely surrounded by water.<br/>- grows coffee.<br/>- is entirely an <a href="http://en.wikipedia.org/wiki/Archipelago" target="_blank"><u>archipelago</font></u></a>.<br/>- has a royal palace.<br/>- does not have a straight line in its state boundary.<br><br><br/><a href="http://en.wikipedia.org/wiki/Hawaii#Topography" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_12: {
            namesId: "ID",
            name: "IDAHO",
            data: '<b><u>Main office in Idaho</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, ID 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, ID 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, ID 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, ID 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_13: {
            namesId: "IL",
            name: "ILLINOIS",
            data: '<b><u>Main office in Illinois</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, IL 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, IL 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, IL 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, IL 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_14: {
            namesId: "IN",
            name: "INDIANA",
            data: '<b><u>Main office in Indiana</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, IN 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, IN 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, IN 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, IN 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_15: {
            namesId: "IA",
            name: "IOWA",
            data: '<b><u>Main office in Iowa</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, IA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, IA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, IA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, IA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_16: {
            namesId: "KS",
            name: "KANSAS",
            data: '<b><u>Main office in Kansas</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, KS 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, KS 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, KS 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, KS 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_17: {
            namesId: "KY",
            name: "KENTUCKY",
            data: '<b><u>Main office in Kentucky</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, KY 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, KY 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, KY 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, KY 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_18: {
            namesId: "LA",
            name: "LOUISIANA",
            data: '<b><u>Main office in Louisiana</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, LA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, LA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, LA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, LA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_19: {
            namesId: "ME",
            name: "MAINE",
            data: '<b><u>Main office in Maine</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, ME 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, ME 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, ME 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, ME 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_20: {
            namesId: "MD",
            name: "MARYLAND",
            data: '<b><u>Main office in Maryland</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, MD 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, MD 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, MD 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, MD 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_21: {
            namesId: "MA",
            name: "MASSACHUSETTS",
            data: '<b>Massachusetts</b><br>is bordered on the north by <font color="#99FF00">New Hampshire</font> and <font color="#FF9900">Vermont</font>;on the west by <font color="#66FFFF">New York</font>;on the south by <font color="#FFFF33">Connecticut</font> and <font color="#FFFF33">Rhode Island</font>;and on the east by the <font color="#FF99FF">Atlantic Ocean</font>.<br><br><b>Population:</b><br>Massachusetts had an estimated 2009 population of 6,593,587. As of 2007, Massachusetts is estimated to be the third most densely populated U.S. state, with 822.7 per square mile, after New Jersey and Rhode Island, and ahead of Connecticut and Maryland.<br><br>Massachusetts has seen both population increases and decreases in recent years. For example, while some Bay Staters are leaving, others including Asian, Hispanic and African immigrants, arrive to replace them. Massachusetts in 2004 included 881,400 foreign-born residents.<br><br/><a href="http://en.wikipedia.org/wiki/Massachusetts" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_22: {
            namesId: "MI",
            name: "MICHIGAN",
            data: '<b>Nicknames:</b><br/>-The Great Lakes State;<br/>-The Wolverine State;<br/>-The Automotive State;<br/>-Water-Winter Wonderland;<br/>-The Lady of Lake;<br/>-The Auto State.<br/><br/><b>Taxation:</b><br>Michigan&#39;s personal income tax is set to a flat rate of 4.35%. Some cities impose additional income taxes. Michigan&#39;s state sales tax is 6%. Property taxes are assessed on the local level, but every property owner&#39;s local assessment contributes six mills (six dollars per thousand dollars of property value) to the statutory State Education Tax. In 2007, Michigan repealed its Single Business Tax (SBT) and replaced it with a Michigan Business Tax (MBT) in order to stimulate job growth by reducing taxes for seventy percent of the businesses in the state. According to the Bureau of Economic Analysis, recent growth in Michigan is 0.1%.<br><br><a href="http://en.wikipedia.org/wiki/Michigan" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_23: {
            namesId: "MN",
            name: "MINNESOTA",
            data: '<b>Nicknames:</b><br/>-North Star State;<br/>-Land of 10,000 Lakes;<br/>-The Gopher State.<br/><br/><b>Cities and towns:</b><br/>Saint Paul, located in east-central Minnesota along the banks of the Mississippi River, has been Minnesota&#39;s capital city since 1849, first as capital of the Territory of Minnesota, and then as state capital since 1858.<br/><br/>Saint Paul is adjacent to Minnesota&#39;s most populous city, Minneapolis;they and their suburbs are known collectively as the Twin Cities metropolitan area, the thirteenth largest metropolitan area in the United States and home to about 60% of the state&#39;s population. The remainder of the state is known as "Greater Minnesota" or "Outstate Minnesota".<br><br>The state has eighteen cities with populations above 50,000 (based on 2005 estimates). In descending order of size they are Minneapolis, Saint Paul, Rochester, Duluth, Bloomington, Plymouth, Brooklyn Park, Eagan, Coon Rapids, Saint Cloud, Burnsville, Eden Prairie, Maple Grove, Woodbury, Blaine, Apple Valley, Lakeville, and Minnetonka. Of these only Rochester, Duluth, and Saint Cloud are outside the Twin Cities metropolitan area.<br><br><a href="http://en.wikipedia.org/wiki/Minnesota" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_24: {
            namesId: "MS",
            name: "MISSISSIPPI",
            data: '<b>Nicknames:</b><br/>-The Magnolia State;<br/>-The Hospitality State.<br/><br/><b>Flooding:</b><br>Due to seasonal flooding possible from December to June, the Mississippi River created a fertile floodplain in the Mississippi Delta, including tributaries. Early planters used slaves to build levees along the Mississippi River to divert flooding. They built on top of the natural levees that formed from dirt deposited after the river flooded. As cultivation of cotton increased in the Delta, planters hired Irish laborers to ditch and drain their land.<br/><br/><a href="http://en.wikipedia.org/wiki/Mississippi" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_25: {
            namesId: "MO",
            name: "MISSOURI",
            data: '<b><u>Main office in Missouri</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, MO 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, MO 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, MO 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, MO 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_26: {
            namesId: "MT",
            name: "MONTANA",
            data: '<b><u>Main office in Montana</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, MT 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, MT 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, T4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, MT 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_27: {
            namesId: "NE",
            name: "NEBRASKA",
            data: '<b>Rural flight:</b><br/>Eighty-nine percent of the cities in Nebraska have fewer than 3,000 people. Nebraska shares this characteristic with five other Midwest and Southern states (Kansas, Oklahoma, North and South Dakota, and Iowa). Hundreds of towns have a population of fewer than 1,000.<br/><br/>Fifty-three of Nebraska&#39;s 93 counties reported declining populations between 1990 and 2000, ranging from a 0.06% loss (Frontier County) to a 17.04% loss (Hitchcock County). Other portions of the state have experienced substantial growth. In 2000, the city of Omaha had a population of 390,007;in 2005, the city&#39;s estimated population was 414,521(427,872 including the recently annexed city of Elkhorn), a 6.3% increase over five years. The city of Lincoln had a 2000 population of 225,581 and a 2005 estimated population of 239,213, a 6.0% change.<br/><br/>Regional population declines have forced many rural schools to consolidate.<br/><br/>Reference: <a href="http://en.wikipedia.org/wiki/Nebraska" target="_blank"><u>Wikipedia</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_28: {
            namesId: "NV",
            name: "NEVADA",
            data: '<b>Nevada:</b><br>is the seventh-largest state in area, and geographically covers the Mojave Desert in the south to the Great Basin in the north. It is the most arid state in the Union. Approximately 86% of the state&#39;s land is owned by the U.S federal government under various jurisdictions both civilian and military. As of 2008, there were about 2.6 million residents, with over 85% of the population residing in the metropolitan areas of Las Vegas and Reno. The state is well known for its easy marriage and divorce proceedings, entertainment, legalized gambling and, in 8 out of its 16 counties, legalized active brothels.<br/><br/><a href="http://en.wikipedia.org/wiki/Nevada" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_29: {
            namesId: "NH",
            name: "NEW HAMPSHIRE",
            data: '<b>New Hampshire:</b><br>is a state in the New England region of the northeastern United States of America. The state was named after the southern English county of Hampshire. It borders Massachusetts to the south, Vermont to the west, Maine and the Atlantic Ocean to the east, and the Canadian province of Quebec to the north. New Hampshire ranks 44th in land area, 46th in total area of the 50 states, and 41st in population. It became the first post-colonial sovereign nation in the Americas when it broke off from Great Britain in January 1776, and was one of the original thirteen states that founded the United States of America six months later. In June 1788, it became the ninth state to ratify the United States Constitution, bringing that document into effect. New Hampshire was the first U.S. state to have its own state constitution. It has no general sales tax, nor is personal income (other than interest and dividends) taxed at either the state or local level. Concord is the state capital, while Manchester is the largest city in the state.<br/><br/><a href="http://en.wikipedia.org/wiki/New_Hampshire" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_30: {
            namesId: "NJ",
            name: "NEW JERSEY",
            data: '<b>History:</b><br/>Around 180 million years ago, during the Jurassic Period, New Jersey bordered North Africa. The pressure of the collision between North America and Africa gave rise to the Appalachian Mountains. Around 18,000 years ago, the Ice Age resulted in glaciers that reached New Jersey. As the glaciers retreated, they left behind Lake Passaic, as well as many rivers, swamps, and gorges.<br/><br/><br/><br/><a href="http://en.wikipedia.org/wiki/New_Jersey" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#bdd8f2",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_31: {
            namesId: "NM",
            name: "NEW MEXICO",
            data: '<b>Area:</b><br/><u>-Total:</u> 121,665 sq mi<br/><u>-Width:</u> 342 miles<br/><u>-Length:</u> 370 miles<br/><u>-% water:</u> 0.2<br/><u>-Latitude:</u> 31°20&#39;N to 37° N<br/><u>-Longitude:</u> 103° W to 109°3&#39;W<br/><br/><b>Languages:</b><br>According the 2000 U.S. Census, 28.76% of the population aged 5 and older speak Spanish at home, while 4.07% speak Navajo. Speakers of New Mexican Spanish dialect are mainly descendants of Spanish colonists who arrived in New Mexico in the 16th, 17th and 18th centuries.<br><br><a href="http://en.wikipedia.org/wiki/New_Mexico" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_32: {
            namesId: "NY",
            name: "NEW YORK",
            data: '<b><u>Main office in New York</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, NY 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, NY 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, NY 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, NY 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_33: {
            namesId: "NC",
            name: "NORTH CAROLINA",
            data: '<b><u>Main office in North Carolina</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, NC 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, NC 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, NC 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, NC 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_34: {
            namesId: "ND",
            name: "NORTH DAKOTA",
            data: '<b><u>Main office in North Dakota</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, ND 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, ND 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, ND 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, ND 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_35: {
            namesId: "OH",
            name: "OHIO",
            data: '<b><u>Main office in Ohio</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, OH 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, OH 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, OH 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, OH 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_36: {
            namesId: "OK",
            name: "OKLAHOMA",
            data: '<b><u>Main office in Oklahoma</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, OK 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, OK 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, OK 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, OK 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_37: {
            namesId: "OR",
            name: "OREGON",
            data: '<b><u>Main office in Oregon</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, OR 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, OR 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, OR 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, OR 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_38: {
            namesId: "PA",
            name: "PENNSYLVANIA",
            data: '<b><u>Main office in Pennsylvania</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, PA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, PA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, PA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, PA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_39: {
            namesId: "RI",
            name: "RHODE ISLAND",
            data: '<b><u>Main office in Rhode Island</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, PA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, RI 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, RI 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, RI 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_40: {
            namesId: "SC",
            name: "SOUTH CAROLINA",
            data: '<b><u>Main office in South Carolina</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, SC 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, SC 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, SC 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, SC 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_41: {
            namesId: "SD",
            name: "SOUTH DAKOTA",
            data: '<b><u>Main office in South Dakota</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, SD 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, SD 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, SD 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, SD 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_42: {
            namesId: "TN",
            name: "TENNESSEE",
            data: '<b><u>Main office in Tennessee</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, TN 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, TN 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, TN 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, TN 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_43: {
            namesId: "TX",
            name: "TEXAS",
            data: 'Texas is the second most populous U.S. state (after California) and the second-largest of the 50 states in the United States of America (after Alaska).<br/><img src="img/texas.png"><br/><br/>- Nickname: The Lone Star State<br/>- Capital: Austin<br/>- Largest city: Houston<br/>- Area: 268,581 sq mi<br/>- Population: 26,059,203 (2012 est)<br><br/><a href="http://en.wikipedia.org/wiki/Texas" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#92badf",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_44: {
            namesId: "UT",
            name: "UTAH",
            data: '<b><u>Main office in Utah</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, UT 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, UT 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, UT 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, UT 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_45: {
            namesId: "VT",
            name: "VERMONT",
            data: '<b><u>Main office in Vermont</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, VT 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, VT 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, VT 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, VT 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_46: {
            namesId: "VA",
            name: "VIRGINIA",
            data: '<b><u>Main office in Virginia</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, VA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, VA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, VA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, VA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#a8caea",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_47: {
            namesId: "WA",
            name: "WASHINGTON",
            data: '<b><u>Main office in Washington</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, WA 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, WA 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, WA 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, WA 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_48: {
            namesId: "WV",
            name: "WEST VIRGINIA",
            data: '<b><u>Main office in West Virginia</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, WV 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, WV 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, WV 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, WV 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_49: {
            namesId: "WI",
            name: "WISCONSIN",
            data: '<b><u>Main office in Wisconsin</u></b><br/><br/><font color="#999999">Street Address:</font><br/>4321 State Docks Rd.Decatur, WI 4321-4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Decatur, WI 54321-4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><b><u>Branch</u></b><br/><br/><font color="#999999">Street Address:</font><br/>54321 County Road 10. Ashville, WI 4321<br/><font color="#999999">Postal Address:</font><br/>P.O. Box 4321 Ashville, WI 4321<br/><br/><font color="#999999">Telephone:</font> (256) 555-4321<br/><font color="#999999">Fax Number:</font> (256) 555-4320<br/><br/><font color="#999999">E-Mail:</font><a href="mailto:me@Site.com" target="_blank"><u>me@Site.com</u></a><br/><font color="#999999">Website:</font><a href="http://html5interactivemaps.com" target="_blank"><u>HTML5InteractiveMaps.com</u></a><br/><br/><a href="http://codecanyon.net/user/Art101?ref=art101" target="_blank"><u>more info.</font></u></a>',
            upcolor: "#e5f3ff",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_50: {
            namesId: "WY",
            name: "WYOMING",
            data: '<b>Location and size:</b><br>As specified in the designating legislation for the Territory of Wyoming, Wyoming&#39;s borders are lines of latitude, 41°N and 45°N, and longitude, 104°3&#39;W and 111°3&#39;W (27° W and 34° W of the Washington Meridian), making the shape of the state a latitude-longitude quadrangle. Wyoming is one of only three states (along with Colorado and Utah) to have only latitudinal and longitudinal, rather than naturally defined, boundaries.<br><br><b>Population:</b><br>As of 2005, Wyoming had an estimated population of 509,293, which was an increase of 3,407, or 0.7%, from the prior year and an increase of 15,512, or 3.1%, since the 2000 census. This includes a natural increase since the last census of 12,165 people (that is 33,704 births minus 21,539 deaths) and an increase from net migration of 4,035 people into the state. Immigration from outside the United States resulted in a net increase of 2,264 people, and migration within the country produced a net increase of 1,771 people. In 2004, the foreign-born population was 11,000 (2.2%). In 2005, total births in Wyoming numbered 7,231 (Birth Rate of 14.04).<br/><br/><a href="http://en.wikipedia.org/wiki/Wyoming" target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#d3e7f9",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        },
        map_51: {
            namesId: "DC",
            name: "WASHINGTON DC",
            data: '<b><u>Economy:</u></b><br/>Washington has a growing, diversified economy with an increasing percentage of professional and business service jobs. The gross state product of the District in 2008 was $97.2 billion, which would rank it No. 35 compared to the 50 U.S. states. In 2008, the federal government accounted for about 27% of the jobs in Washington, D.C. This is thought to immunize Washington to national economic downturns because the federal government continues operations even during recessions.<br><br><a href="http://en.wikipedia.org/wiki/Washington,_D.C." target="_blank"><u>Reference:</font></u></a>',
            upcolor: "#FF6600",
            overcolor: "#f0ab33",
            downcolor: "#3399CC",
            enable: true
        }
    };
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })
    
    $('path[id^="map_"]').click(function(){
        clicked_map_id = parseInt(this.id.substr(4));
        console.log(linkable_page_info[clicked_map_id - 1]);
        location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=" + linkable_page_info[clicked_map_id - 1];
    });
    $('#abbs tspan').click(function(){
        clicked_map_id = $(this).parent().attr('id');
        console.log(clicked_map_id);
        switch(clicked_map_id) {
            case 'AL':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=66";
            break;
            case 'AK':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=65";
            break;
            case 'AZ':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=64";
            break;
            case 'AR':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=63";
            break;
            case 'CA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=62";
            break;
            case 'CO':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=61";	
            break;
            case 'CT':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=60";	
            break;
            case 'DE':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=49";
            break;
            case 'FL':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=48";
            break;
            case 'GA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=47";
            break;
            case 'HI':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=46";
            break;
            case 'ID':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=45";
            break;
            case 'IL':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=44";	
            break;
            case 'IN':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=122";
            break;
            case 'IA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=42";
            break;
            case 'KS':
            location.href = "http://mme.world/blog/category/medical-marijuana-kansas";
            break;
            case 'KY':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=40";
            break;
            case 'LA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=39";
            break;
            case 'ME':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=38";	
            break;
            case 'MD':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=37";
            break;
            case 'MA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=36";	
            break;
            case 'MI':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=35";	
            break;
            case 'MN':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=34";
            break;
            case 'MS':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=33";
            break;
            case 'MO':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=32";
            break;
            case 'MT':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=31";
            break;
            case 'NT':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=30";	
            break;
            case 'NV':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=29";
            break;
            case 'NH':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=28";	
            break;
            case 'NJ':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=27";
            break;
            case 'NM':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=26";
            break;
            case 'NY':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=25";	
            break;
            case 'NC':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=24";
            break;
            case 'ND':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=23";	
            break;
            case 'OH':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=22";
            break;
            case 'OK':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=21";
            break;
            case 'OR':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=20";
            break;
            case 'PA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=19";	
            break;
            case 'RI':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=18";	
            break;
            case 'SC':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=17";
            break;
            case 'SD':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=16";	
            break;
            case 'TN':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=15";	
            break;
            case 'TX':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=14";
            break;
            case 'UT':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=13";
            break;
            case 'VT':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=12";
            break;
            case 'VA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=11";	
            break;
            case 'WA':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=10";	
            break;
            case 'WV':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=9";	
            break;
            case 'WI':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=7";	
            break;
            case 'WY':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=6";
            break;
            case 'DC':
            location.href = "http://mme.world/modules/blockblog/blockblog-category.php?category_id=59";  
            break;	 	
        }
    }); 
});

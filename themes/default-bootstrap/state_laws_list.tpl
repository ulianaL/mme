<div class="state_laws_list">
	<span>State Laws</span>	
	<a href="/content/1428-alabama-cbd-specific-marijuana-law">Alabama</a>
	<a href="/content/1429-alaska-medical-marijuana-law">Alaska</a>
	<a href="/content/1430-arizona-medical-marijuana-law">Arizona</a>
	<a href="/content/1431-arkansas-medical-marijuana-law">Arkansas</a>
	<a href="/content/1434-california-medical-marijuana-law">California</a>
	<a href="/content/1435-colorado-medical-marijuana-law">Colorado</a>
	<a href="/content/1436-connecticut-medical-marijuana-law">Connecticut</a>
	<a href="/content/1437-delaware-medical-marijuana-law">Delaware</a>
	<a href="/content/1528-district-of-columbia-medical-marijuana-law">District of Columbia</a>
	<a href="/content/1438-florida-medical-marijuana-law">Florida</a>
	<a href="/content/1439-georgia-cbd-specific-marijuana-law">Georgia</a>
	<a href="/content/1534-guam-medical-marijuana-law">Guam</a>
	<a href="/content/1440-hawaii-medical-marijuana-law">Hawaii</a>
	<a href="/content/1441-idaho-medical-marijuana-law">Idaho</a>
	<a href="/content/1442-illinois-medical-marijuana-law">Illinois</a>
	<a href="/content/1535-indiana-cbd-law">Indiana</a>
	<a href="/content/1443-iowa-cbd-specific-marijuana-law">Iowa</a>
	<a href="/content/1445-kentucky-cbd-specific-marijuana-law">Kentucky</a>
	<a href="/content/1446-louisiana-medical-marijuana-law">Louisiana</a>
	<a href="/content/1447-maine-medical-marijuana-law">Maine</a>
	<a href="/content/1448-maryland-medical-marijuana-law">Maryland</a>
	<a href="/content/1449-massachusetts-medical-marijuana-law-">Massachusetts</a>
	<a href="/content/1450-michigan-medical-marijuana-law">Michigan</a>
	<a href="/content/1451-minnesota-medical-marijuana-law">Minnesota</a>
	<a href="/content/1453-mississippi-cbd-specific-marijuana-law">Mississippi</a>
	<a href="/content/1452-missouri-cbd-specific-marijuana-law">Missouri</a>
	<a href="/content/1454-montana-medical-marijuana-law">Montana</a>
	<a href="/content/1455-nevada-medical-marijuana-law">Nevada</a>
	<a href="/content/1456-new-hampshire-medical-marijuana-law">New Hampshire</a>
	<a href="/content/1457-new-jersey-medical-marijuana-law">New Jersey</a>
	<a href="/content/1458-new-mexico-medical-marijuana-law">New Mexico</a>
	<a href="/content/1459-new-york-medical-marijuana-law">New York</a>
	<a href="/content/1460-north-carolina-cbd-specific-marijuana-law">North Carolina</a>
	<a href="/content/1461-north-dakota-medical-marijuana-law">North Dakota</a>
	<a href="/content/1462-ohio-medical-marijuana-law">Ohio</a>
	<a href="/content/1463-oklahoma-cbd-specific-marijuana-law">Oklahoma</a>
	<a href="/content/1464-oregon-medical-marijuana-law">Oregon</a>
	<a href="/content/1465-pennsylvania-medical-marijuana-law">Pennsylvania</a>
	<a href="/content/1536-puerto-rico-medical-marijuana-law">Puerto Rico</a>
	<a href="/content/1531-rhode-island-medical-marijuana-law">Rhode Island</a>
	<a href="/content/1466-south-carolina-cbd-specific-marijuana-law">South Carolina</a>
	<a href="/content/1467-south-dakota-medical-marijuana-law">South Dakota</a>
	<a href="/content/1468-tennessee-cbd-specific-marijuana-law">Tennessee</a>
	<a href="/content/1469-texas-cbd-specific-marijuana-law-non-functional">Texas</a>
	<a href="/content/1470-utah-cbd-specific-marijuana-law">Utah</a>
	<a href="/content/1471-vermont-medical-marijuana-law">Vermont</a>
	<a href="/content/1537-virgin-islands-medical-marijuana-law">Virgin Islands</a>
	<a href="/content/1472-virginia-cbd-specific-marijuana-law">Virginia</a>
	<a href="/content/1473-washington-medical-marijuana-law">Washington</a>
	<a href="/content/1474-west-virginia-medical-marijuana-law">West Virginia</a>
	<a href="/content/1475-wisconsin-cbd-specific-marijuana-law">Wisconsin</a>
	<a href="/content/1476-wyoming-cbd-specific-marijuana-law">Wyoming</a>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var url = window.location.href;
		$('.state_laws_list a').filter(function() {
		return this.href == url;
		}).addClass('active');
	});
</script>
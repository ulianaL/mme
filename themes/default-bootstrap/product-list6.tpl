{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{if isset($products6) && $products6}
	<div class="feature_brands_list">
		{foreach from=$products6 item=product name=products}
		<a href="{$product.link|escape:'html':'UTF-8'}" class="single_brand" title="{$product.name|escape:'html':'UTF-8'}">
			<div class="single_brand_img">
				<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}">
			</div>
			<div class="single_brand_content">
				<span class="feature_brand_name">{$product.name|escape:'html':'UTF-8'}</span>
				{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
				{if $smarty.capture.displayProductListReviews}
					<div class="hook-reviews">
					{hook h='displayProductListReviews' product=$product}
					</div>
				{/if}

			</div>
		</a>
		{/foreach}
	</div>
{/if}

<script type="text/javascript">
	$('.feature_brands_list').owlCarousel({
	    loop:true,
	    margin:10,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	        600:{
	            items:3,
	            nav:false
	        },
	        1000:{
	            items:5,
	            nav:true,
	            loop:false
	        }
	    }
	})


	var owl2 = $('.feature_brands_list');
	owl.owlCarousel();
	$('.featured_brands_arrows .slide_left').click(function() {
	     owl2.trigger('prev.owl.carousel', [300]);
	})
	$('.featured_brands_arrows .slide_right').click(function() {
	    owl2.trigger('next.owl.carousel');
	})
</script>
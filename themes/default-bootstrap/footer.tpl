{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			
			{if $page_name !='module-medicalmarijuanaexchangedirectory-mypro'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-directorymanager'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-addpay' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-storeopt' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-storeinfo' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-advmanager'  
				&& $page_name !='module-medicalmarijuanaexchangedirectory-addbanner' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-editcampaign' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-editstories'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-addproduct' 
				&& $page_name !='module-medicalmarijuanaexchangedirectory-importcsv'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-adminjobs'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-jobs'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-off'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-pointsystem'
				&& $page_name !='module-medicalmarijuanaexchangedirectory-adminorders'
				&& $page_name !='my-account'
				&& $page_name !='history'
				&& $page_name !='order-slip'
				&& $page_name !='addresses'
				&& $page_name !='identity'
			}
				{if !empty($bottombaner)}
				<div class="banners_section">
					{if $bottombaner.type == 2}
					<div class="publi">
						<a target="_blank" href="{if strpos($bottombaner.url,'http') !== false}{$bottombaner.url}{else}http://{$bottombaner.url}{/if}">
							{* <img class="publi-img" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/bimg/{$bottombaner.banner_img}" /> *}
							<img class="publi-img" src="{getBuildBannerUrl($bottombaner.banner_img)}" />
						</a>
					</div>
					{elseif $bottombaner.type == 1} 
					<div class="publi">
						<a target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$bottombaner.id_camp}">
							{* <img class="publi-img" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/bimg/{$bottombaner.banner_img}" /> *}
							<img class="publi-img" src="{getBuildBannerUrl($bottombaner.banner_img)}" />
						</a>
					</div>
					{/if}
				</div>
				{/if}
			{/if}
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">



<div class="row">
	<div class="col-md-3" id="newsletter-box">
		{hook h='footer' mod='blocknewsletter'}
	</div>
	<div  class="col-md-9">
  	<ul class="htmlcontent-home clearfix row">
      <li class="htmlcontent-item-1 col-xs-2">
				<div class="item-html">
					<h4>Company</h4>
					<ul>
						<li>
							<a href="{$link->getCmsLink(4)}">
								About Us
							</a>
						</li>
            <li>
							<a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">
								Contact
							</a>
						</li>
            <li>
							<a href="{$link->getCmsLink(595)}">
								Contributor
							</a>
						</li>
            <li>
							<a href="{$link->getCmsLink(3)}">
								Terms of Use
							</a>
						</li>
						
						<li>
							<a href="{$link->getCmsLink(1538)}">
									Ad Standards
							</a>
						</li>
			   		
							
			   		    <li>
							<a href="{$link->getCmsLink(595)}">
								Privacy
							</a>
						</li>
          </ul>
        </div>
      </li>
      <li class="htmlcontent-item-2 col-xs-4">
        <div class="item-html">
        	<h4>
						Business Solutions
					</h4>
          <ul>
						<li>
							<a href="#">
								List your Business
							</a>
						</li>
						<li>
							<a href="#">
								List your Dispensary
							</a>
						</li>
						<li>
							<a href="/addbusinessAccount">
								Business Login
							</a>
						</li>
						<li>
							<a href="/adddispensariesAccount">
								Dispensary Login
							</a>
						</li>
						<li>
							<a href="/advertising-manager">
								Advertise
							</a>
						</li>
          </ul>
        </div>
      </li>
      <li class="htmlcontent-item-3 col-xs-2">
         <div class="item-html">
            <h4>Connect</h4>
            <ul class="social_links_parent">
              <li>
							 	<a href="https://twitter.com/THEMMEXCHANGE" target="_blank">
									<i class="fa fa-twitter-square" ></i>
								</a>
							</li>
              <li>
								<a href="https://www.pinterest.com/medicalmarijuan/" target="_blank">
									<i class="fa fa-pinterest"></i>
								</a>
							</li>
              <li>
								<a href="#" target="_blank">
									<i class="fa fa-facebook-square"></i>
								</a>
							</li>
              <li>
								<a href="http://medicalmarijuanaexchange1.tumblr.com/" target="_blank">
									<i class="fa fa-tumblr-square"></i>
								</a>
							</li>
              <li>
								<a href="https://www.instagram.com/medicalmarijuanaexchange/" target="_blank">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
            </ul>

            <!-- <div class="contact_address">
            	<p>MME Media LLC</p>
            	<p>3410 galt ocean mile</p>
            	<p>Fort lauderdale,Fl 33308 PH2</p>
            	<a href="tel:8007041263">8007041263</a>
            	<a href="mailto:info@medicalmarijuanaexchange.com">info@medicalmarijuanaexchange.com</a>
            </div> -->
         </div>
      </li>
   </ul>
</div>











						
						
						
						
						
						
</div>
<style type="text/css">
	.footer-container .htmlcontent-item-3 {
		width: 20% !important;
	}

	.social_links_parent {
		display: flex;
    justify-content: space-between;
	}

	@media  (min-width: 768px) {
		#footer > .row {
			display: flex;height: 305px;
		}
		#footer > .row > .col-md-9 {
			align-items: center;display: flex;
		}
	}
	@media (max-width: 767px) {
		#footer #newsletter_block_left .form-group .form-control {
			max-width: 100%;
		}

		#footer > .row > .col-md-9 > ul {

		}

		#footer > .row > .col-md-9 > ul > li {
			    width: 50% !important;
		}

		.footer-container .htmlcontent-home {
			display: block;
		}

		.footer-container #footer h4:after {
			display: none;
		}

		.footer-container #footer .htmlcontent-home h4 {
			font-weight: 600;
		}
	}
	@media (max-width: 451px) {
		#footer > .row > .col-md-9 > ul > li {
			    width: 100% !important;
		}

	}
</style>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	<a id="m-somebutton" href="#" onclick="return false;"></a>
	</body>
</html>





<script>


	$('.remove_video').click(function() {
		$(this).closest('.fixed_video_banner').remove();
	});

	$('.changepaytriger').change(function() {

		whatidpick = $(this).attr('payselect');
		pickjvalue = $(this).val();
		attributetopass = $(this).attr('attribute');
		
		$('table#cart_summary').css('opacity','0.3');
		//alert(attributetopass);
		

    var url = "#"; // the script where you handle the form input.

	$.ajax({
           type: "POST",
           url: url,
           data: "updatecart=1&idproooo="+whatidpick+"&pay="+pickjvalue+"&attributetopass="+attributetopass, // serializes the form's elements.
           success: function(data)
           {
               if(data == 'updateok'){
			   updatetotalss();
			   
			   $('table#cart_summary').css('opacity','1');
				     
					if (pickjvalue == 2){
						$('.pickidg'+whatidpick).show();
					}else {
						$('.pickidg'+whatidpick).hide();
					}
				
				
				//alert(data); // show response from the php script.
			   }else{
			   alert(data);
				location.reload();
			   }
           }
         });
		 
	
   

    return false; // avoid to execute the actual submit of the form.
		
		
	})
	
	
	
	
function updatetotalss(){
 
   var url = "#"; 
	$.ajax({
           type: "POST",
           url: url,
           data: 'gettotalsto', // serializes the form's elements.
           success: function(data)
           {
              
			   obj_totals = JSON.parse(data);
			   $('.totalcod').text("$"+obj_totals.totalcod.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   $('.totalpaypal').text("$"+obj_totals.totalpaypal.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   $('.totalpick').text("$"+obj_totals.totalpick.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   allok();
			   setTimeout(allok,100)
			   setTimeout(allok,500)
			   setTimeout(allok,1000)
			   setTimeout(allok,2000)
			   setTimeout(allok,5000)
			   
           }
         });
}






function allok(){
        totalpick2 = $('.totalpick').text()
        number = Number(totalpick2.replace(/[^0-9\.]+/g,""));
		if( number > 0 ){
			$('.totalpick').parent().show(1000);
		
		}else{
			$('.totalpick').parent().hide(1000);
		}
		
		totalcod2 = $('.totalcod').text()
        number = Number(totalcod2.replace(/[^0-9\.]+/g,""));
		if( number > 0 ){
			$('.totalcod').parent().show(1000);
		
		}else{
			$('.totalcod').parent().hide(1000);
		}
		
		
		totalpaypal2 = $('.totalpaypal').text()
        number = Number(totalpaypal2.replace(/[^0-9\.]+/g,""));
		// alert(number);
		if( number > 0 ){
			$('.totalpaypal').parent().show(1000);
			//if paypal hide COD
			$('.cash').hide();
			$('#paypallparallel_payment_button').show();
		
		}else{
		   
			 //if not paypal show COD 
			$('.totalpaypal').parent().hide(1000);
			$('.cash').show();
			$('#paypallparallel_payment_button').hide();
		}
		
		
		 

}




$(document).ready(function(){
	
	if(typeof(tttotalpaypal) !== 'undefined'){
		//process hide or show payments 
		if ( tttotalpaypal > 0 ){
		$('.cash').hide();
		
		}else{
		$('#paypallparallel_payment_button').hide();
		}
	}	
	
})

//openclosecal
function openclosecal(vv){
 $('.chofeatoopenclose').hide();
 $('.chofeatoopenclose'+vv).show();
 



 $('.rnge').hide();
 $('.range_'+vv).show();
 
 $('.bbn').hide();
 $('.bbn_'+vv).show(); 
 $('.datetofea').hide();
 $('.datetofea_'+vv).show();
}

function openclosecal2(vv){
 $('.chofeatoopenclose2').hide();
 $('.chofeatoopenclose2'+vv).show();
 



 $('.rnge2').hide();
 $('.range2_'+vv).show();
 
 $('.bbn2').hide();
 $('.bbn2_'+vv).show(); 
 $('.datetofea2').hide();
 $('.datetofea2_'+vv).show();
}


//submit 


</script>









{if $is_logged}

<style>

.panel{
    margin-bottom: 0px;
	z-index:999999999999999;
}
.chat-window{
    bottom:0;
    position:fixed;
    float:right;
    margin-left:10px;
	right: 0px;
	transition:all 300ms;
	bottom:-344px;
}
.chat-window > div > .panel{
    border-radius: 5px 5px 0 0;
}
.icon_minim{
    padding:2px 10px;
}
.msg_container_base{
  background: #e5e5e5;
  margin: 0;
  padding: 0 10px 10px;
  max-height:300px;
  min-height: 300px;
  overflow-x:hidden;
}
.top-bar {
  background: #666;
  color: white;
  padding: 10px;
  position: relative;
  overflow: hidden;
}
.msg_receive{
    padding-left:0;
    margin-left:0;
}
.msg_sent{
    padding-bottom:20px !important;
    margin-right:0;
}
.messages {
  background: white;
  padding: 10px;
  border-radius: 2px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  max-width:100%;
}
.messages > p {
    font-size: 13px;
    margin: 0 0 0.2rem 0;
  }
.messages > time {
    font-size: 11px;
    color: #ccc;
}
.msg_container {
    padding: 10px;
    overflow: hidden;
    display: flex;
}
.imgchatt {
    display: block;
    width: 100%;
}
.avatar {
    position: relative;
    padding: 0;
}
.p0chat{

	padding:0;
}
.base_receive > .avatar:after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    width: 0;
    height: 0;
    border: 5px solid #FFF;
    border-left-color: rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0);
}

.base_sent {
  justify-content: flex-end;
  align-items: flex-end;
}
.base_sent > .avatar:after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 0;
    height: 0;
    border: 5px solid white;
    border-right-color: transparent;
    border-top-color: transparent;
    box-shadow: 1px 1px 2px rgba(black, 0.2); // not quite perfect but close
}

.msg_sent > time{
    float: right;
}



.msg_container_base::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

.btn-group.dropup{
    position:fixed;
    left:0px;
    bottom:0;
}
.clist {
    border-bottom: 1px solid #ddd;
    font-size: 17px;
    padding: 4px;
	cursor: pointer;
}
.customerlist {
    padding: 0;
    overflow-y: scroll;
	max-height: 300px;
	  
}
.clist.active, .clist:hover{
    background: black;
    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.2);
	color:white;
  
}
img.imgchatt {
    border: 3px solid #5bbe8f;
    border-radius: 4px;
	min-height: 50px;
}
.upchatro{
    position: absolute;
    right: 10px;
    top: 6px;
    font-size: 25px;
	display:none;
	cursor: pointer;
	
}
.upchatro.active{
    position: absolute;
    right: 10px;
    top: 6px;
    font-size: 25px;
	display:block;
}

.online {
    width: 12px;
    height: 12px;
    background-color: #37a000;
    float: right;
    margin-top: 3px;
    border-radius: 100px;
    border: 2px solid white;
}
.offline {
    width: 12px;
    height: 12px;
    background-color: #37a000;
    float: right;
    margin-top: 3px;
    border-radius: 100px;
    border: 2px solid white;
	background-color: #ff7d7d;
}
.counttoread {
    background: #6e6e6e;
    color: white;
    font-size: 11px;
    position: absolute;
    right: 18px;
    padding: 0 4px;
    border-radius: 2px;
	font-weight:800!important;
}



.counttoreadtotal {
    background: #e84747;
    color: white;
    font-size: 11px;
    padding: 2px 4px;
    border-radius: 2px;
    position: relative;
    top: -1px;
    left: 8px;
}
img.loadergif {
    position: absolute;
    right: 0;
    z-index: 22;
    width: 63px;
    top: 20px;
}


	


</style>




<div class="loadingchat"  style="    position: fixed;
    bottom: 0;
    z-index: 9999999;
    background: black;
    padding: 8px;
    color: white;
    right: 0;">
Loading Chat...
<img src="{$img_ps_dir}loader2.gif"  style="    width: 30px;">
</div> 


<div class="row chat-window  col-md-6" id="chat_window_1" style="margin-left:10px;z-index:9999;    padding: 0;display:none;
    margin: 0;">

		   <div class="panel panel-default">
                <div class="panel-heading top-bar">
                        <h3 class="panel-title">Chat <span class="shake counttoreadtotal">0</span></h3>
						<i class="fa fa-caret-up upchatro upchatroup active" onclick="openclosechat(1)" aria-hidden="true"></i>
						<i class="fa fa-times upchatro upchatrodown"  onclick="openclosechat(0)" aria-hidden="true"></i>

                </div>
				<div class="col-md-4 customerlist" style="padding:0;">
				
				</div>
				
				
				<div class="panel-body msg_container_base col-md-8"></div>
                
				
				<div class="panel-footer">
                    <div class="input-group">
                        <input  style="height: 30px;" id="btn-input" type="text"  class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" onclick="addmsn();" id="btn-chat">Send</button>
                        </span>
                    </div>
                </div>
    		</div>
</div>

<script>

$(document).ready(function(){
	openclosechat({$openclosechat});
});


$('.chat_input').keypress(function(e) {
if(e.which == 13) {
             e.preventDefault();
             addmsn();
          }
});


function addcontacttochat(value){
	//alert(value);
	
		$.ajax({
				   type: "POST",
				   url: '#',
				   data: 'addcontacttochat='+value, // serializes the form's elements.
				   success: function(data)
				   {
					  openclosechat(1);
					 //alert(data)
					  getallchats();
				   }
			   });

}



//open close chat !!
function openclosechat(v){
	if(v == 1)
	{
		$('.upchatroup').removeClass('active');
		$('.upchatrodown').addClass('active');
	    $('.chat-window').css('bottom',0);
		activechat = 1
	}else{
		
		$('.upchatroup').addClass('active');
		$('.upchatrodown').removeClass('active');
		 $('.chat-window').css('bottom',-344);
		activechat = 0
	}
	    var url = "#";
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'openclosechat='+activechat, // serializes the form's elements.
				   success: function(data)
				   {
					  //alert(data); // show response from the php script.
					  
				   }
			   });
}


function getallchats(){
uniqueid = new Date().valueOf();
stop_all = $.ajax({
				   type: "POST",
				   url:'#' ,
				   data: 'myusers&uniqueid='+uniqueid,
				   success: function(data)
				   {
					
					$('.customerlist').empty();
					$('.msg_container_base').empty();
					//!!!!!
					total_of_chat_to_read= 0;
					//alert(data);
					chatobj = JSON.parse(data);
					//all chats and setting
                    htmlmodel = '';
					$.each(chatobj.chats, function( index, value ) {
                                 
									user_of_chat = '<div filefrom="'+value.file+'" idof="'+value.user.id+'" class="clist clist'+value.user.id+' active" onclick=" updateselectedcustromeronchat('+value.user.id+')">'+value.user.id+'-'+value.user.firstname+' '+value.user.lastname+'<span class="disable counttoread counttoread'+value.user.id+'">'+value.forread+'</span><div class="'+value.onff+'"></div></div>';
									//its online

									
									$('.customerlist').append(user_of_chat) ; 
									
									
								 
								 
								 total_of_chat_to_read += value.forread;
								 if(value.forread == 0){
								   $('.counttoread'+value.user.id).hide();
								 }
								 
								 
								 $('.counttoreadtotal').text(total_of_chat_to_read)
								 if(total_of_chat_to_read > 0){
								   openclosechat(1);
								   $('.counttoreadtotal').show()
								 }else{
								   $('.counttoreadtotal').hide()
								 }
								  $.each( value.content, function( index, valuecontent ) {
									      
									  if(valuecontent !=''){
										  exploded = valuecontent.split("####");
										  //me or other??
										  if(chatobj.customerobj.id == exploded[3] ){
										      rec_or_send = 'base_sent';
										  }else{
										      rec_or_send ='';
										  }
										 
										  htmlmodel = '';
										  htmlmodel += '<div style="display:none;" class="row msg_container '+rec_or_send+' userchat'+value.user.id+'"  UI="'+exploded[4]+'">'
										  htmlmodel += '<div class="col-md-10 col-xs-10 p0chat">'
										  htmlmodel += '<div class="messages msg_sent">'
										  if(chatobj.customerobj.id != exploded[3] ){
											htmlmodel += '<p><b>'+value.user.firstname+' '+value.user.lastname+':</b></p>'
										  }else{
											htmlmodel += '<p><b>'+chatobj.customerobj.firstname+' '+chatobj.customerobj.lastname+':</b></p>'
										  }
										  htmlmodel += '<p>'+exploded[2]+'</p>'
										  //htmlmodel += '<p  style="color:red;">'+exploded[4]+'</p>'
										  htmlmodel += '<time datetime="">'+exploded[1]+'</time>'
										  htmlmodel += '</div></div></div>';
										  $('.msg_container_base').append(htmlmodel);
								      }
								  }); 
								  
								  
								  
								  /*
								  havetoread = 0;
								  $('.clist').each(function(i, obj) {
								      if( $(this).find('.counttoread').text()!= 0){
										  
										  htmltoclean = $(this).html()
										  $(this).empty()
										  $('.customerlist').prepend( $(this).html(htmltoclean)  ); 
										   $('.clist').first().click();
										   havetoread = 1;
									  }
								  });
								  
								  if(havetoread == 1){
								//	$('.clist').first().click();
								  }
								  */
								  
					 }); 
					 
					 
					 $('.clist'+chatobj.customerobj.chatactive).click();
					 $(".msg_container_base").animate({ scrollTop:$('.msg_container_base').prop("scrollHeight") }, 0);
					  
					  
				   },
				   /*
				   error: function(jqXHR, textStatus, errorThrown) {
						//alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

						$('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
						console.log('jqXHR:');
						console.log(jqXHR);
						console.log('textStatus:');
						console.log(textStatus);
						console.log('errorThrown:');
						console.log(errorThrown);
					},*/
				 });
		
}


jQuery(document).ready(function() {
    
	 setTimeout(function() {
         getallchats();
		 $('.chat-window').fadeIn();
		 $('.loadingchat').fadeOut();
    }, 3000); 
});


window.setInterval(function(){
  getallchats();
}, 8000);



function updateselectedcustromeronchat(value){
	    stop_all.abort();
		actualcustomeris = value;
		
		$('.clist').removeClass('active');
		
		$('.clist'+value).addClass('active');
		
		$('.msg_container').hide();
		$('.userchat'+value).show();
		$(".msg_container_base").animate({ scrollTop:$('.msg_container_base').prop("scrollHeight") }, 0);
		
		var url = "#"; // the script where you handle the form input.
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'updateselectedcustromeronchat&whatuser='+value, // serializes the form's elements.
				   success: function(data)
				   {
					 // alert(data); // show response from the php script.
					 stop_all.abort();
				   }
				 });
	
}


function addmsn(){
    texttttochat = $('#btn-input').val();
    if(texttttochat != ''){
		whatuser = $('.clist.active').attr('idof')
		filefrom = $('.clist.active').attr('filefrom')
		uniqueid = new Date().valueOf();


		
		 $('#btn-input').val('');
		 htmlmodel = '';
		 htmlmodel += '<div style="position:relative;" class="row msg_container base_sent userchat'+whatuser+'">'
		
   		 htmlmodel += '<img uniqueid="'+uniqueid+'" src="{$img_ps_dir}loader2.gif"  class="loadergif">'
		 
		 htmlmodel += '<div class="col-md-10 col-xs-10 p0chat">'
		 htmlmodel += '<div class="messages msg_sent">'
		 htmlmodel += '<p><b>'+chatobj.customerobj.firstname+' '+chatobj.customerobj.lastname+':</b></p>'
		 htmlmodel += '<p>'+texttttochat+'</p>'
		 htmlmodel += '<time datetime=""></time>'
		 htmlmodel += '</div></div></div>';
		 $('.msg_container_base').append(htmlmodel);
		 $(".msg_container_base").animate({ scrollTop:$('.msg_container_base').prop("scrollHeight") }, 0);

		 
		 $.ajax({
				type: "POST",
				url: '#',
				data: 'addmensage&whatuser='+whatuser
				+'&filefrom='+filefrom
				+'&uniqueid='+uniqueid
				+'&texttttochat='+texttttochat
				, // serializes the form's elements.
				success: function(data)
				{
				   
				 
				   $('[uniqueid="'+data+'"]').remove()
				  // getallchats();
				   //alert(data); // show response from the php script.
					  
				}
		});
	}
}


$('.chat_input').click(function(){
  ireadmsns();
})

function ireadmsns(){
	  filefrom = $('.clist.active').attr('filefrom')
	  uniqueid = new Date().valueOf();
	  $.ajax({
				type: "POST",
				url: '#',
				data: 'updatemensageread'+'&filefrom='+filefrom
				+'&uniqueid='+uniqueid,
				
				success: function(data)
				{
				   getallchats();
				 //alert(data);
				}
		});
	
}
</script>

{else}
<script>

function addcontacttochat(value){
   fancyAlert('You need Login/Register to contact this user.');


}


function fancyAlert(msg) {
    jQuery.fancybox({
        'modal' : false,
        'content' : "<div style=\"margin:1px;width:300px;font-size:25px;line-height: 25px;\">"+msg+"<div style=\"text-align:right;margin-top:10px;\"></div></div>",
		
		beforeClose:function(){
				//redirect to login	
			  window.location.replace('{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}')
		}
    
	}
	
	);
}








</script>

{/if}


	
<script>



$(document).ready(function(){
$('#block_top_menu .cat-title').on( "click", function() {
	$('.mobile_menu').toggle();
});
	//get news menu
    $.post("#",
    {
        getnewsmenu: "",
       
    },
    function(data, status){
	   htmltonews = '<ul class="submenu-container clearfix first-in-line-xs newsmenudiv" style="display: none;">';
	   htmltonews += '<div  class="row">';
         htmltonews += '<a href="/blog/category/canada-news" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Canada</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/blog/category/global-cannabis-news" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Global</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/blog/category/politics-news" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Politics</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/content/category/4-state-news" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">States</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/blog/category/hemp" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Hemp</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/financial" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Finance</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/blog/category/marijuana-reform" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Marijuana Law Reform</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

		 htmltonews += '<a href="/blog/category/medical-marijuana" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltonews += '<div class="newsmanuiteminside">';
				htmltonews += '<p class="">Medical Marijuana</p>';
			htmltonews += '</div>';	
		 htmltonews += '</div></a>';

	  htmltonews += '</div>';
	  htmltonews += '</ul>';
	
       $( '[title="News"]' ).after(htmltonews);
    });


    //get learn menu

	   htmltolern = '<ul class="submenu-container clearfix first-in-line-xs newsmenudiv" style="display: none;">';
	   htmltolern += '<div  class="row">';
         htmltolern += '<a href="/ailments" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltolern += '<div class="newsmanuiteminside">';
				htmltolern += '<p class="">Ailments</p>';
			htmltolern += '</div>';	
		 htmltolern += '</div></a>';

		 htmltolern += '<a href="/blog/category/cbd-education" ><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltolern += '<div class="newsmanuiteminside">';
				htmltolern += '<p class="">CBD</p>';
			htmltolern += '</div>';	
		 htmltolern += '</div></a>';

		 htmltolern += '<a href="/content/1432-medical-studies"><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltolern += '<div class="newsmanuiteminside">';
				htmltolern += '<p class="">Medical Studies</p>';
			htmltolern += '</div>';	
		 htmltolern += '</div></a>';

		 htmltolern += '<a href="/blog/category/terpenes"><div class="col-md-12 newsmanuitem" style="padding:15px">';
			htmltolern += '<div class="newsmanuiteminside">';
				htmltolern += '<p class="">Terpenes</p>';
			htmltolern += '</div>';	
		 htmltolern += '</div></a>';

	  htmltolern += '</div>';
	  htmltolern += '</ul>';
	
      $( '[title="LEARN"]' ).after(htmltolern);
	
	//stateslist
	stateslisttomenu = '<ul id="state_lows" class="submenu-container clearfix first-in-line-xs newsmenudiv" style="display: none;">';
	stateslisttomenu += '<div  class="row">';
	stateslisttomenu += '<a href="/content/1427-medical-marijuana-state-specific-legal-issues"><div class="business_item">';
		stateslisttomenu += '<div class="newsmanuiteminside">';
			stateslisttomenu += '<p class="">Medical Marijuana Laws</p>';
		stateslisttomenu += '</div>';	
	stateslisttomenu += '</div></a>';

	stateslisttomenu += '<a href="/hemp-laws"><div class="business_item">';
		stateslisttomenu += '<div class="newsmanuiteminside">';
			stateslisttomenu += '<p class="">Hemp Laws</p>';
		stateslisttomenu += '</div>';	
	stateslisttomenu += '</div></a>';
	stateslisttomenu += '</div>';
	$( '[title="STATE LAWS"]' ).after(stateslisttomenu);
	
	
//Bussines menu	
businessmenu = '';
businessmenu = '<ul id="businessmenu" class="submenu-container clearfix first-in-line-xs newsmenudiv" style="display: none;">';
businessmenu += '<div  class="row">';
businessmenu += '<a href="/financial" ><div class="business_item">';
	businessmenu += '<div class="newsmanuiteminside">';
		businessmenu += '<p class="">Finance</p>';
	businessmenu += '</div>';	
businessmenu += '</div></a>';

// businessmenu += '<a href="/page-jobs"><div class="business_item">';
// 	businessmenu += '<div class="newsmanuiteminside">';
// 		businessmenu += '<p class="">Jobs</p>';
// 	businessmenu += '</div>';
// businessmenu += '</div></a>';
// businessmenu += '</div>';
$( '[title="BUSINESS"]' ).after(businessmenu);

//sectors menu	
sectors = '';
sectors = '<ul id="sectormenu" class="submenu-container clearfix first-in-line-xs newsmenudiv" style="display: none;">';
sectors += '<div  class="row">';
sectors += '<a href="/sector?agtech" ><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>AgTech</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?biotechnology"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Biotechnology</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?consumption-devices"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Consumption Devices</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?cultivation-retail"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Cultivation & Retail</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?hemp-products"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Hemp Products</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?investing-finance"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Investing & Finance</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?marijuana-products"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Marijuana Products</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?other-ancillary"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Other Ancillary</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?real-estate"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Real Estate</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?secondary-services"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Secondary Services</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '<a href="/sector?tech-media"><div class="business_item">';
	sectors += '<div class="newsmanuiteminside">';
		sectors += '<p>Tech & Media</p>';
	sectors += '</div>';	
sectors += '</div></a>';

sectors += '</div>';
$( '[title="Sectors"]' ).after(sectors);
	
});	


	
function truncateString(str, length) {
     return str.length > length ? str.substring(0, length - 3) + '...' : str
}

// $('.')
</script>
	
	
	
	

{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" {if isset($language_code) && $language_code}
      lang="{$language_code|escape:'html':'UTF-8'}" {/if}><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" {if isset($language_code) && $language_code}
      lang="{$language_code|escape:'html':'UTF-8'}" {/if}><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" {if isset($language_code) && $language_code}
      lang="{$language_code|escape:'html':'UTF-8'}" {/if}><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" {if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"
      {/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if} >
<head>

    <meta charset="utf-8"/>
    <title>{$meta_title|escape:'html':'UTF-8'}</title>
    {if isset($meta_description) AND $meta_description}
        <meta name="description" content="{$meta_description|escape:'html':'UTF-8'}"/>
    {/if}
    {if isset($meta_keywords) AND $meta_keywords}
        <meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}"/>
    {/if}
    <meta name="generator" content="PrestaShop"/>
    <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow"/>
    <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <style>  .content_scene_cat .content_scene_cat_bg {
            background-color: #333 !important;
        }</style>
    <link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}"/>
    <link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}"/>
    {if isset($css_files)}
        {foreach from=$css_files key=css_uri item=media}
            {if $css_uri == 'lteIE9'}
                <!--[if lte IE 9]>
                {foreach from=$css_files[$css_uri] key=css_uriie9 item=mediaie9}
                <link rel="stylesheet" href="{$css_uriie9|escape:'html':'UTF-8'}" type="text/css"
                      media="{$mediaie9|escape:'html':'UTF-8'}"/>
                {/foreach}
                <![endif]-->
            {else}
                <link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css"
                      media="{$media|escape:'html':'UTF-8'}"/>
            {/if}
        {/foreach}
    {/if}

    {$js_def}
    {foreach from=$js_files item=js_uri}
        {if strpos($js_uri, 'tree.js') !== false}{continue}{/if}
        <script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
    {/foreach}

    {$HOOK_HEADER}
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext"
          type="text/css" media="all"/>
    <!--[if IE 8]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    {if   $page_name|strstr:"module-blockblog-blockblog-category" != false || $page_name|strstr:"module-blockblog-blockblog-post" != false}
        <script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/themes/default-bootstrap/js/tools/treeManagement.js"></script>
    {/if}

    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">

    <!--AdSense code-->
    <script data-ad-client="ca-pub-5432158646688841" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>


<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if}
        class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">

{if !isset($content_only) || !$content_only}
{if isset($restricted_country_mode) && $restricted_country_mode}
    <div id="restricted-country">
        <p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country}
                <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
    </div>
{/if}







{if $agepopup  == 0 }
    <div class="popupage">
        <div class="popupage-bg-2">
            <img style="margin-bottom: 3.8em;" src="/themes/default-bootstrap/img/white__logo.svg"
                 style="max-width:100%" alt="Medical Marijuana Exchange">
            <div class="popupagecontent">
                <h3 class="h3-welcome" style="color:#333">{l s='Welcome!'}</h3>
                <h3 class="h3-verify" style="color:#333">{l s='Please verify your age to enter.'}</h3>
                <div class="popupagebot">
                    <div onclick="popupage(1)" class="popupagebotrea">{l s='I am 21 or Older'}</div>
                    <div onclick="popupage(0)" class="popupagebotrea">{l s='I am Under 21'}</div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .popupage {
            top: 0px;
            background: white;
            /*background: url(/themes/default-bootstrap/img/header_image_new.jpg);
            background-repeat: no-repeat;
            background-position-y: 56px;
            background-color: #faf9ff;
            background-size: cover;
            background-position: center;*/
        }

        .popupage-bg-2 {
            position: fixed;
            height: 100vh;
            width: 100vw;
            background-color: rgba(0, 0, 0, 0.74);
            display: flex;
            flex-direction: column;
        }

        .popupagecontent {
            width: 316px;
            border-radius: 0px;
            padding: 55px 68px 54px 68px;
            height: auto;
        }

        .h3-welcome {
            color: #333;
            font-weight: bold;
            font-family: PlayfairDisplay;
            font-size: 28px;
            padding-bottom: 9px;
            margin-bottom: 0px;
            border-bottom: 1px solid #000;
        }

        .h3-verify {

        }
    </style>
{/if}


<div id="page">
    <h1 style="    position: fixed;top: -142px;"><a href="https://pequenos.pt">pequenos.pt</a></h1>
    <h1 style="    position: fixed;top: -142px;"><a href="https://emcarga.pt">Baterias para Portáteis</a></h1>
    <div class="header-container">
        <header id="header" style="padding-bottom:0;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}"
                           title="{$shop_name|escape:'html':'UTF-8'}">
                            <img class="mme_logo" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"/>
                        </a>

                        {capture name='displayNav'}{hook h='displayNav'}{/capture}
                        <div class="header_nav_right">
                            {if $smarty.capture.displayNav}
                                {$smarty.capture.displayNav}
                            {/if}
                        </div>
                        {if $cccustomer->bussiness != 1 }
                            <a class="bussines_link"
                               href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">BUSINESS
                                SERVICES</a>
                        {else}
                            <a class="bussines_link"
                               href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}">BUSINESS
                                SERVICES</a>
                        {/if}

                        <div class="changelocalization">
                            <div class="divtoicongeo" onclick="deletecookietogeohome()">
                                <img src="/themes/default-bootstrap/img/header-location.png" alt="">
                            </div>
                            <input class="changelocalizationinput" type="text"
                                   value="{$cityinfos.cityName}, {$cityinfos.regionName}">
                            <div class="serresult">
                                <ul class="list-group">
                                </ul>
                            </div>
                        </div>

                        <div id="search_block_top" class="">
                            <form id="searchbox" method="get" action="//mme.world/search">
                                <input type="hidden" name="controller" value="search">
                                <input type="hidden" name="orderby" value="position">
                                <input type="hidden" name="orderway" value="desc">
                                <input class="search_query ac_input" type="text" id="search_query_top"
                                       name="search_query" placeholder="Search" value="" autocomplete="off">
                                <button type="submit" name="submit_search" class="btn btn-default button-search">
                                    <img src="/themes/default-bootstrap/img/header-search.png" alt="">
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script>

                function popupage(v) {

                    /*
                    if(!Cookies.get('agepopup')){
                    alert('yes')
                    }*/ ///

                    if (v == 1) {
                        Cookies.set('agepopup', 1);
                        location.reload();
                    } else {
                        Cookies.remove('agepopup');
                        location.reload();
                    }
                }


                function change_localization_fun(value) {
                    $.ajax({
                        type: "POST",
                        url: '#',
                        data: 'change_localization_fun&value=' + encodeURIComponent(value), // serializes the forms elements.
                        success: function (data) {
                            //alert(data);
                            if (data == 'change_localization_fun_ok') {
                                location.reload();
                            } else {
                                alert('error');
                                location.reload();
                            }
                        }
                    });

                }

                function deletecookietogeohome() {
                    $.ajax({
                        type: "POST",
                        url: '#',
                        data: 'deletecookietogeohome', // serializes the form's elements.
                        success: function (data) {
                            // alert(data);

                            location.reload();

                        }
                    });

                }

                $(".changelocalizationinput").on('input', function () {
                    console.log("changelocalizationinput(" + $(".changelocalizationinput").val().length + ")->" + $(".changelocalizationinput").val());


                    if ($(".changelocalizationinput").val().length > 2) {
                        $.ajax({
                            type: "POST",
                            url: '#',
                            data: 'changelocalizationinput&value=' + encodeURIComponent($(".changelocalizationinput").val()), // serializes the form's elements.
                            success: function (data) {
                                // alert(data);
                                $('.list-group').html(data)
                            }
                        });

                    } else {
                        $('.list-group').html('')
                    }


                });


            </script>


            {capture name='displayBanner'}{hook h='displayBanner'}{/capture}
            {if $smarty.capture.displayBanner}
                <div class="banner">
                    <div class="container">
                        <div class="row">
                            {$smarty.capture.displayBanner}
                        </div>
                    </div>
                </div>
            {/if}

            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}</div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    {if $page_name =='index'}
        <div class="home_top_banner">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-xs-12 top_banner_content">
                        <span>Premium Certified</span>
                        <span>Organic Cannabis</span>
                        <a href="/" class="home_learn_more_btn">LEARN MORE</a>
                    </div> -->

                </div>
            </div>
        </div>
    {/if}
    <div class="columns-container">
        {if $page_name =='module-medicalmarijuanaexchangedirectory-ailments'}
            <div class="ailments-header"
                 style="background: url(/themes/default-bootstrap/img/Ailments.jpg), linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5));">
                <span>Ailments</span>
            </div>
        {elseif $page_name =='module-medicalmarijuanaexchangedirectory-hemplaws'}
            <div class="ailments-header"
                 style="background: url(/themes/default-bootstrap/img/hemp_laws_page.jpg), linear-gradient(rgba(53,55,54,1),rgba(53,55,54,1));">
                <span>State Laws</span>
            </div>
        {elseif isset($category_id) && $category_id =='373'}
            <div class="ailments-header" style="background: url(/themes/default-bootstrap/img/global.jpg);">
                <span>Global</span>
            </div>
        {elseif isset($category_id) && $category_id =='380'}
            <div class="ailments-header" style="background: url(/themes/default-bootstrap/img/terpenes.jpg);">
                <span>Terpenes</span>
            </div>
        {elseif isset($category_id) && $category_id =='378'}
            <div class="ailments-header" style="background: url(/themes/default-bootstrap/img/cbdeducation.jpg);">
                <span>Сbd Education</span>
            </div>
        {else}
            {if isset($cms) && !isset($cms_category) && $cms->background_image !=='' && $cms->background_image != null}
                <div class="ailments-header"
                     style="background: url({$cms->background_image}), linear-gradient(rgba(53,55,54,0.65),rgba(53,55,54,0.65));">
                    <span>{$cms->background_title}</span>
                </div>
            {/if}

        {/if}
        <div id="columns" class="container">


            {hook h='banner'}

            {if $page_name !='index'
            && $page_name !='pagenotfound'
            && $page_name !='blockblog-post'
            && $page_name !='cms'
            && $page_name !='blockblog-posts'
            && $page_name !='module-medicalmarijuanaexchangedirectory-financial'
            && $page_name !='category'
            && $page_name !='module-blocknewsadv-news'
            && $page_name|strstr:"module-blockblog-blockblog-category" == false
            && $page_name|strstr:"module-blockblog-blockblog-post" == false}

                {*{include file="$tpl_dir./breadcrumb.tpl"}*}
            {/if}


            <div class="row">
                {if isset($left_column_size) && !empty($left_column_size)}
                    <div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">
                        {if isset($cms) && !isset($cms_category) && $cms->id_cms_category == "495" && $cms->id !== 1427 }
                            {include file="$tpl_dir./state_laws_list.tpl"}
                        {else}
                            {$HOOK_LEFT_COLUMN}
                        {/if}

                        {if $page_name !='module-medicalmarijuanaexchangedirectory-mypro'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-directorymanager'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-addpay'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-storeopt'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-storeinfo'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-advmanager'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-addbanner'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-editcampaign'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-editstories'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-addproduct'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-importcsv'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-adminjobs'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-jobs'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-off'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-pointsystem'
                        && $page_name !='module-medicalmarijuanaexchangedirectory-adminorders'
                        && $page_name !='my-account'
                        && $page_name !='history'
                        && $page_name !='order-slip'
                        && $page_name !='addresses'
                        && $page_name !='identity'
                        }
                            {if !empty($leftbanner)}
                                <div class="banners_section">
                                    {if $leftbanner.type == 2}
                                        <div class="publi {if $leftbanner.size == '4'}fixed_video_banner{/if}">
                                            <a target="_blank"
                                               href="{if strpos($leftbanner.url,'http') !== false}{$leftbanner.url}{else}http://{$leftbanner.url}{/if}">
                                                {if $leftbanner.size == '4'}
                                                    <script type="text/javascript">
                                                        if (Cookies.get('showed_video1')) {

                                                            Cookies.set('showed_video2', 1);
                                                        } else {

                                                            Cookies.set('showed_video1', {$leftbanner.id_banner});
                                                        }
                                                    </script>
                                                    <a class="go_to_campain" target="_blank"
                                                       href="{if strpos($leftbanner.url,'http') !== false}{$leftbanner.url}{else}http://{$leftbanner.url}{/if}">Go
                                                        to Campaign</a>
                                                    <span class="remove_video">&times;</span>
                                                    {include file="$tpl_dir./video_banner.tpl" banner=$leftbanner}
                                                {else}
                                                    <img class="publi-img"
                                                         src="{getBuildBannerUrl($leftbanner.banner_img)}" alt=""/>
                                                {/if}
                                            </a>
                                        </div>
                                    {elseif $leftbanner.type == 1}
                                        <div class="publi {if $leftbanner.size == '4'}fixed_video_banner{/if}">
                                            <a target="_blank"
                                               href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$leftbanner.id_camp}">

                                                {if $leftbanner.size == '4'}
                                                    <a class="go_to_campain" target="_blank"
                                                       href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$leftbanner.id_camp}">Go
                                                        to Campaign</a>
                                                    <span class="remove_video">&times;</span>
                                                    {include file="$tpl_dir./video_banner.tpl" banner=$leftbanner}
                                                    <script type="text/javascript">
                                                        if (Cookies.get('showed_video1')) {

                                                            Cookies.set('showed_video2', 1);
                                                        } else {

                                                            Cookies.set('showed_video1', {$leftbanner.id_banner});
                                                        }

                                                        console.log(Cookies.get('showed_video1'));
                                                        console.log(Cookies.get('showed_video2'));
                                                    </script>
                                                {else}
                                                    <img class="publi-img"
                                                         src="{getBuildBannerUrl($leftbanner.banner_img)}" alt=""/>
                                                {/if}
                                            </a>
                                        </div>
                                    {/if}
                                </div>
                            {/if}
                        {/if}
                    </div>
                {/if}

                {if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
                <div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
{/if}
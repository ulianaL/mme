{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div  style="margin-bottom:15px">
	
	
	|<a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}?allusa=1">
	<button   {if isset($smarty.get.allusa)} style="border-bottom: 1px solid #333!important"{/if}      class="btofist">
	<span>USA</span>
	</button>
	</a>|

	<a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}?allcanada=1">
	<button  {if isset($smarty.get.allcanada)} style="border-bottom: 1px solid #333!important"{/if} class="btofist">
	<span>Canada</span>
	</button>
	</a>|
	
	<a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}">
	<button   {if !isset($smarty.get.allcanada) && !isset($smarty.get.allusa) } style="border-bottom: 1px solid #333!important"{/if} class="btofist">
	<span>Local ({$cityinfos.cityName}, {$cityinfos.regionName},  100 miles radius)</span>
	</button>
	</a>|
	
</div>


{include file="$tpl_dir./errors.tpl"}
{if isset($hpoint_id) && $hpoint_id}

		<h1 class="page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if}"><span class="cat-name">{$hpoint_name|escape:'html':'UTF-8'}</span>{include file="$tpl_dir./category-count.tpl"}
		
		
		
		{if isset($smarty.get.allcanada)}
			<span style="font-weight:900!important;">(All Canada)</span></h1>
		{/if}
		
		{if isset($smarty.get.allusa)}
			<span style="font-weight:900!important;">(All USA)</span></h1> 
		{/if}
		
		
		{if !isset($smarty.get.allcanada) && !isset($smarty.get.allusa) }
		<span style="font-weight:900!important;">({$cityinfos.cityName}, {$cityinfos.regionName},  100 miles radius)</span></h1>
		{/if}
		
		
		{if $products}
			{* <div class="content_sortPagiBar clearfix">
            	<div class="sortPagiBar clearfix">
            		{include file="./product-sort.tpl"}
                	{include file="./nbr-product-page.tpl"}
				</div>
                <div class="top-pagination-content clearfix">
                	{include file="./product-compare.tpl"}
					{include file="$tpl_dir./pagination.tpl"}
                </div>
			</div> *}
			{include file="./product-list$hpoint_id.tpl" products=$products}
			{* <div class="content_sortPagiBar">
				<div class="bottom-pagination-content clearfix">
					{include file="./product-compare.tpl" paginationId='bottom'}
                    {include file="./pagination.tpl" paginationId='bottom'}
				</div>
			</div> *}
		{else}
			<p class="alert alert-warning">{l s='There are no products in this category.'}</p>
		{/if}
{/if}

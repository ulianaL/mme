{if $MENU != ''}
    <!-- Menu -->
    <div id="block_top_menu" class="">
        <div class="cat-title">{l s="Menu" mod="blocktopmenu"}</div>
        <ul class="sf-menu clearfix menu-content">
            {if $page_name !== 'module-medicalmarijuanaexchangedirectory-financial' && $page_name !== 'module-medicalmarijuanaexchangedirectory-sector' && $page_name !== 'module-medicalmarijuanaexchangedirectory-stock'}
                {$MENU}
            {else}
                <li><a href="/" title="Home">Home</a></li>
                <li><a href="/blog/category/categorynjzv8w" title="News Financial">News</a></li>
                <li><a href="/stock" title="Marijuana stocks">Marijuana stocks</a></li>
                <li><a href="#" title="Featured">Featured</a></li>
                {* <li><a href="/blog/category/politics-news" title="Political">Political</a></li> *}
                <li><a href="/sector?agtech" title="Sectors">Sectors</a></li>
                <li><a href="/blog/category/press-releases" title="Press Releases" class="sf-with-ul">Press Releases</a></li>
            {/if}
            {if $MENU_SEARCH}
                <li class="sf-search noBack" style="float:right">
                    <form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
                        <p>
                            <input type="hidden" name="controller" value="search"/>
                            <input type="hidden" value="position" name="orderby"/>
                            <input type="hidden" value="desc" name="orderway"/>
                            <input type="text" name="search_query"
                                   value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}"/>
                        </p>
                    </form>
                </li>
            {/if}
        </ul>
        {if $page_name == 'module-medicalmarijuanaexchangedirectory-financial' || $page_name == 'module-medicalmarijuanaexchangedirectory-sector' || $page_name !== 'module-medicalmarijuanaexchangedirectory-stock'}
            <ul class="mobile_menu">
                <li><a href="/" title="Home" class="sf-with-ul">Home</a></li>
                <li><a href="/blog/category/categorynjzv8w" class="sf-with-ul">News</a></li>
                <li><a href="/stock" title="Marijuana stocks" class="sf-with-ul">Marijuana stocks</a></li>
                <li><a href="#" title="Featured" class="sf-with-ul">Featured</a></li>
                <li><a href="/blog/category/politics-news" title="Political" class="sf-with-ul">Political</a></li>
                <li><a href="/sector?agtech" class="sf-with-ul">Sectors</a>
                    <ul>
                        <li><a href="/sector?agtech">AgTech</a></li>
                        <li><a href="/sector?biotechnology">Biotechnology</a></li>
                        <li><a href="/sector?consumption-devices">Consumption Devices</a></li>
                        <li><a href="/sector?cultivation-retail">Cultivation & Retail</a></li>
                        <li><a href="/sector?hemp-products">Hemp Products</a></li>
                        <li><a href="/sector?investing-finance">Investing & Finance</a></li>
                        <li><a href="/sector?marijuana-products">Marijuana Products</a></li>
                        <li><a href="/sector?other-ancillary">Other Ancillary</a></li>
                        <li><a href="/sector?real-estate">Real Estate</a></li>
                        <li><a href="/sector?secondary-services">Secondary Services</a></li>
                        <li><a href="/sector?/sector?tech-media">Tech & Media</a></li>
                    </ul>
                </li>
                <li><a href="/blog/category/press-releases" title="Press Releases" class="sf-with-ul">Press Releases</a></li>


            </ul>
        {else}
            <ul class="mobile_menu">
                <li><a href="/1438-marketplace" title="Products" class="sf-with-ul">Products</a></li>
                <li><a href="/news" class="sf-with-ul">News</a>
                    <ul>
                        <li><a href="/blog/category/canada-news">Canada</a></li>
                        <li><a href="/blog/category/global-cannabis-news">Global</a></li>
                        <li><a href="/blog/category/politics-news">Politics</a></li>
                        <li><a href="/content/category/4-state-news">States</a></li>
                        <li><a href="/blog/category/hemp">Hemp</a></li>
                        <li><a href="/blog/category/marijuana-reform">Marijuana Law Reform</a></li>
                        <li><a href="/blog/category/medical-marijuana">Medical Marijuana</a></li>

                    </ul>
                </li>
                <li><a href="/content/category/4-blog-" class="sf-with-ul">LEARN</a>
                    <ul>
                        <li><a href="/">Ailmens</a></li>
                        <li><a href="/blog/category/cbd-education">CBD</a></li>
                        <li><a href="/content/1432-medical-studies">Medical Studies</a></li>
                        <li><a href="/blog/category/terpenes">Terpenes</a></li>
                    </ul>
                </li>
                <li><a href="/content/1427-medical-marijuana-state-specific-legal-issues">STATE LAWS</a>
                    <ul>
                        <li><a href="#">Medical Marijuana Laws</a></li>
                        <li><a href="#">Hemp Laws</a></li>
                    </ul>
                </li>
                <li><a href="index.php?fc=module&amp;module=medicalmarijuanaexchangedirectory&amp;controller=front"
                       title="DIRECTORY">DIRECTORY</a></li>
                <li>
                    <a href="/financial" title="BUSINESS">BUSINESS</a>
                    <ul>
                        <li><a href="/financial">Finance</a></li>
                        {*						<li><a href="/page-jobs">Jobs</a></li>*}
                    </ul>
                </li>
                <li><a href="/page-jobs" title="JOBS">Careers</a></li>
            </ul>
        {/if}
    </div>
    <!--/ Menu -->
{/if}
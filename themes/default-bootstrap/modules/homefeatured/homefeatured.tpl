<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/css-stars.css" />



<div class="lnebg clearfix">
	<div class="hdnam" >
		<a href="#">
			<span>Featured Brands</span>
		</a>
	</div>
	<div class="featured_brands_arrows">
		<span class="slide_left">
	     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
	    </span>
		<span class="slide_right">
	     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
	    </span>
	</div>
	<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 6])}">SHOW ALL</a>
</div>
{if isset($products6) && $products6}
	{include file="$tpl_dir./product-list6.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow"  style="display:block;">
	{*<li class="alert alert-info">{l s='No Featured Brands at this time.' mod='homefeatured'}</li>*}
</ul>
{/if}


{*<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span>Dispensary Storefronts</span>
	</a>
</div>
<div class="dispansery_arrows">
		<span class="slide_left">
	     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
	    </span>
		<span class="slide_right">
	     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
	    </span>
	</div>
	<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 4])}">SHOW ALL</a>
	<span class="btn_divider">|</span>
	<a class="map_btn" href="/dispensaries?type=storefronts"><img src="/themes/default-bootstrap/img/map.png">MAP</a>
</div>
{if isset($products4) && $products4}
	{include file="$tpl_dir./product-list4.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
	
</ul>
{/if}*}





<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span>Delivery</span>
	</a>
</div>
<div class="delivery_arrows">
	<span class="slide_left">
     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
    </span>
	<span class="slide_right">
     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
    </span>
</div>
<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 3])}">SHOW ALL</a>
<span class="btn_divider">|</span>
<a class="map_btn" href="/dispensaries?type=deliveries"><img src="/themes/default-bootstrap/img/map.png">MAP</a>
</div>
{if isset($products3) && $products3}
	{include file="$tpl_dir./product-list3.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
	{*<li class="alert alert-info">{l s='No Delivery at this time.' mod='homefeatured'}</li>*}
</ul>
{/if}











<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span>Deals Nearby</span>
	</a>
</div>
<div class="deals_arrows">
	<span class="slide_left">
     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
    </span>
	<span class="slide_right">
     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
    </span>
</div>
<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 2])}">SHOW ALL</a>
</div>
{if isset($products2) && $products2}
	{include file="$tpl_dir./product-list2.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow" style="display:block;">
	{*<li class="alert alert-info">{l s='No Deals Nearby at this time.' mod='homefeatured'}</li>*)
</ul>
{/if}










<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span style="font-size:25px">Deals</span>
	</a>
	
	
</div>
</div>

{if isset($products1) && $products1}
	{include file="$tpl_dir./product-list1.tpl" class='homefeatured tab-pane' id='homefeatured'}
{else}
<ul id="homefeatured" class=" tab-pane forceshow" style="display:block;">
	{*<li class="alert alert-info">{l s='No Deals at this time.' mod='homefeatured'}</li>*}
</ul>
{/if}




















<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span>Doctors Nearby</span>
	</a>
</div>
<div class="doctors_arrows">
	<span class="slide_left">
     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
    </span>
	<span class="slide_right">
     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
    </span>
</div>
<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 5])}">SHOW ALL</a>
<span class="btn_divider">|</span>
<a class="map_btn" href="/dispensaries?type=doctors"><img src="/themes/default-bootstrap/img/map.png">MAP</a>
</div>
{if isset($products5) && $products5}
	{include file="$tpl_dir./product-list5.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
	{*<li class="alert alert-info">{l s='No Doctors at this time.' mod='homefeatured'}</li>*}
</ul>
{/if}

{*<div class="lnebg   clearfix">
<div class="hdnam" >
	<a href="#">
		<span>Medical</span>
	</a>
</div>
<div class="medical_arrows">
	<span class="slide_left">
     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
    </span>
	<span class="slide_right">
     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
    </span>
</div>
<a class="all_featured_btn"href="{$link->getPageLink('pointshome', null, null, ['hpoint' => 7])}">SHOW ALL</a>
</div>
{if isset($products7) && $products7}
	{include file="$tpl_dir./product-list7.tpl" class='homefeatured ' id='homefeatured'}
{else}
<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
</ul>
{/if}*}


<style>
.forceshow{
display:block!important;
}


ul#homefeatured {
display:block!important;

}
</style>

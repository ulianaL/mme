{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
<div class="row">
{$HOOK_FOOTER}
</div>						
						
						
						

<div class="row" style="margin-top:25px;">
<div  class="col-md-12">
   <ul class="htmlcontent-home clearfix row">
      <li class="htmlcontent-item-1 col-xs-3">
         <div class="item-html">
            <h4>Contact Us</h4>
            <ul>
               <li><a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">Contact</a></li>
               <li><a href="{$link->getCmsLink(595)}">Privacy Policy</a></li>
               <li><a href="{$link->getCmsLink(3)}">Terms of Use</a></li>
            </ul>
           
         </div>
      </li>
	  
	  
	  
	  
      <li class="htmlcontent-item-2 col-xs-3">
         <div class="item-html">
            <h4>Business Resources</h4>
            <ul>
               
			   
			   {if $cccustomer->bussiness != 1 }
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">Business Services</a></li>
			   {else}
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}">Business Services</a></li>
			   {/if}
			   
			  
			  
			 {if $cccustomer->bussiness != 1 }
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">List Your Business on MME</a></li>
			   {else}
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}">List Your Business on MME</a></li>
			   {/if}
               
			  
              {if $cccustomer->bussiness != 1 }
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">Advertise on MME</a></li>
			   {else}
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}">Advertise on MME</a></li>
			   {/if}
			  
			  
			  
			   {if $cccustomer->bussiness != 1 }
			   <li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">Business Log In</a></li>
               {/if}
			   
			    {if $cccustomer->bussiness != 1 }
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbusiness')|escape:'html'}">Ad Standards</a></li>
			   {else}
					<li><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}">Ad Standards</a></li>
			   {/if}
			   
			  
			   
			   
            </ul>
         </div>
      </li>
      <li class="htmlcontent-item-3 col-xs-3">
         <div class="item-html">
            <h4>Connect</h4>
            <ul>
               <li><a href="https://twitter.com/THEMMEXCHANGE" target="_blank">Twitter</a></li>
               <li><a href="https://www.pinterest.com/medicalmarijuan/" target="_blank">Pinterest</a></li>
               <li><a href="https://www.facebook.com/Medical-Marijuana-Exchange-239747869540386/" target="_blank">Facebook</a></li>
               <li><a href="https://www.tumblr.com/blog/medicalmarijuanaexchange1" target="_blank">Tumbler</a></li>
               <li><a href="https://www.instagram.com/medicalmarijuanaexchange/" target="_blank">Instagram</a></li>
              
           
            </ul>
         </div>
      </li>
	  
	   
	   
	   <li class="htmlcontent-item-1 col-xs-3">
         <div class="item-html">
            <h4>Company</h4>
            <ul>
               <li><a href="{$link->getCmsLink(4)}">About Us</a></li>
            </ul>
         </div>
      </li>
	  
   </ul>
</div>











						
						
						
						
						
						
</div>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>





<script>




	$('.changepaytriger').change(function() {

		whatidpick = $(this).attr('payselect');
		pickjvalue = $(this).val();
		attributetopass = $(this).attr('attribute');
		
		$('table#cart_summary').css('opacity','0.3');
		//alert(attributetopass);
		

    var url = "#"; // the script where you handle the form input.

	$.ajax({
           type: "POST",
           url: url,
           data: "updatecart=1&idproooo="+whatidpick+"&pay="+pickjvalue+"&attributetopass="+attributetopass, // serializes the form's elements.
           success: function(data)
           {
               if(data == 'updateok'){
			   updatetotalss();
			   
			   $('table#cart_summary').css('opacity','1');
				     
					if (pickjvalue == 2){
						$('.pickidg'+whatidpick).show();
					}else {
						$('.pickidg'+whatidpick).hide();
					}
				
				
				//alert(data); // show response from the php script.
			   }else{
			   alert(data);
				location.reload();
			   }
           }
         });
		 
	
   

    return false; // avoid to execute the actual submit of the form.
		
		
	})
	
	
	
	
function updatetotalss(){
 
   var url = "#"; 
	$.ajax({
           type: "POST",
           url: url,
           data: 'gettotalsto', // serializes the form's elements.
           success: function(data)
           {
              
			   obj_totals = JSON.parse(data);
			   $('.totalcod').text("$"+obj_totals.totalcod.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   $('.totalpaypal').text("$"+obj_totals.totalpaypal.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   $('.totalpick').text("$"+obj_totals.totalpick.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
			   allok();
			   setTimeout(allok,100)
			   setTimeout(allok,500)
			   setTimeout(allok,1000)
			   setTimeout(allok,2000)
			   setTimeout(allok,5000)
			   
           }
         });
}






function allok(){
        totalpick2 = $('.totalpick').text()
        number = Number(totalpick2.replace(/[^0-9\.]+/g,""));
		if( number > 0 ){
			$('.totalpick').parent().show(1000);
		
		}else{
			$('.totalpick').parent().hide(1000);
		}
		
		totalcod2 = $('.totalcod').text()
        number = Number(totalcod2.replace(/[^0-9\.]+/g,""));
		if( number > 0 ){
			$('.totalcod').parent().show(1000);
		
		}else{
			$('.totalcod').parent().hide(1000);
		}
		
		
		totalpaypal2 = $('.totalpaypal').text()
        number = Number(totalpaypal2.replace(/[^0-9\.]+/g,""));
		// alert(number);
		if( number > 0 ){
			$('.totalpaypal').parent().show(1000);
			//if paypal hide COD
			$('.cash').hide();
			$('#paypallparallel_payment_button').show();
		
		}else{
		   
			 //if not paypal show COD 
			$('.totalpaypal').parent().hide(1000);
			$('.cash').show();
			$('#paypallparallel_payment_button').hide();
		}
		
		
		 

}




$(document).ready(function(){
	
	if(typeof(tttotalpaypal) !== 'undefined'){
		//process hide or show payments 
		if ( tttotalpaypal > 0 ){
		$('.cash').hide();
		
		}else{
		$('#paypallparallel_payment_button').hide();
		}
	}	
	
})

//openclosecal
function openclosecal(vv){
 $('.chofeatoopenclose').hide();
 $('.chofeatoopenclose'+vv).show();
 



 $('.rnge').hide();
 $('.range_'+vv).show();
 
 $('.bbn').hide();
 $('.bbn_'+vv).show(); 
 $('.datetofea').hide();
 $('.datetofea_'+vv).show();
}


//submit 


</script>











<style>

.panel{
    margin-bottom: 0px;
	z-index:999999999999999;
}
.chat-window{
    bottom:0;
    position:fixed;
    float:right;
    margin-left:10px;
	right: 0px;
	transition:all 300ms;
	bottom:-344px;
}
.chat-window > div > .panel{
    border-radius: 5px 5px 0 0;
}
.icon_minim{
    padding:2px 10px;
}
.msg_container_base{
  background: #e5e5e5;
  margin: 0;
  padding: 0 10px 10px;
  max-height:300px;
  min-height: 300px;
  overflow-x:hidden;
}
.top-bar {
  background: #666;
  color: white;
  padding: 10px;
  position: relative;
  overflow: hidden;
}
.msg_receive{
    padding-left:0;
    margin-left:0;
}
.msg_sent{
    padding-bottom:20px !important;
    margin-right:0;
}
.messages {
  background: white;
  padding: 10px;
  border-radius: 2px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  max-width:100%;
}
.messages > p {
    font-size: 13px;
    margin: 0 0 0.2rem 0;
  }
.messages > time {
    font-size: 11px;
    color: #ccc;
}
.msg_container {
    padding: 10px;
    overflow: hidden;
    display: flex;
}
.imgchatt {
    display: block;
    width: 100%;
}
.avatar {
    position: relative;
    padding: 0;
}
.p0chat{

	padding:0;
}
.base_receive > .avatar:after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    width: 0;
    height: 0;
    border: 5px solid #FFF;
    border-left-color: rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0);
}

.base_sent {
  justify-content: flex-end;
  align-items: flex-end;
}
.base_sent > .avatar:after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 0;
    height: 0;
    border: 5px solid white;
    border-right-color: transparent;
    border-top-color: transparent;
    box-shadow: 1px 1px 2px rgba(black, 0.2); // not quite perfect but close
}

.msg_sent > time{
    float: right;
}



.msg_container_base::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

.btn-group.dropup{
    position:fixed;
    left:0px;
    bottom:0;
}
.clist {
    border-bottom: 1px solid #ddd;
    font-size: 17px;
    padding: 4px;
	cursor: pointer;
}
.customerlist {
    padding: 0;
    overflow-y: scroll;
	max-height: 300px;
	  
}
.clist.active, .clist:hover{
    background: #e5e5e5;
    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.2);
  
}
img.imgchatt {
    border: 3px solid #5bbe8f;
    border-radius: 4px;
	min-height: 50px;
}
.upchatro{
    position: absolute;
    right: 10px;
    top: 6px;
    font-size: 25px;
	display:none;
	cursor: pointer;
	
}
.upchatro.active{
    position: absolute;
    right: 10px;
    top: 6px;
    font-size: 25px;
	display:block;
}

.online {
    width: 12px;
    height: 12px;
    background-color: #37a000;
    float: right;
    margin-top: 3px;
    border-radius: 100px;
    border: 2px solid white;
}
.offline {
    width: 12px;
    height: 12px;
    background-color: #37a000;
    float: right;
    margin-top: 3px;
    border-radius: 100px;
    border: 2px solid white;
	background-color: #ff7d7d;
}
.counttoread {
    background: #6e6e6e;
    color: white;
    font-size: 11px;
    position: absolute;
    right: 18px;
    padding: 0 4px;
    border-radius: 2px;
	font-weight:800!important;
}
.counttoread.disable {
    display:none;
}


.counttoreadtotal {
    background: #e84747;
    color: white;
    font-size: 11px;
    padding: 2px 4px;
    border-radius: 2px;
    position: relative;
    top: -1px;
    left: 8px;
}
</style>




















{if $me != 0}
    <div class="row chat-window  col-md-6" id="chat_window_1" style="margin-left:10px;z-index:999999999999;    padding: 0;
    margin: 0;">
		   <div class="panel panel-default  ">
                <div class="panel-heading top-bar">
                        <h3 class="panel-title">Chat <span class=" counttoreadtotal">0</span></h3>
						<i class="fa fa-caret-up upchatro upchatroup active" onclick="openclosechat(1)" aria-hidden="true"></i>
						<i class="fa fa-times upchatro upchatrodown"  onclick="openclosechat(0)" aria-hidden="true"></i>

                </div>
				<div class="col-md-4 customerlist" style="padding:0;">
					{foreach $customerlist item=cu}
					<div cuid="{$cu.cuid}"  onclick="updateselectedcustromeronchat({$cu.cuid})" class="clist clist{$cu.cuid}  {if $chatactive == $cu.cuid or count($customerlist) == 1}active{/if}">{$cu.firstname} {$cu.lastname} <span class="disable counttoread counttoread{$cu.cuid}"></span><div class="offline  onoffline{$cu.cuid}"></div></div>
					{/foreach}
				</div>
				
				
				<div class="panel-body msg_container_base col-md-8"></div>
                
				
				<div class="panel-footer">
                    <div class="input-group">
                        <input style="height: 30px;" id="btn-input" type="text" onfocus="ireadmsns()" class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" onclick="addmsn();" id="btn-chat">Send</button>
                        </span>
                    </div>
                </div>
    		</div>
      </div>





<script>
//each user have msn to read 

//openclosechat


$('.chat_input').keypress(function(e) {
//13 == ENTER KEY...
if(e.which == 13) {
             e.preventDefault();
             addmsn();
          }
});



function msntoread(){
	user = '';
	$('.clist').each(function(){
		user += $(this).attr('cuid')+':';
	});	
	$.ajax({
			type: "POST",
			url: '#',
			data: 'msntoread&user='+user,
				success: function(data)
				{
					//alert(data);
					countmsguser = $.parseJSON(data);
					
					$ccctotaltoread = 0;
					countmsguser.forEach(function(item, index){
						//console.log("item.user="+item.user+":::::item.count="+item.count);
						$ccctotaltoread += Number(item.count);
						if(item.count > 0){
							$('.counttoread'+item.user).removeClass('disable');
							$('.counttoread'+item.user).text(item.count);
						}else{
							$('.counttoread'+item.user).addClass('disable');
							$('.counttoread'+item.user).text(item.count);
						}
					});
					//alert($ccctotaltoread);
					$('.counttoreadtotal').text($ccctotaltoread);
				}
		   });
}




function ireadmsns(){
	whatuser = $('.clist.active').attr('cuid');
	lastmsgtoiread = $('.msg_container').last().attr('maxchat');
	$.ajax({
			type: "POST",
			url: '#',
			data: 'ireadmsns&whatuser='+whatuser+"&lastmsgtoiread="+lastmsgtoiread,
				success: function(data)
				{
					//msntoread();
					//console.log(data);
				}
		   });
	//console.log('I read msn..  user: '+whatuser+'lastmsn:'+lastmsgtoiread);
}






// are online //all users
function areonline(){
	iiiii = 0;
	user = '';
	$('.clist').each(function(){
		user += $(this).attr('cuid')+':';
	});
	
	
	    var url = "#"; // the script where you handle the form input.
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'areonline&user='+user,
				   success: function(data)
				   {
					cuonline = $.parseJSON(data);
					cuonline.forEach(function(item, index){
					 if(item.lasttime > 10){
						//offline
						$('.onoffline'+item.userid).addClass('offline');
						$('.onoffline'+item.userid).removeClass('online');
					 }else{
						$('.onoffline'+item.userid).addClass('online');
						$('.onoffline'+item.userid).removeClass('offline');
					 
					 }
					 
					// alert( item.lasttime+'-'+item.userid )
					})


				   }
				 });


}

$(document).ready(function(){
openclosechat({$openclosechat});
});


//open close chat !!
function openclosechat(v){
	if(v == 1)
	{
		$('.upchatroup').removeClass('active');
		$('.upchatrodown').addClass('active');
	    $('.chat-window').css('bottom',0);
		activechat = 1
	}else{
		$('.upchatroup').addClass('active');
		$('.upchatrodown').removeClass('active');
		 $('.chat-window').css('bottom',-344);
		activechat = 0
	}
	    var url = "#";
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'openclosechat='+activechat, // serializes the form's elements.
				   success: function(data)
				   {
					  //alert(data); // show response from the php script.
					  
				   }
			   });
	
	
}







function updateselectedcustromeronchat(value){
	
		$('.clist').removeClass('active');
		$('.clist'+value).addClass('active');
		$('.msg_container_base').empty(); 
		checknewmsn(3);
		
		
		var url = "#"; // the script where you handle the form input.
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'updateselectedcustromeronchat&whatuser='+value, // serializes the form's elements.
				   success: function(data)
				   {
					 // alert(data); // show response from the php script.
					  
				   }
				 });
	
}






function addcontacttochat(value){
	var url = "#"; // the script where you handle the form input.
	$.ajax({
			   type: "POST",
			   url: url,
			   data: 'addcontacttochat&addthiscontact='+value, // serializes the form's elements.
			   success: function(data)
			   {
				 //alert(data); // show response from the php script.
				 chatculistarray = $.parseJSON(data);
				 
				
				 if(chatculistarray.error.count == 0){
					 openclosechat(1);
					 if (chatculistarray.sucess.whatisme == 1){
						updateselectedcustromeronchat(chatculistarray.sucess.user2);
					 
					 }else{
						updateselectedcustromeronchat(chatculistarray.sucess.user1);
					 
					 }
					
					
					
				 }else{
					//manage errors...
					$(".chat-window").load(location.href + " .chat-window",function(){
					
					openclosechat(1);
					});
					
					
				 }
				 
				 
				 
				 
				   
			   }
			 });
}




run = 0;

function checknewmsn(scroll=1){
	
	
	var url = "#"; // the script where you handle the form input.
	$.ajax({
			   type: "POST",
			   url: url,
			   data: 'checknewmsn&maxchat='+$('.msg_container').last().attr('maxchat')
			   +'&chatactive='+$('.clist.active').attr('cuid'), 
			   success: function(data)
			   {
				   
				   
				   //alert(data); // show response from the php script.
				   $('.msg_container_base').append(data);
				   
				   if (scroll == 3){
				   		$(".msg_container_base").animate({ scrollTop:$('.msg_container_base').prop("scrollHeight") }, 0);
				   }
				   
				   if(data != '' && scroll != 3 ){
				    openclosechat(1);
					$(".msg_container_base").animate({ scrollTop:$('.msg_container_base').prop("scrollHeight") }, 1000);
				   }
				   
				   
				   
			   }
			 });
}
		 
function addmsn(){
	
	texttttochat = $('#btn-input').val();
	$('#btn-input').val('');
	
	if (texttttochat != '') {
		var url = "#"; // the script where you handle the form input.
		$.ajax({
				   type: "POST",
				   url: url,
				   data: 'addmsn&msg='+ encodeURIComponent(texttttochat)
				   +'&chatactive='+$('.clist.active').attr('cuid'), // serializes the form's elements.
				   success: function(data)
				   {
					  
					  
					  checknewmsn(1)
					  //alert(data); // show response from the php script.
					   
					  
				   }
				 });
	}
	return false;
}
		


	
setInterval(function(){
    
	if (jQuery.active == 0){
		checknewmsn(1) // this will run after every 5 seconds
	}
	areonline();
	msntoread();
}, 5000);

checknewmsn(3);
areonline();
msntoread();





</script>
{else}
<script>

function addcontacttochat(value){

fancyAlert('You need Login/Register to contact this user.')

}

function fancyAlert(msg) {
    jQuery.fancybox({
        'modal' : false,
        'content' : "<div style=\"margin:1px;width:300px;font-size:25px;line-height: 25px;\">"+msg+"<div style=\"text-align:right;margin-top:10px;\"></div></div>",
		
		beforeClose:function(){
				//redirect to login	
			  window.location.replace('{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}')
		}
    
	}
	
	);
}

</script>
{/if}



<script>
$.ajax({
				  type: "POST",
				   url:'#' ,
				   data: 'myusers',
				   success: function(data)
				   {
					 alert(data) 
				   }
				 });


</script>










	
	
	
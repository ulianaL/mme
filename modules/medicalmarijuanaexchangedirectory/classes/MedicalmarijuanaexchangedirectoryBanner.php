<?php
class MedicalmarijuanaexchangedirectoryBanner extends ObjectModel
{
	
	public $id_banner;
	public $size;
	public $name;
	public $banner_img;
	public $date_add;
	public $customer; 
	public $city;
	public $state;
	public $country;
    public $status; 
    public $autoplay;
    public $banner_loop; 

	public static $definition = array(
        'table' => 'banner',
        'primary' => 'id_banner',
        'fields' => array(
            
          
            'id_banner' => array('type' => self::TYPE_INT),
            'size' => array('type' => self::TYPE_INT),
            'name' => array('type' => self::TYPE_STRING),
            'banner_img' => array('type' => self::TYPE_STRING),
            'date_add' => array('type' => self::TYPE_STRING),
            'customer' => array('type' => self::TYPE_INT),
            'city' => array('type' => self::TYPE_INT),
            'state' => array('type' => self::TYPE_INT),
            'country' => array('type' => self::TYPE_INT),
            'status' => array('type' => self::TYPE_INT),
            'autoplay' => array('type' => self::TYPE_INT),
            'banner_loop' => array('type' => self::TYPE_INT),
            
            
        ),
    );

    public function delete()
    {
        removeBannerFromCloud($this->banner_img);exit;
        if (parent::delete()) {

        }
        return false;
    }
}
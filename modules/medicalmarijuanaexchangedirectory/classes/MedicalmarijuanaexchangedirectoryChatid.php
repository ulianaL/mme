<?php
//LLL 12/09/2018
class MedicalmarijuanaexchangedirectoryChatid extends ObjectModel
{
	
	public $id_chatid;
	public $file;
	public $uniqueid;
	public $uniqueid2;

	
	public static $definition = array(
        'table' => 'chatid',
        'primary' => 'id_chatid',
        'fields' => array(
            
          
            'id_chatid' => array('type' => self::TYPE_INT),
            'file' => array('type' => self::TYPE_STRING),
            'uniqueid' => array('type' => self::TYPE_STRING),
            'uniqueid2' => array('type' => self::TYPE_STRING),
          
            
        ),
    );
	
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
      
	     /*Shop::addTableAssociation('classes', array('type' => 'shop'));*/
        parent::__construct($id, $id_lang, $id_shop);
    }
	
	public function update($null_values = false)
    {
		return parent::update($null_values);
	}
	
	
	
}
{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='Add Business Account'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe}
    </span>
    <span class="navigation_page">
        {l s='Custom Page'}
    </span>
{/capture}



<div class="col-md-12">
<h1>Add business Account</h1>
</div>

<hr>


<form method="post">

<div class="row">
	<div class="col-md-12 form-group">
		<label class="radio-inline"><input type="radio" name="mr">MR.</label>
		<label class="radio-inline"><input type="radio" name="mrs">MRS.</label>
	</div>
</div>



<div class="row">
	<div class="col-md-6 form-group">
		<label for="firstname">First Name*</label>
		<input class="form-control" id="firstname"  name="firstname" type="text" required />
	</div>

	<div class="col-md-6 form-group">
		<label for="lastname">Last Name*</label>
		<input class="form-control" id="lastname"  name="lastname" type="text" required />
	</div>
</div>


<div class="row">
	<div class="col-md-6 form-group">
		<label for="companyname">Company Name*</label>
		<input class="form-control" id="companyname"  name="companyname" type="text" required />
	</div>

	<div class="col-md-6 form-group">
		<label for="emailaddress">Email Address*</label>
		<input class="form-control" id="emailaddress"  name="emailaddress" type="email" required />
	</div>
</div>


<div class="row">
	<div class="col-md-6 form-group">
		<label for="phonenumber">Phone Number*</label>
		<input class="form-control" id="phonenumber"  name="phonenumber" type="text" required />
	</div>

	<div class="col-md-6 form-group">
		<label for="postalcode">Postal Code*</label>
		<input class="form-control" id="postalcode"  name="postalcode" type="text" required />
	</div>
</div>

<div class="row">
	<div class="col-md-6 form-group">
	  <label for="companytype">Company Type*:</label>
	  <select class="form-control" id="companytype" name="companytype" required >
		<option value="0">- Please Select -</option>
		<option value="1">Storefront</option>
		<option value="2">Clinic</option>
		<option value="3">Delivery</option>
		<option value="4">Producer/Processor</option>
		<option value="5">CBD/Hemp</option>
		<option value="6">Hardware (Glass, Vapes, Accessories)</option>
		<option value="7">Grow Supply</option>
		<option value="8">Service</option>
	  </select>
	</div>

	<div class="col-md-6 form-group clearfix">
		<label for="password">Password*</label>
		<input class="form-control" id="password"  name="password" type="password" required />
	</div>
</div>

<div class="row">
	<div class="col-md-6 form-group clearfix">
	  <label for="monthlyadvertisingbudget">Monthly Advertising Budget*:</label>
	  <select class="form-control" id="monthlyadvertisingbudget" name="monthlyadvertisingbudget" required >
		<option value="0">- Please Select -</option>
		<option value="1">No budget</option>
		<option value="2">$500-$999</option>
		<option value="3">$1,000-$2,499</option>
		<option value="4">$2,500-$4,999</option>
		<option value="5">$5,000-$9,999</option>
		<option value="6">$10,000+</option>
	  </select>
	</div>
</div>

<div class="row">
	<div class="col-md-12 form-group clearfix">
	  <label for="interestedin">What are you interested in?</label>
	  <textarea class="form-control" rows="5" id="interestedin" name="interestedin"></textarea>
	</div>
</div>

<div class="row">
	<div class="col-md-12 form-group clearfix">
		<label for="websiteurl">Website URL</label>
		<input class="form-control" id="websiteurl"  name="websiteurl" type="text"/>
	</div>
</div>


<div class="col-md-12 checkbox clearfix ">
	<label><input type="checkbox" value="" name="agree">I agree to the Terms of Service and Privacy Policy.</label>
</div>



<div class="form-group clearfix" style="text-align: center">
	<input class="form-control" value="Submit" type="submit" style="width: 150px; margin: 0 auto;"/>
</div>







</form>

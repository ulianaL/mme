{capture name=path}
	<span class="navigation_page">Latest news</span>
{/capture}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script type="text/javascript">
	// var markets = ['ACAN', 'NYSENasdaq:ACB', 'ACCA', 'ACOL', 'ACRL', 'ADVT', 'AERO', 'AGTK', 'AMFE', 'AMMJ', 'NYSENasdaq:APHA', 'ATTBF', 'AXIM', 'BLOZF', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CSAX', 'CURR', 'CVSI', 'DEWM', 'DIGP', 'DIRV', 'DPWW', 'EAPH', 'EFFI', 'EMHTF', 'EMMBF', 'ERBB', 'ETST', 'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GNBT', 'GRCU', 'GRNH', 'GRSO', 'GRWC', 'GRWG', 'GTBP', 'GTSO', 'HLIX', 'HLSPY', 'HMPQ', 'HVST', 'HYYDF', 'ICCLF', 'IMLFF', 'IVITF', 'KAYS', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LSCG', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYDX', 'MYHI', 'NDEV', 'NHLE', 'NMUS', 'NSPDF', 'NVGT', 'OGRMF', 'OWCP', 'PKPH', 'PLPL', 'PMCB', 'PNPL', 'PNTV', 'POTN', 'PRMCF', 'PRRE', 'PUFXF', 'QRSRF', 'REFG', 'REVI', 'RMHB', 'RSSFF', 'SING', 'SIPC', 'SNNC', 'SPLIF', 'SPRWF', 'SRNA', 'STEV', 'STWC', 'TAUG', 'TBPMF', 'TECR', 'THCBF', 'TRTC', 'NYSENasdaq:CGC', 'UAMM', 'UMBBF', 'VAPI', 'VAPR', 'VATE', 'VHUB', 'VRTHF', 'WCIG', 'WDRP', 'WTII', 'ZDPY'];

  // var markets = ['ACAN', 'NYSENasdaq:ACB','AGTK', 'AMFE', 'AMMJ', 'NYSENasdaq:APHA', 'ATTBF', 'AXIM', 'BLOZF', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CSAX', 'CURR', 'CVSI', 'DEWM', 'DIGP', 'DIRV',  'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GNBT', 'GRCU', 'GRNH', 'GRSO', 'GRWC', 'GRWG', 'GTBP', 'GTSO', 'HLIX', 'HLSPY', 'HMPQ', 'HVST', 'HYYDF', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LSCG', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYDX', 'MYHI', 'NDEV', 'NHLE', 'NMUS', 'NSPDF', 'NVGT', 'OGRMF', 'OWCP', 'PKPH', 'PLPL', 'PMCB', 'PNPL', 'PNTV', 'POTN', 'PRMCF', 'PRRE', 'PUFXF', 'QRSRF', 'REFG', 'REVI', 'RMHB', 'RSSFF', 'SRNA', 'STEV', 'STWC', 'TAUG', 'NYSENasdaq:CGC','WCIG', 'WDRP', 'WTII', 'ZDPY'];

    var markets = ['ACAN', 'NYSENasdaq:ACB','AGTK', 'AMFE', 'AMMJ', 'NYSENasdaq:APHA', 'ATTBF', 'AXIM', 'BLOZF', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CURR', 'CVSI', 'DIGP', 'DIRV',  'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GNBT', 'GRCU', 'GRNH', 'GRWC', 'GRWG', 'GTSO', 'HLIX', 'HLSPY', 'HVST', 'HYYDF', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYHI', 'NDEV', 'NMUS', 'NSPDF', 'OGRMF', 'OWCP', 'PKPH', 'PMCB', 'PNPL', 'POTN', 'PRMCF', 'PUFXF', 'QRSRF', 'REFG', 'RMHB', 'SRNA', 'TAUG', 'NYSENasdaq:CGC','WCIG', 'ZDPY'];

  var news = [];
  var j = 0;
  var i = 0;
    getnews();
    function getnews() {
      if (i <= markets.length-1) {
        i++;
        $.ajax({
          url: 'https://api.stockdio.com/data/financial/info/v1/getNewsEx?app-key=9F294CB158A94C73B7D84E38BC128E54&nItems=1&includeImage=false&includeDescription=false&maxItems=1&stockExchange=OTCMKTS&symbol='+markets[i],
          context: document.body
        }).done(function (r) {
            news_one = r['data']['news']['values'];
            news = $.merge( $.merge( [], news ), news_one);
            if(news.length >= 5){
              shownews();
            }
            getnews();
        }).fail(function (e) {
           console.log('my fail code');
           getnews()
        });
        j++;
        if (j == markets.length-1) {
          shownews();
        }
      }
    }


 function shownews() {
	console.log(news);
	news.sort(function(a,b){
	  return new Date(b[0]) - new Date(a[0]);
	});
	console.log(news);
	news_list = '';
	for (var i = 0; i <= news.length-1; i++) {
		news_list += '<div class="single_news_block"><a class="singlr_title" href="'+news[i][2]+'" target="_blank">'+news[i][1]+'</a> <span class="singl_date">'+moment(news[i][0]).fromNow()+' | '+news[i][5]+'</span></div>';
	}
	$('#news_list').html(news_list);
  $(".single_news_block").slice(0, 25).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".single_news_block:hidden").slice(0, 10).slideDown();
        if ($(".single_news_block:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
 }  
</script>

<script>
   if (typeof(stockdio_events) == "undefined") {
      stockdio_events = true;
      var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
      var stockdio_eventer = window[stockdio_eventMethod];
      var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
      stockdio_eventer(stockdio_messageEvent, function (e) {
         if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
            eval(e.data.method);
         }
      },false);
   }
</script>

<div class="left_width">
  <div id="stocks_news">
    <div class="stocks_news_title">Latest news</div>
    <div id="news_list">
      <div class="single_loading_block">News Loading...</div>
    </div>
      <a href="#" id="loadMore">Load More</a>
  </div>
</div>
<div class="right_width">
  <iframe id="relatedcompanies" frameborder="0" scrolling="no" width="100%" height="268" src="https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?stockExchange=otcmkts&amp;symbol=ACBFF&amp;presetlist=related_indices&amp;limit=6&amp;palette=Financial-Light&amp;width=100%25&amp;height=auto&amp;onlink=true&amp;onload=iframemarkets&amp;template=2451116a-1563-4a96-be20-60554d6748ba&amp;&amp;app-key=9F294CB158A94C73B7D84E38BC128E54"></iframe>

  <div class="stocks_list">
    <iframe id='st_c0b82d488b814ee7882627a227052610' frameBorder='0' scrolling='no' width='100%' height='600' src='https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols=ACAN;NYSENasdaq:ACB;ACCA;AERO;AGTK;AMFE;AMMJ;NYSENasdaq:APHA;ATTBF;AXIM;BLOZF;BUDZ;BXNG;CAFS;CANL;CANN;NYSENasdaq:CARA;CBCA;CBDS;CBGI;CBIS;CBMJ;CBNT;CBSC;CCAN;NYSENasdaq:CGC;CGRA;CGRW;CHUM;CIIX;CLSH;CMMDF;CNAB;CNBX;CNZCF;CPMD;CRWG;CURR;CVSI;DIGP;DIRV;DPWW;EAPH;EFFI;EMHTF;EMMBF;ERBB;ETST;EVIO;GBHPF;GBLX;GLAG;GNBT;GRCU;GRNH;GRWC;GRWG;GTSO;NYSENasdaq:GWPH;HLIX;HLSPY;HVST;HYYDF;ICCLF;NYSENasdaq:IIPR;NYSENasdaq:IGC;IMLFF;NYSENasdaq:INSY;KAYS;KGKG;KSHB;LCTC;LDSYF;LVVV;MCIG;MCOA;MCPI;MDCL;MDEX;MDRM;MGWFF;MJMD;MJNA;MJNE;MNTR;MQPXF;MRPHF;MSRT;MYHI;NDEV;NMUS;NSPDF;OGRMF;OWCP;PKPH;PMCB;PNPL;POTN;PRMCF;PUFXF;QRSRF;REFG;;RMHB;SING;SIPC;NYSENasdaq:SMG;SNNC;SPLIF;SPRWF;SRNA;TAUG;TBPMF;TECR;THCBF;NYSENasdaq:TLRY;NYSENasdaq:TRPX;TRTC;UMBBF;VAPI;VAPR;VATE;VRTHF;WCIG;ZDPY;NYSENasdaq:ZYNE&includeLogo=false&palette=Financial-Light&title=Marijuana%20Stocks&linkUrl=https://mme.world/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=600px'></iframe>
  </div>

  {include file="$includedir/financial_right_banner.tpl"}

</div>

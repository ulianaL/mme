<div class="container">
	<div class="row">
		<div class="col-sm-3 lftpan">
			<div class="blogarea">
				<div><a href="{$linkaddfree}"><h2>All Categories</h2></a></div>
				<div class="clearfix"></div>
					<div id="categories_block_left1" class="mainlft">
						<ul>
							<li>
							<a href="{$linkaddfree}&catid=1">Accounting Firms</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=2">Advertising / Marketing</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=3">Apparel & Accessories</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=4">Breeders, Genetics, or Clones</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=5">Consulting</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=6">Detoxification</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=32">Deliveries</a>  
							</li>

							<li>
							<a href="{$linkaddfree}&catid=7">Dispensaries, Collectives and Wellness Centers</a>  
							</li>
							<li>
							<a href="{$linkaddfree}&catid=8">Doctors & Referral Services</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=9">Education</a>  
							</li>
							
							<li>
							<a href="{$linkaddfree}&catid=10">Extraction Companies</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=11">Glass Companies</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=12">Government Agencies</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=13">Hemp-Based Products</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=14">Hospitality & Tourism</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=15">Infused Product Companies</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=16">Investment</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=17">Legal</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=18">Lighting</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=19">Manufacturers</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=20">Organizations & Non-Profits</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=21">Public Companies</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=22">Publications/Resources</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=23">Research / Testing Labs</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=24">Retailers</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=25">Security / Insurance</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=26">Seed Banks</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=27">Seed Banks</a>  
							</li>
							
							
							<li>
							<a href="{$linkaddfree}&catid=28">Technology</a>  
							</li>
							
							
						</ul>

					</div>
				<div class="clearfix"></div>
			</div>
		</div><!---md-3--->
		<div class="col-sm-9 rtp">
			
			
			
			{if $results.0.logo != ""}
				<div style="width:100%">
				<img  style="margin:0 auto;max-width:250px" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/upload/{$results.0.logo}"/>
				</div>
				{/if}
		
			
		
			
			
			
			
			<h1 style="    font-size: 36px;
							font-weight: 600;
							-webkit-font-smoothing: antialiased;
							line-height: 44px;">{$results.0.name}</h1>
			<h2 class="heading heading-main-1">About this Organization</h2>
			
			
				
			
			<p class="">{$results.0.about|replace:'\n':'<br>'|replace:'\r':'<br>'}</p>
			
			
			
			
			
			
			
{if  $results.0.review == 1 && $results.0.pay == 1}
	
	
				{if $results.0.bio != ""}
				<h2 style="margin-top:25px;" class="heading heading-main-1">Company Biography</h2>
				<p class="">{$results.0.bio}</p>
				{/if}
				
				
				
				{if $results.0.image1 != ""}
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.css" />
					<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
					<h2 style="margin-top:25px;" class="heading heading-main-1">Image Gallery</h2>
					<div class="owl-carousel owl-theme">
						{if $results.0.image1 != ""}
							<div class="item">
								<img style="margin:0 auto;max-width:100%" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/gal/{$results.0.image1}"/>
							</div>
						{/if}
						{if $results.0.image2 != ""}
							<div class="item">
								<img  style="margin:0 auto;max-width:100%" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/gal/{$results.0.image2}"/>
							</div>
						{/if}
						{if $results.0.image3 != ""}
							<div class="item">
								<img  style="margin:0 auto;max-width:100%" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/gal/{$results.0.image3}"/>
							</div>
						{/if}
						{if $results.0.image4 != ""}
							<div class="item">
								<img  style="margin:0 auto;max-width:100%" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/gal/{$results.0.image4}"/>
							</div>
						{/if}
						{if $results.0.image5 != ""}
							<div class="item">
								<img  style="margin:0 auto;max-width:100%" class="img-responsive" src="/modules/medicalmarijuanaexchangedirectory/gal/{$results.0.image5}"/>
							</div>
						{/if}
					</div>
					<script>
						$('.owl-carousel').owlCarousel({
						items:1,
						loop:true,
						margin:10,
						responsiveClass:true,
						responsive:{
							0:{
								items:1,
								nav:true
							},
							600:{
								items:1,
								nav:false
							},
							1000:{
								items:1,
								nav:true,
								loop:false
							}
						}
					})
					</script>
				{/if}
				
			
			{if $results.0.videourl != ""}
				<h2 style="margin-top:25px;" class="heading heading-main-1">Video</h2>
				<iframe width="100%" height="600" src="{$results.0.videourl}" frameborder="0" allowfullscreen></iframe>
			{/if}
			
			
			 {if $results.0.address != "" || ($results.0.lng != "" && $results.0.address != "lat")} 
				<h2 style="margin-top:25px;" class="heading heading-main-1">Address</h2>
				<p>{$results.0.address}</p>
				
				<div class="">
				<address>{$results.0.address}</address>
				</div>
						  
						  
				{if $results.0.lng != "" && $results.0.lat != ""} 
				 <script>
				     $(document).ready(function(){
				        $("address").each(function(){
				             var embed ="<iframe width='100%' height='350' frameborder='1' scrolling='no' marginheight='0' marginwidth='0' src = 'https://maps.google.com/maps?q={$results.0.lng},{$results.0.lat}&hl=es;z=14&amp;output=embed'></iframe>";
				             $(this).html(embed);
				       });
				     });
				 </script> 
				 {else}
				 	<script type="text/javascript">
				 		$(document).ready(function(){
					        $("address").each(function(){
					             var embed ="<iframe width='100%' height='350' frameborder='1' scrolling='no' marginheight='0' marginwidth='0'  src='https://maps.google.com/maps?&amp;q="+encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
					             $(this).html(embed);
					       });
					     });
				 	</script>
				 {/if}
			
			{/if} 
			
			
			
			
			
			
{/if}		
			
			
			<h2 style="margin-top:25px;margin-bottom: -1px!important;" class="heading heading-main-1">CONTACT INFORMATION</h2>
			<ul class="list-group">
			 

{if  $results.0.review == 1 && $results.0.pay == 1}

			 {if $results.0.facebook != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.facebook}"><i class="fa fa-facebook" aria-hidden="true"></i>  Facebook</a></li>
			  {/if}
			  {if $results.0.twitter != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.twitter}"><i class="fa fa-twitter" aria-hidden="true"></i>  Twitter</a></li>
			  {/if}
			  {if $results.0.youtube != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.youtube}"><i class="fa fa-youtube" aria-hidden="true"></i>  YouTube</a></li>
			  {/if}
			  {if $results.0.instagram != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.instagram}"><i class="fa fa-instagram" aria-hidden="true"></i>  Instagram</a></li>
			  {/if}
			  {if $results.0.pinterest != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.pinterest}"><i class="fa fa-pinterest" aria-hidden="true"></i>  Pinterest</a></li>
			  {/if}
			  {if $results.0.website != ""}
			  <li class="list-group-item"><a target="_blank" href="{$results.0.website}"><i class="fa fa-external-link" aria-hidden="true"></i>  Website</a></li>
			  {/if}
{/if}
			  {if $results.0.phone != ""}
			  <li class="list-group-item"><a target="_blank" href="tel:{$results.0.phone}"><i class="fa fa-phone" aria-hidden="true"></i>
				{$results.0.phone}</a></li>
			  {/if}
			  
			 {if $results.0.email != ""}
			  <li  style="text-transform: lowercase;" class="list-group-item"><a target="_blank" href="mailto:{$results.0.email}"><i class="fa fa-envelope-o" aria-hidden="true"></i>
				{$results.0.email}</a></li>
			  {/if}


			</ul>

		</div><!---md-9--->
		
		<!---
		<div class="col-sm-3">
		<a class="button btn_primary btn-block" href="http://medicalmarijuanaexchange.com/index.php?fc=module&module=medicalmarijuanaexchangedirectory&controller=addfree">Add a company</a>
		</div>md-3end--->
		
		
	</div>
</div>


<style>
.loop__text {
    padding: 8px;
}
.heading-main-1 {
    font-size: 18px;
    font-weight: 600;
    -webkit-font-smoothing: antialiased;
    padding-bottom: 10px;
    border-bottom: 1px solid #e3e3e3;
    margin-bottom: 20px;
    color: #2e2e2e;
}

</style>
<li class="tree-item{if isset($node['disabled']) && $node['disabled'] == true} tree-item-disable{/if}">
	<span class="tree-item-name">
		<input type="radio" name="{$input_name}" value="{$node['id_category']}"{if isset($node['disabled']) && $node['disabled'] == true} disabled="disabled"{/if} {if in_array($node.id_category, $_selected_categories)}checked="checked"{/if}/>
		<i class="icon-dot"></i>
		<label class="tree-toggler">{$node['name']}</label>
	</span>
</li>
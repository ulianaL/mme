<link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet">

<style>
#left_column{
	display: none;
}
.breadcrumb {
    display: none;
}
p.pjob {
    font-size: 16px;
    border-bottom: 1px solid #333;
}
input.apply {
    min-width: 185px;
    /*padding: 18px 20px;*/
    padding: 12px 20px;
    display: block;
    white-space: nowrap;
    /*background-color: #5bbe8f;*/
	background: #078AE9;
    border-bottom-width: 1px;
    color: #fff;
    box-sizing: border-box;
    text-decoration: none;
    text-align: center;
    outline: none;
    font-size: 14px;
    font-family: 'robotobold', sans-serif;
    text-transform: uppercase;
    border: 0;
	margin: 15px 0;
}

</style>



<h1 style="text-transform: capitalize;font-family: 'Roboto', sans-serif;">{$job.tittle}</h1>
<div class="row">
	<div class="col-md-7">
		
		<p style="color: black;font-size: 20px;">
		{$job.geo.state} ({$job.geo.state_code}), {$job.geo.city} 
		</p>
		<p class="pjob" >Job Id: <span style="float: right;">{$job.id_jobs}</span></p>
		<p class="pjob">Employer: <span style="float: right;">{$customer.firstname} {$customer.lastname}</span></p>
		<p class="pjob">Job Type: <span style="float: right;" >{if $job.jobtype == 1}Part time{else}Full time{/if}</p>
		<p style="font-size: 16px;">Descriton:</p>
		<p class="pjob" style="text-align: justify;font-size: 13px;">{$job.description}</p>
		<p class="pjob">Post Date:<span style="float: right;" >{$job.date_add}</p>
		<p class="pjob">Last Update:<span style="float: right;" >{$job.date_upd}</p>

		
			
		<div>
			<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'appjob')|escape:'html'}?idjob={$job.id_jobs}">
				<input name="apply" type="button" class="apply" value="Apply" onclick="apply();">
		    </a>
		</div>
	
		
		
		
		
	
		
	</div>
	<div class="col-md-5">
		<p style="color: black;font-size: 20px;">Location</p>
		
		<p>Country: {if $job.geo.country == 1}United States{else}Canada{/if}</p>
		<p>State: {$job.geo.state} ({$job.geo.state_code})</p>
		<p>City: {$job.geo.city}</p>
		<address></address>
	</div>
</div>








 <script>
     $(document).ready(function(){
        $("address").each(function(){
             var embed ="<iframe width='425' height='350' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'  src='https://maps.google.com/maps?&amp;q="+encodeURIComponent( '{$job.geo.state}{$job.geo.city}' ) +"&amp;output=embed'></iframe>";
             $(this).html(embed);
       });
     });
 </script> 








<script>
$(document).ready(function(){
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');
});
</script>
<link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet">
<style>
#left_column{
	display: none;
}
.breadcrumb {
    display: none;
}
.gotojbu {
    background: white;
    border: 1px solid;
    padding: 7px 44px;
    margin-top: 25px;
    text-align: center;
    width: 179px;
	display:block;
}
</style>


{if $thanks == 1}
<h2 class="clearfix">Thank you.</h2>

<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobs')|escape:'html'}" class="gotojbu">Go to Jobs.</a>

{else}




<h1 style="font-family: 'Roboto', sans-serif;">{$h1text}</h1>



<div class="row">
	<div class="col-md-12">
		<form method="post" action="#" enctype="multipart/form-data">
		  <div class="form-group">
			<label>Email address:</label>
			<p class="form-control" >{$session->email}</p>
		  </div>
		  <div class="form-group">
			<label>First name:</label>
			<p class="form-control" >{$session->firstname}</p>
		  </div>
		  
		  <div class="form-group">
			<label>Last name:</label>
			<p class="form-control" >{$session->lastname}</p>
		  </div>

		  
		  <div class="form-group" style="width:500px;" >
			  <label>Job Titles:</label>
			  <select  name="jobtitles" class="form-control">
				  <option value="1">Accounting-Finance</option>         
				  <option value="2">Administrative-Clerical</option> 
				  <option value="3">Banking-Mortgage Professionals</option>  
				  <option value="4">Building Construction</option>     
				  <option value="5">Biotech-R&amp;D-Science</option>  
				  <option value="6">Business-Strategic Management</option> 
				  <option value="7">Creative-Design</option>  
				  <option value="8">Customer Support</option>   
				  <option value="9">Editorial Writing</option>    
				  <option value="10">Education</option>  
				  <option value="11">Engineering</option>       
				  <option value="12">Food Services-Hospitality</option> 
				  <option value="13">Human Resources</option>     
				  <option value="14">Installation-Maintenance-Repair</option>  
				  <option value="15">IT-Software Development</option>  
				  <option value="16">Legal</option>   
				  <option value="17">Logistics-Transportation</option>  
				  <option value="18">Manufacturing-Production</option>  
				  <option value="19">Marketing-Product</option>     
				  <option value="20">Medical-Health</option>   
				  <option value="21">Other</option>      
				  <option value="22">Project-Program Management</option>  
				  <option value="23">Quality Assurance-Safety</option>    
				  <option value="24">Real Estate</option>   
				  <option value="25">Sales-Retail</option>      
				  <option value="26">Security-Protective Services</option>   
			  </select>
		  </div>
		  
		  
		  <div class="form-group" style="width:500px;">
				<label>Country:</label>
					<select  name="country"  class="form-control countryselect">
						<option value="1">USA</option>
						<option value="2">CANADA</option>
					</select>
		  </div>
		  
		  
		  <div class="form-group" style="width:500px;" >
			  <label>State:</label>
			  <select  name="state" class="form-control stateselect">
				  {foreach $states item=state}
				  <option  country="{$state.country}" value="{$state.state_code}">{$state.state}</option>
				  {/foreach}
			  </select>
		  </div>
		
		  
		  <div class="form-group" style="width:500px;" >
			  <label>City:</label>
			  <select  name="city" class="form-control cityselect">
				  {foreach $cities item=city}
				  <option value="{$city.city}">{$city.city}</option>
				  {/foreach}
			  </select>
		  </div>
		  
		  
		  <div class="form-group">
			<label>Phone:</label>
			<input type="text" name="phone" class="form-control" >
		  </div>
		  
		  <div class="form-group">
			<label>Zipcode</label>
			<input type="number" name="zipcode" class="form-control" >
		  </div>
		  
		  <div class="form-group">
			<label>Resume:</label>
			<textarea  name="resume" class="form-control" rows="5" ></textarea><br/>
			<p>
				<label>{l s='You can upload your CV here.' mod='medicalmarijuanaexchangedirectory'}</label>
				<input type="file" name="resume_file" accept=".doc, .docx, .pdf">
			</p>
		  </div>
		  
		  
		
		  <button type="submit" name="addappjob" class="btn btn-default">Submit</button>
		</form>
	</div>
</div>




{/if}





{*****
<pre>
{$session|@print_r}
</pre>
<pre>
{$job|@print_r}
</pre>
<pre>
{$customer|@print_r}
</pre>
*****}


<script>
$(document).ready(function(){
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');
});


$('.countryselect').change(function(){
	
	//alert($(this).val());
	if ($(this).val() == 1 ){
		$(".stateselect option[country=CA]").hide();
		$(".stateselect option[country=US]").show();
		$(".stateselect").val($(".stateselect option[country=US]:first").val());
	}else{
		$(".stateselect option[country=US]").hide();
		$(".stateselect option[country=CA]").show();
		$(".stateselect").val($(".stateselect option[country=CA]:first").val());
	}
	$.uniform.update();
	
	 whatstate = $('.stateselect').val();
		$.ajax({
           type: "POST",
           url: '#',
           data:'getcitiesfromstate='+whatstate , // serializes the form's elements.
           success: function(data)
           {
              // alert(data); // show response from the php script.
			   $('.cityselect').empty();
			   $('.cityselect').append(data);
			   $.uniform.update();

           }
         });
});




$('.stateselect').change(function() {
  whatstate = $(this).val();
		$.ajax({
           type: "POST",
           url: '#',
           data:'getcitiesfromstate='+whatstate , // serializes the form's elements.
           success: function(data)
           {
              // alert(data); // show response from the php script.
			   $('.cityselect').empty();
			   $('.cityselect').append(data);
			   $.uniform.update();

           }
         });

return false;
})



</script>
<div class="panel">
	{if isset($header)}{$header}{/if}
	{if isset($nodes)}
	<ul id="{$id|escape:'html':'UTF-8'}" class="cattree tree categories-tree-html">
		{$nodes}
	</ul>
	{/if}
</div>

<style type="text/css">
	.cattree .tree {
		padding: 0 0 0 20px;
	}
</style>
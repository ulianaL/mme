<style>
.panel-heading {
    padding-bottom: 0;
}

.jobs_countries_filters,
.countries_list {
	list-style-type: none;
  columns: 2;
  -webkit-columns: 2;
  -moz-columns: 2;
	column-gap: 32px;
}

img.imgbackjobs {
    width: 100%;
    min-height: 374px;
}
.breadcrumb {
    display: none;
}
#left_column{
	display: none;
}

.formcontentjobs {
    position: relative;
    width: 100%;
    top: 0;
    left: 0;
    padding: 15px;
    color: white;
}
.wrapjobs {
	text-align: center;
    width: 80%;
    margin: 0 auto;
}

.formjobsser {
		width: 90%;
		max-width: 350px;
		margin-bottom: 20px;
		padding: 20px 10px;
		font-size: 18px;
	border-radius: 0;
    background: rgba(255, 255, 255, 0.75);
    color: black;
}
.btnjobssear {
    font-size: 16px;
    padding: 10px 40px;
    background: #078AE9;
    color: #fff;
    text-transform: uppercase;
}

#tab1default ul li {
    width: 100%;
    padding: 7px 0;
    border-bottom: 1px solid #ddd;
}
#tab2default ul li {
    width: 100%;
    padding: 7px 0;
    border-bottom: 1px solid #ddd;
}

.tittleg {
    color: white;
    background: #5bbe8f;
    padding: 12px 17px;
    font-size: 24px;
}
</style>
<div class="custom_breadcrumps font_futura">
	<a class="font_16px" href="/">
		Home
	</a>
	<a class="font_16px">
		Jobs
	</a>
</div>

{if $list == 0}
<div class="jobs__header_form">
	<img  src="{$img_dir}jobs_page.png" class="jobs_header_banner"/>
	<form class="jobs_header_form_content" method="get" action="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobslist')|escape:'html'}">
		<input type="text"  name="keywords" placeholder="Keywords" class="form-control formjobsser font_futura" >
		<input type="text"  name="location" placeholder="Location" class="form-control formjobsser font_futura" >
		<button type="submit"   class="btn btn-default btnjobssear font_futura">Search Jobs</button>
	</form>
</div>


{*<div class="formcontentjobs">
	<div class="wrapjobs">*}
		{*<h1 style="margin-bottom:50px">Where The Cannabis Industry & Job Seekers Connect</h1>*}
		{*<form method="post" action="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobslist')|escape:'html'}">
		  <div class="row">
			  <div class="form-group col-md-6"> 
					<input type="text"  name="keywords" placeholder="keywords" class="form-control formjobsser" >
			  </div>
			  
			  <div class="form-group col-md-6">
					<input type="text"  name="location" placeholder="Location" class="form-control formjobsser" >
			  </div>
		  </div>
		  <div class="row">
			  <div class="form-group col-md-12">
			  <button type="submit"   class="btn btn-default btnjobssear">Search Jobs</button>
			  </div>
		  </div>
		</form>

	</div><!--end wrapjobs--->
</div><!--end formcontentjobs--->*}
<div class="browse_jobs_container">
	<h2 class="font_baskervile">Browse jobs</h2>
	<div class="jobs_tabs_container">
		<ul class="job_tabs_header nav nav-tabs">
			<li class="active">
				<a class="font_futura_book" href="#tab1default" data-toggle="tab">Jobs by State</a>
			</li>     
			<li>
				<a class="font_futura_book" href="#tab2default" data-toggle="tab">Jobs by Industry</a>
			</li>
		</ul>
		<div class="tab-content" style="padding: 0;margin: 0;">
			<div class="tab-pane fade in active" id="tab1default">
				<div>
					<div class="jobs_countries_filters">
						<p onclick="changeliststatestojobf($(this))" data-type="1" class="jobs_countries_filters_button active font_futura_book" >Usa</a>
						<p onclick="changeliststatestojobf($(this))" data-type="2" class="jobs_countries_filters_button font_futura_book" >Canada</a>
					</div>
					<div class="countries_list">
						{foreach $statestolist item=state}
							<li class="font_futura_book"  licountry="{$state.country}" >
								<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobslist')|escape:'html'}?state={$state.state_code}" title="{$state.state} ({$state.state_code})">
									{$state.state} ({$state.state_code})
								</a> 
								({$state.count})
							</li>
						{/foreach}
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tab2default">
				<div class="countries_list"> 
					{foreach $indus item=ind}
						<li class="font_futura_book">
							<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobslist')|escape:'html'}?industry={$ind.key}" >
								{$ind.name} ({$ind.count})
							</a>
						</li>
					{/foreach}
				</div>
			</div>
		</div>
	</div>
</div>


<!-- add job -->
{elseif  $list == 3}
	{if $error_bu ==1}
          <p class="alert alert-danger">
			You need a Business Account to add job.
			</p>


	{else}
		<h2 class="tittleg">{if isset($edit)}Edit job{else}Add job{/if}</h2>

		<form method="post" action="#">
		 
		  <div class="form-group">
			<label for="email">Job tittle</label>
			<input  type="text"  value="{if isset($edit.0.tittle)}{$edit.0.tittle}{/if}" name="tittle" class="form-control"  required>
		  </div>
		  <div class="form-group">
			<label for="email">Job description</label>
			<textarea   rows="10" name="description" class="form-control" >{if isset($edit.0.description)}{$edit.0.description}{/if}</textarea>
		  </div>
		  
		  
		  
		   <div class="form-group" style="max-width:500px;" >
					<label>Country:</label>
					<select  name="country"  class="form-control countryselect">
						<option value="1">USA</option>
						<option value="2">CANADA</option>
					</select>
		   </div>
		  
		  
		  
		  <div class="spinnerloadcity"  style="opacity: 0;">
				<i class="fa fa-spinner fa-spin " aria-hidden="true"></i>
		  </div>
		  <div class="form-group loadcity" style="width:500px;">
			 <label>State</label>
			 <select name="state" class="form-control stateselect">
			  {foreach $states item=state}
			  <option value="{$state.state_code}"  country="{$state.country}"  {if isset($edit.0.state)}{if $state.state_code == $edit.0.state}selected="selected"{/if}{/if}>{$state.state} ({$state.state_code})</option>
			  {/foreach}
			</select>
		  </div>

		  <div class="spinnerloadcity"  style="opacity: 0;">
				<i class="fa fa-spinner fa-spin " aria-hidden="true"></i>
		  </div>
		  <div class="form-group loadcity" style="width:500px;">
			 <label>City</label>
			 <select name="city" class="form-control cityselect">
			  {foreach $city item=cit}
			  <option  class="state_{$cit.state_code} citiess" value="{$cit.city_id}"   {if isset($edit.0.city)}{if $cit.city_id == $edit.0.city}selected="selected"{/if}{/if}>{$cit.city}</option>
			  {/foreach}
			</select>
		  </div> 
		  

		   <div class="form-group" style="width:500px;">
			 <label>Industry</label>
			 <select name="industry" class="form-control">	
				<option value="5" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==5}selected="selected"{/if}{/if}>Admin - Clerical</option>
				<option value="7" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==7}selected="selected"{/if}{/if}>Design</option>
				 <option value="9" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==9}selected="selected"{/if}{/if}>Education</option>
				  <option value="10" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==10}selected="selected"{/if}{/if}>Executive</option> 
				  <option value="8" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==8}selected="selected"{/if}{/if}>Finance</option>
				   <option value="6"  {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==6}selected="selected"{/if}{/if}>Information Technology</option>
				   <option value="4" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==4}selected="selected"{/if}{/if}>Manufacturing</option>
				   <option value="3" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==3}selected="selected"{/if}{/if}>Marketing</option>
				<option value="1" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==1}selected="selected"{/if}{/if}>Other</option>
				 <option value="2" {if isset($edit.0.insdustry)}{if $edit.0.insdustry ==2}selected="selected"{/if}{/if}>Sales</option>
				
				 
				
				 
				
				
				
			</select>
		  </div>
		  
		  <div class="form-group" style="width:500px;">
			 <label>Job Type</label>
			 <select name="jobtype" class="form-control ">
			  <option value="1" {if isset($edit.0.jobtype)}{if $edit.0.jobtype ==1}selected="selected"{/if}{/if}>Part time</option>
			  <option value="2" {if isset($edit.0.jobtype)}{if $edit.0.jobtype ==2}selected="selected"{/if}{/if}>Full time</option>
			</select>
		  </div>
		  <p class="alert alert-danger alert-danger-jobs" style="display: none;">You do not have a subscription</p>
		  <button type="submit" name="subaddjob" class="btn btn-default {if !$active_subscription}no_subscriptions_jobs{/if}">Submit</button>
		</form>






		<!-- end  add job -->
    {/if}
{else}




{/if}












<script>
//changeliststatestojobf

$(document).ready(function(){
	$("li[licountry=US]").show();
	$("li[licountry=CA]").hide();
})




{* /*function changeliststatestojobf(vv){
	if(vv == 1){
		$('.counttttt2').removeClass('active');
		$('.counttttt1').addClass('active');
		$("li[licountry=US]").show();
		$("li[licountry=CA]").hide();
	}else{
		$('.counttttt1').removeClass('active');
		$('.counttttt2').addClass('active');
		$("li[licountry=CA]").show();
		$("li[licountry=US]").hide();
	}
}*/ *}

function changeliststatestojobf(el) {
	if (!el.hasClass('active')) {
		$('.jobs_countries_filters_button').removeClass('active');
		el.addClass('active');
		var type = el.attr('data-type');
		if (type == 1) {
			$("li[licountry=US]").show().css('display','inline-block');
			$("li[licountry=CA]").hide();
		} else {
			$("li[licountry=CA]").show().css('display','inline-block');
			$("li[licountry=US]").hide();
		}
	}

	{* /*if(vv == 1){
		$('.counttttt2').removeClass('active');
		$('.counttttt1').addClass('active');
		$("li[licountry=US]").show();
		$("li[licountry=CA]").hide();
	}else{
		$('.counttttt1').removeClass('active');
		$('.counttttt2').addClass('active');
		$("li[licountry=CA]").show();
		$("li[licountry=US]").hide();
	} */*}
}










// country from db...

{if isset($edit.0.country)}
$(document).ready(function(){
	{if $edit.0.country == 1}
		$(".stateselect option[country=CA]").hide();
		$(".stateselect option[country=US]").show();
		$('.countryselect').val(1);
		
	{else}
		$(".stateselect option[country=US]").hide();
		$(".stateselect option[country=CA]").show();
		$('.countryselect').val(2);
	{/if}
	$.uniform.update();
});
{/if}


//state and city 
$('.countryselect').change(function ttttttt(){

	//alert($(this).val());
	if ($(this).val() == 1 ){
		$(".stateselect option[country=CA]").hide();
		$(".stateselect option[country=US]").show();
		$(".stateselect").val($(".stateselect option[country=US]:first").val());
	}else{
		$(".stateselect option[country=US]").hide();
		$(".stateselect option[country=CA]").show();
		$(".stateselect").val($(".stateselect option[country=CA]:first").val());
	}
	
		
		$('.cityselect').empty();
		$('.loadcity').fadeTo( "slow" , 0.3);
		$('.spinnerloadcity').fadeTo( "slow" , 1);
		$.uniform.update();
		whatstate = $(".stateselect").val();
		$.ajax({
			type: "POST",
			url: '#',
			data:'getcitiesfromstate='+whatstate+'&activecity=0' , // serializes the form's elements.
			success: function(data)
				{
			    	//alert(data); // show response from the php script.
					$('.cityselect').empty();
					$('.cityselect').append(data);	
					$.uniform.update();
					$('.loadcity').fadeTo( "slow" , 1);
					$('.spinnerloadcity').fadeTo( "slow" , 0);	
				}
		});
});






$(document).ready(function(){
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');

activecity = '';
{if isset($edit.0.city)}
activecity = {$edit.0.city};
{/if}

$('.stateselect').change(function() {
        $('.loadcity').fadeTo( "slow" , 0.3);
		$('.spinnerloadcity').fadeTo( "slow" , 1);
		whatstate = $(this).val();
		$.ajax({
           type: "POST",
           url: '#',
           data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
           success: function(data)
           {
               //alert(data); // show response from the php script.
			   $('.cityselect').empty();
			   $('.cityselect').append(data);
			   $.uniform.update();
			   $('.loadcity').fadeTo( "slow" , 1);
			   $('.spinnerloadcity').fadeTo( "slow" , 0);	
           }
         });

return false;
})

        $('.loadcity').fadeTo( "slow" , 0.3);
		$('.spinnerloadcity').fadeTo( "slow" , 1);
        whatstate = $('.stateselect').val();
		$.ajax({
           type: "POST",
           url: '#',
           data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
           success: function(data)
           {
               //alert(data); // show response from the php script.
			   $('.cityselect').empty();
			   $('.cityselect').append(data);
			   $.uniform.update();
			   $('.loadcity').fadeTo( "slow" , 1);
			   $('.spinnerloadcity').fadeTo( "slow" , 0);	
           }
         });



})
 

</script>

{* Softsprint Uliana *}
<script type="text/javascript">
	$(".no_subscriptions_jobs").off("click").on("click", function(e) {
  	    e.preventDefault();
  		$(".alert-danger-jobs").show();
		return false;
	});
</script>
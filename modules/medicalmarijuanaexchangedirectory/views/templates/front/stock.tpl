
<div class="stock_search_container">        
    <div class="despensaries_search_input_parent">
        <img src="/themes/default-bootstrap/img/header-search.png">
        <input type="text" id="s-product-name" autocomplete="off" placeholder="{l s='Search Symbol' mod='medicalmarijuanaexchangedirectory'}" onkeyup="searchProducts(1);">
    </div>
</div>
<div id="symbols_tabel">
	<span id="not_found">No matching records found</span>
	<span id="loading"></span>
	<iframe id='st_c0b82d488b814ee7882627a227052610' frameBorder='0' scrolling='no' width='100%' height="703" src='https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols={implode(';', $all_symbols[0])}&includeLogo=false&includeVolume=true&palette=Financial-Light&showBorderAndTitle=false&linkUrl=https://mme.world/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=100%'></iframe>
</div>

<ul id="pagination">
	{foreach from=$all_symbols key=_k item=_s}
		{if $_k < 7}
			{if $_k == 0}
				<li id="{$_k}" class="current" onclick="showPage(this, {$_k|intval});">{$_k+1}</li>
			{else}
				<li id="{$_k}" onclick="showPage(this, {$_k|intval});">{$_k+1}</li>
			{/if}
		{elseif $_k == 16}
			<span>...</span><li id="{$_k}" onclick="showPage(this, {$_k|intval});">{$_k+1}</li>
		{/if}	
	{/foreach}
</ul>

<script type="text/javascript">
	var all_symbols = {$all_symbols|json_encode};
	var _symbols = Object.assign([], all_symbols);
	// all_symbols[1].join(';');
	var page_symbols = '';
	function showPage(el, page){

		$('#pagination li').removeClass('current');
		var id = $(el).attr('id');
	
		change_pagination(id);

		$(el).addClass('current');
		if (typeof _symbols[page]=='undefined') {
			$('iframe').attr('src', '');
			$('iframe').attr('height', '0');
			$('#not_found').show();
			return false;
		}else{
			$('#not_found').hide();
		}
		page_symbols = _symbols[page].join(';');

		var height = _symbols[page].length * 27 + 28 ;
		$('iframe').attr('height', height);
		$('iframe').attr('src', 'https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols='+page_symbols+'&includeLogo=false&includeVolume=true&palette=Financial-Light&showBorderAndTitle=false&linkUrl=https://mme.world/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=100%');
	}

	 function searchProducts(page) {
	 	var query = $("#s-product-name").val().trim().toLowerCase();
	 	_symbols = [];
	 	for (var i = 0; i < all_symbols.length; i++) {

	 		for (var j = 0; j < all_symbols[i].length; j++) {
	 			if ((all_symbols[i][j].toLowerCase().indexOf(query) === 0 && all_symbols[i][j].indexOf(':') == -1) ||
	 				all_symbols[i][j].toLowerCase().indexOf(':' + query) > 0) {
	 				_symbols.push(all_symbols[i][j]);
	 			}
	 		}
	 	}

	 	
	 	var i,j,temparray = [], chunk = 25, c=0;
		for (i=0,j=_symbols.length; i<j; i+=chunk) {
		    temparray[c] = _symbols.slice(i,i+chunk);
		    c++;
		    // do whatever
		}
		_symbols = Object.assign([], temparray);
		updatePagination ();
	 	showPage('#0', 0);
	 }

	 function updatePagination (){
	 	var pages = '';
	 	for (var g = 0; g < _symbols.length; g++) {

	 		pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
	 	}

	 	$('#pagination').html(pages);
	 }

	 function change_pagination(current){
	 	var pages = '';
	 	for (var g = 0; g < _symbols.length; g++) {
	 		if(current >= 6 && current <= 10){
	 			if (g == 0) {
		 			pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li><span>...</span>';
		 		}else if (g >= 5 && g <= 11 ){
		 			pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
		 		}else if (g ==  _symbols.length -1){
		 			pages += '<span>...</span><li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
		 		}
	 		}else if (current <= 5){
	 			if(g <= 6){
	 				pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
	 			}else if (g ==  _symbols.length -1){
	 				pages += '<span>...</span><li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
	 			}
	 		}else if (current >= 11){
	 			if(g == 0){
	 				pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li><span>...</span>';
	 			}else if (g >= 10){
	 				pages += '<li id="'+g+'" onclick="showPage(this, '+g+');">'+(g+1)+'</li>';
	 			}
	 		}
	 	}

	 	$('#pagination').html(pages);

	 	$('#'+current).addClass('current');
	 }

	 function preloader() {
        return '<img style="display: flex; margin: 20px auto; width: 150px;" src="/themes/default-bootstrap/img/marijuana__preloader.gif" />';
     }
</script>
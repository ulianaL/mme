{capture name=path}
    <a href="#BACCOUNT">
        {l s='Business Account'}
    </a>
    
{/capture}


<h1 id="BACCOUNT" style="margin-bottom: 25px;text-align: center;">Bussiness Account</h1>

<div class="container">
        <div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="" method="post" role="form" style="display: block;">
									<h2   style="    margin-top: 5px;
										text-align: center;
										margin-bottom: 10px;
										color: black;">Login</h2>
									
									<div class="form-group">
										<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="password" name="passwd" id="passwd" tabindex="2" class="form-control">
									</div>
									
									<input type="hidden" name="SubmitLogin">
									<input type="hidden" name="bussiness"  value="1">
									
									
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									
									{if isset($authentification_error)}
										{foreach $authentification_error   item=erro}
											<div class="alert alert-danger">
											{$erro}
											</div>
										{/foreach}	
									{/if}
									
								</form>
								<form id="register-form" action="" method="post" role="form" style="display: none;">
									<h2   style="    margin-top: 5px;
										text-align: center;
										margin-bottom: 10px;
										color: black;">Create a Bussiness Account</h2>
									
									
									
									<div class="form-group">
										<input type="text" name="firstname" id="firstname " tabindex="1" class="form-control" placeholder="First Name" value="" required="required">
									</div>
									
									<div class="form-group">
										<input type="text" name="lastname" id="lastname " tabindex="1" class="form-control" placeholder="Last Name" value="" required="required">
									</div>
									
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required="required">
									</div>
									<div class="form-group">
										<input type="password" name="passwd" id="passwd" tabindex="2" class="form-control" placeholder="Password" required="required">
									</div>

									<div class="form-group">
					                    <label>
					                        {l s='Date of Birth'}
					                    </label>
					                    <div class="row">
					                        <div class="col-xs-4">
					                            <select name="days" id="days">
					                                <option value="">Day</option>
					                                {foreach from=$days item=v}
					                                    <option value="{if strlen($v) == 1}0{/if}{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
					                                {/foreach}
					                            </select>
					                        </div>
					                        <div class="col-xs-4">
					                            <select id="months" name="months">
					                                <option value="">Month</option>
					                                {foreach from=$months key=k item=v}
					                                    <option value="{if strlen($k) == 1}0{/if}{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v}&nbsp;</option>
					                                {/foreach}
					                            </select>
					                        </div>
					                        <div class="col-xs-4">
					                            <select id="years" name="years">
					                                <option value="">Year</option>
					                                {foreach from=$years item=v}
					                                    <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
					                                {/foreach}
					                            </select>
					                        </div>
					                    </div>
					                </div>

									{* <div class="form-group">
										<input type="text" name="date_birthday" id="date_birthday" tabindex="3" class="form-control" placeholder="Date of Birth" value="" required="required">
									</div> *}

									<div class="form-group">
										<select name="id_country" required="required">
											<option value="4">Canada</option>
											<option value="21">United States</option>
										</select>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
									<input type="hidden" name="submitAccount">
									<input type="hidden" name="frombussi">
									{if isset($account_error)}
										{foreach $account_error   item=erro}
											<div class="alert alert-danger">
											{$erro}
											</div>
										{/foreach}	
									{/if}
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
<script>
$(function() {

    $('#login-form-link').click(function(e) {
    	$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	// $("#date_birthday").datepicker({
 //        // prevText: "",
 //        // nextText: "",
 //        // dateFormat: "yy-mm-dd",
 //        autoclose: true,
 //        format: {
 //        	toDisplay: function (date, format, language) {
	//             // return moment(date).format('YYYY-MM-DD HH:mm:ss');
	//             return moment(date).format('MM/DD/YYYY');
	//         },
 //        	toValue: function (date, format, language) {
 //        		console.log(moment(date).format('YYYY-MM-DD HH:mm:ss'));
 //        		return date;
 //        	}
 //        }
 //    });

    $("#register-form").on('submit', function (e) {
    	if ($("#days").val() == '' || $("#months").val() == '' || $("#years").val() == '') {
    		e.preventDefault();
    		alert('Please enter your date of birth');
    		return false;
    	}
    	var end = moment($("#years").val() + '-' + $("#months").val() + '-' + $("#days").val());
    	try {
			end._d.toISOString();
		} catch (ex) {
			e.preventDefault();
    		alert('Please enter correct date of birth');
    		return false;
		}
    	
    	var duration = moment.duration(moment().diff(end));
    	if (duration.asYears() < 18) {
    		e.preventDefault();
    		alert('You are under 18 years old, you cannot register');
    		return false;
    	}
    });
});



{if isset($smarty.post.submitAccount)}
   $(document).ready(function(){
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$('#register-form-link').addClass('active');
		$(this).addClass('active');
	})
{/if}
</script>




<style>
#register-form select {
	    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff!important;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    height: 45px;
    border: 1px solid #ddd;
    font-size: 16px;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
    border: 1px solid #333!important;
    font-weight: 800;
}
.panel-login {
    border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

input#register-submit {
    color: #333!important;
    border: 1px solid #333!important;
}

.panel-login input[type="text"], .panel-login input[type="email"], .panel-login input[type="password"] {
  
    border: 1px solid #333!important;
  
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer;
    border: 1px solid #333;
    color: #333;
}


.panel-login {
    border-color: #333;
}

.panel-login>.panel-heading {
    color: #00415d;
    background-color: #fff;
    border-color: #333;
    text-align: center;
}
.panel-login>.panel-heading {

    padding-bottom: 0;
}


.btn-login:hover, .btn-login:focus {
    color: black;
    background-color: #53A3CD;
    border-color: #53A3CD;
}
</style>

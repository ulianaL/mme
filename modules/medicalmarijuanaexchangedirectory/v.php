<?php

if (!class_exists('Tools')) {
    require_once dirname(__FILE__).'/../../classes/Tools.php';

    class Tools extends ToolsCore {}
}

function getMimeType($ext) {
    $ext = strtolower($ext);
    switch ($ext) {
        case 'webm':
            return 'video/webm';
            break;

        case 'ogg':
            return 'video/ogg';
            break;
        
        default:
            return 'video/mp4';
            break;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Video <?php echo Tools::getValue('id', '000'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        

        <!-- Docs styles -->
        <link rel="stylesheet" href="https://mme.world/modules/medicalmarijuanaexchangedirectory/plyr/demo.css" />

        <!-- Preload -->
        <link
            rel="preload"
            as="font"
            crossorigin
            type="font/woff2"
            href="https://cdn.plyr.io/static/fonts/gordita-medium.woff2"
        />
        <link
            rel="preload"
            as="font"
            crossorigin
            type="font/woff2"
            href="https://cdn.plyr.io/static/fonts/gordita-bold.woff2"
        />
    </head>

    <body>
        <div class="grid">
            <main>
                <div id="container">
                    <!-- poster="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg" -->
                    <!-- playsinline -->
                    <!-- crossorigin -->
                    <!-- autoplay -->
                    <!-- loop -->
                    <!-- muted -->
                    <!-- controls -->
                    <video
                        <?php echo Tools::getValue('c', '1') === '1' ? ' controls ' : ' controls '; ?>
                        playsinline

                        <?php echo Tools::getValue('loop', '0') === '1' ? ' loop ' : ''; ?>
                        <?php echo Tools::getValue('m', Tools::getValue('p', '0')) === '1' ? ' muted ' : ''; ?>
                        id="player"
                    >
                        <!-- Video files -->
                        <!-- <source
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4"
                            type="video/mp4"
                            size="576"
                        /> -->
                        <!-- src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-720p.mp4" -->
                        <source                            
                            src="http://162.249.4.188/img-adv/<?php echo Tools::getValue('n') ?>"
                            type="<?php echo getMimeType(pathinfo(Tools::getValue('n'), PATHINFO_EXTENSION)); ?>"
                        />
                        <!-- <source
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-1080p.mp4"
                            type="video/mp4"
                            size="1080"
                        /> -->

                        <!-- Caption files -->
                        <!-- <track
                            kind="captions"
                            label="English"
                            srclang="en"
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt"
                            default
                        />
                        <track
                            kind="captions"
                            label="Français"
                            srclang="fr"
                            src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.fr.vtt"
                        /> -->

                        <!-- Fallback for browsers that don't support the <video> element -->
                        <!-- <a href="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" download>Download</a> -->
                    </video>
                </div>

                
            </main>
        </div>

        <style type="text/css">
            .plyr__controls, button[data-plyr="play"]{
                <?php echo Tools::getValue('c', '1') === '0' ? 'display: none' : ''; ?>
            }
            button[data-plyr="pip"], button[data-plyr="settings"] {
                display: none !important;
            }
        </style>

        <script type="text/javascript">
            var fDomLoaded = function(){
                <?php if(Tools::getValue('p', '0') === '1') : ?>
                    setTimeout(function(){
                        player.muted = true;
                        // document.querySelector('button[data-plyr="play"]').click();
                        player.play();
                        <?php //echo Tools::getValue('m', '0') === '0' ? 'player.muted = false;' : '' ?>
                         // enable volume
                         <?php // echo Tools::getValue('m', '0') === '0' ? 'document.querySelector(\'button[data-plyr="mute"]\').click();' : '' ?>
                    }, 2000);
                <?php endif; ?>
                <?php //echo Tools::getValue('p', '0') === '1' ? 'setTimeout(function(){document.querySelector(\'button[data-plyr="play"]\').click();}, 2000);' : '' ?>
                    // document.querySelector('button[data-plyr="mute"]').click();
                    // document.querySelector('button[data-plyr="mute"]').click();
                <?php // echo Tools::getValue('m', '0') === '1' ? 'document.querySelector(\'button[data-plyr="mute"]\').click();' : '' ?>
                player.muted = document.getElementById('player').hasAttribute('muted');
                <?php // echo Tools::getValue('m', '0') === '1' ? 'document.querySelector(\'button[data-plyr="mute"]\').click();' : '' ?>
            };
        </script>

        <!-- Polyfills -->
        <script
            src="https://cdn.polyfill.io/v2/polyfill.min.js?features=es6,Array.prototype.includes,CustomEvent,Object.entries,Object.values,URL"
            crossorigin="anonymous"
        ></script>

        <!-- Plyr core script -->
        <script src="https://mme.world/modules/medicalmarijuanaexchangedirectory/plyr/plyr.js" crossorigin="anonymous"></script>

        <!-- Sharing libary (https://shr.one) -->
        <script src="https://cdn.shr.one/1.0.1/shr.js" crossorigin="anonymous"></script>

        <!-- Rangetouch to fix <input type="range"> on touch devices (see https://rangetouch.com) -->
        <script src="https://cdn.rangetouch.com/1.0.1/rangetouch.js" async crossorigin="anonymous"></script>

        <!-- Docs script -->
        <script src="https://mme.world/modules/medicalmarijuanaexchangedirectory/plyr/demo.js?2" crossorigin="anonymous"></script>

    </body>
</html>

<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2015 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(dirname(__FILE__) . '/classes/MedicalmarijuanaexchangedirectoryChatid.php');
require_once(dirname(__FILE__) . '/classes/MedicalmarijuanaexchangedirectoryPrem.php');


class Medicalmarijuanaexchangedirectory extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'medicalmarijuanaexchangedirectory';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'luis leitao';
        $this->need_instance = 0;
        $this->linkdosub = "";

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('medicalmarijuanaexchangedirectory ');
        $this->description = $this->l('medicalmarijuanaexchangedirectory ');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MEDICALMARIJUANAEXCHANGEDIRECTORY_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('leftColumn') &&
            $this->registerHook('actionOrderStatusPostUpdate') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MEDICALMARIJUANAEXCHANGEDIRECTORY_LIVE_MODE');

        return parent::uninstall();
    }


    /**
     * Load the configuration form
     */
    public function getContent()
    {

        if (isset($_GET['deletee'])) {
            //print $_GET['deletee'];

            Db::getInstance()->delete('directory', 'id_directory = ' . $_GET['deletee'], 1);
        }


        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMedicalmarijuanaexchangedirectoryModule')) == true) {
            $this->postProcess();
        }


        if (isset($_POST["submitaddfree"])) {

            $name = $_POST['name'];
            $cat = $_POST['cat'];
            $about = $_POST['about'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $website = $_POST['website'];
            $twitter = $_POST['twitter'];
            //unset($_POST);

            if ($_FILES["fileToUpload"]["name"] != '') {
                ############UPLOAD#######################
                $target_dir = _PS_MODULE_DIR_ . "medicalmarijuanaexchangedirectory/upload/";
                $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uniquename = '';


                $uploadOk = 1;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check if image file is a actual image or fake image
                if (isset($_POST["submit"])) {
                    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                    if ($check !== false) {
                        //echo "File is an image - " . $check["mime"] . ".";
                        $uploadOk = 1;
                    } else {
                        //	echo "File is not an image.";
                        $uploadOk = 0;
                    }
                }


                // Check file size
                if ($_FILES["fileToUpload"]["size"] > 500000) {

                    //	echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
                // Allow certain file formats
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                }
                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    //echo "Sorry, your file was not uploaded.";
                    // if everything is ok, try to upload file
                } else {
                    $uniquename = time() . '.jpg';
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . $uniquename)) {
                        //  echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        //echo "Sorry, there was an error uploading your file.";
                    }
                }
                ############END UPLOAD#######################
            }


            ############UPLOAD  GALERIE#######################
            $picnames = array();
            for ($i = 1; $i <= 5; $i++) {
                if ($_FILES["image" . $i]["name"] != '') {

                    $target_dir = _PS_MODULE_DIR_ . "/medicalmarijuanaexchangedirectory/gal/";


                    $target_file = $target_dir . basename($_FILES["image" . $i]["name"]);
                    $picnames[$i] = '';


                    $uploadOk = 1;
                    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                    // Check if image file is a actual image or fake image
                    if (isset($_POST["submit"])) {
                        $check = getimagesize($_FILES["image" . $i]["tmp_name"]);
                        if ($check !== false) {
                            // echo "File is an image - " . $check["mime"] . ".";
                            $uploadOk = 1;
                        } else {
                            //echo "File is not an image.";
                            $uploadOk = 0;
                        }
                    }
                    /*
                    // Check if file already exists
                    if (file_exists($target_file)) {
                        echo "Sorry, file already exists.";
                        $uploadOk = 0;
                    }*/

                    // Check file size
                    if ($_FILES["image" . $i]["size"] > 500000) {
                        //	echo "Sorry, your file is too large.";
                        $uploadOk = 0;
                    }
                    // Allow certain file formats
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {
                        //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOk = 0;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                        // if everything is ok, try to upload file
                    } else {
                        $picnames[$i] = time() . $i . '.jpg';
                        if (move_uploaded_file($_FILES["image" . $i]["tmp_name"], $target_dir . $picnames[$i])) {
                            //  echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                        } else {
                            echo "Sorry, there was an error uploading your file.";
                        }
                    }
                }

            }
            ############END UPLOAD#######################


            $extras = "";

            //logo
            if ($_FILES["fileToUpload"]["name"] != '') {
                $extras .= " logo ='" . pSQL($uniquename) . "',";
            }

            //pictures
            for ($i = 1; $i <= 5; $i++) {
                if ($_FILES["image" . $i]["name"] != '') {
                    $extras .= " image" . $i . " ='" . pSQL($picnames[$i]) . "',";
                }
            }


            $string = $_POST['videourl'];
            $search = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
            $replace = "youtube.com/embed/$1";
            $vvideo = preg_replace($search, $replace, $string);


            $sql = "UPDATE " . _DB_PREFIX_ . "directory 
												SET name ='" . pSQL($name) . "',
												cat ='" . pSQL($cat) . "', 
												" . $extras . "
												about ='" . pSQL($about) . "', 
												bio ='" . pSQL($_POST['bio']) . "',
												email ='" . pSQL($_POST['email']) . "',
												phone ='" . pSQL($_POST['phone']) . "',
												address ='" . pSQL($_POST['address']) . "',
												website ='" . pSQL($_POST['website']) . "',
												twitter ='" . pSQL($_POST['twitter']) . "',
												facebook ='" . pSQL($_POST['facebook']) . "',
												videourl ='" . pSQL($vvideo) . "',
												youtube ='" . pSQL($_POST['youtube']) . "',
												instagram ='" . pSQL($_POST['instagram']) . "',
												pinterest ='" . pSQL($_POST['pinterest']) . "',
												lat ='" . pSQL($_POST['lat']) . "',
												lng ='" . pSQL($_POST['lng']) . "'
												
												
												WHERE id_directory =" . pSQL($_POST['idtoupdate']);

            //print $sql;


            if (!Db::getInstance()->execute($sql))
                die('error!');

            //print_r ($picnames);

            /*
                Db::getInstance()->insert('directory', array(

                    'name'      => pSQL($name),
                    'cat' => (int)$cat,
                    'about'      => pSQL($about),
                    'email'      => pSQL($email),
                    'phone'      => pSQL($phone),
                    'website'      => pSQL($website),
                    'logo'      => pSQL($uniquename),
                    'image1'      => pSQL($picnames[1]),
                    'image2'      => pSQL($picnames[2]),
                    'image3'      => pSQL($picnames[3]),
                    'image4'      => pSQL($picnames[4]),
                    'image5'      => pSQL($picnames[5]),
                    'extra'      => 0,
                    'review'      => 0,
                    'pay'      => 1,
                    'bio'      => pSQL($_POST['bio']),
                    'youtube'      => pSQL($_POST['youtube']),
                    'facebook'      => pSQL($_POST['facebook']),
                    'instagram'      => pSQL($_POST['instagram']),
                    'pinterest'      => pSQL($_POST['pinterest']),
                    'twitter'      => pSQL($twitter),

                ));
                */

            unset($_POST["submit"]);
        }


        if (isset($_GET['updatee'])) {

            $update = $_GET['updatee'];

            $sql = 'UPDATE ' . _DB_PREFIX_ . 'directory SET review=' . $update
                . ' WHERE id_directory=' . $_GET['idd'];


            if (!Db::getInstance()->execute($sql))
                die('error!');

        }

        if (isset($_GET['edit'])) {
            $edit = 1;

            $sql = "SELECT * FROM " . _DB_PREFIX_ . "directory 
		WHERE id_directory = " . pSQL($_GET['edit']);

            $row = Db::getInstance()->getRow($sql);

            //added new rows SoftSprint Uliana

            $sql = "SELECT state FROM cities WHERE state_code = '" . $row['state'] . "'";
            $state = Db::getInstance()->getValue($sql);

            $sql = "SELECT city FROM cities WHERE city_id = " . $row['city'];
            $city = Db::getInstance()->getValue($sql);

            $toedit = $_GET['edit'];
        } else {
            $row = array();
            $edit = 0;
            $toedit = 0;
            $state = '';
            $city = '';
        }


        if (isset($_GET['p'])) {

            $page_number = $_GET['p'];
            $get_p = 1;
        } else {

            $page_number = 1;
            $get_p = 0;

        }

        $sort = 0;
        $sort_a = 0;
        $status = 0;
        $status_a = 0;
        if (isset($_GET['sort'])) {
            $sort = 1;

            if ($_GET['sort'] == 'name_a') {
                $sort_a = 1;
            }
        }

        if (isset($_GET['status'])) {
            $status = 1;

            if ($_GET['status'] == 0) {
                $status_a = 1;
            }
        }


        $count_all = 0;
        $countArtOnStep = 20;
        $start_from = $countArtOnStep * ($page_number - 1);
        $limit = ' LIMIT ' . $start_from . ',' . $countArtOnStep;

        $sql2 = 'SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'directory';
        $count_all = Db::getInstance()->getValue($sql2);

        $where = '';
        if (isset($_GET['letter']) && $_GET['letter'] != "") {
            $where = 'WHERE name LIKE "'.$_GET['letter'].'%"';
        }
        if (isset($_GET['usa']) && $_GET['usa'] == 1 && isset($_GET['canada']) && $_GET['canada'] == 1) {
            if ($where == '') {
                $where = 'WHERE (country = 1 OR country = 2)';
            }else{
                $where = $where.' AND (country = 1 OR country = 2)';
            }    
        }elseif (isset($_GET['usa']) && $_GET['usa'] == 1) {
            if ($where == '') {
                $where = 'WHERE country = 1';
            }else{
                $where = $where.' AND country = 1';
            }  
        }elseif (isset($_GET['canada']) && $_GET['canada'] == 1) {
            if ($where == '') {
                $where = 'WHERE country = 2';
            }else{
                $where = $where.' AND country = 2';
            } 
        }

        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory '.$where.' ORDER BY id_directory DESC ' . $limit;
        // var_dump($sql);
        // exit();
        // if (isset($_GET['sort'])) {
        //     if ($_GET['sort'] == 'name_a') {
        //         $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory ORDER BY IF(name RLIKE "^[a-z]", 1, 2), name ' . $limit;
        //     } else {
        //         $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory ORDER BY name DESC ' . $limit;
        //     }

        // } elseif (isset($_GET['status'])) {

        //     if ($_GET['status'] == 0) {
        //         $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory ORDER BY review ASC' . $limit;
        //     } else {
        //         $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory ORDER BY review DESC ' . $limit;
        //     }

        // } else {
        //     $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'directory ORDER BY id_directory DESC ' . $limit;
        // }
        $results = Db::getInstance()->ExecuteS($sql);


        $i = 0;
        foreach ($results as $result) {


            $results2[$i] = $result;
            $results2[$i]['doorderandpay'] = $this->doorderandpay(9, $result['customer']);
            $custobj = new Customer($result['customer']);
            $results2[$i]['user_name'] = $custobj->firstname . ' ' . $custobj->lastname;
            $i++;

        }


        /*
           print "<pre>";
           print_r ($results2);
           print "</pre>";

         */

        $module_link = Tools::getHttpHost(true) . __PS_BASE_URI__;


        $this->linkdosub = "/admin123/index.php?controller=AdminModules&token="
            . Tools::getAdminTokenLite('AdminModules') .
            "&configure=medicalmarijuanaexchangedirectory&tab_module=administration";

        $linkb = new Link();
        $tokenmaislink = '/admin123/' . $linkb->getAdminLink('AdminModules');


        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('module_link', $module_link);
        $this->context->smarty->assign('edit', $edit);
        $this->context->smarty->assign('row', $row);
        $this->context->smarty->assign('state', $state);
        $this->context->smarty->assign('city', $city);
        $this->context->smarty->assign('toedit', $toedit);
        $this->context->smarty->assign('results', $results2);
        $this->context->smarty->assign('linkdosub', $this->linkdosub);
        $this->context->smarty->assign('token', $tokenmaislink);

        $this->context->smarty->assign('page_number', $page_number);
        $this->context->smarty->assign('get_p', $get_p);
        $this->context->smarty->assign('count_all', $count_all);
        $this->context->smarty->assign('sort', $sort);
        $this->context->smarty->assign('sort_a', $sort_a);

        $this->context->smarty->assign('status', $status);
        $this->context->smarty->assign('status_a', $status_a);

        $this->context->smarty->assign('letter', $_GET['letter']);
        $this->context->smarty->assign('filter_usa', $_GET['usa']);
        $this->context->smarty->assign('filter_canada', $_GET['canada']);


        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output; //.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMedicalmarijuanaexchangedirectoryModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MEDICALMARIJUANAEXCHANGEDIRECTORY_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'MEDICALMARIJUANAEXCHANGEDIRECTORY_LIVE_MODE' => Configuration::get('MEDICALMARIJUANAEXCHANGEDIRECTORY_LIVE_MODE', true),
            'MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_EMAIL' => Configuration::get('MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_PASSWORD' => Configuration::get('MEDICALMARIJUANAEXCHANGEDIRECTORY_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookLeftColumn($params)
    {
        if (isset($this->context->controller->hide_lastes_news) && $this->context->controller->hide_lastes_news) {
            return $this->display(dirname(__FILE__) . '/medicalmarijuanaexchangedirectory.php', 'views/templates/front/left_list.tpl');
        }
    }

    public function hookBackOfficeHeader()
    {


        $this->linkdosub = "/admin123/index.php?controller=AdminModules&token="
            . Tools::getAdminTokenLite('AdminModules') .
            "&configure=medicalmarijuanaexchangedirectory&tab_module=administration";

        $sql = 'SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'directory  WHERE review = 0';
        $x = Db::getInstance()->getValue($sql);


        $return = '
		<script>
		$( document ).ready(function() {

		$("#header_notifs_icon_wrapper").after("<a href=\'' . $this->linkdosub . '\'  style=\'color:white;    color: white;top: 9px;position: relative;background: rgba(255, 0, 0, 0.86);padding: 5px;border-radius: 5px; \' >Directory entries to review (' . $x . ')</a>")
		});
		
		
		
		</script>
		
		<style>
		a.addons_connect.toolbar_btn {
          display: none!important;
        }
		</style>
		';


        return $return;

        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }


    public function hookactionDispatcher()
    {
        //GEO
        global $cookie;
        $context = Context::getContext();
        if ($context->cookie->__isset('change_localization_fun')) {
            $city_id = $context->cookie->__get('change_localization_fun');

        } else {
            //Tenho qie melhorar isto .
            $city_id = 0;
        }
        // end GEO.....


        global $keystext;
        //GET ALL BANNERS
        $sql = "
		SELECT *
		FROM `pscl_camp` 
		WHERE `city` =" . pSQL($city_id) . "
		AND `used` != 0
		";

        $allbanners = Db::getInstance()->ExecuteS($sql);

        $keystext = $allbanners;

    }

    public function hookBanner($params)
    {
        //$this->registerHook('actionDispatcher');


        //GEO
        // global $cookie;
        //    $context = Context::getContext();
        //       if ( $context->cookie->__isset('change_localization_fun'))
        // {
        // 	$city_id = $context->cookie->__get('change_localization_fun');

        // }else{
        // 	//Tenho qie melhorar isto .
        // $city_id = 0;
        // }
        // end GEO.....


        //GET ALL BANNERS
        // $sql = "
        // SELECT *
        // FROM `pscl_camp`
        // WHERE `city` =".pSQL($city_id)."
        // AND `used` != 0
        // ";

        //SoftSprint Uliana
        // $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE pscl_camp.city = '.pSQL($city_id).' AND pscl_camp.used !=0 AND '._DB_PREFIX_.'banner.status = 1';

        //    $allbanners = Db::getInstance()->ExecuteS($sql);

        // if (count($allbanners) >0 ){
        // 	$randx =  rand(0,count($allbanners)-1);
        // 	$banner =  $allbanners[$randx];

        // 	//get banner information from campaign
        // 	$sql = "
        // 	SELECT * FROM `pscl_banner`
        // 	WHERE `id_banner` =".pSQL($banner['id_banner']);
        //     $banner['b'] = Db::getInstance()->getRow($sql);


        // 	//credit
        // 	if($banner['type'] == '2'){

        // 		//if seller is see your banner....
        // 		if((int)Context::getContext()->customer->id != $banner['customer'] ){
        // 			$camppppp_obj =  New Camp( (int)$banner['id_camp'] );
        // 			$camppppp_obj->used = $camppppp_obj->used -1;
        // 			$camppppp_obj->update();
        // 		}

        // 	}


        // }else{
        // 	$banner = array();
        // }

        //$this->context->smarty->assign('banner',$banner);

        //if( !empty($banner) ){
        return $this->display(__FILE__, '/views/templates/front/bannerview.tpl');
        //}


    }


    public function getcitiesnearby($city_id, $rangg = 100)
    {
        $sql = "
			SELECT *
			FROM `cities`
			WHERE `city_id` = " . pSQL($city_id);

        $alldatafromcity = Db::getInstance()->ExecuteS($sql);

        if (!empty($alldatafromcity)) {

            $locationtourl = $alldatafromcity[0]['city'];
            $locationtourl .= ', ';
            $locationtourl .= $alldatafromcity[0]['state_code'];
            $locationtourl .= ', ';
            if ($alldatafromcity[0]['country'] == "CA") {
                $locationtourl .= 'Canada';
            } else {
                $locationtourl .= 'United States';
            }
            //example //New York, NY, United States
            $contentfromapinearby = file_get_contents('http://getnearbycities.geobytes.com/GetNearbyCities?radius=' . (int)$rangg . '&locationcode=' . urlencode($locationtourl));
            $arraycontentfromapinearby = json_decode($contentfromapinearby, true);


            foreach ($arraycontentfromapinearby as $f) {

                if ($f[3] == 'United States') {
                    $countryselect = 'US';
                } else {
                    $countryselect = 'CA';
                }

                $sql = "
					SELECT *
					FROM `cities`
					WHERE `city` ='" . pSQL($f[1]) . "'
					AND  `state_code` ='" . pSQL($f[2]) . "'
					AND `country` = '" . pSQL($countryselect) . "'
					";
                $citiesnearbyfromusdb[] = Db::getInstance()->getRow($sql);
            }


            return $citiesnearbyfromusdb;
        } else {
            return false;
        }

    }


    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

        $sql = "SELECT * 
	FROM `pscl_blocknewsadv` 
	LEFT JOIN `pscl_blocknewsadv_data`
	ON pscl_blocknewsadv.id = pscl_blocknewsadv_data.id_item
	ORDER BY `pscl_blocknewsadv`.`time_add` DESC
	LIMIT 4
	";
        $newsmenu = Db::getInstance()->ExecuteS($sql);

        /*
        print "<pre>";
        print_r($newsmenu);
        print "</pre>";
            */

//get news to menu 
        if (isset($_POST['getnewsmenu'])) {
            $sql = "SELECT * 
	FROM `pscl_blocknewsadv` 
	LEFT JOIN `pscl_blocknewsadv_data`
	ON pscl_blocknewsadv.id = pscl_blocknewsadv_data.id_item
	ORDER BY `pscl_blocknewsadv`.`time_add` DESC
	LIMIT 4
	";
            $newsmenu = Db::getInstance()->ExecuteS($sql);
            print json_encode($newsmenu);
            die();
        }


//get blogs to menu 
        if (isset($_POST['getblogmenu'])) {
            $sql = "SELECT * 
	FROM `pscl_blog_post`
	LEFT JOIN `pscl_blog_post_data`
	ON pscl_blog_post_data.id_item = pscl_blog_post.id
	ORDER BY `pscl_blog_post`.`time_add` DESC
	LIMIT 4
	";


            $blogmenu = Db::getInstance()->ExecuteS($sql);
            print json_encode($blogmenu);
            die();
        }

        /*
        $sql = "SELECT *
        FROM `pscl_blog_post`
        LEFT JOIN `pscl_blog_post_data`
        ON pscl_blog_post_data.id_item = pscl_blog_post.id
        ORDER BY `pscl_blog_post`.`time_add` DESC
        LIMIT 4
        ";

        $blogmenu = Db::getInstance()->ExecuteS($sql);
        print_r($blogmenu);*/

//test sms api 
        /*

        if(isset($_GET['sendsms'])){

        require_once (dirname(__FILE__) . '/classes/sms.php'); // Include SMS base class file. download from the URL above

        $expertTexting= new experttexting_sms(); // Create SMS object.

        $expertTexting->from= 'MME'; // Sender of the SMS – PreRegistered through the Customer Area.
        $expertTexting->to= '351960072834'; // The full International mobile number of the without + or 00
        $expertTexting->msgtext= 'ola tudo bem estou a testar a api sms'; // The SMS content.
        //echo $expertTexting->send(); // Send SMS method.

        echo 'SMS::'.$expertTexting->sendUnicode(); // Send Multilangual SMS method.

        echo $expertTexting->QueryBalance(); // Query Your Account Balance method.

        }

        */


//NEW Chat		 
        if (isset($_POST['myusers'])) {


            $mycustomer_id = $this->context->customer->id;
            $updateonlinee = New Customer($mycustomer_id);
            $updateonlinee->lasttimeonline = $_POST['uniqueid'];
            $updateonlinee->update();


            $chats = scandir(_PS_ROOT_DIR_ . '/chat');
            //MY CHATS
            $mychats = array();
            $i = 0;
            $mychats['customerobj'] = $this->context->customer;
            unset($mychats['customerobj']->secure_key);
            foreach ($chats as $chat) {
                if ($chat != '.' && $chat != '..') {
                    $parts = explode('-', $chat);
                    if ($parts[0] == $mycustomer_id || $parts[1] == $mycustomer_id) {


                        $mychats['chats'][$i]['file'] = $chat;

                        if ($parts[0] == $this->context->customer->id) {
                            $mychats['chats'][$i]['user'] = New Customer($parts[1]);

                        } else {
                            $mychats['chats'][$i]['user'] = New Customer($parts[0]);
                        }


                        if ($mychats['chats'][$i]['user']->lasttimeonline + 60000 > $_POST['uniqueid']) {

                            $mychats['chats'][$i]['onff'] = 'online';
                        } else {

                            $mychats['chats'][$i]['onff'] = 'offline';
                        }


                        $onserver = _PS_ROOT_DIR_ . '/chat/' . $chat;
                        $b = 0;


                        //check if file exist
                        $sql = "SELECT *
						    FROM pscl_chatid 
							WHERE `file` = '" . pSQL($chat) . "'";
                        $existnowondb = Db::getInstance()->getRow($sql);


                        $to_lastline = '';
                        $lines_for_read = 0;
                        foreach (explode("\n", file_get_contents($onserver)) as $line) {
                            $mychats['chats'][$i]['content'][$b] = $line;
                            $lastline = explode('####', $line);


                            $users_of_chat = explode('-', $chat);
                            if (isset($lastline[4])) {
                                $to_lastline = $lastline[4];

                                if ($users_of_chat[0] == $this->context->customer->id) {
                                    if ($lastline[4] > $existnowondb['uniqueid']) {
                                        if ($lastline[3] != $this->context->customer->id) {
                                            $lines_for_read++;
                                        }
                                    }
                                } else {
                                    if ($lastline[4] > $existnowondb['uniqueid2']) {
                                        if ($lastline[3] != $this->context->customer->id) {
                                            $lines_for_read++;
                                        }
                                    }
                                }
                            }
                            $b++;
                        }

                        $mychats['chats'][$i]['forread'] = $lines_for_read;

                        // update read last uniqueIDS .....
                        //$id_chatid; $file; $uniqueid; $uniqueid2;
                        if (!$existnowondb) {
                            //$chatid_obj = New MedicalmarijuanaexchangedirectoryChatid($existnowondb['id_chatid']);
                            //}else{
                            $chatid_obj = New MedicalmarijuanaexchangedirectoryChatid();
                            $chatid_obj->file = $chat;
                            $users_of_chat = explode('-', $chat);
                            if ($users_of_chat[0] == $this->context->customer->id) {
                                $chatid_obj->uniqueid = $to_lastline;
                            } else {
                                $chatid_obj->uniqueid2 = $to_lastline;
                            }
                            $chatid_obj->add();
                        }

                        $i++;


                    }
                }
            }

            echo json_encode($mychats);
            exit();
        }


        //addmensage addmensage addmensage addmensage addmensage
        if (isset($_POST['addmensage'])) {

            $mycustomer_id = $this->context->customer->id;
            $date = date('m/d/Y h:i:s a', time());
            //prepare line
            //47-50####15/09/2018 15:48####Test from 47 50 111####
            $line = $_POST['filefrom'];
            $line .= '####';
            $line .= $date;
            $line .= '####';
            $line .= $_POST['texttttochat'];
            $line .= '####';
            $line .= $this->context->customer->id;
            $line .= '####';
            $line .= $_POST['uniqueid'] . PHP_EOL;


            $onserver = _PS_ROOT_DIR_ . '/chat/' . $_POST['filefrom'];


            $myfile = file_put_contents($onserver, $line, FILE_APPEND | LOCK_EX);

            print $_POST['uniqueid'];

            // SoftSprint Uliana send mail
            $shop_name = Configuration::get('PS_SHOP_NAME');
            $mail_message = $_POST['texttttochat'];
            $reciver_id = '';
            $filefrom = explode('-', $_POST['filefrom']);
            if ($mycustomer_id != $filefrom[0]) {

                $reciver_id = $filefrom[0];

            } else {

                $reciver_id = $filefrom[1];

            }

            $receiver = new Customer($reciver_id);
            $receiver_email = $receiver->email;
            $sender = new Customer($this->context->customer->id);
            $sender_name = $sender->firstname . ' ' . $sender->lastname;

            $id_lang = (int)$this->context->language->id;
            $id_shop = (int)$this->context->shop->id;
            $configuration = Configuration::getMultiple(
                array(
                    'PS_SHOP_EMAIL',
                    'PS_MAIL_METHOD',
                    'PS_MAIL_SERVER',
                    'PS_MAIL_USER',
                    'PS_MAIL_PASSWD',
                    'PS_SHOP_NAME',
                    'PS_MAIL_COLOR'
                ),
                $id_lang,
                null,
                $id_shop
            );

            $dir_mail = dirname(__FILE__) . '/mails/';
            Mail::Send(
                (int)$this->context->language->id,
                'chat_mail',
                Mail::l($sender_name . ' send you a new mensage'),
                array(
                    '{sender_name}' => $sender_name,
                    '{mail_message}' => $mail_message
                ),
                $receiver_email,
                null,
                $configuration['PS_SHOP_EMAIL'],
                $configuration['PS_SHOP_NAME'],
                null,
                null,
                $dir_mail,
                null,
                $id_shop
            );


            //send sms
            if ($receiver->phonesms != '') {
                require_once(dirname(__FILE__) . '/classes/sms.php'); // Include SMS base class file. download from the URL above

                $expertTexting = new experttexting_sms(); // Create SMS object.

                $expertTexting->from = 'MME'; // Sender of the SMS – PreRegistered through the Customer Area.
                $expertTexting->to = $receiver->phonesms; // The full International mobile number of the without + or 00
                $expertTexting->msgtext = $mail_message; // The SMS content.
                $expertTexting->send(); // Send SMS method.

                //$expertTexting->sendUnicode(); // Send Multilangual SMS method.

                //echo $expertTexting->QueryBalance(); // Query Your Account Balance method.
            }

        }

        //getchatupdate getchatupdate getchatupdate getchatupdate
        if (isset($_POST['getchatupdate'])) {
            $mychats = array();
            $passfiles = explode(';', $_POST['passfiles']);
            $i = 0;
            foreach ($passfiles as $passfile) {
                if ($passfile != '') {
                    $onserver = _PS_ROOT_DIR_ . '/chat/' . $passfile;


                    $newmsn = array();
                    //get new mensages
                    $b = 0;
                    foreach (explode("\n", file_get_contents($onserver)) as $line) {
                        $newmsn['chats'][$i]['content'][$b] = $line;
                        $b++;
                    }


                    $i++;
                }
            }


            print json_encode($newmsn);
            exit();
        }


        if (isset($_POST['updatemensageread'])) {


            //check if file exist
            $sql = "SELECT *
			FROM pscl_chatid 
			WHERE `file` = '" . pSQL($_POST['filefrom']) . "'";
            $existnowondb = Db::getInstance()->getRow($sql);

            $chatid_obj = New MedicalmarijuanaexchangedirectoryChatid($existnowondb['id_chatid']);
            $chatid_obj->file = $_POST['filefrom'];
            $users_of_chat = explode('-', $_POST['filefrom']);

            if ($users_of_chat[0] == $this->context->customer->id) {
                $chatid_obj->uniqueid = $_POST['uniqueid'];
            } else {
                $chatid_obj->uniqueid2 = $_POST['uniqueid'];
            }
            $chatid_obj->update();
            exit();

        }


        if (isset($_POST['addcontacttochat'])) {

            if ($_POST['addcontacttochat'] == $this->context->customer->id) {
                exit();
            }

            $customer_obj = New Customer($_POST['addcontacttochat']);
            if ($customer_obj->id_lang != 1) {
                exit();
            }


            $file_possible1 = _PS_ROOT_DIR_ . '/chat/' . $_POST['addcontacttochat'] . '-' . $this->context->customer->id;
            $file_possible2 = _PS_ROOT_DIR_ . '/chat/' . $this->context->customer->id . '-' . $_POST['addcontacttochat'];

            if (file_exists($file_possible1) || file_exists($file_possible2)) {
                exit();
            } else {
                fopen($file_possible2, 'w');
            }

            exit($_POST['addcontacttochat']);
        }


//END NEW Chat


        $cccustomer = new Customer($this->context->customer->id);
        $this->context->smarty->assign('cccustomer', $cccustomer);

        //$this->registerHook('banner');

        $this->context->controller->addJS($this->_path . '/views/js/front.js');

        $this->context->controller->addCSS($this->_path . '/views/css/front.css');


        if (isset($_COOKIE['agepopup'])) {
            $this->context->smarty->assign('agepopup', '1');
        } else {

            $this->context->smarty->assign('agepopup', '0');
        }


        $this->context->smarty->assign('menucat', _PS_MODULE_DIR_ . 'medicalmarijuanaexchangedirectory/views/templates/front/menucat.tpl');
        $this->context->smarty->assign('includedir', _PS_MODULE_DIR_ . 'medicalmarijuanaexchangedirectory/views/templates/front/');


// CHat// CHat// CHat// CHat// CHat// CHat// CHat// CHat// CHat// CHat

        // updateselectedcustromeronchat
        //SoftSprint Uliana fixed (identity page not show)
        if (isset($_POST['openclosechat']) && isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST') {

            $cus_obj = new Customer((int)$this->context->customer->id);
            $cus_obj->openclosechat = (int)$_POST['openclosechat'];
            $cus_obj->update();

            exit();
        }


        //what msg I have to read in all users msntoread
        if (isset($_POST['msntoread'])) {
            $users = explode(':', substr($_POST['user'], 0, -1));
            $online = Array();
            $i = 0;

            $i = 0;
            foreach ($users as $use) {
                //what  chat main ?
                $sql = "
						SELECT *
						FROM `pscl_chatmain`
						WHERE  `metric` = '" . (int)$this->context->customer->id . "-" . (int)$use . "'
						OR  `metric` = '" . (int)$use . "-" . (int)$this->context->customer->id . "'";
                //print $sql ;
                $exist = Db::getInstance()->getRow($sql);
                $json[$i]['user'] = $use;
                $json[$i]['chatmain'] = $exist['id_chatmain'];
                $json[$i]['metric'] = $exist['metric'];
                //what chat main is me 1 or 2
                $arraymetric = explode('-', $exist['metric']);
                if ((int)$this->context->customer->id == $arraymetric[0]) {
                    $whatisme = 1;
                } else {
                    $whatisme = 2;
                }
                $json[$i]['whatisme'] = $whatisme;
                //this whay my last msn is
                if ($whatisme == 1) {
                    $json[$i]['lastmsg'] = $exist['lastread'];
                    $lastreadd = $exist['lastread'];
                } else {
                    $json[$i]['lastmsg'] = $exist['lastread2'];
                    $lastreadd = $exist['lastread2'];
                }
                //now count rows after last read msg...
                $sql = "
					SELECT COUNT(*) AS count FROM `pscl_chat` 
					WHERE `chat_id` > " . (int)$lastreadd . "
				    AND `chatmain` =" . (int)$exist['id_chatmain'] . "
					AND `customer` =" . (int)$use . "
					";

                $json[$i]['count'] = Db::getInstance()->getValue($sql);
                $i++;
            }

            echo json_encode($json);
            die();
        }


        // updateselectedcustromeronchat
        if (isset($_POST['updateselectedcustromeronchat'])) {

            print $_POST['whatuser'];
            $cus_obj = new Customer((int)$this->context->customer->id);
            $cus_obj->chatactive = (int)$_POST['whatuser'];
            $cus_obj->update();
            die();
        }


        //assign my contacts---
        $sql = "
		SELECT *
		FROM `pscl_chatmain`
		WHERE `user1` =" . (int)$this->context->customer->id . "
		OR `user2` =" . (int)$this->context->customer->id . "
		";
        $customerlist = Db::getInstance()->ExecuteS($sql);
        foreach ($customerlist as &$c) {
            if ($c['user1'] == $this->context->customer->id) {
                $cu_obj = new customer($c['user2']);
            } else {
                $cu_obj = new customer($c['user1']);
            }
            $c['firstname'] = $cu_obj->firstname;
            $c['lastname'] = $cu_obj->lastname;
            $c['cuid'] = $cu_obj->id;

        }

        /*
        print "<pre>";
        print_R($customerlist);
        print "</pre>";
        */
        //my chat active
        $cu2_obj = new Customer($this->context->customer->id);
        //print ($this->context->customer->id);


        $this->context->smarty->assign('me', $this->context->customer->id);
        $this->context->smarty->assign('chatactive', $cu2_obj->chatactive);
        $this->context->smarty->assign('openclosechat', $cu2_obj->openclosechat);
        $this->context->smarty->assign('customerlist', $customerlist);


        //END chat	//END chat	//END chat	//END chat	//END chat	//END chat	//END chat


        if (isset($_POST['submitpointsFDS'])) {
            $typeofcategoriejava = (int)$_POST['typeofcategoriejava'];
            $needpoint = $_POST['needpoint'];
            //sdate="0" enddate="0"
            $sdate = $_POST['sdate'];
            $enddate = $_POST['enddate'];
            //count days selected
            $sdate2 = strtotime($sdate);
            $enddate2 = strtotime($enddate);
            $datediff = $enddate2 - $sdate2;
            $days_selected = floor($datediff / (60 * 60 * 24)) + 1;
            $customer_id = Context::getContext()->customer->id;
            //seller have points ???
            $customer_objjj = new customer($customer_id);
            if ($days_selected > $customer_objjj->points) {
                print "<h4>You only have " . $customer_objjj->points . " point avaliable</h4>";
                print "<br>";
                print '<p>Please click on this <a  style="color:red;" href="' . $this->context->link->getProductLink(81) . '">LINK</a> to buy points.</p>';
                die();
            } else {
                //time is greater than??

                if ($sdate == 0) {
                    exit('<h4 style="color:red">Please select dates.</h4>');
                }

                $date = new DateTime($sdate);

                $now = new DateTime();

                //all ok save db!!

                $sql = "
							INSERT INTO `pointsfds` (`id_pointsfds`,`categorie`,`id_customer`, `start`, `end`, `pointsbefore`, `pointsused`, `pointsafter`)
							VALUES (NULL," . pSQL($typeofcategoriejava) . "," . pSQL($customer_id) . ",'" . pSQL(date('Y-m-d', $sdate2)) . "', '" . pSQL(date('Y-m-d', $enddate2)) . "','" . pSQL($customer_objjj->points) . "','" . pSQL($days_selected) . "','" . pSQL((int)$customer_objjj->points - (int)$days_selected) . "')
							";
                if (Db::getInstance()->Execute($sql)) {
                    $actual_pointss = $customer_objjj->points;
                    $customer_obj2 = new customer($customer_id);
                    $customer_obj2->points = (int)$customer_objjj->points - (int)$days_selected;
                    if ($customer_obj2->update()) {
                        print "<h4 style='font-weight:900!important'>Thank you.</h4>";
                        print "<p>Your product will be visible on the homepage ";
                        print "from <b style='font-weight:900!important'>" . date('m-d-Y', $sdate2) . "</b> to <b style='font-weight:900!important'>" . date('m-d-Y', $enddate2) . "</b></p>";
                        print "<p>You use <b style='font-weight:900!important'>" . (int)$days_selected . "</b> Tokens.</p>";
                        print "<p>Your current point balance is <b style='font-weight:900!important'>" . $customer_obj2->points . "</b> Tokens.</p>";


                    } else {
                        print "ERROR: Please contact us.";
                    }
                    //print 'ok';

                } else {
                    print "ERROR: Please contact us.";
                }


            }


            exit();

        }


        if (isset($_POST['submitpoints'])) {


            $typeoffeatureseproducts = $_POST['typeoffeatureseproducts'];


            $needpoint = $_POST['needpoint'];
            $productid = $_POST['productid'];
            $sdate = $_POST['sdate'];
            $enddate = $_POST['enddate'];
            //count days selected
            $sdate2 = strtotime($sdate);
            $enddate2 = strtotime($enddate);
            $datediff = $enddate2 - $sdate2;
            $days_selected = floor($datediff / (60 * 60 * 24)) + 1;
            $customer_id = Context::getContext()->customer->id;
            //seller have points ???
            $customer_objjj = new customer($customer_id);
            if ($days_selected > $customer_objjj->points) {
                print "<h4>You only have " . $customer_objjj->points . " point avaliable</h4>";
                print "<br>";
                print '<p>Please click on this <a  style="color:red;" href="' . $this->context->link->getProductLink(81) . '">LINK</a> to buy points.</p>';
                die();
            } else {
                //time is greater than??

                if ($sdate == 0) {
                    exit('<h4 style="color:red">Please select dates.</h4>');
                }

                $date = new DateTime($sdate);

                $now = new DateTime();


                //all ok save db!!

                $sql = "
							INSERT INTO `pointshome` (`id_pointshome`,`categoriehome`, `product_id`, `start`, `end`, `pointsbefore`, `pointsused`, `pointsafter`)
							VALUES (NULL ,'" . pSQL($typeoffeatureseproducts) . "', '" . pSQL($_POST['productid']) . "','" . pSQL(date('Y-m-d', $sdate2)) . "', '" . pSQL(date('Y-m-d', $enddate2)) . "','" . pSQL($customer_objjj->points) . "','" . pSQL($days_selected) . "','" . pSQL((int)$customer_objjj->points - (int)$days_selected) . "')
							";
                if (Db::getInstance()->Execute($sql)) {
                    $actual_pointss = $customer_objjj->points;
                    $customer_obj2 = new customer($customer_id);
                    $customer_obj2->points = (int)$customer_objjj->points - (int)$days_selected;
                    if ($customer_obj2->update()) {
                        print "<h4 style='font-weight:900!important'>Thank you.</h4>";
                        print "<p>Your product will be visible on the homepage ";
                        print "from <b style='font-weight:900!important'>" . date('m-d-Y', $sdate2) . "</b> to <b style='font-weight:900!important'>" . date('m-d-Y', $enddate2) . "</b></p>";
                        print "<p>You use <b style='font-weight:900!important'>" . (int)$days_selected . "</b> Tokens.</p>";
                        print "<p>Your current point balance is <b style='font-weight:900!important'>" . $customer_obj2->points . "</b> Tokens.</p>";


                    } else {
                        print "ERROR: Please contact us.";
                    }
                    //print 'ok';

                } else {
                    print "ERROR: Please contact us.";
                }


                //print $sdate = $_POST['sdate']."<br>";
                //print $enddate = $_POST['enddate'];
            }

            //print_r($_POST);

            exit();
        }


//get geo !!
        if (isset($_POST['deletecookietogeohome'])) {
            global $cookie;
            $context = Context::getContext();
            $context->cookie->__unset('change_localization_fun');
            die();
        }


        if (isset($_POST['change_localization_fun'])) {
            global $cookie;
            $context = Context::getContext();
            $context->cookie->__set('change_localization_fun', (int)$_POST['value']);

            $cookie->write();
            print 'change_localization_fun_ok';

            die();
        }


        if (isset($_POST['changelocalizationinput'])) {
            if ($_POST['value'] == '') {
                exit();
            }
            $sql = "
		SELECT *
		FROM `cities`
		WHERE 1
		AND `city` = 'yyyy'
		";

            $nodoublewhite = str_replace("  ", "", trim($_POST['value']));
            $words = explode(' ', $nodoublewhite);


            //print_R ($words);


            foreach ($words as $word) {
                if ($word != '' OR $word != ' ') {
                    $sql .= " OR `city` LIKE '%" . pSQL($word) . "%' ";
                }
            }


            foreach ($words as $word) {
                $sql .= " OR `state` LIKE '%" . pSQL($word) . "%' ";
            }

            $sql .= " order by country ASC , city ASC Limit 100 ";


            $results = Db::getInstance()->ExecuteS($sql);
            //print_R($results);


            foreach ($results as $r) {
                if ($r['country'] == 'CA') {
                    print '<li  onclick="change_localization_fun(' . $r['city_id'] . ')"  value="' . $r['city_id'] . '" class="list-group-item lifromhomecity">' . $r['city'] . ', <bold style="font-weight: 900!important;">(' . $r['state_code'] . ')</bold> ' . $r['state'] .
                        '<span><img style="position: absolute;width: 25px;height: 25px;top: 6px;right: 7px;"  src="' . _THEME_DIR_ . '/img/canada.png"></span></li>';
                } else {
                    print '<li  onclick="change_localization_fun(' . $r['city_id'] . ')"  value="' . $r['city_id'] . '" class="list-group-item lifromhomecity">' . $r['city'] . ', <bold style="font-weight: 900!important;">(' . $r['state_code'] . ')</bold> ' . $r['state'] .
                        '<span><img style="position: absolute;width: 25px;height: 25px;top: 6px;right: 7px;" src="' . _THEME_DIR_ . '/img/usa.png"></span></li></li>';
                }
            }
            die();

        }

        global $cookie;
        $context = Context::getContext();


        if ($context->cookie->__isset('change_localization_fun')) {

            //print $context->cookie->__get('change_localization_fun');

            $sql = "
			SELECT * From `cities`
			where `city_id`  = " .
                pSQL(($context->cookie->__get('change_localization_fun')));

            $loc = Db::getInstance()->getRow($sql);

            $parsedJson2 = array();
            $parsedJson2['cityName'] = $loc['city'];
            $parsedJson2['regionName'] = $loc['state'];


            $this->context->smarty->assign('cityinfos', $parsedJson2);
        } else {


            $pageContent2 = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=7a04e084f3d53d9d0e99544de15ecebce3329fa2722f73c6126d044dd055c1f1&format=json&ip=' . $_SERVER['REMOTE_ADDR']);
//$pageContent2 = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=7a04e084f3d53d9d0e99544de15ecebce3329fa2722f73c6126d044dd055c1f1&format=json&ip=192.206.151.131'  );
            $this->context->cityCookie = $pageContent2;
//$context->cookie->__unset('change_localization_fun');
            $parsedJson2 = array();
            $parsedJson2 = json_decode($pageContent2, true);


            /*	print "<pre>";print_r($parsedJson2);die;*/

            $sql = "
		SELECT *
		FROM `cities` 
		WHERE `state` = '" . pSQL($parsedJson2['regionName']) . "'
		AND `city` = '" . pSQL($parsedJson2['cityName']) . "'
		
		";
            //print $sql."<br>";
            if ($locid = Db::getInstance()->getRow($sql)) {
                //print_r( $locid)."<br>";
            }


            $this->context->smarty->assign('cityinfos', $parsedJson2);

        }


    }

    //feature products on home page
    public function hookDisplayHome($params)
    {
        $this->context->smarty->assign('idtoedit', 'jhgygyg');

        return $this->display(__FILE__, '/views/templates/front/featurehome.tpl');
    }


    public function doorderandpay($id_product, $customer_id)
    {
        //$customer_id = Context::getContext()->customer->id;
        //print "<pre>";
        $allorder = Order::getIdOrderProduct($customer_id, $id_product);

        $ooodet = new Order($allorder);
        // order is valid???
        if ($ooodet->valid == 1) {
            //calc 365 days
            //print $ooodet->date_add;
            $now = time(); // or your date as well
            $your_date = strtotime($ooodet->date_add);
            $datediff = $now - $your_date;
            $daysbet = floor($datediff / (60 * 60 * 24));
            // if more then 365 days
            if ($daysbet < 365) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }


}

<?php
class MedicalmarijuanaexchangedirectoryEventregistryModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;


        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }

		if (isset($_GET['get_articles'])) {
			Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'eventregistry` (
				`id_eventregistry` int(15) unsigned NOT NULL AUTO_INCREMENT,
				`uri` int(10) unsigned NOT NULL ,
				`title` varchar(255) NOT NULL,
				`image` varchar(255) NOT NULL,
				`content` text NOT NULL,
				`date` varchar(255) NOT NULL,
				`categories` varchar(255),
				`city` varchar(255),
				`country` varchar(255),
				`category_to_show` varchar(255),
				`show_in` varchar(255) NOT NULL,
				`url` varchar(255) NOT NULL,
				`source` varchar(255) NOT NULL,
				`confirm` int(10) NOT NULL,
				`admin_category` varchar(255) NOT NULL,
				PRIMARY KEY (`id_eventregistry`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
			');

			$keywords = ['cannabis', 'hemp', 'cannabidiol (CBD)', 'medical cannabis', 'cannabis industry'];
			$dateStart = date('Y-m-d' ,strtotime("-1 days"));
			$dateEnd = date('Y-m-d');

			foreach ($keywords as $key => $keyword) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL =>'http://eventregistry.org/api/v1/article/getArticles',
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => json_encode(array(
					"apiKey" => "ee410621-40d2-4f88-8b17-b12df59fce72",
					"keyword"=> $keyword,
					// "sourceLocationUri" => array(
					// 	"http://en.wikipedia.org/wiki/United_States",
					// 	"http://en.wikipedia.org/wiki/Canada",
					// 	"http://en.wikipedia.org/wiki/Ireland",
					// 	"http://en.wikipedia.org/wiki/Italy",
					// 	"http://en.wikipedia.org/wiki/Brazil",
					// 	"http://en.wikipedia.org/wiki/United_Kingdom",
					// 	"http://en.wikipedia.org/wiki/Germany",
					// 	"http://en.wikipedia.org/wiki/Mexico"
					// ),
					"sourceUri" => array(
						"Benzinga.com",
						"fool.com",
						"newwestrecord.ca",
						"mjbizdaily.com",
						"marketrealist.com",
						"seekingalpha.com",
						"thefreshtoast.com",
						"bostonglobe.com",
						"forbes.com",
						"thechronicle.com.au",
						"theguardian.com",
						"hightimes.com",
						"merryjane.com",
						"cnn.com",
						"blog.norml.org",
						"msn.com"
					),
					// "categoryUri" => array(
					// 	"dmoz/Business/Agriculture_and_Forestry/Industrial_Hemp",
					// 	"dmoz/Health/Specific_Substances/Cannabis",
					// 	"dmoz/Recreation/Drugs/Cannabis",
					// ),
					"dateStart" => $dateStart,
					"dateEnd" => $dateEnd,
					// "dateStart" => "2020-03-08",
					// "dateEnd" => "2020-03-08",
					"includeArticleLocation" => true,
					// "includeCategoryParentUri" => true,
					"lang" => "eng",
					"articlesSortByAsc" => false,
					"includeArticleCategories" => true,
					"includeArticleLocation" => true,
					"includeArticleConcepts" => true,
					"includeSourceLocation" => true,

				)),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"content-type: application/json"
				),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);
				
				$response = json_decode($response);
				$articles = $response->articles->results;
				if (!empty($articles)) {
					foreach ($articles as $article) {
						$categories_list ='';
						if (!empty($article->categories)) {
							$categories_list = array();
							foreach ($article->categories as $category) {
								$categories_list[] = $category->uri;
							}
							$categories_list = json_encode($categories_list);
						}

						$check = Eventregistry::checkDuplicate($article->uri, $article->title);
						if ($check == false && $article->source->uri != "pressherald.com"){

							$a=Db::getInstance()->insert('eventregistry', array(
							'uri'      => $article->uri,
							'title' 		=> pSQL($article->title),
							'image'     => pSQL(isset($article->image)?$article->image:'noimage.jpg'),
							'content'     =>pSQL($article->body),
							'date'     => $article->dateTime,
							'categories'   => pSQL(isset($categories_list)?$categories_list:NULL),
							'city'   => pSQL(isset($article->location->label->eng)?$article->location->label->eng:$article->source->location->label->eng),
							'country'      => pSQL(isset($article->location->country->label->eng)?$article->location->country->label->eng:$article->source->location->country->label->eng),
							'url'     => $article->url,
							'source'    => $article->source->uri,
							'admin_category' => $keyword
							));
						}
						//  else{
						// 	echo $article->uri.': duplicate;</br>';
						// }
					}
				}
			}
			exit();
		}

		if (isset($_GET['get_articles_editional'])) {
			$keywords = ['cannabis'];
			$dateStart = date('Y-m-d' ,strtotime("-1 days"));
			// $dateEnd = date('Y-m-d' ,strtotime("-1 days"));
			//$dateStart = date('Y-m-d');
			$dateEnd = date('Y-m-d');

			foreach ($keywords as $key => $keyword) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL =>'http://eventregistry.org/api/v1/article/getArticles',
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => json_encode(array(
					"apiKey" => "ee410621-40d2-4f88-8b17-b12df59fce72",
					"keyword"=> $keyword,
					"articlesCount"=> 200,
					"locationUri" =>array(
					 	"http://en.wikipedia.org/wiki/United_States",
					 	"http://en.wikipedia.org/wiki/Canada",
					),
					"startSourceRankPercentile" => 0,
    				"endSourceRankPercentile" => 90,
					"dateStart" => $dateStart,
					"dateEnd" => $dateEnd,
					"includeArticleLocation" => true,
					"lang" => "eng",
					"articlesSortByAsc" => false,
					"includeArticleCategories" => true,
					"includeArticleLocation" => true,
					"includeArticleConcepts" => true,
					"includeSourceLocation" => true,

				)),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"content-type: application/json"
				),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);
				
				$response = json_decode($response);
				// echo '<pre>';
				// print_r($response);exit;
				$articles = $response->articles->results;
				if (!empty($articles)) {
					foreach ($articles as $article) {
						$categories_list ='';
						if (!empty($article->categories)) {
							$categories_list = array();
							foreach ($article->categories as $category) {
								$categories_list[] = $category->uri;
							}
							$categories_list = json_encode($categories_list);
						}
						//if (!$check = Eventregistry::checkDuplicate($article->uri)){
						$check = Eventregistry::checkDuplicate($article->uri, $article->title);
					
						if ($check == false && $article->source->uri != "pressherald.com"){

							$a=Db::getInstance()->insert('eventregistry', array(
					
							'uri'      => $article->uri,
							'title' 		=> pSQL($article->title),
							'image'     => pSQL(isset($article->image)?$article->image:'noimage.jpg'),
							'content'     =>pSQL($article->body),
							'date'     => $article->dateTime,
							'categories'   => pSQL(isset($categories_list)?$categories_list:NULL),
							'city'   => pSQL(isset($article->location->label->eng)?$article->location->label->eng:$article->source->location->label->eng),
							'country'      => pSQL(isset($article->location->country->label->eng)?$article->location->country->label->eng:$article->source->location->country->label->eng),
							'url'     => $article->url,
							'source'    => $article->source->uri,
							'admin_category' => $keyword
							));
						}
						//  else{
						// 	echo $article->uri.': duplicate;</br>';
						// }
					}
				}
			}
			exit();
		}

		if (isset($_GET['get_articles_editional2'])) {
			$keywords = ['cannabis'];
			$dateStart = date('Y-m-d' ,strtotime("-1 days"));
			// $dateEnd = date('Y-m-d' ,strtotime("-1 days"));
			//$dateStart = date('Y-m-d');
			$dateEnd = date('Y-m-d');

			foreach ($keywords as $key => $keyword) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL =>'http://eventregistry.org/api/v1/article/getArticles',
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => json_encode(array(
					"apiKey" => "ee410621-40d2-4f88-8b17-b12df59fce72",
					"keyword"=> $keyword,
					"articlesCount"=> 200,
					"locationUri" =>array(
					 	"http://en.wikipedia.org/wiki/United_States",
					 	"http://en.wikipedia.org/wiki/Canada",
					),
					"startSourceRankPercentile" => 0,
    				"endSourceRankPercentile" => 90,
					"dateStart" => $dateEnd,
					"dateEnd" => $dateEnd,
					"includeArticleLocation" => true,
					"lang" => "eng",
					"articlesSortByAsc" => false,
					"includeArticleCategories" => true,
					"includeArticleLocation" => true,
					"includeArticleConcepts" => true,
					"includeSourceLocation" => true,

				)),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"content-type: application/json"
				),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);
				
				$response = json_decode($response);
				// echo '<pre>';
				// print_r($response);exit;
				$articles = $response->articles->results;
				if (!empty($articles)) {
					foreach ($articles as $article) {
						$categories_list ='';
						if (!empty($article->categories)) {
							$categories_list = array();
							foreach ($article->categories as $category) {
								$categories_list[] = $category->uri;
							}
							$categories_list = json_encode($categories_list);
						}

						$check = Eventregistry::checkDuplicate($article->uri, $article->title);
						if ($check == false && $article->source->uri != "pressherald.com"){
							$a=Db::getInstance()->insert('eventregistry', array(
					
							'uri'      => $article->uri,
							'title' 		=> pSQL($article->title),
							'image'     => pSQL(isset($article->image)?$article->image:'noimage.jpg'),
							'content'     =>pSQL($article->body),
							'date'     => $article->dateTime,
							'categories'   => pSQL(isset($categories_list)?$categories_list:NULL),
							'city'   => pSQL(isset($article->location->label->eng)?$article->location->label->eng:$article->source->location->label->eng),
							'country'      => pSQL(isset($article->location->country->label->eng)?$article->location->country->label->eng:$article->source->location->country->label->eng),
							'url'     => $article->url,
							'source'    => $article->source->uri,
							'admin_category' => $keyword
							));
						}
						//  else{
						// 	echo $article->uri.': duplicate;</br>';
						// }
					}
				}
			}
			exit();
		}


		//get articles for global category
		if (isset($_GET['get_articles_global'])) {
			$keywords = ['cannabis', 'medical cannabis', 'hemp'];
			$dateStart = date('Y-m-d' ,strtotime("-1 days"));
			// $dateEnd = date('Y-m-d' ,strtotime("-1 days"));
			//$dateStart = date('Y-m-d');
			$dateEnd = date('Y-m-d');

			foreach ($keywords as $key => $keyword) {
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL =>'http://eventregistry.org/api/v1/article/getArticles',
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => json_encode(array(
					"apiKey" => "ee410621-40d2-4f88-8b17-b12df59fce72",
					"keyword"=> $keyword,
					"articlesCount"=> 200,
					"locationUri" =>array(
					 	"http://en.wikipedia.org/wiki/Australia",
					 	"http://en.wikipedia.org/wiki/Brazil",
					 	"http://en.wikipedia.org/wiki/Germany",
					 	"http://en.wikipedia.org/wiki/Greece",
					 	"http://en.wikipedia.org/wiki/Israel",
					 	"http://en.wikipedia.org/wiki/Mexico",
					 	"http://en.wikipedia.org/wiki/Spain",
					 	"http://en.wikipedia.org/wiki/United_Kingdom",
					),
					// "startSourceRankPercentile" => 0,
    	// 			"endSourceRankPercentile" => 90,
					"dateStart" => $dateStart,
					"dateEnd" => $dateEnd,
					"includeArticleLocation" => true,
					"lang" => "eng",
					"articlesSortByAsc" => false,
					"includeArticleCategories" => true,
					"includeArticleLocation" => true,
					"includeArticleConcepts" => true,
					"includeSourceLocation" => true,

				)),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"content-type: application/json"
				),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);
				
				$response = json_decode($response);
				// echo '<pre>';
				// print_r($response); exit;
				$articles = $response->articles->results;
				if (!empty($articles)) {
					foreach ($articles as $article) {
						$categories_list ='';
						if (!empty($article->categories)) {
							$categories_list = array();
							foreach ($article->categories as $category) {
								$categories_list[] = $category->uri;
							}
							$categories_list = json_encode($categories_list);
						}

						$check = Eventregistry::checkDuplicate($article->uri, $article->title);
						if ($check == false && $article->source->uri != "pressherald.com"){
							$a=Db::getInstance()->insert('eventregistry', array(
					
							'uri'      => $article->uri,
							'title' 		=> pSQL($article->title),
							'image'     => pSQL(isset($article->image)?$article->image:'noimage.jpg'),
							'content'     =>pSQL($article->body),
							'date'     => $article->dateTime,
							'categories'   => pSQL(isset($categories_list)?$categories_list:NULL),
							'city'   => pSQL(isset($article->location->label->eng)?$article->location->label->eng:$article->source->location->label->eng),
							'country'      => pSQL(isset($article->location->country->label->eng)?$article->location->country->label->eng:$article->source->location->country->label->eng),
							'url'     => $article->url,
							'source'    => $article->source->uri,
							'admin_category' => 'global'
							));
						}
						//  else{
						// 	echo $article->uri.': duplicate;</br>';
						// }
					}
				}
			}
			exit();
		}

		if(isset($_GET['move_articles'])) {
			$sucess = false;
			$articles = array_merge(
				array_map(function ($a) {$a['model'] = 'Eventregistry'; return $a;}, Eventregistry::geteventregistry()),
				array_map(function ($a) {$a['model'] = 'BoardArticle'; return $a;}, BoardArticle::getBoardArticle())
			);
			// echo "<pre>"; print_r($articles); echo "</pre>";
			// exit;
			if(!empty($articles)){
				foreach ($articles as $article) {
					$article['category_to_show'] = json_decode($article['category_to_show']);
					$languages = Language::getLanguages(false);
					$data_title_content_lang = array();

					foreach ($languages as $language){
						$id_lang = $language['id_lang'];
			    		$post_title = $article['title'];
			    		$post_seokeywords = $article['seo_keywords'];
			    		$post_seodescription = $article['seo_description'];
			    		$post_content = $article['content'];
			    		
			    		if(strlen($post_title)>0)
			    		{
			    			$data_title_content_lang[$id_lang] = [
				    			'post_title' => $article['title'],
				 				'post_seokeywords' => $post_seokeywords,
								'post_seodescription' => $post_seodescription,
								'post_content' => $post_content,
								'seo_url' => $article['url']
						    ];
						    $data_title_content_lang_new[$id_lang] = [
					    		'title' => $article['title'],
					    		'seokeywords' => $post_seokeywords,
					    		'seodescription' => $post_seodescription,
					    		'content' => $post_content,
					    		'seo_url' => $article['url']
					    	];
			    		}
			    	}
					$data = array(
					  'data_title_content_lang'=>$data_title_content_lang,
					  'ids_categories' => $article['category_to_show'],
					  'post_status' => 1,
					  'post_iscomments' => 1,
					  'dateee' => date('Y-m-d H:i:s', strtotime($article['date'])), //date("Y-m-d H:i:s"),
					  'bl_author' => $article['source'],
					  'mention_name' => '',
					  'hashtags' => $article['hashtags'],
					  'show_in_news' => (int)$article['show_in']);

					if(!empty($article['show_in'])&&isset($article['show_in'])){
						$newsData = array(
			         		'data_title_content_lang' => $data_title_content_lang_new,
			         		'item_status' => 1,
			         		'iscomments' => 1,
			         		'post_images' => '',
			         		'cat_shop_association' => array(1),
			         		'related_products' => '',
			         		'ids_related_posts' => '',
			         		'time_add' => date('Y-m-d H:i:s', strtotime($article['date'])), //date("Y-m-d H:i:s"),
			         		'author' => $article['source'],
			         		'hashtags' => $article['hashtags'],
			         		'post_id' => 0
			         	);
					}
					if(strpos($article['image'], "?")){
						$article['image'] = substr($article['image'], 0, strpos($article['image'], "?"));
					}
					$type = end(explode('.',$article['image']));
					$name = str_replace('.'.$type, '', $article['image']);
					$name = end(explode('/',$name));
		         	if (isset($article['image'])) {
		         		$imageExist = (bool)file_exists(_PS_MODULE_DIR_ . "../upload/blockblog/".$article['image']);
		         		$_FILES['post_image'] = ['img' => $article['image'], 'name' => $name, 'tmp_name' => $article['image'], 'error'=> '', 'type'=>'image/'.$type, 'source'=> $article['url'],'saved' => $imageExist];
		         		$_FILES['name'] = 'temp';
		         		$_FILES['news_image'] = $_FILES['post_image'];
		         	}
		         	// echo "<pre>"; print_r($_FILES); echo "</pre>";
			        include_once(_PS_MODULE_DIR_.'blockblog'.DIRECTORY_SEPARATOR.'blog.class.php');
					$obj_blog = new blog();
			        $post_id = $obj_blog->savePost($data);
			        $sucess = true;
		     		if ($post_id) {
		     			if (empty($_FILES['news_image']['name'])) {
			         		$_img = Db::getInstance()->getValue('SELECT img FROM '._DB_PREFIX_.'blog_post WHERE id='.(int)$post_id);
			         		if ($_img) {
				         		$_img = _PS_UPLOAD_DIR_ . 'blockblog/' . $_img;
				         		copy($_img, _PS_UPLOAD_DIR_ . 'blockblog/__TMP_IMG_POST.jpg');
				         		@chmod(_PS_UPLOAD_DIR_ . 'blockblog/__TMP_IMG_POST.jpg', 0777);
				         		$_FILES['news_image'] = array(
				         			'name' => 'tmp_img_post.jpg',
				         			'type' => 'image/jpeg',
				         			'tmp_name' => _PS_UPLOAD_DIR_ . 'blockblog/__TMP_IMG_POST.jpg',
				         			'error' => 0,
				         			'size' => filesize(_PS_UPLOAD_DIR_ . 'blockblog/__TMP_IMG_POST.jpg')
				         		);
				         	}
			         	}

		     			if (!class_exists('blocknewsadvfunctions')) {
			         		require_once _PS_MODULE_DIR_ . 'blocknewsadv/classes/blocknewsadvfunctions.class.php';
			         	}

			         	if(isset($article['show_in'])&&!empty($article['show_in'])){
			         		$blocknewsadvfunctions_obj = new blocknewsadvfunctions();
				         	$newsData['post_id'] = $post_id;
				         	$blocknewsadvfunctions_obj->saveItem($newsData);
			         	}
			         	// remove from list
			         	if($sucess) {
			         		if ($article['model'] == 'Eventregistry') {
			         			Eventregistry::removeRowEventregistry($article['id_eventregistry']);
			         		} elseif ($article['model'] == 'BoardArticle') {
			         			BoardArticle::markAsDeleted($article['id_board_article']);
			         		}
			         	}
		     		}
				}
			}
     		exit;
		}
	}
}
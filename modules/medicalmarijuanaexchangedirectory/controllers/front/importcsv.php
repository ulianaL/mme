<?php
/** Used for validatefields diying without user friendly error or not */
define('UNFRIENDLY_ERROR2', false);
class MedicalmarijuanaexchangedirectoryImportcsvModuleFrontController extends ModuleFrontController
{

	public $product = null;
	public $attribute_unit = '';
    
	public function __construct()
    {

        parent::__construct();

        $this->display_column_left = false;
    }

  	public function initContent()
    {
        
    	$is_bussiness = New Customer((int)Context::getContext()->customer->id);
    	// if ($is_bussiness->bussiness !== "1") {
    	// 	Tools::redirect($this->context->link->getPageLink("index"));
    	// }
        
  	    parent::initContent();

        $products_array = array();
        $success_messege = '';
        $import_error = '';

        $uploaddir = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'import'
            .DIRECTORY_SEPARATOR;


        if(isset($_POST['upload_file'])){

          //validate whether uploaded file is a csv file
	        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
	        if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){

	          	$filename_prefix = date('YmdHis').'-';
	          	$uploadfile = $uploaddir . basename($_FILES['file']['name']);

	            if(is_uploaded_file($_FILES['file']['tmp_name'])){

	            	if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
					    $this->context->cookie->__set('csv_file',$uploadfile);
		                //open uploaded csv file with read only mode
		                $csvFile = fopen($uploadfile, 'r');
		                  
		                //skip first line
		                fgetcsv($csvFile);
		                
		                //parse data from csv file line by line
		                while(($line = fgetcsv($csvFile, 1000, ";")) !== FALSE){
		                   $products_array[]=$line;
		                }
		                 
		                //close opened csv file
		                fclose($csvFile);
					} else {
					    $import_error = 'This file can not be loaded!';
					}

	            }else{
	            	$import_error = 'This file can not be loaded!';
	            }
	        }else{
	            $import_error = 'This file can not be loaded!';
	        }
	    }

	    if(isset($_POST['import'])){

	    	$csvFile = fopen($this->context->cookie->csv_file, 'r');
	                  
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile, 1000, ";")) !== FALSE){
               $products_array[]=$line;
            }
             
            //close opened csv file
            fclose($csvFile);

            foreach ($products_array as $value) {
        
        		if ($value[0] !=='' && (int)$value[0]) {
        			$product = new Product((int)$value[0]);
        		}else{
        			$product = new Product();
        		}

        		$product->ismedical = $value[1];
        		$product->name[1] = ($value[2] !=='' ? $value[2]:'(Draft)');
        		$product->link_rewrite[1]=$this->slugify($value[2] !=='' ? $value[2]:'(Draft)');
        		$product->id_owner = Context::getContext()->customer->id;
        		if ($value[3] !=='') {
        			$cat_info = Category::searchByName(1, $value[3]);
        			if (!empty($cat_info)) {
        				$cat_id = $cat_info[0]["id_category"];
        				$product->id_category_default = (int)$cat_id;
        				
        			}else{
        				$product->id_category_default = 2;
        				$cat_id = 2;
        			}
        		}else{
        			$product->id_category_default = 2;
        		}

            	$product->price = (int)$value[4];
            	$product->reference = $value[5];
            	$product->quantity = 0;
            	$product->description_short[1] = $value[7];
            	$product->description[1] = $value[8];
            	

                $product->own_store_link = $value[10];
                
                if ($value[11] !== '') {
                	if ($value[11]== 'USA') {
	                	$product->country = 1;
	                	$real_country_id = 21;
	                }elseif ($value[11]== 'Canada') {
	                	$product->country = 2;
	                	$real_country_id = 4;
	                }else{
	                	$real_country_id = '';
	                }

	                if ($value[12] !== '' && $real_country_id !== '') {
	                	$sql="SELECT id_state FROM `pscl_state` WHERE `name` ='".$value[12]."' AND `id_country` =".$real_country_id;
			    		$state = Db::getInstance()->getValue($sql);
			    		$product->state = $state;

			    		$sql="SELECT city_id FROM `cities` WHERE `city` ='".$value[13]."' AND `state` = '".$value[12]."'";
			    		$city = Db::getInstance()->getValue($sql);
			    		$product->city = $city;

	                }
                }

                if (isset($value[16])) $product->brand = $value[16];
                if (isset($value[17])) $product->thc = $value[17];
                if (isset($value[18])) $product->cbd = $value[18];
                if (isset($value[19])) $product->strain = $value[19];

                if ($value[0] !=='' && (int)$value[0]) {
                	if ($product->update()){
	            		StockAvailable::setQuantity($product->id, null,$value[6]);

	            		if (isset($value[15]) && trim($value[15]) != '' && isset($value[14]) && trim($value[14]) != '') {
		            		$attribute_values = $this->parse_attribute_values($value[15]);
			            	$prices_array = $this->build_prices_array($attribute_values);
		            		$this->product = $product;
		            		$this->buildCombinations($value[14], $prices_array);
		            	}

	            		$cat_array = explode(",",$cat_id);
	        			$product->updateCategories($cat_array);

	        			$value[9] = explode(',', $value[9]);

		            	if (isset($value[9]) && is_array($value[9]) && count($value[9])) {
		                    $product_has_images = (bool)Image::getImages(1, (int)$product->id);
		                    foreach ($value[9] as $key => $url) {
		                        $url = trim($url);
		                        $error = false;
		                        if (!empty($url)) {
		                            $url = str_replace(' ', '%20', $url);
		                            $image = new Image();
		                            $image->id_product = (int)$product->id;
		                            $image->position = Image::getHighestPosition($product->id) + 1;
		                            $image->cover = (!$key && !$product_has_images) ? true : false;
		                            // file_exists doesn't work with HTTP protocol
		                            if (($field_error = $image->validateFields(UNFRIENDLY_ERROR2, true)) === true &&
		                                ($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR2, true)) === true && $image->add()) {
		                                // associate image to selected shops
		                                //$image->associateTo($shops);
		                                $products_array = array();
		                            	$success_messege = "Products have been successfully uploaded!";

		                                if (!AdminImportController::copyImg($product->id, $image->id, $url, 'products')) {
		                                    $image->delete();
		                                    $this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
		                                }

		                            } else {
		                                $import_error = 'This file can not be loaded!';
		                            }
		                        } else {
		                            $import_error = 'This file can not be loaded!';
		                        }

		                        if ($error) {
		                            $this->warnings[] = sprintf(Tools::displayError('Product #%1$d: the picture (%2$s) cannot be saved.'), $image->id_product, $url);
		                        }
		                    }
		                }
	            	}

                }else{
                	if ($product->add()){
	            		StockAvailable::setQuantity($product->id, null,$value[6]);

	            		if (isset($value[15]) && trim($value[15]) != '' && isset($value[14]) && trim($value[14]) != '') {
		            		$attribute_values = $this->parse_attribute_values($value[15]);
			            	$prices_array = $this->build_prices_array($attribute_values);
			            	// echo "<pre>"; print_r($value); echo "</pre>"; exit;
		            		$this->product = $product;
		            		$this->buildCombinations($value[14], $prices_array);
		            	}

	            		$cat_array = explode(",",$cat_id);
	        			$product->updateCategories($cat_array);

	        			$value[9] = explode(',', $value[9]);

		            	if (isset($value[9]) && is_array($value[9]) && count($value[9])) {
		                    $product_has_images = (bool)Image::getImages(1, (int)$product->id);
		                    foreach ($value[9] as $key => $url) {
		                        $url = trim($url);
		                        $error = false;
		                        if (!empty($url)) {
		                            $url = str_replace(' ', '%20', $url);
		                            $image = new Image();
		                            $image->id_product = (int)$product->id;
		                            $image->position = Image::getHighestPosition($product->id) + 1;
		                            $image->cover = (!$key && !$product_has_images) ? true : false;
		                            // file_exists doesn't work with HTTP protocol
		                            if (($field_error = $image->validateFields(UNFRIENDLY_ERROR2, true)) === true &&
		                                ($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR2, true)) === true && $image->add()) {
		                                // associate image to selected shops
		                                //$image->associateTo($shops);
		                                $products_array = array();
		                            	$success_messege = "Products have been successfully downloaded!";

		                                if (!AdminImportController::copyImg($product->id, $image->id, $url, 'products')) {
		                                    $image->delete();
		                                    $this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
		                                }

		                            } else {
		                                $import_error = 'This file can not be loaded!';
		                            }
		                        } else {
		                            $import_error = 'This file can not be loaded!';
		                        }

		                        if ($error) {
		                            $this->warnings[] = sprintf(Tools::displayError('Product #%1$d: the picture (%2$s) cannot be saved.'), $image->id_product, $url);
		                        }
		                    }
		                }

	            	}
                }
            	

            }
	    }


          $this->resulttt = array();
          $this->display_header = true;
          $this->display_footer = true;
          
  		$context = Context::getContext();
          $context = $context;
          $languages = Language::getLanguages(true, $this->context->shop->id);
          $this->htmlcat = '';
          if (!count($languages)) {
              return false;
          }
          $this->context->smarty->assign('success_messege', $success_messege);
          $this->context->smarty->assign('import_error', $import_error);
          $this->context->smarty->assign(array(
          	'products_array' => $products_array,
          	'user' => $this->context->customer
          ));
      $this->setTemplate('importcsv.tpl');
      }

    public function slugify($text)
	{
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '-');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '-', $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
	    return 'n-a';
	  }

	  return $text;
	}

	protected function addAttribute($attributes, $price = 0, $weight = 0)
    {
        foreach ($attributes as $attribute) {
            $price += (float) preg_replace('/[^0-9.-]/', '', str_replace(',', '.', Tools::getValue('price_impact_' . (int) $attribute)));
            // $weight += (float) preg_replace('/[^0-9.]/', '', str_replace(',', '.', Tools::getValue('weight_impact_' . (int) $attribute)));
            $weight += 0;
        }
        if ($this->product->id) {
            return array(
                'id_product' => (int) $this->product->id,
                'price' => (float) $price,
                'weight' => (float) $weight,
                'ecotax' => 0,
                'quantity' => 1000, ///(int) Tools::getValue('quantity'),
                'reference' => $this->product->reference ? pSQL($this->product->reference) : 'W' . str_pad($this->product->id, 6, '0', STR_PAD_LEFT), // pSQL($_POST['reference']),
                'default_on' => 0,
                'available_date' => '0000-00-00'
            );
        }
        return array();
    }
    protected static function createCombinations($list)
    {
        if (count($list) <= 1) {
            return count($list) ? array_map(create_function('$v', 'return (array($v));'), $list[0]) : $list;
        }
        $res   = array();
        $first = array_pop($list);
        foreach ($first as $attribute) {
            $tab = self::createCombinations($list);
            foreach ($tab as $to_add) {
                $res[] = is_array($to_add) ? array_merge($to_add, array(
                    $attribute
                )) : array(
                    $to_add,
                    $attribute
                );
            }
        }
        return $res;
    }

    protected static function setAttributesImpacts($id_product, $tab)
    {
        $attributes = array();
        foreach ($tab as $group) {
            foreach ($group as $attribute) {
                $price = preg_replace('/[^0-9.]/', '', str_replace(',', '.', Tools::getValue('price_impact_'.(int)$attribute)));
                // $weight = preg_replace('/[^0-9.]/', '', str_replace(',', '.', Tools::getValue('weight_impact_'.(int)$attribute)));
                $weight = 0.0;
                $attributes[] = '('.(int)$id_product.', '.(int)$attribute.', '.(float)$price.', '.(float)$weight.')';
            }
        }

        return Db::getInstance()->execute('
		INSERT INTO `'._DB_PREFIX_.'attribute_impact` (`id_product`, `id_attribute`, `price`, `weight`)
		VALUES '.implode(',', $attributes).'
		ON DUPLICATE KEY UPDATE `price` = VALUES(price), `weight` = VALUES(weight)');
    }

    private function buildCombinations($attribute_group_name, array $prices_array)
    {
    	if (empty($prices_array) || !$this->product->id) {
    		return;
    	}
    	$attribute_group_name = Tools::strtolower(trim($attribute_group_name));
        $selected_attribues = array();
        $attribute_gorup_id = 0;
        if ($attribute_group_name == 'size') {
        	$attribute_gorup_id = 1;
        	$attributesList = array();
        } elseif ($attribute_group_name == 'weight') {
        	$attribute_gorup_id = 4;
        	$attributesList = Attribute::getWeightAttribues();
        }

        $selected_attribues = array_keys($prices_array);

        foreach ($prices_array as $id_attribute => $price) {
        	$_POST['price_impact_' . $id_attribute] = (float)$price - (float)$this->product->price;
        }

		// if (in_array(Tools::getValue('attribute_unit'), array('oz', 'g'))) {
		// 	$attribute_unit = Tools::getValue('attribute_unit');
		// 	foreach ($attributesList[$attribute_unit] as $attr) {
		// 		if (isset($prices_array[$attr['id_attribute']]) && $prices_array[$attr['id_attribute']]) {
		// 			$selected_attribues[] = $attr['id_attribute'];
		// 			$_POST['price_impact_' . $attr['id_attribute']] =
		// 				(float)preg_replace('/[^0-9.-]/', '', str_replace(',', '.', $prices_array[$attr['id_attribute']])) -
		// 				(float)$this->product->price;
		// 		}
		// 	}
		// }
		$this->product->attribute_unit = $this->attribute_unit; //Tools::getValue('attribute_unit', '');
		$this->product->attrinute_prices = $prices_array;
		$this->product->save();

		if ($this->product->attribute_unit == 'item') {
			SpecificPriceRule::disableAnyApplication();
			$this->product->deleteProductAttributes();
			SpecificPriceRule::enableAnyApplication();
            SpecificPriceRule::applyAllRules(array((int)$this->product->id));
			return;
		}

		if ($selected_attribues) {
			self::setAttributesImpacts($this->product->id, array($selected_attribues));
			$combinations = array_values(self::createCombinations(array($selected_attribues)));
			$values = array_values(array_map(array($this, 'addAttribute'), $combinations));
			// echo "<pre>"; print_r($combinations); echo "</pre>";
			// echo "<pre>"; print_r($values); echo "</pre>";
			// exit;
			// @since 1.5.0
            if ($this->product->depends_on_stock == 0) {
                $attributes = Product::getProductAttributesIds($this->product->id, true);
                foreach ($attributes as $attribute) {
                    StockAvailable::removeProductFromStockAvailable($this->product->id, $attribute['id_product_attribute'], Context::getContext()->shop);
                }
            }

            SpecificPriceRule::disableAnyApplication();

            $this->product->deleteProductAttributes();
            $this->product->generateMultipleCombinations($values, $combinations);

            // Reset cached default attribute for the product and get a new one
            Product::getDefaultAttribute($this->product->id, 0, true);
            Product::updateDefaultAttribute($this->product->id);

            // @since 1.5.0
            if ($this->product->depends_on_stock == 0) {
                $attributes = Product::getProductAttributesIds($this->product->id, true);
                // $quantity = (int)Tools::getValue('quantity', Tools::getValue('qt'));
                $quantity = 10000;
                foreach ($attributes as $attribute) {
                    if (Shop::getContext() == Shop::CONTEXT_ALL) {
                        $shops_list = Shop::getShops();
                        if (is_array($shops_list)) {
                            foreach ($shops_list as $current_shop) {
                                if (isset($current_shop['id_shop']) && (int)$current_shop['id_shop'] > 0) {
                                    StockAvailable::setQuantity($this->product->id, (int)$attribute['id_product_attribute'], $quantity, (int)$current_shop['id_shop']);
                                }
                            }
                        }
                    } else {
                        StockAvailable::setQuantity($this->product->id, (int)$attribute['id_product_attribute'], $quantity);
                    }
                }
            } else {
                StockAvailable::synchronize($this->product->id);
            }

            SpecificPriceRule::enableAnyApplication();
            SpecificPriceRule::applyAllRules(array((int)$this->product->id));
		}
    }

    private function parse_attribute_values($attribute_values = '')
    {
    	$attribute_values = explode(',', trim($attribute_values));
    	$list = array();
    	foreach ($attribute_values as $k => $v) {
    		$v = explode(':', trim($v));
    		if (isset($v[0])) {
    			$v[0] = Tools::strtolower(trim($v[0]));
    			if ($this->attribute_unit == '' && strpos($v[0], 'oz') !== false) {
    				$this->attribute_unit = 'oz';
    			} elseif ($this->attribute_unit == '' && strpos($v[0], 'g') !== false) {
    				$this->attribute_unit = 'g';
    			}
    		} else {
    			continue;
    		}
    		if (isset($v[1])) {
    			$v[1] = (float)preg_replace('/[^0-9.]/', '', str_replace(',', '.', trim($v[1])));
    		} else {
    			$v[1] = 0.0;
    		}
    		$list[$v[0]] = $v;
    	}
    	// echo "<pre>"; print_r($list); echo "</pre>";
    	return $list;
    }

    private function build_prices_array(array $attribute_values)
    {
    	if (empty($attribute_values)) {
    		return array();
    	}
    	$list = array();
    	foreach ($attribute_values as $v) {
    		// search attribute in database
    		$id_attribute = (int)Db::getInstance()->getValue(
    			"SELECT id_attribute FROM "._DB_PREFIX_."attribute_lang WHERE LOWER(TRIM(name)) = \"".addslashes($v[0])."\""
    		);
    		if ($id_attribute) {
    			$list[$id_attribute] = $v[1];
    		}
    	}
    	// echo "<pre>"; print_r($list); echo "</pre>";
    	return $list;
    }
}
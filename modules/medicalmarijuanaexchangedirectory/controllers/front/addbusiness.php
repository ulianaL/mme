<?php


class MedicalmarijuanaexchangedirectoryAddbusinessModuleFrontController extends ModuleFrontController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
    }
    
    
    public function initContent()
    {
        parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }


        
        if (Context::getContext()->customer->isLogged() && Context::getContext()->customer->bussiness == 1) {
            
            Tools::redirect('index');
        } else {
            
                

        }
        
        
        
        if(isset($_POST['SubmitLogin'])){
            // login !!
            $Aut_obj = New AuthController();
            $Aut_obj->postProcess();
    
        }
        
        if(isset($_POST['submitAccount'])){
            // login !!
            $Aut_obj = New AuthController();
            $Aut_obj->postProcess();
        }
        
        
        /*
        print "<pre>";
        print_r($_POST);
        print "</pre>";
        
        */
        
        
        $this->context->smarty->assign('testing', 'test');
        $this->setTemplate('addbusiness.tpl');
            
            
        $this->context->smarty->assign(array(
            'years' => Tools::dateYears(),
            'months' => Tools::dateMonths(),
            'days' => Tools::dateDays()
        ));
    }

    public function setMedia()
    {
        parent::setMedia();

        // $jquery_ui_files = Media::getJqueryUIPath('ui.datepicker', 'base', true);

        // foreach ($jquery_ui_files['js'] as $j) {
        //     $this->addJS($j);
        // }

        // foreach ($jquery_ui_files['css'] as $css => $media) {
        //     $this->addCSS($css, $media);
        // }

        // $this->addJS('https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js', false);
        $this->addJS('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js', false);
        // $this->addCSS('https://netdna.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css', 'all', null, false);
        // $this->addCSS('https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css', 'all', null, false);
    }
}

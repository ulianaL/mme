<?php
class MedicalmarijuanaexchangedirectoryEditcampaignModuleFrontController extends ModuleFrontController
{
	public function __construct()
    {

        parent::__construct();

        $this->display_column_left = false;

    }
    public function initContent()
    {
        parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }
		
		$customer_id = Context::getContext()->customer->id;
		
		if (!Context::getContext()->customer->isLogged()) {
			Tools::redirect('authentication?back=free');
		}
		$campaign_id = $_GET['campaign'];

		if(isset($_POST['updatecampaign'])){
			$camp_obj = new Camp($campaign_id);

			
			if(isset($_POST['campaign_clicks'])){

				$camp_obj->used = $_POST['campaign_clicks'];
				$camp_obj->avaliable = $_POST['campaign_clicks'];

				$banner = new Banner($camp_obj->id_banner);
				$price = 0;
				if ($banner->size == "5" || $banner->size == "6") {

					if ($camp_obj->type == "1") {

						$price = $_POST['campaign_clicks'] * Configuration::get('CPC_PRODUCT_BANNER');

					}elseif ($camp_obj->type == "2") {

						$price = $_POST['campaign_clicks'] * Configuration::get('CPM_PRODUCT_BANNER');

					}

				}else{

					if ($camp_obj->type == "1") {

						$price = $_POST['campaign_clicks'] * Configuration::get('CPC_STANDART_BANNER');

					}elseif ($camp_obj->type == "2") {

						$price = $_POST['campaign_clicks'] * Configuration::get('CPM_STANDART_BANNER');
						
					}
				}

				$customer = new Customer($camp_obj->customer);
				$customer->points = $customer->points - $price;
				$customer->update();
			}
			$camp_obj->name = $_POST['campaign_title'];
			$camp_obj->url = $_POST['campaign_link'];
			$camp_obj->update();
			
		}
		$camp_obj = new Camp;
		$campaign = $camp_obj->getcamp("WHERE customer=".(int)Context::getContext()->customer->id." AND id_camp=".$campaign_id);
		$banner = new Banner($campaign[0]['id_banner']);
		$price = 0;
	
		if ($banner->size == "5" || $banner->size == "6") {
			if ($campaign[0]['type'] == "1") {

				$price = Configuration::get('CPC_PRODUCT_BANNER');

			}elseif ($campaign[0]['type'] == "2") {

				$price = Configuration::get('CPM_PRODUCT_BANNER');

			}
			
		}else{
			if ($campaign[0]['type'] == "1") {

				$price = Configuration::get('CPC_STANDART_BANNER');

			}elseif ($campaign[0]['type'] == "2") {

				$price = Configuration::get('CPM_STANDART_BANNER');
				
			}
		}
		$this->context->smarty->assign('price', $price);
		$this->context->smarty->assign('campaign', $campaign[0]); 
		$this->setTemplate('editcampaign.tpl');
			
    } 

}

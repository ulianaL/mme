<?php


class MedicalmarijuanaexchangedirectoryEditstoriesModuleFrontController extends ModuleFrontController
{
	public function __construct()
    {

        parent::__construct();

        $this->display_column_left = false;

    }
    public function initContent()
    {
        parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }
		
		$customer_id = Context::getContext()->customer->id;
		
		if (!Context::getContext()->customer->isLogged()) {
			Tools::redirect('authentication?back=free');
		}
		$story_id = $_GET['story'];

		if(isset($_POST['story_title'])){
			$story_obj = new Sponsoredstories($story_id);
			$target_dir = _PS_MODULE_DIR_ . "/medicalmarijuanaexchangedirectory/storyimg/";

			if($_FILES["fileToUpload"]["error"] == 0 ){
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					 $errors[] = "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
				} else {
					//$uniquename = time().'.mp4';
					$uniquename = time().'.'.pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
					
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],  $target_dir.$uniquename)) {
					   $errors[] = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
					   $story_obj->image = $uniquename;
					} else {
						 $errors[] = "Sorry, there was an error uploading your file.";
					}

						
				}
			}
			if(isset($_POST['story_clicks'])){

				$story_obj->used = $_POST['story_clicks'];
				$story_obj->avaliable = $_POST['story_clicks'];

				$price = $_POST['story_clicks'] * Configuration::get('CPC_SPONSORED_STORIES');
				$customer = new Customer($story_obj->customer);
				$customer->points = $customer->points - $price;
				$customer->update();
			}
			$story_obj->title = $_POST['story_title'];
			$story_obj->description = $_POST['add_description'];
			$story_obj->update();
			
		}
		$stories_obj = new Sponsoredstories;
		$story = $stories_obj->getStory("WHERE customer=".(int)Context::getContext()->customer->id." AND id_sponsoredstories=".$story_id);
		$story_cost = Configuration::get('CPC_SPONSORED_STORIES');

		$this->context->smarty->assign('story_cost', $story_cost);
		$this->context->smarty->assign('story', $story); 
		
		$this->setTemplate('editstories.tpl');
			
    } 

}

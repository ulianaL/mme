<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{magnalister}prestashop>magnalister_666311f430c7791d0c7d2e3eeb138d23'] = 'magnalister';
$_MODULE['<{magnalister}prestashop>magnalister_67935e2056976693feaf0653726c288e'] = 'Verbinde Deinen PrestaShop über magnalister spielend einfach mit Marktplätzen wie eBay, Amazon, Rakuten oder Hitmeister ';
$_MODULE['<{magnalister}prestashop>configure_666311f430c7791d0c7d2e3eeb138d23'] = 'magnalister';
$_MODULE['<{magnalister}prestashop>configure_6dcf6c728c1cfd0a73880ce2b8f3f892'] = 'PrestaShop official Marketplace partner';
$_MODULE['<{magnalister}prestashop>configure_659b9b6c975f647e41a842f94b654d7b'] = '//www.youtube.com/embed/jzpJK_nmJu8';
$_MODULE['<{magnalister}prestashop>configure_e98eed5d8fed809d9d07b0ae383482a8'] = 'Kernfunktionen sind: Produkt-Upload, Bestellimport, Preis-/Lagersynchronisation und Bestellstatus-Abgleich (\"Versendet\"). Dazu gibt es viele weitere Features.';
$_MODULE['<{magnalister}prestashop>configure_6f1f95fca2766c15b44fafa0951bb123'] = 'magnalister in vollem Umfang 30 Tage kostenlos testen : Sofort nach der Anmeldung und Shop-Registrierung erhalten Sie einen persönlichen Aktivierungsschlüssel (Passphrase), mit dem Sie die Software aktivieren und in vollem Umfang testen können. Während der 30-tägigen Testphase entstehen keinerlei Kosten.';
$_MODULE['<{magnalister}prestashop>configure_7510d6cf2ab2a309e923a488424a9f22'] = 'Das Zertifikat stellt mindestens diese 5 Garantien für den Verbraucher zur Verfügung:';
$_MODULE['<{magnalister}prestashop>configure_9431a48354486c3121e7945303771fd5'] = 'Verkaufe mehr als jemals zuvor: 78% aller Online-Kaufentscheidungen werden auf Marktplätzen wie eBay, Amazon, Rakuten, Hitmeister oder Cdiscount getroffen.';
$_MODULE['<{magnalister}prestashop>configure_128f7f889e49cf1a63f0f53ac73b56e2'] = 'Sie sind die wichtigsten Vertriebskanäle und erhöhen deine Verkäufschancen drastisch.';
$_MODULE['<{magnalister}prestashop>configure_ec74a708516044b61a6bd48f121f0dd5'] = 'In Verbindung mit dem magnalister-Plugin wird Dein Web-Shop gleichzeitig zu einem Multi-Channel-Vertriebskanal, Bestellverwaltungs- und Inventar-Verwaltungs-Tool.';
$_MODULE['<{magnalister}prestashop>configure_e767b1137979cbceea023dab78c9864b'] = 'https://www.magnalister.com/de/prestashop-schnittstelle-magnalister/?campaign=PrestaShopAddonsDE';
$_MODULE['<{magnalister}prestashop>configure_97c28047f3f8085e557c5e31219df0e6'] = 'Entdecke magnalister';

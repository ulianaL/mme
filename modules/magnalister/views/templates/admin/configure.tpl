{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row moduleconfig-header">
		<div class="col-xs-6 text-center">
			<img id="header_logo" src="{$module_dir|escape:'html':'UTF-8'}/views/img/header_logo.png" alt="{l s='magnalister' mod='magnalister'}" />
			<br><h4>{l s='PrestaShop official Marketplace partner' mod='magnalister'}</h4>
		</div>
		<div class="col-xs-6 text-center">
			<span class="items-video-promotion"><object type="text/html" data="{l s='//www.youtube.com/embed/I1m1Qj3Pgy8' mod='magnalister'}" width="400" height="225"></object></span>
		</div>
	</div>
	<hr />
	<div class="moduleconfig-content">
		<div class="row">
			<div class="col-xs-12">
				<p>
					<h4><strong>{l s='Upload products, import orders or synchronize stock and prices automatically.' mod='magnalister'}</strong></h4><br>
					</h4>{l s='Free 30-day trial! : Immediately after the notification and shop-registration, you will receive your personal unique activation key (pass phrase). With this key you can activate and use the software to it\'s full extent. No additional or further costs are incurred during the 30 days test period' mod='magnalister'}</h4><br><br>
					</h4>{l s='The certificate provides at least these 5 guarantees to the consumer:' mod='magnalister'}</h4>
						<li>{l s='Sell more than ever :78% of all online-buyer-decisions are made on marketplaces like eBay, Amazon, Rakuten, PriceMinister or Cdiscount' mod='magnalister'}</li>
						<li>{l s='These are the most important distribution channels to increase your sales dramatically.' mod='magnalister'}</li>
						<li>{l s='Use your Prestashop in combination with the magnalister plugin and make your store a distribution, inventory- and order-management-system.' mod='magnalister'}</li>
					</ul>
				</p>
				<br />
				<p class="col-md-12 text-center">
					<a href="{l s='https://www.magnalister.com/en/prestashop-interface-magnalister/?campaign=PrestaShopAddonsENG' mod='magnalister'}" target="_blank" class="btn btn-primary" id="create-account-btn">{l s='Discover Magnalister' mod='magnalister'}</a>
				</p>
			</div>
		</div>
	</div>
</div>

<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Allgooglefonts extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'allgooglefonts';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Luis Leitao';
        $this->need_instance = 0;
        $this->module_key = 'c908f4c821aaa1f8d493d9499f850ee8';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('All Google Fonts');
        $this->description = $this->l('All Google Fonts All Google Fonts');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        $this->updategooglefonts();
        Configuration::updateValue('ALLGOOGLEFONTS_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('ALLGOOGLEFONTS_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitAllgooglefontsModule')) == true) {
            $this->postProcess();
        }
        

        $file = __FILE__ .'.json';
        $current = Tools::file_get_contents($file);
        $this->fonts = Tools::jsonDecode($current, true);

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('fonts', $current);
        $this->context->smarty->assign('active', Configuration::get('fontt'));

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        
        if (isset($this->succ)) {
            return $this->succ.$output.$this->renderForm();
        } else {
            return $output.$this->renderForm();
        }
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitAllgooglefontsModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {

        $options = $this->fonts;

        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                   
                    array(
                          'type' => 'select',                              // This is a <select> tag.
                          'label' => $this->l('Google font:'),         // The <label> for this <select> tag.
                          'desc' => $this->l('Choose Google font to use in your shop.'),
                          'name' => 'fontt',
                          'required' => true,                              // If set to true, this option must be set.
                          'options' => array(
                            'query' => $options,                           // $options contains the data itself.
                            'id' => 'id',
                            'name' => 'family'
                          )
                    ),
                    
                    

                    
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'ALLGOOGLEFONTS_a' => Configuration::get('ALLGOOGLEFONTS_a'),
            'fontt' => Configuration::get('fontt'),
           
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
 
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
        
        $file = __FILE__ .'.json';
        $current = Tools::file_get_contents($file);
        $this->fonts = Tools::jsonDecode($current, true);
        $idactive = Configuration::get('fontt');
        $finalurl = $this->fonts[$idactive]['files']['regular'];
        Configuration::updateValue('activefonturl', $finalurl);
        $this->succ = $this->displayConfirmation($this->l('Settings updated'));
        
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }


    public function hookDisplayHeader()
    {
       
        $this->smarty->assign('activefonturl', Configuration::get('activefonturl'));
        return $this->display(__FILE__, '/views/templates/front/front.tpl');
        
    }
    public function updategooglefonts()
    {
        $API_KEY = 'AIzaSyA2hUD8aj_F_4NdoVwbRVMBJSB441E3UPw';
        $request = Tools::file_get_contents("https://www.googleapis.com/webfonts/v1/webfonts?key=".$API_KEY);
        $array = Tools::jsonDecode($request, true);
        
        $newarray = array();
        $i = 0;
        foreach ($array['items'] as $arr) {
            $newarray[$i] = $arr;
            $newarray[$i]['id'] = $i;
            $i++;
        }
        
        $request  = Tools::jsonEncode($newarray);

        $file = __FILE__ .'.json';
        file_put_contents($file, $request);
                
    }
}

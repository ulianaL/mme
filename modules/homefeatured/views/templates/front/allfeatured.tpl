<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/css-stars.css" />

{if $cat_id == 6}
	<div class="lnebg clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Featured Brands</span>
		</a>
	</div>
	</div>
	{if isset($products6) && $products6}
		{include file="$tpl_dir./product-list6.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow"  style="display:block;">
		<li class="alert alert-info">{l s='No Featured Brands at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 4}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Dispensary Storefronts</span>
		</a>
	</div>
	</div>
	{if isset($products4) && $products4}
		{include file="$tpl_dir./product-list4.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
		<li class="alert alert-info">{l s='No Dispensary Storefronts at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 3}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Delivery</span>
		</a>
	</div>
	</div>
	{if isset($products3) && $products3}
		{include file="$tpl_dir./product-list3.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
		<li class="alert alert-info">{l s='No Delivery at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 2}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Deals Nearby</span>
		</a>
	</div>
	</div>
	{if isset($products2) && $products2}
		{include file="$tpl_dir./product-list2.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;">
		<li class="alert alert-info">{l s='No Deals Nearby at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 1}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Deals</span>
		</a>	
	</div>
	</div>

	{if isset($products1) && $products1}
		{include file="$tpl_dir./product-list1.tpl" class='homefeatured tab-pane' id='homefeatured'}
	{else}
	<ul id="homefeatured" class=" tab-pane forceshow" style="display:block;">
		<li class="alert alert-info">{l s='No Deals at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 5}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Doctors</span>
		</a>
	</div>
	</div>
	{if isset($products5) && $products5}
		{include file="$tpl_dir./product-list5.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
		<li class="alert alert-info">{l s='No Doctors at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{elseif $cat_id == 7}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Medical</span>
		</a>
	</div>
	</div>
	{if isset($products7) && $products7}
		{include file="$tpl_dir./product-list7.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
		<li class="alert alert-info">{l s='No Medical at this time.' mod='homefeatured'}</li>
	</ul>
{/if}
{elseif $cat_id == 8}
	<div class="lnebg   clearfix">
	<div class="hdnam" >
		<a href="#">
			<span style="font-size:25px">Events</span>
		</a>
	</div>
	</div>
	{if isset($products8) && $products8}
		{include file="$tpl_dir./product-list8.tpl" class='homefeatured ' id='homefeatured'}
	{else}
	<ul id="homefeatured" class="homefeatured forceshow" style="display:block;" >
		<li class="alert alert-info">{l s='No Events at this time.' mod='homefeatured'}</li>
	</ul>
	{/if}
{/if}
<style>
.forceshow{
display:block!important;
}


ul#homefeatured {
display:block!important;

}
.img-responsive{
	width: 100%;
}
ul.product_list.grid > li.first-in-line {
    clear: none;
}
</style>
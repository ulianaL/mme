<?php


class homefeaturedAllFeaturedModuleFrontController extends ModuleFrontController
{
	public function init()
	{
		$this->page_name = 'allfeatured';
		$this->display_column_left = false;
		parent::init();
	} 

    public function initContent()
    {
        parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }
        if (isset($_GET['allfeatured'])) {
        	$cat_id = $_GET['allfeatured'];
        }
        else{
        	$cat_id = 999999999999999;
        }

        $category = new Category((int)Configuration::get('HOME_FEATURED_CAT'), (int)Context::getContext()->language->id);

		$products6 = $category->getProductshome($cat_id,(int)Context::getContext()->language->id, 1, (999999999), null, null, false, true, true, (999999999),true,null);


			$this->context->smarty->assign(
				array(
					'products6' => $products6,
					'cat_id' => $cat_id,
				)
			);
           
            $this->context->smarty->assign(
                array(
                    'products' => $category->getProductshome(999999999999999,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products1' => $category->getProductshome(1,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products2' => $category->getProductshome(2,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products3' => $category->getProductshome(3,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products4' => $category->getProductshome(4,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products5' => $category->getProductshome(5,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products6' => $category->getProductshome(6,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products7' => $category->getProductshome(7,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'products8' => $category->getProductshome(8,(int)Context::getContext()->language->id, 1, 10000, null, null, false, true, true, 10000,true,null),
                    'cat_id' => $cat_id,
                    'ishome' => 0,
                )
            );
        $this->setTemplate('allfeatured.tpl');
	}

		
}

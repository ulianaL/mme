/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
/*
<div class="g-recaptcha" data-sitekey="6LcvmykTAAAAAJLkwc4Ss1eur1qEKZXHjCHoKIqh"></div>
*/


$(document).ready(function(){
		if(captaccount != 0 && recakey != ''){
			$(document).ajaxSuccess(function () {
				  setTimeout(function() {
					  if($( "#authentication .account_creation" ).length !=0){

							  $('#authentication .account_creation').append($('<div id="captcha_container" style="margin:15px 0;" class="google-cpatcha"></div>'));
								
									grecaptcha.render('captcha_container', {
									  'sitekey': recakey
									});
								
					  }	
				  }, 1000);
				  
			});
			$('#submitAccount').before('<div class="g-recaptcha" style="margin:15px 0;" data-sitekey="'+recakey+'"></div>');
		}

		
		
		if(captcontact != 0 && recakey != ''){
			$('#contact form .submit').before('<div class="g-recaptcha" data-sitekey="'+recakey+'"></div>');
		}
		
		
		if(captlogin != 0 && recakey != ''){
			$('#module-medicalmarijuanaexchangedirectory-adddispensaries #register-submit,#module-medicalmarijuanaexchangedirectory-adddispensaries #login-form #login-submit,#module-medicalmarijuanaexchangedirectory-addbusiness #register-submit,#module-medicalmarijuanaexchangedirectory-addbusiness #login-form #login-submit,#authentication #submit-login, #authentication #SubmitLogin, #order-opc #login_form #SubmitLogin ').before('<div style="margin-bottom: 15px;" class="g-recaptcha" data-sitekey="'+recakey+'"></div>');
				
				
				$('#order-opc #SubmitLogin').addClass('extraSubmitLogin');
				$('#order-opc #SubmitLogin').removeAttr('id');
				// LOGIN FORM SENDING
				$(document).on('click', '.extraSubmitLogin', function(e){
				
					e.preventDefault();
					var that = $(this);
					$.ajax({
						type: 'POST',
						headers: { "cache-control": "no-cache" },
						url: authenticationUrl + '?rand=' + new Date().getTime(),
						async: false,
						cache: false,
						dataType : "json",
						data: 'SubmitLogin=true&ajax=true&email='+encodeURIComponent($('#login_email').val())+'&passwd='+encodeURIComponent($('#login_passwd').val())+'&g-recaptcha-response='+encodeURIComponent($('#g-recaptcha-response').val())+'&token=' + static_token ,
						success: function(jsonData)
						{
							if (jsonData.hasError)
							{
								var errors = '<b>'+txtThereis+' '+jsonData.errors.length+' '+txtErrors+':</b><ol>';
								for(var error in jsonData.errors)
									//IE6 bug fix
									if(error !== 'indexOf')
										errors += '<li>'+jsonData.errors[error]+'</li>';
								errors += '</ol>';
								$('#opc_login_errors').html(errors).slideDown('slow');
							}
							else
							{
								// update token
								static_token = jsonData.token;
								updateNewAccountToAddressBlock(that.attr('data-adv-api'));
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							if (textStatus !== 'abort')
							{
								error = "TECHNICAL ERROR: unable to send login informations \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus;
								if (!!$.prototype.fancybox)
									$.fancybox.open([
										{
											type: 'inline',
											autoScale: true,
											minHeight: 30,
											content: '<p class="fancybox-error">' + error + '</p>'
										}
									], {
										padding: 0
									});
								else
									alert(error);
							}
						}
					});
				});

		}
		
		
		
});
{**
* Luis Leitao
*
* Module Prestashop 
*
*  Module megarecaptcha
*
*  @author    Luis Leitao    <luisleitao>
*  @copyright LuisLeitao LuisLeitao
*  @license   http://opensource.org/licenses/afl-3.0.php   Academic Free License (AFL 3.0)


*}

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
{if $MEGARECAPTCHA_PUBLIC_var != ''}
recakey = '{$MEGARECAPTCHA_PUBLIC_var|escape:'htmlall':'UTF-8'}';
{else}
recakey = '';
{/if}


{if $MEGARECAPTCHA_ACCOUNT_ENABLE_var}
captaccount = 1;
{else}
captaccount = 0;
{/if}

{if $MEGARECAPTCHA_LIVE_MODE_var}
captcontact = 1;
{else}
captcontact = 0;
{/if}

{if $MEGARECAPTCHA_LOGIN_ENABLE_var}
captlogin = 1;
{else}
captlogin = 0;
{/if}




</script>
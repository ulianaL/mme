<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blocknewsadv';
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');


$name_module = 'blocknewsadv';

if (version_compare(_PS_VERSION_, '1.5', '<')){
	require_once(_PS_MODULE_DIR_.$name_module.'/backward_compatibility/backward.php');
} else{
	$smarty = Context::getContext()->smarty;
}

include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();


include_once(dirname(__FILE__).'/blocknewsadv.php');
$obj_blocknews = new blocknewsadv();

$obj_blocknews->setSEOUrls();


$step = Configuration::get($name_module.'news_page');
$p = (int)Tools::getValue('p');

$search = Tools::getValue("search");
$is_search = 0;

### search ###
if(Tools::strlen($search)>0){
    $is_search = 1;
}

### archives ####
$year = (int)Tools::getValue("y");
$month = (int)Tools::getValue("m");
$is_arch = 0;
if($year!=0 && $month!=0){
    $is_arch = 1;
}

$start = (int)(($p - 1)*$step);
if($start<0)
    $start = 0;


$_data = $obj_blocknewshelp->getItems(array('start'=>$start,'step'=>$step,
        'is_search'=>$is_search,'search'=>$search,
        'is_arch'=>$is_arch,'month'=>$month,'year'=>$year
    )
);



$paging = $obj_blocknewshelp->PageNav($start,$_data['count_all'],$step,
    array('all_items'=>1,
        'is_search'=>$is_search,'search'=>$search,
        'is_arch'=>$is_arch,'month'=>$month,'year'=>$year
    )
);
// strip tags for content
foreach($_data['items'] as $_k => $_item){
	$_data['items'][$_k]['content'] = strip_tags($_item['content']);
}

$smarty->assign(array('posts' => $_data['items'], 
					  'count_all' => $_data['count_all'],
					  'paging' => $paging,
                        $name_module.'is_search' => $is_search,
                        $name_module.'search' => $search
					  )
				);

$smarty->assign($name_module.'rew_on', Configuration::get($name_module.'rew_on'));

$smarty->assign($name_module.'l_display_date', Configuration::get($name_module.'l_display_date'));
	

if(version_compare(_PS_VERSION_, '1.6', '>')){
 	$smarty->assign($name_module.'is16' , 1);
} else {
 	$smarty->assign($name_module.'is16' , 0);
}
				
$seo_text_data = $obj_blocknewshelp->getTranslateText();
$seo_text = $seo_text_data['seo_text'];
$smarty->assign('meta_title' , $seo_text);
$smarty->assign('meta_description' , $seo_text);
$smarty->assign('meta_keywords' , $seo_text);
				
if (version_compare(_PS_VERSION_, '1.5', '>')  && version_compare(_PS_VERSION_, '1.6', '<')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				if(version_compare(_PS_VERSION_, '1.5', '<'))
					include_once(dirname(__FILE__).'/../../header.php');
			}




if(version_compare(_PS_VERSION_, '1.5', '>')){
	
	if(version_compare(_PS_VERSION_, '1.6', '>')){
					
		$obj_front_c = new ModuleFrontController();
		$obj_front_c->module->name = 'blocknewsadv';
		$obj_front_c->setTemplate('news.tpl');
		
		$obj_front_c->setMedia();
		
		$obj_front_c->initHeader();
		
		$obj_front_c->initContent();
		
		$obj_front_c->initFooter();
		
		
		$obj_front_c->display();
		
	} else {
		echo $obj_blocknews->renderTplItems();
	}
} else {
	echo Module::display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/front/news.tpl');
}

if (version_compare(_PS_VERSION_, '1.5', '>') && version_compare(_PS_VERSION_, '1.6', '<')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				if(version_compare(_PS_VERSION_, '1.5', '<'))
					include_once(dirname(__FILE__).'/../../footer.php');
			}
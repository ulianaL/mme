{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}
 
{if $blocknewsadvis_news != 0}
        <meta property="fb:app_id" content="{$blocknewsadvappid|escape:'htmlall':'UTF-8'}" />
        <meta property="fb:admins" content="{$blocknewsadvappadmin|escape:'htmlall':'UTF-8'}"/>
       <meta property="og:title" content="{$blocknewsadvname|escape:'htmlall':'UTF-8'}"/>
       {if strlen($blocknewsadvimg) >0}
	   <meta property="og:image" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}upload/blocknewsadv/{$blocknewsadvimg|escape:'htmlall':'UTF-8'}"/>
	   {/if}
	   <meta property="og:type" content="product"/>
       <meta property="og:url" content="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$blocknewsadvnews_seo_url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$blocknewsadvnews_id|escape:'htmlall':'UTF-8'}{/if}"/>
{/if}
 {if $blocknewsadvis15==0}
 <link href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/css/blocknewsadv.css" rel="stylesheet" type="text/css" media="all" />
 <link href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/css/blocknewsadv15.css" rel="stylesheet" type="text/css" media="all" />
 <link href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/css/font-custom.min.css" rel="stylesheet" type="text/css" media="all" />
 <script type="text/javascript" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/js/blocknewsadv.js"></script>
 {/if}

{if $blocknewsadvrsson == 1}
<link rel="alternate" type="application/rss+xml" href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/rss.php" />
{/if}



{if $blocknewsadvis17 == 1}
{literal}
    <script type="text/javascript">
        //<![CDATA[
        var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
        //]]>
    </script>
{/literal}
{/if}
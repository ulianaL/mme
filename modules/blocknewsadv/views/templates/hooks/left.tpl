{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}
 
 {if $blocknewsadvleftnews == 1}
     <div id="blocknewsadvnews_block_left" class="block {if $blocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} blocknewsadv-block" >
         
             <a class="left_lastest_news" href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='blocknewsadv'}"
                     >{l s='Latest News' mod='blocknewsadv'}</a>
         <div class="block_content">
         	{if $page_name != 'module-medicalmarijuanaexchangedirectory-stock' && $page_name != 'module-medicalmarijuanaexchangedirectory-sector'}
             {if count($blocknewsadvitemsblock) > 0}
                 <div class="items-articles-block">

                     {foreach from=$blocknewsadvitemsblock item=items name=myLoop1}
                     {foreach from=$items.data item=item name=myLoop}
                     {if $item.seo_url|strstr:'http'}
                        {* <a href="{$item.seo_url}" target="_blank"> *}
                        <a href="{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}">
                     {else}
                        <a href="/news/{$item.seo_url}">
                     {/if}
                            
                                 <div class="current-item-block">
                                     {if $blocknewsadvblock_display_img == 1}
                                         {if strlen($item.img)>0}
                                             <div class="block-side">
                                                 <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|replace:'-50x50':''}"
                                                      title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />
                                                      <div class="block-side_title">{$item.title|escape:'htmlall':'UTF-8'}</div>
                                             </div>
                                         {/if}
                                     {/if}

                                 </div>
                             </a>
                         {/foreach}
                     {/foreach}
                     <p class="block-view-all">
                         <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='View all news' mod='blocknewsadv'}"
                            class="left_view_all"
                                 >{l s='View all news' mod='blocknewsadv'}</a>
                     </p>

                 </div>
             {else}
                 <div class="block-no-items">
                     {l s='News not found.' mod='blocknewsadv'}
                 </div>
             {/if}
             {else}
             {if count($fin_news) > 0}
                 <div class="items-articles-block">
                     {foreach from=$fin_news item=news}
                            {* <a href="{$news.seo_url}" target="_blank"> *}
                            <a href="{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$news.id|escape:'htmlall':'UTF-8'}" target="_blank">
                                 <div class="current-item-block">
                                     {if !empty($news.img)}
                                         <div class="block-side">
                                             <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}/upload/blockblog/{$news.img}"
                                                  title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />
                                                  <div class="block-side_title">{$news.title|escape:'htmlall':'UTF-8'}</div>
                                         </div>
                                     {/if}
                                 </div>
                             </a>
                     {/foreach}
                     <p class="block-view-all">
                         <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='View all news' mod='blocknewsadv'}"
                            class="left_view_all"
                                 >{l s='View all news' mod='blocknewsadv'}</a>
                     </p>

                 </div>
             {else}
                 <div class="block-no-items">
                     {l s='News not found.' mod='blocknewsadv'}
                 </div>
             {/if}
            {/if}
         </div>

     </div>

{/if}


{if $blocknewsadvarch_left_n == 1}
    <div id="blocknewsadvarch_block_left" class="block {if $blocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} search_items" >
        <h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase{/if}">{l s='News Archives' mod='blocknewsadv'}</h4>

        <div class="block_content{if $blocknewsadvis17 == 1}17{/if}">
            {if sizeof($blocknewsadvarch)>0}
                <ul class="bullet">
                    {foreach from=$blocknewsadvarch item=items key=year name=myarch}
                        <li><a class="arch-category" href="javascript:void(0)"
                               onclick="show_arch({$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'},'left')">{$year|escape:'htmlall':'UTF-8'}</a></li>
                        <div id="arch{$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'}left"
                             {if $smarty.foreach.myarch.first}{else}class="display-none"{/if}
                                >
                            {foreach from=$items item=item name=myLoop1}
                                <li class="arch-subcat">
                                    <a class="arch-subitem" href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}{if $blocknewsadvrew_on == 1}?{else}&{/if}y={$year|escape:'htmlall':'UTF-8'}&m={$item.month|escape:'htmlall':'UTF-8'}">
                                        {$item.time_add|date_format:"%B"|escape:'htmlall':'UTF-8'}&nbsp;({$item.total|escape:'htmlall':'UTF-8'})
                                    </a>
                                </li>
                            {/foreach}
                        </div>
                    {/foreach}
                </ul>
            {else}
                {l s='There are not Archives yet.' mod='blocknewsadv'}
            {/if}

        </div>

    </div>
{/if}

{if $blocknewsadvsearch_left_n == 1}
    <div id="blocknewsadvsearch_block_left" class="block {if $blocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} search_items" >
        <h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase{/if}">{l s='Search News' mod='blocknewsadv'}</h4>
        <form method="get" action="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            <div class="block_content{if $blocknewsadvis17 == 1}17{/if}">
                <input type="text" value="" class="search_items {if $blocknewsadvis17 == 1}search-blocknewsadv17{/if} {if $blocknewsadvis_ps15 == 0}search_text{/if}" name="search" >
                <input type="submit" value="go" class="button_mini {if $blocknewsadvis17 == 1}button-mini-blocknewsadv{/if} {if $blocknewsadvis_ps15 == 0}search_go{/if}"/>
                {if $blocknewsadvis_ps15 == 0}<div class="clear"></div>{/if}
            </div>
        </form>
    </div>
{/if}
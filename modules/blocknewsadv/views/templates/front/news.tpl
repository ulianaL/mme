{capture name=path}
{$meta_title}
{/capture}
<div class="blog-section paddingTB60 ">
	<div class="container">
		<div class="row">
			<div class="site-heading text-center">
				<h3>News</h3>

				{*<p  style="font-size: 18px;margin-bottom: 5px;">
					<strong>{l s='News' mod='blocknewsadv'} ( 
						<span id="count_items_top" style="color: #333;">
							{$count_all}
						</span> )
					</strong>
				</p>*}
				<div class="border"></div>
			</div>
		</div>
		<div class="row text-center custom_blog_list">
			{foreach from=$posts item=post name=myLoop}
				<div class="col-sm-6 col-md-4">
					<div class="blog-box">
						<div class="blog-box-image">
							{* {if strlen($post.img)>0}
								{if $post.seo_url|strstr:'http'}
								<a style="width: 100%;"  target="_blank" href="{$post.seo_url}" title="{$post.title|escape:'htmlall':'UTF-8'}">
										<img src="{$base_dir|replace:'http':'https'}upload/blocknewsadv/{$post.img}" title="" alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive imgcatbblog" style="min-width: 100%;"/>
									</a>
								{else}
									<a style="width: 100%;"  href="{$base_dir|replace:'http':'https'}news/{$post.seo_url}" title="{$post.title|escape:'htmlall':'UTF-8'}">
										<img src="{$base_dir|replace:'http':'https'}upload/blocknewsadv/{$post.img}" title="" alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive imgcatbblog" style="min-width: 100%;"/>
									</a>
								{/if}
							{/if} *}
							<a href="{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}" title="{$post.title|escape:'htmlall':'UTF-8'}" style="width: 100%;">
								<img src="{$base_dir|replace:'http':'https'}upload/blocknewsadv/{$post.img}" title="" alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive imgcatbblog" style="min-width: 100%;"/>
							</a>
						</div>
						<div class="blog-box-content">
							<h4>
								{* {if $post.seo_url|strstr:'http'}
								<a href="{$post.seo_url}" target="_blank" title="{$post.title|escape:'htmlall':'UTF-8'}">
									{$post.title|escape:'htmlall':'UTF-8':true}
								</a>
								{else}
								<a href="{$base_dir|replace:'http':'https'}news/{$post.seo_url}" title="{$post.title|escape:'htmlall':'UTF-8'}">
									{$post.title|escape:'htmlall':'UTF-8':true}
								</a>
								{/if} *}
								<a href="{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}" title="{$post.title|escape:'htmlall':'UTF-8'}">{$post.title|escape:'htmlall':'UTF-8':true}</a>
								<div class="blog_box_content_hideed">
									<p>
										{strip}
											{$post.content|truncate:250:"...":true}
										{/strip}
									</p>
									<p>
										{$post.time_add|date_format:"%B %e, %Y"}
									</p>
								</div>
							</h4>
						</div>
					</div>
				</div> <!-- End Col -->	
			{/foreach}
		</div>
	</div>
</div>

<style>
#left_column{
	display: none;
}
</style>
<script>
$(document).ready(function(){
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');
});
</script>




{******************************************************
*******************************************************
*******************************************************
*******************************************************
*******************************************************
***}

















{******************************




{if $count_all > 0}
<p class="blocnewsadv-header"><span class="bold">{l s='News' mod='blocknewsadv'}  ( {$count_all} )</span></p>

<div class="container-fluid">
<ul id="manufacturers_list" class="blocknewsadv-list row container-fluid">
{foreach from=$posts item=post name=myLoop}
	<li class="col-sm-3 blogmax">
		<div class="content-blocknew">
				<div class="left_side" style="width:100%">
			
			<div class="logo">
				{if strlen($post.img)>0}
					
					{if 2 == 1}
						<a class="lnk_img" href="{$base_dir}news/{$post.seo_url}/" 
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{else}
						<a class="lnk_img" href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{/if}
							<img src="{$base_dir}upload/blocknewsadv/{$post.img}" 
							     title="{$post.title|escape:'htmlall':'UTF-8'}" 
							      width="100%" height="auto" />
						</a>
					
				{/if}
			</div>
				<h3>
				{if 1 == 2}
					<a href="{$base_dir}news/{$post.seo_url}/" 
						   title="{$post.title|escape:'htmlall':'UTF-8'}"
							  >
				
				{else}
					<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}"
						  >
				{/if}
						{$post.title|escape:'htmlall':'UTF-8'}
					</a>
				</h3>
					
			
			<p class="description rte">
				{$post.content|substr:0:140}
				{if strlen($post.content)>140}...{/if}
				{if 1 == 2}
				<a href="{$base_dir}news/{$post.seo_url}/" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="font-size:11px;text-decoration:underline;">
				{else}
				<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="font-size:11px;text-decoration:underline;">
				{/if}
						{l s='more' mod='blocknews'}
				</a>
				<br/>
			</p>
				
			</div>
			<div class="blocknewsadv-clear"></div>
			<div class="right_side" style="margin-top:5px">
			<span class="blocknewsadv-time">{$post.time_add}</span>
			</div>
		</div>
	</li>
{/foreach}
</ul>
</div>
			
		
{$paging}
	

{else}
	<div class="blocnewsadv-noitems">
	{l s='There are not news yet' mod='blocknewsadv'}
	</div>
{/if}


*************}
<div class="row custom_blog_list">
	<div class="col-12 pagination custom_blog_list">
		{$paging}
	</div>
</div>
{*{$paging}*}



{literal}
<script type="text/javascript">
//<![CDATA[
	var baseDir = '{/literal}{$base_dir_ssl}{literal}';
//]]>
</script>
<script type="text/javascript">
	function go_page_news(page,item_id) {
		$('#manufacturers_list').css('opacity',0.5);
		$.post(baseDir + 'modules/blocknewsadv/process.php', {
			action:'pagenav',
			page : page,
			item_id : item_id
		}, 
		function (data) {
			if (data.status == 'success') {
				$('#manufacturers_list').css('opacity',1);
				$('#manufacturers_list').html('');
				$('#manufacturers_list').prepend(data.params.content);
				$('#page_nav').html('');
				$('#page_nav').prepend(data.params.page_nav);
			} else {
				$('#manufacturers_list').css('opacity',1);
				alert(data.message);
			}
		}, 'json');
	}
</script>
{/literal}

{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}


        {*<a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            {l s='News' mod='blocknewsadv'}
        </a>
        <span class="navigation-pipe">></span>
        {$meta_title|escape:'htmlall':'UTF-8'}*}


{capture name=path}
    <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
        {l s='News' mod='blocknewsadv'}
    </a>
    <span class="navigation-pipe">></span>
    {$meta_title|escape:'htmlall':'UTF-8'}
{/capture}


{if $blocknewsadvis16 == 0}
    <h2>{$meta_title|escape:'htmlall':'UTF-8'}</h2>
{else}
    <h1 class="page-heading">{$meta_title|escape:'htmlall':'UTF-8'}</h1>

    <span class="blogpostauthor">{$posts[0]['author']}</span>
    <span class="blogpostdate">{$posts[0].time_add|date_format:"%B %e, %Y"}</span>

{/if}





{if count($posts) > 0}


<div class="news-block-item">


    {foreach from=$posts item=post name=myLoop}
       

<img src="{$base_dir|replace:'http':'https'}upload/blocknewsadv/{$post.img_orig}" class="img-responsive imgcatbblog" style=""/>



	   <div class="item-page" itemscope itemtype="http{if $blocknewsadvis_ssl == 1}s{/if}://schema.org/Article">

            <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage"
                  itemid="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"

                    />

            <meta itemprop="datePublished" content="{$post.time_add_rss|escape:'htmlall':'UTF-8'}"/>
            <meta itemprop="dateModified" content="{$post.time_add_rss|escape:'htmlall':'UTF-8'}"/>
            <meta itemprop="headline" content="{$post.title|escape:'htmlall':'UTF-8'}"/>
            <meta itemprop="alternativeHeadline" content="{$post.title|escape:'htmlall':'UTF-8'}"/>

        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
             <meta itemprop="name" content="admin"/>
        </span>


        <span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg">
                <meta itemprop="width" content="600">
                <meta itemprop="height" content="60">
            </span>
            <meta itemprop="name" content="{$blocknewsadvsnip_publisher|escape:'htmlall':'UTF-8'}">
        </span>

            <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                {if strlen($post.img)>0}
                    <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'}">
                    <meta itemprop="width" content="{$blocknewsadvsnip_width|escape:'htmlall':'UTF-8'}">
                    <meta itemprop="height" content="{$blocknewsadvsnip_height|escape:'htmlall':'UTF-8'}">

                {else}
                    <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg"/>
                    <meta itemprop="width" content="600">
                    <meta itemprop="height" content="60">
                {/if}
            </div>

            <meta itemprop="description" content="{$post.content|strip_tags|substr:0:140|escape:'htmlall':'UTF-8'}"/>

            
			
			{*****if strlen($post.img)>0}
                <div class="image">
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'}"
                         alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive" />
                </div>
            {/if******}


            <div class="top-item">
                <h1>{$meta_title|escape:'html':'UTF-8'}</h1>
                <!-- <div class="clear"></div> -->
                {if $blocknewsadvi_display_date == 1}
                   {* <p class="float-left">
                        <time datetime="{$post.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}" pubdate="pubdate"
                                ><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$post.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</time>
                    </p>*}
                {/if}

              {*  <p class="float-right">


                    {if $blocknewsadvis_like == 1}
                        {if $post.is_liked_news}
                            <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)
                        {else}
                            <span class="block-item-like-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                        {/if}
                    {/if}
                    &nbsp;
                    {if $blocknewsadvis_unlike == 1}
                        {if $post.is_unliked_news}
                            <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                        {else}
                            <span class="block-item-unlike-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                        {/if}
                    {/if}




                </p> *}
                <!-- <div class="clear"></div> -->



            </div>



            {* <!-- {$post.content|replace:'<p> </p>':''|nl2br nofilter} --> *}
            {* <!-- {$post|print_r} --> *}

            {assign var="post_content" value=$post.content|replace:'<p> </p>':''|nl2br}
            <div class="body-post">
                {* {$post.content|strip|replace:'<p> </p>':'' nofilter} *}
                <p>{$post_content|truncate:1000:'...' nofilter}</p>
                <br/>
                <p>
                    <i style="font-style: italic;">{if strlen($post_content) > 1000}{l s='- read the rest of the article'}{else}{l s='- read the original article'}{/if} <a href="{$post.seo_url}" style="color: #59b849;" target="_blank">{l s='here'}</a></i>
                </p>
            </div>





            {if $blocknewsadvis_soc_but == 1}
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <div class="fb-like" data-href="http{if $blocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                     data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>
                            </td>
                            <td>
                                <!-- <a href="http{if $blocknewsadvis_ssl == 1}s{/if}://twitter.com/share" class="twitter-share-button"
                                   data-url="http{if $blocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                   data-counturl="http{if $blocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                   data-via="{$posts[0]['hashtags']} {$posts[0]['mention_name']}"
                                   data-count="horizontal">Tweet</a> -->

                                   <a href="http://twitter.com/?status={$meta_title|escape:'htmlall':'UTF-8'}%0A%0Ahttp{if $blocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'} {$posts[0]['hashtags']|escape:'url'} {$posts[0]['mention_name']|escape:'url'}" rel="nofollow" target="_blank" title="Twitter">
                           <img  class="tweet_img" src="{$img_dir}social/tweet.png" alt="Twitter">
                    </a>
                            </td>
                            <td>
                                <!-- Place this render call where appropriate -->
                                {literal}
                                    <script type="text/javascript">
                                        (function() {
                                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                        })();
                                    </script>
                                {/literal}

                                <div class="g-plusone" data-size="medium" href="http{if $blocknewsadvis_ssl == 1}s{/if}://$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}" data-count="true"></div>

                            </td>
                            <td>
                                {literal}
                                <script type="IN/Share" data-url="http{if $blocknewsadvis_ssl == 1}s{/if}://{/literal}{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}{literal}"
                                        data-counter="right"></script>
                                {/literal}
                            </td>
                            <td>
                                <a href="http{if $blocknewsadvis_ssl == 1}s{/if}://pinterest.com/pin/create/button/?url=http{if $blocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}&media={if strlen($post.img)>0}{$base_dir_ssl|escape:'htmlall':'UTF-8'}upload/blocknewsadv/{$post.img|escape:'htmlall':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg{/if}&description={$meta_title|escape:'htmlall':'UTF-8'}"
                                   class="pin-it-button" count-layout="horizontal">
                                    <img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                            </td>
                        </tr>
                    </table>

                {if $blocknewsadvis_comments == 0}
                {if $post.is_comments == 1}
                    <div id="fb-root"></div>
                    {literal}

                        <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/{/literal}{$blocknewsadvlng_iso|escape:'htmlall':'UTF-8'}{literal}/all.js#xfbml=1&appid={/literal}{$blocknewsadvappid|escape:'htmlall':'UTF-8'}{literal}";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                    {/literal}
                {/if}
                {/if}

                {literal}

                        <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
                        <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
                        <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
                    {/literal}
                
            {/if}


        </div>
    {/foreach}




{if count($related_products)>0}
<div class="rel-products-block">
<div class="related-products-title"><i class="fa fa-book fa-lg"></i>&nbsp;{l s='Related Products' mod='blocknewsadv'}</div>
<div class="clear"></div>

<ul>
	{foreach from=$related_products item=product name=myLoop}
	<li class="clearfix">
					{if $product.picture}
					<a title="{$product.title|escape:'htmlall':'UTF-8'}" href="{$product.product_url|escape:'htmlall':'UTF-8'}" class="products-block-image">
						<img alt="{$product.title|escape:'htmlall':'UTF-8'}" src="{$product.picture|escape:'htmlall':'UTF-8'}">
					</a>
					{/if}
					<div class="clear"></div>
					<div class="product-content">
						<h5>
							<a title="{$product.title|escape:'htmlall':'UTF-8'}" href="{$product.product_url|escape:'htmlall':'UTF-8'}" 
								class="product-name">
								{$product.title|escape:'htmlall':'UTF-8'}
							</a>
						</h5>
						<p class="product-description">{$product.description|strip_tags|substr:0:$blocknewsadvitem_rp_tr|escape:'htmlall':'UTF-8'}{if strlen($product.description)>$blocknewsadvitem_rp_tr}...{/if}</p>
					</div>
				</li>
		{/foreach}
		
</ul>

<div class="clear"></div>
</div>
{/if}





    {if count($related_posts)>0}
    <div class="rel-posts-block">

        <div class="related-posts-title"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;{l s='Related News' mod='blocknewsadv'}</div>

        <div class="other-posts">


            <ul class="row-custom">
                {foreach from=$related_posts item=relpost name=myLoop}
                   {if $smarty.foreach.myLoop.index%3 == 0}<div class="clear"></div>{/if}
                    <li class="col-sm-4-custom">
                        {if strlen($relpost.img)>0}
                            <div class="block-top">
                                <a title="{$relpost.title|escape:'htmlall':'UTF-8'}"
                                   href="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.id|escape:'htmlall':'UTF-8'}{/if}"
                                        >
                                    <img alt="{$relpost.title|escape:'htmlall':'UTF-8'}" class="img-responsive"
                                         src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$relpost.img|escape:'htmlall':'UTF-8'}">
                                </a>
                            </div>
                        {/if}


                        <div class="block-content"><h4 class="block-heading">
                                <a title="{$relpost.title|escape:'htmlall':'UTF-8'}"
                                   href="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.id|escape:'htmlall':'UTF-8'}{/if}"

                                        >{$relpost.title|escape:'htmlall':'UTF-8'}</a></h4></div>
                        <div class="block-footer">
                            <p class="float-left">
                                <time pubdate="pubdate" datetime="{$relpost.time_add|date_format:"%m/%d/%Y"|escape:'htmlall':'UTF-8'}"
                                        >{*<i class="fa fa-clock-o fa-lg"></i>&nbsp;&nbsp;*}{$relpost.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}
                                </time>
                            </p>
                           {* <p class="float-right comment">


                                {if $blocknewsadvis_like == 1}
                                    {if $relpost.is_liked_news}
                                        <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_like|escape:'htmlall':'UTF-8'}</span>)
                                    {else}
                                        <span class="block-item-like-{$relpost.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$relpost.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                    {/if}
                                {/if}
                                &nbsp;
                                {if $blocknewsadvis_unlike == 1}
                                    {if $relpost.is_unliked_news}
                                        <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                    {else}
                                        <span class="block-item-unlike-{$relpost.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$relpost.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                    {/if}
                                {/if}






                            </p>*}

                            <div class="clear"></div>
                        </div>
                    </li>
                {/foreach}

            </ul>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>


    </div>
    {/if}

    {if $blocknewsadvis_comments == 1}
    {if $post.is_comments == 0}
        <div class="blocnewsadv-noitems">
            {l s='Comments are сlosed for this post' mod='blocknewsadv'}
        </div>
    {else}
        <div class="fcomment-title"><i class="fa fa-facebook fa-lg"></i> {l s='Facebook comments' mod='blocknewsadv'}</div>

        <div class="fcomment-content">
            <div id="fb-root"></div>
            {literal}

            <script type="text/javascript">
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/{/literal}{$blocknewsadvlng_iso|escape:'htmlall':'UTF-8'}{literal}/all.js#xfbml=1&appid={/literal}{$blocknewsadvappid|escape:'htmlall':'UTF-8'}{literal}";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            {/literal}

        <div class="fb-comments" data-href="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"
             data-numposts="{$blocknewsadvnumber_fc|escape:'htmlall':'UTF-8'}" data-width="100%" data-mobile="false"></div>


        </div>

    {/if}
    {/if}


{else}
	<div class="blocnewsadv-noitems">
		{l s='There are not news yet' mod='blocknewsadv'}
	</div>
{/if}

</div>
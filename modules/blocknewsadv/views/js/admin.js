/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

function blocknewsadv_list(id,action,value,type_action){

    var text_action = '';
    var prefixid = '';
    if(type_action == 'news'){
        text_action = 'news';
    } else if(type_action == 'comment'){
        text_action = 'comment';
        prefixid = 'c';
    }


    if(action == 'active') {
        $('#activeitem'+prefixid + id).html('<img src="../img/admin/../../modules/blocknewsadv/views/img/loader.gif" />');
    }

    $.post('../modules/blocknewsadv/ajax_admin.php',
        { id:id,
            action:action,
            value: value,
            type_action: type_action
        },
        function (data) {
            if (data.status == 'success') {


                var data = data.params.content;



                if(action == 'active'){

                    $('#activeitem'+prefixid+id).html('');
                    if(value == 0){
                        var img_ok = 'ok';
                        var action_value = 1;
                    } else {
                        var img_ok = 'no_ok';
                        var action_value = 0;
                    }
                    var html = '<span class="label-tooltip" data-original-title="Click here to activate or deactivate '+text_action+' on your site" data-toggle="tooltip">'+
                            '<a href="javascript:void(0)" onclick="blocknewsadv_list('+id+',\'active\', '+action_value+',\''+type_action+'\');" style="text-decoration:none">'+
                        '<img src="../img/admin/../../modules/blocknewsadv/views/img/'+img_ok+'.gif" />'+
                        '</a>'+
                    '</span>';
                    $('#activeitem'+prefixid+id).html(html);


                }

            } else {
                alert(data.message);

            }
        }, 'json');
}







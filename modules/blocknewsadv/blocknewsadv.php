<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

class blocknewsadv extends Module{

	private $_step = 10;
	private $_is15;
	private $_iso_lng;
	private $_is_friendly_url;
	private $_is16;
    private $_is_cloud;
    private $_translate;
    private $_token_cron;
    public $is_demo = 0;

	public function __construct(){

		$this->name = 'blocknewsadv';
 	 	$this->version = '1.2.0';
 	 	$this->tab = 'content_management';
		$this->module_key = 'e12d5ba9b99d59ebd0ecda8ec7841096';
		$this->author = 'mitrocops';
		
		//if (version_compare(_PS_VERSION_, '1.5', '<')){
			require_once(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
		//}


        $this->_token_cron = md5($this->name._PS_BASE_URL_);

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $this->bootstrap = true;
            $this->need_instance = 0;
        }

        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;

        //$this->_is_cloud = 1;

        if($this->_is_cloud){
            $this->path_img_cloud = "modules/".$this->name."/upload/";
        } else {
            $this->path_img_cloud = "upload/".$this->name."/";

        }

        $this->_translate = array(
            'name'=>$this->name,
            'title'=>$this->l('Automatically posts Integration'),
            'title_pstwitterpost'=>$this->l('Facebook Wall Posts + Twitter Cards (2 in 1)'),
            'title_psvkpost'=>$this->l('Vkontakte Wall Post'),

            'buy_module_pstwitterpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Facebook Wall Posts + Twitter Cards (2 in 1) module.')
                .' '
                .$this->l('You may purchase it on:')
                .' <a href="http://addons.prestashop.com/product.php?id_product=17676" target="_blank" style="font-weight:bold;text-decoration:underline">'
                .$this->l('PrestaShop Addons').'</a>. '
                .$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'buy_module_psvkpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Vkontakte Wall Post module.')
                .' '
                .$this->l('You may purchase it on:')
                .' <a href="http://addons.prestashop.com/product.php?id_product=17731" target="_blank" style="font-weight:bold;text-decoration:underline">'
                .$this->l('PrestaShop Addons').'</a>. '
                .$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'hint1'=>$this->l('This section lets you integrate with our')
                .' <b><a href="http://addons.prestashop.com/product.php?id_product=17676" target="_blank" style="font-weight:bold;text-decoration:underline">'
                .$this->l('Facebook Wall Posts + Twitter Cards (2 in 1) module')
                .'</a></b> '.$this->l('and')
                .' <b><a href="http://addons.prestashop.com/product.php?id_product=17731" target="_blank" style="font-weight:bold;text-decoration:underline">'
                .$this->l('Vkontakte Wall Post module')
                .'</a></b> '
                .$this->l(', and allows you posted news on your PrestaShop website to be automatically posted to your')
                .' '
                .$this->l(' Facebook, Twitter, Vkontakte fan pages').'.'.' ',


            'update_button'=>$this->l('Update settings'),
            'form_action'=>Tools::safeOutput($_SERVER['REQUEST_URI']),

            'enable_psvkpost'=> $this->l('Enable Vkontakte Wall Post'),

            'enable_pstwitterpost'=> $this->l('Enable Facebook Wall Posts + Twitter Cards (2 in 1)'),

            'template_text'=>$this->l('Post template text'),

        );
		
		if(version_compare(_PS_VERSION_, '1.5', '>'))
			$this->_is15 = 1;
		else
			$this->_is15 = 0;

		if(version_compare(_PS_VERSION_, '1.6', '>')){
 	 		$this->_is16 = 1;
 	 	} else {
 	 		$this->_is16 = 0;
 	 	}
		
		parent::__construct();
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('News Advanced + Google Rich Snippets');
		$this->description = $this->l('News Advanced + Google Rich Snippets');
		$this->confirmUninstall = $this->l('Are you sure you want to remove it ? Your News Advanced will no longer work. Be careful, all your configuration and your data will be lost');
		
		$this->initContext();
		
		
		$this->_loadVariables();

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            require_once(_PS_MODULE_DIR_.$this->name.'/classes/ps17helpblocknewsadv.class.php');
            $ps17help = new ps17helpblocknewsadv();
            $ps17help->setMissedVariables();
        } else {
            $smarty = $this->context->smarty;
            $smarty->assign($this->name.'is17' , 0);
        }
        ## prestashop 1.7 ##
		
	}
	
	private function initContext()
	{
	  $this->context = Context::getContext();
	  if (version_compare(_PS_VERSION_, '1.5', '>')){
	 	 $this->context->currentindex = isset(AdminController::$currentIndex)?AdminController::$currentIndex:'index.php?controller=AdminModules';
	  } else {
	  	$variables14 = variables14_blocknewsadv();
	  	$this->context->currentindex = $variables14['currentindex'];
	  }
	}

    public function getCloudImgPath(){
        return $this->path_img_cloud;
    }
	
	private function _loadVariables(){
		
		$is_friendly_url = $this->isURLRewriting();
		$this->_is_friendly_url = $is_friendly_url;
		$this->_iso_lng = $this->getLangISO();
	}
	
	public function isURLRewriting(){
    	$_is_rewriting_settings = 0;
    	if(Configuration::get('PS_REWRITING_SETTINGS') && Configuration::get($this->name.'rew_on') == 1){
			$_is_rewriting_settings = 1;
		} 
		return $_is_rewriting_settings;
    }
    
	public function getLangISO(){
    	$cookie = $this->context->cookie;
		$id_lang = (int)$cookie->id_lang;
		if($this->isURLRewriting())
			$iso_lang = Language::getIsoById((int)($id_lang))."/";
		else
			$iso_lang = '';
			
    	return $iso_lang;
    	
    }
	
	public function install(){

		if (!parent::install()) return false;

        if(Configuration::get('PS_REWRITING_SETTINGS')){
            Configuration::updateValue($this->name.'rew_on', 1);
        }

		#### posts api ###
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		$iso = Tools::strtoupper(Language::getIsoById($i));
    		Configuration::updateValue($this->name.'twdesc_'.$i, $this->l('New news add in our shop').' '.$iso);
		}
		
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		$iso = Tools::strtoupper(Language::getIsoById($i));
    		Configuration::updateValue($this->name.'vkdesc_'.$i, $this->l('New news add in our shop').' '.$iso);
		}
		#### posts api ###
		
		// only for prestashop 1.3, 1.4, 1.5
		Configuration::updateValue($this->name.'switch_s', 0);
        // only for prestashop 1.3, 1.4, 1.5



        Configuration::updateValue($this->name.'block_display_img', 1);



		if($this->_is16){
            Configuration::updateValue($this->name.'leftnews', 1);


            Configuration::updateValue($this->name.'search_left_n', 1);
            Configuration::updateValue($this->name.'arch_left_n', 1);
		} else {
		    Configuration::updateValue($this->name.'rightnews', 1);
            Configuration::updateValue($this->name.'search_right_n', 1);
            Configuration::updateValue($this->name.'arch_right_n', 1);
			
		}



        Configuration::updateValue($this->name.'footernews', 1);
        Configuration::updateValue($this->name.'arch_footer_n', 1);
        Configuration::updateValue($this->name.'search_footer_n', 1);
		Configuration::updateValue($this->name.'homenews', 1);

        Configuration::updateValue($this->name.'is_like', 1);
        Configuration::updateValue($this->name.'is_unlike', 1);

		
		Configuration::updateValue($this->name.'n_block_img_w', 50);
		
		Configuration::updateValue($this->name.'lists_img_w', 100);


		Configuration::updateValue($this->name.'item_img_w', 500);
        Configuration::updateValue($this->name.'is_soc_but', 1);

        if(version_compare(_PS_VERSION_, '1.5', '>')) {
            $medium_default = "medium"."_"."default";
            Configuration::updateValue($this->name . 'img_size_rp', $medium_default);
        } else {
            Configuration::updateValue($this->name . 'img_size_rp', 'medium');
        }
        Configuration::updateValue($this->name.'item_rp_tr', 75);

        Configuration::updateValue($this->name.'rp_img_width', 150);

		
		Configuration::updateValue($this->name.'b_display_date', 1);
		Configuration::updateValue($this->name.'l_display_date', 1);
		Configuration::updateValue($this->name.'i_display_date', 1);
		
		
		
		Configuration::updateValue($this->name.'number_ni', 5);
		Configuration::updateValue($this->name.'news_page', 5);
		
		Configuration::updateValue($this->name.'hnumber_ni', 5);
        Configuration::updateValue($this->name.'items_w_h', 150);
        Configuration::updateValue($this->name.'item_h_tr', 250);
        Configuration::updateValue($this->name.'h_display_date', 1);

        Configuration::updateValue($this->name.'is_comments', 1);
        Configuration::updateValue($this->name.'number_fc', 5);




		Configuration::updateValue($this->name.'rsson', 1);
		Configuration::updateValue($this->name.'number_rssitems', 10);
		
		
		
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		
    		$rssname = Configuration::get('PS_SHOP_NAME');
    		Configuration::updateValue($this->name.'rssname_'.$i, $rssname);
			$rssdesc = Configuration::get('PS_SHOP_NAME');
			Configuration::updateValue($this->name.'rssdesc_'.$i, $rssdesc);
		}

        if(version_compare(_PS_VERSION_, '1.6', '<'))
		    $this->generateRewriteRules();


        if($this->_is15 == 1)
            $this->createAdminTabs15();
		
		
		if (!$this->registerHook('leftColumn') 
			|| !$this->registerHook('rightColumn')
			|| !$this->registerHook('Header') 
			|| !$this->registerHook('home') 
			|| !$this->registerHook('Footer') 
			|| !$this->_installDatabaseTable()
            || !$this->createLikePostTable()

            OR !((version_compare(_PS_VERSION_, '1.6', '>'))? $this->registerHook('ModuleRoutes') : true)

            OR !($this->_is_cloud? true : $this->_createFolderAndSetPermissions())
            OR !((version_compare(_PS_VERSION_, '1.6', '>'))? $this->registerHook('DisplayBackOfficeHeader') : true)
            OR !((version_compare(_PS_VERSION_, '1.5', '>'))? $this->registerHook('newsMitrocops') : true)
			 )
			return false;

		return true;
	}

    public function hookModuleRoutes()
    {
        return array(

            ## news ##
            'blocknewsadv-news-id' => array(
                'controller' =>	null,
                'rule' =>		'{controller}/{id}',
                'keywords' => array(
                    'id'		=>	array('regexp' => '[a-zA-Z0-9-_]+','param'=>'id'),
                    'controller'	=>	array('regexp' => 'news', 'param' => 'controller')
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'blocknewsadv'
                )
            ),



            'blocknewsadv-news' => array(
                'controller' =>	null,
                'rule' =>		'{controller}',
                'keywords' => array(
                    'controller'	=>	array('regexp' => 'news', 'param' => 'controller')
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'blocknewsadv'
                )
            ),

            /*'blocknewsadv-news-p' => array(
                'controller' =>	null,
                'rule' =>		'{controller}/{p}',
                'keywords' => array(
                    'p'				=>	array('regexp' => '[0-9]+', 'param' => 'p'),
                    'controller'	=>	array('regexp' => 'news', 'param' => 'controller')
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'blocknewsadv'
                )
            ),*/



            ## news ##


        );
    }

    public function hookDisplayBackOfficeHeader()
    {

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $base_dir = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;
        } else {
            $base_dir = _PS_BASE_URL_.__PS_BASE_URI__;
        }


        $css = '';
        $css .= '<style type="text/css">
		.icon-AdminBlocknewsadv:before {
			content: url("'.$base_dir.'modules/'.$this->name.'/AdminBlocknewsadvold.gif");
		}
		</style>
		';
        return $css;
    }
	
	public function uninstall(){

        Configuration::deleteByName($this->name.'rew_on');

        #### posts api ###
        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];

            Configuration::deleteByName($this->name.'twdesc_'.$i);
            Configuration::deleteByName($this->name.'vkdesc_'.$i);
        }

        #### posts api ###

        // only for prestashop 1.3, 1.4, 1.5
        Configuration::deleteByName($this->name.'switch_s');
        // only for prestashop 1.3, 1.4, 1.5


        Configuration::deleteByName($this->name.'block_display_img');


        Configuration::deleteByName($this->name.'leftnews');
        Configuration::deleteByName($this->name.'search_left_n');
        Configuration::deleteByName($this->name.'arch_left_n');

        Configuration::deleteByName($this->name.'rightnews');
        Configuration::deleteByName($this->name.'search_right_n');
        Configuration::deleteByName($this->name.'arch_right_n');

        Configuration::deleteByName($this->name.'footernews');
        Configuration::deleteByName($this->name.'arch_footer_n');
        Configuration::deleteByName($this->name.'search_footer_n');
        Configuration::deleteByName($this->name.'homenews');



        Configuration::updateValue($this->name.'is_like', 1);
        Configuration::updateValue($this->name.'is_unlike', 1);


        Configuration::updateValue($this->name.'n_block_img_w', 50);

        Configuration::updateValue($this->name.'lists_img_w', 100);


        Configuration::updateValue($this->name.'item_img_w', 500);
        Configuration::updateValue($this->name.'is_soc_but', 1);

        if(version_compare(_PS_VERSION_, '1.5', '>')) {
            $medium_default = "medium"."_"."default";
            Configuration::updateValue($this->name . 'img_size_rp', $medium_default);
        } else {
            Configuration::updateValue($this->name . 'img_size_rp', 'medium');
        }
        Configuration::updateValue($this->name.'item_rp_tr', 75);

        Configuration::updateValue($this->name.'rp_img_width', 150);


        Configuration::updateValue($this->name.'b_display_date', 1);
        Configuration::updateValue($this->name.'l_display_date', 1);
        Configuration::updateValue($this->name.'i_display_date', 1);



        Configuration::updateValue($this->name.'number_ni', 5);
        Configuration::updateValue($this->name.'news_page', 5);

        Configuration::updateValue($this->name.'hnumber_ni', 5);
        Configuration::updateValue($this->name.'items_w_h', 150);
        Configuration::updateValue($this->name.'item_h_tr', 250);
        Configuration::updateValue($this->name.'h_display_date', 1);

        Configuration::updateValue($this->name.'is_comments', 1);
        Configuration::updateValue($this->name.'number_fc', 5);




        Configuration::updateValue($this->name.'rsson', 1);
        Configuration::updateValue($this->name.'number_rssitems', 10);



        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];

            $rssname = Configuration::get('PS_SHOP_NAME');
            Configuration::updateValue($this->name.'rssname_'.$i, $rssname);
            $rssdesc = Configuration::get('PS_SHOP_NAME');
            Configuration::updateValue($this->name.'rssdesc_'.$i, $rssdesc);
        }

        if($this->_is15 == 1)
            $this->uninstallTab15();

		if (!parent::uninstall() || !$this->uninstallTable()) return false;
		
		return true;
	}
	
	private function uninstallTable() {
		Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blocknewsadv');
		Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blocknewsadv_data');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blocknewsadv_news_like');
		
		return true;
	}


	
	private function _installDatabaseTable(){
		
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blocknewsadv` (
							  `id` int(11) NOT NULL auto_increment,
							  `img` text, 
							  `status` int(11) NOT NULL default \'1\',
							  `id_shop` varchar(1024) NOT NULL default \'0\',
							  `related_products` varchar(1024) NOT NULL default \'0\',
					  		  `related_posts` varchar(1024) NOT NULL default \'0\',
					  		  `is_comments` int(11) NOT NULL default \'1\',
							  `time_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
                              `author` varchar(1024) NOT NULL default \'0\',
							  PRIMARY KEY  (`id`)
							) ENGINE='.(defined('_MYSQL_ENGINE_')?_MYSQL_ENGINE_:"MyISAM").' DEFAULT CHARSET=utf8;';
		if (!Db::getInstance()->Execute($sql))
			return false;
			
		$query = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blocknewsadv_data` (
							  `id_item` int(11) NOT NULL,
							  `id_lang` int(11) NOT NULL,
							  `seo_description` text,
							  `seo_keywords` varchar(5000) default NULL,
							  `seo_url` varchar(5000) default NULL,
							  `title` varchar(5000) NOT NULL,
							  `content` text NOT NULL,
							  
							  KEY `id_item` (`id_item`)
							) ENGINE='.(defined('_MYSQL_ENGINE_')?_MYSQL_ENGINE_:"MyISAM").' DEFAULT CHARSET=utf8';
		Db::getInstance()->Execute($query);
		return true;
		
	}

    public function createLikePostTable(){
        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blocknewsadv_news_like` (
					  `id` int(11) NOT NULL auto_increment,
					  `news_id` int(11) NOT NULL,
					  `like` int(11) NOT NULL,
					  `ip` varchar(255) default NULL,
					  `time_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
					  PRIMARY KEY  (`id`)
					) ENGINE='.(defined('_MYSQL_ENGINE_')?_MYSQL_ENGINE_:"MyISAM").' DEFAULT CHARSET=utf8;';
        if (!Db::getInstance()->Execute($sql))
            return false;

        return true;
    }

	private function _createFolderAndSetPermissions(){
		
		$prev_cwd = getcwd();
		
		$module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
		@chdir($module_dir);
		//folder avatars
		$module_dir_img = $module_dir."blocknewsadv".DIRECTORY_SEPARATOR; 
		@mkdir($module_dir_img, 0777);

		@chdir($prev_cwd);
		
		return true;
	} 
	
		private function generateRewriteRules(){
            
            if(Configuration::get('PS_REWRITING_SETTINGS')){

                $rules = "#blocknewsadv - not remove this comment \n";
                
                $physical_uri = array();
                 if($this->_is15){
                foreach (ShopUrl::getShopUrls() as $shop_url)
				{
                 if(in_array($shop_url->physical_uri,$physical_uri)) continue;
                    
                    
                  $rules .= "RewriteRule ^(.*)news/([0-9a-zA-Z-_]+)/?$ ".$shop_url->physical_uri."modules/".$this->name."/news-item.php?id=$2 [QSA,L]\n";
				  $rules .= "RewriteRule ^(.*)news/?$ ".$shop_url->physical_uri."modules/".$this->name."/news.php [QSA,L]\n";
                 
                    $physical_uri[] = $shop_url->physical_uri;
                } 
                
                 } else{
                 	
                 	$rules .= "RewriteRule ^(.*)news/([0-9a-zA-Z-_]+)/?$ /modules/".$this->name."/news-item.php?id=$2 [QSA,L]\n";
					$rules .= "RewriteRule ^(.*)news/?$ /modules/".$this->name."/news.php [QSA,L]\n";
                 	 
                }
                $rules .= "#blocknewsadv \n\n";
                
                $path = _PS_ROOT_DIR_.'/.htaccess';

                  if(is_writable($path)){
                      
                      $existingRules = Tools::file_get_contents($path);
                      
                      if(!strpos($existingRules, "blocknewsadv")){
                        $handle = fopen($path, 'w');
                        fwrite($handle, $rules.$existingRules);
                        fclose($handle);
                      }
                  }
              }
        }

    public function createAdminTabs15(){

        if(version_compare(_PS_VERSION_, '1.6', '>')) {
            $prefix = '';
        } else {
            $prefix = 'old';
        }

        @copy_custom_blocknewsadv(dirname(__FILE__)."/img/AdminBlocknewsadv".$prefix.".gif",_PS_ROOT_DIR_."/img/t/AdminBlocknewsadv".$prefix.".gif");

        $langs = Language::getLanguages();


        $tab0 = new Tab();
        $tab0->class_name = "AdminBlocknewsadv".$prefix;
        $tab0->module = $this->name;
        $tab0->id_parent = 0;
        foreach($langs as $l){
            $tab0->name[$l['id_lang']] = $this->l('News');
        }
        $tab0->save();
        $main_tab_id = $tab0->id;

        unset($tab0);

        $tab1 = new Tab();
        $tab1->class_name = "AdminBlocknewsadvnews".$prefix;
        $tab1->module = $this->name;
        $tab1->id_parent = $main_tab_id;
        foreach($langs as $l){
            $tab1->name[$l['id_lang']] = $this->l('Moderate News');
        }
        $tab1->save();


        unset($tab1);

    }

    private function uninstallTab15(){

        if(version_compare(_PS_VERSION_, '1.6', '>')) {
            $prefix = '';
        } else {
            $prefix = 'old';
        }

        $tab_id = Tab::getIdFromClassName("AdminBlocknewsadv".$prefix);
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        $tab_id = Tab::getIdFromClassName("AdminBlocknewsadv".($prefix == 'old'?'N':'n')."ews".$prefix);
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }



        @unlink(_PS_ROOT_DIR_."/img/t/AdminBlocknewsadv".$prefix.".gif");
    }
	
	public function hookHeader($params){
    	$smarty = $this->context->smarty;
		$smarty->assign($this->name.'is16', $this->_is16);
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

        $smarty->assign($this->name.'appid', Configuration::get($this->name.'appid'));
        $smarty->assign($this->name.'appadmin', Configuration::get($this->name.'appadmin'));


        $smarty->assign($this->name.'is15', $this->_is15);
    	if(version_compare(_PS_VERSION_, '1.5', '>')){
            $this->context->controller->addCSS(($this->_path).'views/css/font-custom.min.css', 'all');
    		$this->context->controller->addCSS(($this->_path).'views/css/blocknewsadv.css', 'all');
            $this->context->controller->addJS($this->_path.'views/js/'.$this->name.'.js');

            if(version_compare(_PS_VERSION_, '1.6', '<')){
                $this->context->controller->addCSS(($this->_path).'views/css/blocknewsadv15.css', 'all');
            }
    	} 
    	
    	
    	$item_id = Tools::getValue('id');
    	$is_news_page = 0;
    	if($item_id && stripos($_SERVER['REQUEST_URI'],"news")){
    		$is_news_page = 1;
    		
    		
    		include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
			$obj_blocknewsadvfunctions = new blocknewsadvfunctions();
			
			$item_id = $obj_blocknewsadvfunctions->getTransformSEOURLtoID(array('id'=>$item_id));
				
			$_info_cat = $obj_blocknewsadvfunctions->getItem(array('id' => $item_id,'site'=>1));
    		
			$name = isset($_info_cat['item'][0]['title'])?$_info_cat['item'][0]['title']:'';
    		$img = isset($_info_cat['item'][0]['img_orig'])?$_info_cat['item'][0]['img_orig']:'';
            $news_seo_url = isset($_info_cat['item'][0]['seo_url'])?$_info_cat['item'][0]['seo_url']:'';
            $news_id = isset($_info_cat['item'][0]['id'])?$_info_cat['item'][0]['id']:'';
    		
    		$smarty->assign($this->name.'name', $name);
    		$smarty->assign($this->name.'img', $img);

            $smarty->assign($this->name.'news_seo_url', $news_seo_url);
            $smarty->assign($this->name.'news_id', $news_id);
            $smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
    	}

        $this->setSEOUrls();
    	
    	$smarty->assign($this->name.'is_news', $is_news_page);
    	
    	return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/head.tpl');	
    }


    public function setSEOUrls(){
        $smarty = $this->context->smarty;
        include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
        $obj_blocknewsadvfunctions = new blocknewsadvfunctions();

        $data_url = $obj_blocknewsadvfunctions->getSEOURLs();
        $news_url = $data_url['news_url'];
        $news_item_url = $data_url['news_item_url'];

        $smarty->assign(
            array($this->name.'news_url' => $news_url,
                $this->name.'news_item_url' => $news_item_url,
            )
        );

        $smarty->assign($this->name.'pic', $this->path_img_cloud);
        $smarty->assign($this->name.'is_ps15', $this->_is15);

        $smarty->assign($this->name.'is_like', Configuration::get($this->name.'is_like'));
        $smarty->assign($this->name.'is_unlike', Configuration::get($this->name.'is_unlike'));



    }
	
	public function hookLeftColumn($params)
	{
        if (isset($this->context->controller->hide_lastes_news) && $this->context->controller->hide_lastes_news) {
            return '';
        }
        if (isset($this->context->controller->remove_lastes_news) && $this->context->controller->remove_lastes_news) {
            return '';
        }
		$smarty = $this->context->smarty;

		$smarty->assign($this->name.'is16', $this->_is16);
		include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
		$obj_blocknewshelp = new blocknewsadvfunctions();
        $_data = $obj_blocknewshelp->getItemsBlock();
        $_data_arch = $obj_blocknewshelp->getArchives();

        $smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                $this->name.'arch' => $_data_arch['items'],
            )
        );
		
		$smarty->assign($this->name.'leftnews', Configuration::get($this->name.'leftnews'));
        $smarty->assign($this->name.'arch_left_n', Configuration::get($this->name.'arch_left_n'));
        $smarty->assign($this->name.'search_left_n', Configuration::get($this->name.'search_left_n'));
		
		
						
		$smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

		$smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
        $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));

        $this->setSEOUrls();

        $sql = 'SELECT info.title, pc.id, info.seo_url, pc.img
            FROM `'._DB_PREFIX_.'blog_post` pc 
            LEFT JOIN `'._DB_PREFIX_.'blog_category2post` c2p ON(pc.id = c2p.post_id)
            LEFT JOIN `'._DB_PREFIX_.'blog_post_data` info ON(pc.id = info.id_item)
            WHERE c2p.category_id = 368
            ORDER BY pc.`time_add` DESC
            LIMIT 3';
        $fin_news = Db::getInstance()->ExecuteS($sql);
        $this->context->smarty->assign('fin_news', $fin_news);
				
		return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/left.tpl');
			
	}
	
	public function hookRightColumn($params)
	{
		$smarty = $this->context->smarty;
		$smarty->assign($this->name.'is16', $this->_is16);
		include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
		$obj_blocknewshelp = new blocknewsadvfunctions();
        $_data = $obj_blocknewshelp->getItemsBlock();
        $_data_arch = $obj_blocknewshelp->getArchives();

        $smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                             $this->name.'arch' => $_data_arch['items'],
            )
        );
		
		$smarty->assign($this->name.'rightnews', Configuration::get($this->name.'rightnews'));
        $smarty->assign($this->name.'arch_right_n', Configuration::get($this->name.'arch_right_n'));
        $smarty->assign($this->name.'search_right_n', Configuration::get($this->name.'search_right_n'));



        $smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
		

		$smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
        $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));

        $this->setSEOUrls();
		
		return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/right.tpl');
		
	}

    public function hooknewsMitrocops($params)
    {


            $smarty = $this->context->smarty;
            $cookie = $this->context->cookie;
            $smarty->assign($this->name.'is16', $this->_is16);
            include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
            $obj = new blocknewsadvfunctions();

            $data = $obj->getHomeLastNews();
            $current_language = (int)$cookie->id_lang;
            // strip tags for content
            foreach($data['items'] as $_k => $_item){
                if($current_language == @$_item['data'][$_k]['id_lang']){
                    $data['items'][$_k]['data'][$current_language]['content'] = strip_tags($_item['data'][$_k]['content']);
                }
            }
            $smarty->assign(array($this->name.'items_home' => $data['items']));
            $smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
            $smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

            $smarty->assign($this->name.'is_ps15', $this->_is15);


            $smarty->assign($this->name.'item_h_tr', Configuration::get($this->name.'item_h_tr'));

            $smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
            $smarty->assign($this->name.'h_display_date', Configuration::get($this->name.'h_display_date'));

            $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));



            $smarty->assign($this->name.'news_m', Configuration::get($this->name.'news_m'));
            $smarty->assign($this->name.'arch_m', Configuration::get($this->name.'arch_m'));
            $smarty->assign($this->name.'search_m', Configuration::get($this->name.'search_m'));





            $this->setSEOUrls();

            return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/newsmitrocops.tpl');

    }

	public function hookHome($params){
		if(Configuration::get($this->name.'homenews') == 1){
			
			$smarty = $this->context->smarty;
			$cookie = $this->context->cookie;
			$smarty->assign($this->name.'is16', $this->_is16);
			include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
			$obj = new blocknewsadvfunctions();

	    	$data = $obj->getHomeLastNews();
	    	$current_language = (int)$cookie->id_lang;
			// strip tags for content
			foreach($data['items'] as $_k => $_item){
				if($current_language == @$_item['data'][$_k]['id_lang']){
				$data['items'][$_k]['data'][$current_language]['content'] = strip_tags($_item['data'][$_k]['content']);
				}
			}
			$smarty->assign(array($this->name.'items_home' => $data['items']));
			$smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
			$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
			
			$smarty->assign($this->name.'is_ps15', $this->_is15);


            $smarty->assign($this->name.'item_h_tr', Configuration::get($this->name.'item_h_tr'));

			$smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
            $smarty->assign($this->name.'h_display_date', Configuration::get($this->name.'h_display_date'));

            $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));


            $this->setSEOUrls();
		
			return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/home.tpl');
		}
	
	}
	
	public function hookFooter($params)
	{
		$smarty = $this->context->smarty;

        $smarty->assign($this->name.'footernews', Configuration::get($this->name.'footernews'));

        $smarty->assign($this->name.'is16', $this->_is16);
		$smarty = $this->context->smarty;
		include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
		$obj_blocknewshelp = new blocknewsadvfunctions();
    	$_data = $obj_blocknewshelp->getItemsBlock();
        $_data_arch = $obj_blocknewshelp->getArchives();
		
    	$smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                              $this->name.'arch' => $_data_arch['items'],
							  )
						);
		
		$smarty->assign($this->name.'leftnews', Configuration::get($this->name.'leftnews'));
        $smarty->assign($this->name.'arch_footer_n', Configuration::get($this->name.'arch_footer_n'));
        $smarty->assign($this->name.'search_footer_n', Configuration::get($this->name.'search_footer_n'));
		
		
						
						
		$smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
	

			
		$smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
            $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));

        $this->setSEOUrls();
		
		
		return $this->display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/hooks/footer.tpl');

		
	}


    protected function addBackOfficeMedia()
    {
        $this->context->controller->addCSS($this->_path.'views/css/font-custom.min.css');
        //CSS files
        $this->context->controller->addCSS($this->_path.'views/css/menu16.css');

        // JS files
        $this->context->controller->addJs($this->_path.'views/js/menu16.js');


    }

	private $_html;

    public function getCurrentShopCustom(){
        $current_shop_id = Shop::getContextShopID();
        return $current_shop_id;

    }
	
	public function getContent()
    {
    	$currentIndex = $this->context->currentindex;
    	$cookie = $this->context->cookie;
    	include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
    	$blocknewsadvfunctions_obj = new blocknewsadvfunctions();
		
    	$this->_html = '';


    	if(version_compare(_PS_VERSION_, '1.6', '>')){
            $this->addBackOfficeMedia();
        } else {
            $this->_html .= $this->_jsandcss();
        }
    	
    	
    	#### posts api ###
    	$vkset = Tools::getValue("vkset");
        if (Tools::strlen($vkset)>0) {
        	$this->_html .= '<script>init_tabs(8);</script>';
        }
    	if (Tools::isSubmit('psvkpostsettings'))
        {
        	Configuration::updateValue($this->name.'vkpost_on', Tools::getValue('vkpost_on'));
        	
        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'vkdesc_'.$i, Tools::getValue('vkdesc_'.$i));
        	}
        	
        	$url = $currentIndex.'&conf=6&tab=AdminModules&vkset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        
        
        
    	$twset = Tools::getValue("twset");
        if (Tools::strlen($twset)>0) {
        	$this->_html .= '<script>init_tabs(8);</script>';
        }
    	if (Tools::isSubmit('pstwitterpostsettings'))
        {
        	Configuration::updateValue($this->name.'twpost_on', Tools::getValue('twpost_on'));
        	
        	
        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'twdesc_'.$i, Tools::getValue('twdesc_'.$i));
        	}
        	
        	$url = $currentIndex.'&conf=6&tab=AdminModules&twset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }
    	#### posts api ###
    	
    	
    	$pageitems_url = Tools::getValue("pageitems");
   	    if (Tools::strlen($pageitems_url)>0) {
        	$this->_html .= '<script>init_tabs(4);</script>';
        }
        
   		 if (Tools::isSubmit('cancel_item'))
        {
        	$page = Tools::getValue("pageitems");
        	Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'&pageitems='.$page);
		}
        
    	if (Tools::isSubmit('update_item'))
        {
        	$id = Tools::getValue("id");
     		
        	$languages = Language::getLanguages(false);
	    	$data_title_content_lang = array();
	    	$seo_url = Tools::getValue("seo_url");
	    	$time_add = Tools::getValue("time_add");
            $author = Tools::getValue("author");
	    	
	    	$ids_related_products = Tools::getValue("inputAccessories");
	    	
	    	$ids_related_posts = Tools::getValue("ids_related_posts");
	    	

       		if($this->_is15){
	    		$cat_shop_association = Tools::getValue("cat_shop_association");
	    	} else{
	    		$cat_shop_association = array(0=>1);
	    	}

	    	foreach ($languages as $language){
	    		$id_lang = $language['id_lang'];
	    		$title = Tools::getValue("title_".$id_lang);
	    		$content = Tools::getValue("content_".$id_lang);
	    		$seokeywords = Tools::getValue("seokeywords_".$id_lang);
	    		$seodescription = Tools::getValue("seodescription_".$id_lang);
	    		
	    		if(Tools::strlen($title)>0 && Tools::strlen($content)>0)
	    		{
	    			$data_title_content_lang[$id_lang] = array('title' => $title,
	    									 				   'content' => $content,
	    													   'seokeywords' => $seokeywords,
	    													   'seodescription' => $seodescription,
	    													   'seo_url' => $seo_url
	    													    );		
	    		}
	    	}
        	
         	$item_status = Tools::getValue("item_status");
        	$post_images = Tools::getValue("post_images");
            $item_iscomments = Tools::getValue("item_iscomments");
        	
         	$data = array('data_title_content_lang'=>$data_title_content_lang,
        				  'id' => $id,
                            'iscomments' => $item_iscomments,
         				  'item_status' => $item_status,
         				  'post_images' => $post_images,
         				  'related_products'=>$ids_related_products,
         				  'ids_related_posts'=>$ids_related_posts,
         				  'cat_shop_association' => $cat_shop_association,
         				  'time_add' => $time_add,
                          'author' => $author
         				 );
         	if(sizeof($data_title_content_lang)>0)
         		$blocknewsadvfunctions_obj->updateItem($data);
         		
         	$page = Tools::getValue("pageitems");
         	Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'&pageitems='.$page);
		
        }
    	
    	if (Tools::isSubmit('submit_item'))
        {
        	$languages = Language::getLanguages(false);
	    	$data_title_content_lang = array();
	    	$seo_url = Tools::getValue("seo_url");
	    	$time_add = Tools::getValue("time_add");
            $author = Tools::getValue("author");
	    	
	    	$ids_related_products = Tools::getValue("inputAccessories");
	    	
	    	$ids_related_posts = Tools::getValue("ids_related_posts");
	    	
	    	
       		if($this->_is15){
	    		$cat_shop_association = Tools::getValue("cat_shop_association");
	    	} else{
	    		$cat_shop_association = array(0=>1);
	    	}
	    	
	    	foreach ($languages as $language){
	    		$id_lang = $language['id_lang'];
	    		$title = Tools::getValue("title_".$id_lang);
	    		$content = Tools::getValue("content_".$id_lang);
	    		$seokeywords = Tools::getValue("seokeywords_".$id_lang);
	    		$seodescription = Tools::getValue("seodescription_".$id_lang);
	    		
	    		if(Tools::strlen($title)>0 && Tools::strlen($content)>0)
	    		{
	    			$data_title_content_lang[$id_lang] = array('title' => $title,
	    									 				   'content' => $content,
	    													   'seokeywords' => $seokeywords,
	    													   'seodescription' => $seodescription,
	    													   'seo_url' => $seo_url
	    													    );		
	    		}
	    	}
	    	
        	$item_status = Tools::getValue("item_status");
            $item_iscomments = Tools::getValue("item_iscomments");
        	
        	$data = array('data_title_content_lang'=>$data_title_content_lang,
                          'iscomments' => $item_iscomments,
         				  'item_status' => $item_status,
        				  'cat_shop_association' => $cat_shop_association,
        				  'related_products'=>$ids_related_products,
         				  'ids_related_posts'=>$ids_related_posts,
        	 			  'time_add' => $time_add,
                          'author' => $author
         				  );
         	//echo "<pre>"; var_dump($data); exit;
         	if(sizeof($data_title_content_lang)>0)
         		$blocknewsadvfunctions_obj->saveItem($data);
        		
         	$page = (int) Tools::getValue("pageitems");
			Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'&pageitems='.$page);
		}
    	
		
    	// delete news item
    	if (Tools::isSubmit("delete_item")) {
			if (Validate::isInt(Tools::getValue("id"))) {
				
				$data = array('id' => Tools::getValue("id"));
				$blocknewsadvfunctions_obj->deleteItem($data);
				
				$page = Tools::getValue("pageitems");
				Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'&pageitems='.$page);
			}
		}
		// delete news item
		
    	
    	// url rewriting settings
    	$urlrewriting_settingsset = Tools::getValue("urlrewriting_settingsset");
   	    if (Tools::strlen($urlrewriting_settingsset)>0) {
        	$this->_html .= '<script>init_tabs(5);</script>';
        }
    	if (Tools::isSubmit('submit_urlrewriting'))
        {
        	Configuration::updateValue($this->name.'rew_on', Tools::getValue('rew_on'));
        	
        	
        	$url = $currentIndex.'&tab=AdminModules&urlrewriting_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // url rewriting settings
        
        
        // sitemap
    	$submitsitemap_settingsset = Tools::getValue("submitsitemap_settingsset");
   	    if (Tools::strlen($submitsitemap_settingsset)>0) {
        	$this->_html .= '<script>init_tabs(5);</script>';
        }
    	if (Tools::isSubmit('submitsitemap'))
        {
        	$blocknewsadvfunctions_obj->generateSitemap();
        	
        	$url = $currentIndex.'&tab=AdminModules&submitsitemap_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // sitemap
       
        	
       
    	// news settings
   	    $news_settingsset = Tools::getValue("news_settingsset");
   	    if (Tools::strlen($news_settingsset)>0) {
        	$this->_html .= '<script>init_tabs(3);</script>';
        }
        
        if (Tools::isSubmit('news_settings'))
        {
        	
        	Configuration::updateValue($this->name.'switch_s', Tools::getValue('switch_s'));

            Configuration::updateValue($this->name.'block_display_img', Tools::getValue('block_display_img'));

        	
        	Configuration::updateValue($this->name.'leftnews', Tools::getValue('leftnews'));
        	Configuration::updateValue($this->name.'rightnews', Tools::getValue('rightnews'));
        	Configuration::updateValue($this->name.'homenews', Tools::getValue('homenews'));
        	Configuration::updateValue($this->name.'footernews', Tools::getValue('footernews'));
            Configuration::updateValue($this->name.'news_m', Tools::getValue('news_m'));


            Configuration::updateValue($this->name.'arch_left_n', Tools::getValue('arch_left_n'));
            Configuration::updateValue($this->name.'arch_right_n', Tools::getValue('arch_right_n'));
            Configuration::updateValue($this->name.'arch_footer_n', Tools::getValue('arch_footer_n'));
            Configuration::updateValue($this->name.'arch_m', Tools::getValue('arch_m'));


            Configuration::updateValue($this->name.'search_footer_n', Tools::getValue('search_footer_n'));
            Configuration::updateValue($this->name.'search_left_n', Tools::getValue('search_left_n'));
            Configuration::updateValue($this->name.'search_right_n', Tools::getValue('search_right_n'));
            Configuration::updateValue($this->name.'search_m', Tools::getValue('search_m'));

            Configuration::updateValue($this->name.'is_like', Tools::getValue('is_like'));
            Configuration::updateValue($this->name.'is_unlike', Tools::getValue('is_unlike'));


            Configuration::updateValue($this->name.'n_block_img_w', Tools::getValue('n_block_img_w'));
    		
    		Configuration::updateValue($this->name.'lists_img_w', Tools::getValue('lists_img_w'));

            Configuration::updateValue($this->name.'is_soc_but', Tools::getValue('is_soc_but'));

            Configuration::updateValue($this->name.'img_size_rp', Tools::getValue('img_size_rp'));
            Configuration::updateValue($this->name.'item_rp_tr', Tools::getValue('item_rp_tr'));
            Configuration::updateValue($this->name.'rp_img_width', Tools::getValue('rp_img_width'));



			
    		Configuration::updateValue($this->name.'item_img_w', Tools::getValue('item_img_w'));
			
    		Configuration::updateValue($this->name.'b_display_date', Tools::getValue('b_display_date'));
			Configuration::updateValue($this->name.'l_display_date', Tools::getValue('l_display_date'));
			Configuration::updateValue($this->name.'i_display_date', Tools::getValue('i_display_date'));
    		
			Configuration::updateValue($this->name.'number_ni', Tools::getValue('number_ni'));
			Configuration::updateValue($this->name.'news_page', Tools::getValue('news_page'));
			
			Configuration::updateValue($this->name.'hnumber_ni', Tools::getValue('hnumber_ni'));
            Configuration::updateValue($this->name.'items_w_h', Tools::getValue('items_w_h'));
            Configuration::updateValue($this->name.'item_h_tr', Tools::getValue('item_h_tr'));
            Configuration::updateValue($this->name.'h_display_date', Tools::getValue('h_display_date'));

            Configuration::updateValue($this->name.'is_comments', Tools::getValue('is_comments'));
            Configuration::updateValue($this->name.'number_fc', Tools::getValue('number_fc'));

            Configuration::updateValue($this->name.'appid', Tools::getValue('appid'));
            Configuration::updateValue($this->name.'appadmin', Tools::getValue('appadmin'));





			Configuration::updateValue($this->name.'rsson', Tools::getValue('rsson'));
			Configuration::updateValue($this->name.'number_rssitems', Tools::getValue('number_rssitems'));
			
			
        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'rssname_'.$i, Tools::getValue('rssname_'.$i));
        		Configuration::updateValue($this->name.'rssdesc_'.$i, Tools::getValue('rssdesc_'.$i));
        	}
			
        	$url = $currentIndex.'&conf=6&tab=AdminModules&news_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // news settings
        
        
        

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $this->_html .= $this->_displayForm16();
        } else {
            $this->_html .= $this->_displayForm13_14_15();
        }

        return $this->_html;
    }


    private function _displayForm16(){
        $_html = '';

        $_html .= '<div class="row">
    	<div class="col-lg-12">
    	<div class="row">';

        $_html .= '<div class="productTabs col-lg-12 col-md-3">

						<div class="list-group">';
        $_html .= '<ul class="nav nav-pills" id="navtabs16">

							    <li class="active"><a href="#welcome" data-toggle="tab" class="list-group-item"><i class="fa fa-home fa-lg"></i>&nbsp;'.$this->l('Welcome').'</a></li>
							    <li><a href="#newssettings" data-toggle="tab" class="list-group-item"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;'.$this->l('News Settings').'</a></li>
							    <li><a href="#seourlrewrite" data-toggle="tab" class="list-group-item"><i class="fa fa-link fa-lg"></i>&nbsp;'.$this->l('SEO URL Rewriting, Sitemap').'</a></li>
							    <li><a href="#autoposts" data-toggle="tab" class="list-group-item"><i class="fa fa-facebook fa-lg"></i>&nbsp;'.$this->l('Automatically posts Integration').'</a></li>
							    <li><a href="#info" data-toggle="tab" class="list-group-item"><i class="fa fa-question-circle fa-lg"></i>&nbsp;'.$this->l('Help / Documentation').'</a></li>
							    <li><a href="http://addons.prestashop.com/en/2_community-developer?contributor=189784" target="_blank"  class="list-group-item"><img src="../modules/'.$this->name.'/views/img/mitrocops-logo.png"  />&nbsp;&nbsp;'.$this->l('Other Mitrocops Modules').'</a></li>


							</ul>';
        $_html .= '</div>
    				</div>';


        $_html .= '<div class="tab-content col-lg-12 col-md-9">';
        $_html .= '<div class="tab-pane active" id="welcome">'.$this->_welcome().'</div>';


        $_html .= '<div class="tab-pane" id="seourlrewrite">'.$this->_urlrewrite().$this->_sitemap().'</div>';
        $_html .= '<div class="tab-pane" id="newssettings">'.$this->_newssettings16().'</div>';
        $_html .= '<div class="tab-pane" id="autoposts">'.$this->_autoposts16().'</div>';
        $_html .= '<div class="tab-pane" id="info">'.$this->_help_documentation().'</div>';
        $_html .= '</div>';



        $_html .= '</div></div></div>';




        return $_html;
    }

    private function _newssettings16(){
        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News Settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block Last News'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block Last News'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'leftnews',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'rightnews',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'homenews',
                                    'name' => $this->l('Home'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'footernews',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'news_m',
                                    'name' => $this->l('Block Last News in CUSTOM HOOK (newsMitrocops)').' (<a href="javascript:void(0)" onclick="tabs_custom(6)" style="text-decoration:underline;font-weight:bold">'.$this->l('How to configure CUSTOM HOOK?').'</a>)',
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),


                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block News Archives'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block News Archives'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'arch_left_n',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'arch_right_n',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),

                                array(
                                    'id' => 'arch_footer_n',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'arch_m',
                                    'name' => $this->l('Block News Archives in CUSTOM HOOK (newsMitrocops)').' (<a href="javascript:void(0)" onclick="tabs_custom(6)" style="text-decoration:underline;font-weight:bold">'.$this->l('How to configure CUSTOM HOOK?').'</a>)',
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),


                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block Search News'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block Search News'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'search_left_n',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'search_right_n',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),

                                array(
                                    'id' => 'search_footer_n',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'search_m',
                                    'name' => $this->l('Block Search News in CUSTOM HOOK (newsMitrocops)').' (<a href="javascript:void(0)" onclick="tabs_custom(6)" style="text-decoration:underline;font-weight:bold">'.$this->l('How to configure CUSTOM HOOK?').'</a>)',
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),



                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Like Functional'),
                        'name' => 'is_like',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable UnLike Functional'),
                        'name' => 'is_unlike',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),


                ),



            ),


        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Block "Last News" settings'),
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in the Block Last News'),
                        'name' => 'number_ni',
                        'id' => 'number_ni',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the block "Last News"'),
                        'name' => 'n_block_img_w',
                        'value' => (int)Configuration::get($this->name.'n_block_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in the block "Last News"'),
                        'name' => 'b_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'b_display_date')
                        ),
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display images in the block "Last News"'),
                        'name' => 'block_display_img',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'block_display_img')
                        ),
                    ),



                ),



            ),


        );


        $fields_form2 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News in the list view settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(


                    array(
                        'type' => 'text',
                        'label' => $this->l('News per Page in the list view'),
                        'name' => 'news_page',
                        'id' => 'news_page',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the lists items'),
                        'name' => 'lists_img_w',
                        'value' => (int)Configuration::get($this->name.'lists_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in the lists items'),
                        'name' => 'l_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'l_display_date')
                        ),
                    ),



                ),



            ),


        );

        $fields_form3 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News page settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width on the news page'),
                        'name' => 'item_img_w',
                        'value' => (int)Configuration::get($this->name.'item_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date on the news page'),
                        'name' => 'i_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'i_display_date')
                        ),
                    ),
                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Active Social share buttons'),
                        'name' => 'is_soc_but',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'is_soc_but')
                        ),
                    ),


                ),



            ),


        );


        $data_img_sizes = array();

        $available_types = ImageType::getImagesTypes('products');

        foreach ($available_types as $type){

            $id = $type['name'];
            $name = $type['name'].' ('.$type['width'].' x '.$type['height'].')';

            $data_item_size = array(
                'id' => $id,
                'name' => $name,
            );

            array_push($data_img_sizes,$data_item_size);


        }



        $fields_form4 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Related products on the news page settings'),
                    'icon' => 'fa fa-book fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'select',
                        'label' => $this->l('Image size for related products'),
                        'name' => 'img_size_rp',
                        'options' => array(
                            'query' => $data_img_sizes,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),

                    array(
                        'type' => 'text_truncate',
                        'label' => $this->l('Truncate product description'),
                        'name' => 'item_rp_tr',
                        'id' => 'item_rp_tr',
                        'lang' => FALSE,
                        'value' => (int)Configuration::get($this->name.'item_rp_tr'),
                    ),



                ),



            ),


        );

        $fields_form5 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Related News on the news page settings'),
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the related news block on the news page'),
                        'name' => 'rp_img_width',
                        'value' => (int)Configuration::get($this->name.'rp_img_width')
                    ),
                ),

            ),


        );

        $admin_img = "";
        $admin_explode = explode(",", Configuration::get($this->name.'appadmin'));
        if(sizeof($admin_explode)>0) {
            foreach ($admin_explode as $admin_explode) {
                if(Tools::strlen($admin_explode)==0) continue;

                $admin_img .= '<a href="http://www.facebook.com/profile.php?&id=' . $admin_explode . '" target="_blank">
								<img class="fb-admins" width="50"
									 src="https://graph.facebook.com/' . $admin_explode . '/picture?type=large">
							</a>';
            }
        }

        $fields_form6 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Facebook Comments settings'),
                    'icon' => 'fa fa-facebook fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Facebook comments'),
                        'name' => 'is_comments',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Numbers of comments visible'),
                        'name' => 'number_fc',
                        'id' => 'number_fc',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook Application Id'),
                        'name' => 'appid',
                        'id' => 'appid',
                        'lang' => FALSE,
                        'desc' => $this->l('To configure the Facebook Application Id read').
                                    '&nbsp;<a style="text-decoration:underline;font-weight:bold" href="../modules/'.$this->name.'/readme.pdf" target="_blank">readme.pdf</a> ,&nbsp;'.
                                 $this->l('which is located in the folder  with the module.'),
			         ),

                    array(
                        'type' => 'text_fcom',
                        'label' => $this->l('My App'),
                        'name' => 'fcom_text',
                        'id' => 'fcom_text',
                        'lang' => FALSE,
                        'f_appid' => Configuration::get($this->name.'appid'),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Moderators'),
                        'name' => 'appadmin',
                        'id' => 'appadmin',
                        'lang' => FALSE,
                        'desc' => $this->l('Id administrators, separated by commas').'<br/>'.
                                  $this->l('Add a facebook accounts ID for beeing comments moderators. ID Can be found on').
                                  '&nbsp;<a style="text-decoration:underline;font-weight:bold" href="http://findmyfbid.com/" target="_blank">http://findmyfbid.com/</a>'.
                                  '<br/><br/>'.$admin_img,
                    ),

                    array(
                        'type' => 'text_fcom_set',
                        'label' => $this->l('Moderate Facebook comments'),
                        'name' => 'fcom_text',
                        'id' => 'fcom_text',
                        'lang' => FALSE,
                        'f_appid' => Configuration::get($this->name.'appid'),

                    ),
                ),

            ),


        );

        $fields_form7 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News on the home page settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the block "Latest News" on home page'),
                        'name' => 'items_w_h',
                        'value' => (int)Configuration::get($this->name.'items_w_h')
                    ),

                    array(
                        'type' => 'text_truncate',
                        'label' => $this->l('Truncate text in the block "Latest News" on home page'),
                        'name' => 'item_h_tr',
                        'id' => 'item_h_tr',
                        'lang' => FALSE,
                        'value' => (int)Configuration::get($this->name.'item_h_tr'),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in Block Last News on Home page'),
                        'name' => 'hnumber_ni',
                        'id' => 'hnumber_ni',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in Block Last News on Home page'),
                        'name' => 'h_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'h_display_date')
                        ),
                    ),

                  ),



            ),


        );

        $fields_form8 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $this->l('RSS Feed'),
                    'icon' => 'fa fa-rss fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable RSS Feed'),
                        'name' => 'rsson',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Title of your RSS Feed'),
                        'name' => 'rssname',
                        'id' => 'rssname',
                        'lang' => TRUE,
                        //'required' => TRUE,
                        'size' => 50,
                        //'maxlength' => 50,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Description of your RSS Feed'),
                        'name' => 'rssdesc',
                        'id' => 'rssdesc',
                        'lang' => TRUE,
                        //'required' => TRUE,
                        'size' => 50,
                        //'maxlength' => 50,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in RSS Feed'),
                        'name' => 'number_rssitems',
                        'class' => ' fixed-width-sm',

                    ),


                ),



            ),


        );

        $fields_form9 = array(
            'form' => array(


                'submit' => array(
                    'title' => $this->l('Update Settings'),
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'news_settings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesNewsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1,$fields_form2,$fields_form3,$fields_form4,$fields_form5,$fields_form6,$fields_form7,$fields_form8,$fields_form9));
    }

    public function getConfigFieldsValuesNewsSettings(){

        $languages = Language::getLanguages(false);
        $fields_rssname = array();
        $fields_rssdesc = array();

        foreach ($languages as $lang)
        {
            $fields_rssname[$lang['id_lang']] =  Configuration::get($this->name.'rssname_'.$lang['id_lang']);

            $fields_rssdesc[$lang['id_lang']] =  Configuration::get($this->name.'rssdesc_'.$lang['id_lang']);
        }


        $data_config = array(
            'switch_s' => (int)Configuration::get($this->name.'switch_s'),

            'leftnews' => Configuration::get($this->name.'leftnews'),
            'rightnews' => Configuration::get($this->name.'rightnews'),
            'homenews' => Configuration::get($this->name.'homenews'),
            'footernews' => Configuration::get($this->name.'footernews'),
            'news_m' => Configuration::get($this->name.'news_m'),

            'arch_left_n' => Configuration::get($this->name.'arch_left_n'),
            'arch_right_n' => Configuration::get($this->name.'arch_right_n'),
            'arch_footer_n' => Configuration::get($this->name.'arch_footer_n'),
            'arch_m' => Configuration::get($this->name.'arch_m'),

            'search_left_n' => Configuration::get($this->name.'search_left_n'),
            'search_right_n' => Configuration::get($this->name.'search_right_n'),
            'search_footer_n' => Configuration::get($this->name.'search_footer_n'),
            'search_m' => Configuration::get($this->name.'search_m'),

            'is_like' => (int)Configuration::get($this->name.'is_like'),
            'is_unlike' => (int)Configuration::get($this->name.'is_unlike'),

            'number_ni' => (int)Configuration::get($this->name.'number_ni'),

            'news_page' => (int)Configuration::get($this->name.'news_page'),

            'hnumber_ni' => (int)Configuration::get($this->name.'hnumber_ni'),

            'rsson' => Configuration::get($this->name.'rsson'),
            'rssname' => $fields_rssname,
            'rssdesc' => $fields_rssdesc,
            'number_rssitems' => (int)Configuration::get($this->name.'number_rssitems'),

            'img_size_rp' => Configuration::get($this->name.'img_size_rp'),

            'is_comments' => Configuration::get($this->name.'is_comments'),
            'number_fc' => Configuration::get($this->name.'number_fc'),
            'appid' => Configuration::get($this->name.'appid'),
            'appadmin' => Configuration::get($this->name.'appadmin'),

        );

        return $data_config;

    }

    private function _sitemap(){
        $_html = '';
        $_html .= '<form method="post" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'">';
        $_html .= '<input type="hidden" value="1" name="sitemapsettings"/>';

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '<div class="panel">

                    <div class="panel-heading"><i class="fa fa-sitemap fa-lg"></i>&nbsp;'.$this->l('Sitemap').'</div>';
        } else {
            $_html .= '
                        <h3 class="title-block-content"><i class="fa fa-sitemap fa-lg"></i>&nbsp;'.$this->l('Sitemap').'</h3>';
        }







        $_html .= '

                    <b>'.$this->l('Your CRON URL to call').'</b>:&nbsp;
                    <a target="_blank" href="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/newssitemap.php?token='.$this->_token_cron.'"
                        style="text-decoration:underline;font-weight:bold">
                        '._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/newssitemap.php?token='.$this->_token_cron.'
                        </a>

                         <br/><br/><br/><br/>';

        $_html .= '<input type="submit" value="'.$this->l('Regenerate Google sitemap').'" name="submitsitemap"
                                class="'.(version_compare(_PS_VERSION_, '1.6', '>')?'btn btn-primary pull':'button').'"/>';


        $_html .= ' &nbsp; <a target="_blank" style="text-decoration:underline;font-weight:bold" ';
        if($this->_is_cloud){
            $_html .= 'href="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/upload/news.xml"';
        } else {
            $_html .= 'href="'._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml"';
        }
        $_html .= '>';
        if($this->_is_cloud){
            $_html .=	''._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/upload/news.xml';
        } else {
            $_html .=	''._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml';
        }
        $_html .=	'</a>';

        $_html .= '</p>';




        $_html .= '<br/><br/><p>
                            '.$this->l('To declare news sitemap xml, add this line at the end of your robots.txt file').': <br><br>
							  <code>
								Sitemap ';
        if($this->_is_cloud){
            $_html .= ''._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/upload/news.xml';
        } else {
            $_html .= ''._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml';
        }


        $_html .= '</code>
                            </p>
                ';




        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '</div>';
        }


        $_html .= '</form>';



        $_html .= $this->_cronhelp(array('url'=>'newssitemap'));

        return $_html;
    }


    private function _cronhelp($data = null){
        $url_cron = isset($data['url'])?$data['url']:'';
        $_html = '';

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '<div class="panel">

				<div class="panel-heading"><i class="fa fa-tasks fa-lg"></i>&nbsp;'.$this->l('CRON HELP').'</div>';
        } else {
            $_html .= '<h3 class="title-block-content"><i class="fa fa-tasks fa-lg"></i>&nbsp;'.$this->l('CRON HELP').'</h3>';

        }




        $_html .= '<p class="hint clear" style="display: block; font-size: 12px; width: 95%;position:relative">';

        $_html .= '<b>';
        $_html .= $this->l('You can configure sending email messages through cron. You have 2 possibilities:');
        $_html .= '</b>';
        $_html .= '<br/><br/><br/>';
        $_html .= '<b>1.</b> '.$this->l('You can enter the following url in your browser: ');
        $_html .= '<b>'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/'.$url_cron.'.php?token='.$this->_token_cron.'</b>';
        $_html .= '<br/><br/><br/>';
        $_html .= '<b>2.</b> '.$this->l('You can set a cron\'s task (a recursive task that fulfills the sending of reminders)');
        $_html .= '<br/><br/>';
        $_html .= $this->l('The task run every hour').':&nbsp;&nbsp;&nbsp; <b>* */1 * * * /usr/bin/wget -O - -q '._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/'.$url_cron.'.php?token='.$this->_token_cron.'</b>';
        $_html .= '<br/><br/>';
        $_html .= $this->l('or');
        $_html .= '<br/><br/>';
        $_html .= $this->l('The task run every hour').':&nbsp;&nbsp;&nbsp; <b>* */1 * * * php -f /var/www/vhosts/myhost/httpdocs/prestashop/modules/'.$this->name.'/'.$url_cron.'.php?token='.$this->_token_cron.'</b>';
        $_html .= '<br/><br/><br/>';
        $_html .= '<b>'.$this->l('How to configure a cron task ?').'</b>';
        $_html .= '<br/><br/>';
        $_html .= $this->l('On your server, the interface allows you to configure cron\'s tasks');
        $_html .= '<br/>';
        $_html .= $this->l('About CRON').'&nbsp;&nbsp;&nbsp;<a href=http://en.wikipedia.org/wikimg/Cron target=_blank>http://en.wikipedia.org/wikimg/Cron</a>';
        $_html .= '</p>';


        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '</div>';
        }

        return $_html;
    }

    private function _urlrewrite(){
        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('URL Rewriting'),
                    'icon' => 'fa fa-link fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable URL rewriting:'),
                        'name' => 'rew_on',
                        'desc' => $this->l('Enable only if your server allows URL rewriting (recommended).'),

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                ),



            ),


        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $this->l('Update Settings'),
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_urlrewriting';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesUrlrewriteSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesUrlrewriteSettings(){

        $data_config = array(
            'rew_on' => (int)Configuration::get($this->name.'rew_on'),


        );

        return $data_config;

    }

    public function psvkform16($data){
        $update_button = $data['translate']['update_button'];
        $enable_pstwitterpost = $data['translate']['enable_psvkpost'];

        $template_text = $data['translate']['template_text'];
        $name = $data['translate']['name'];


        $fields_form = array(
            'form'=> array(
                //'tinymce' => FALSE,
                'legend' => array(
                    'title' => $enable_pstwitterpost,
                    'icon' => 'fa fa-vk fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost,
                        'name' => 'vkpost_on',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text,
                        'name' => 'vkdesc',
                        'id' => 'vkdesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',

                    ),


                ),



            ),


        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_button,
                )
            ),
        );


        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'psvkpostsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$name.'&tab_module='.$this->tab.'&module_name='.$name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesPsvkpostsettingsSettings($name),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesPsvkpostsettingsSettings($name){
        $languages = Language::getLanguages(false);
        $fields_vkdesc = array();

        foreach ($languages as $lang)
        {
            $fields_vkdesc[$lang['id_lang']] =  Configuration::get($name.'vkdesc_'.$lang['id_lang']);

        }


        $data_config = array(
            'vkdesc' => $fields_vkdesc,
            'vkpost_on' => Configuration::get($name.'vkpost_on'),
        );

        return $data_config;


    }


    public function pstwitterform16($data){
        $update_button = $data['translate']['update_button'];
        $enable_pstwitterpost = $data['translate']['enable_pstwitterpost'];

        $template_text = $data['translate']['template_text'];
        $name = $data['translate']['name'];


        $fields_form = array(
            'form'=> array(
                //'tinymce' => FALSE,
                'legend' => array(
                    'title' => $enable_pstwitterpost,
                    'icon' => 'fa fa-facebook fa-twitter fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost,
                        'name' => 'twpost_on',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text,
                        'name' => 'twdesc',
                        'id' => 'twdesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',

                    ),


                ),



            ),


        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_button,
                )
            ),
        );


        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'pstwitterpostsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$name.'&tab_module='.$this->tab.'&module_name='.$name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesPstwitterpostsettingsSettings($name),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesPstwitterpostsettingsSettings($name){
        $languages = Language::getLanguages(false);
        $fields_twdesc = array();

        foreach ($languages as $lang)
        {
            $fields_twdesc[$lang['id_lang']] =  Configuration::get($name.'twdesc_'.$lang['id_lang']);

        }


        $data_config = array(
            'twdesc' => $fields_twdesc,
            'twpost_on' => Configuration::get($name.'twpost_on'),
        );

        return $data_config;


    }

    private function _autoposts16(){
        #### posts api ###
        include_once(dirname(__FILE__).'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        return $postshelp->postsSettings(array('translate'=>$this->_translate));
        #### posts api ###
    }


    public function _displayForm13_14_15(){
   		$_html = '';
    	
    	$_html .= '
		<fieldset '.(($this->_is15 == 1)?"class=\"ps15-width\"":"").'>
					<legend><img src="../modules/'.$this->name.'/logo.gif"  />
					'.$this->displayName.'</legend>
					
		<ul class="leftMenu">
			<li><a href="javascript:void(0)" onclick="tabs_custom(1)" id="tab-menu-1" class="selected"><img src="../modules/'.$this->name.'/views/img/btn/ico-news.gif" />'.$this->l('Welcome').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(3)" id="tab-menu-3"><img src="../modules/'.$this->name.'/views/img/btn/ico-news.gif" />'.$this->l('News Settings').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(4)" id="tab-menu-4"><img src="../modules/'.$this->name.'/views/img/btn/ico-news.gif" />'.$this->l('Moderate News').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(5)" id="tab-menu-5"><img src="../modules/'.$this->name.'/views/img/btn/ico-news.gif" />'.$this->l('SEO URL Rewriting, Sitemap').'</a></li>';
    		#### posts api ###
			$_html .= '<li><a href="javascript:void(0)" onclick="tabs_custom(8)" id="tab-menu-8"><img src="../modules/'.$this->name.'/views/img/AdminTools.gif" />'.$this->l('Automatically posts Integration').'</a></li>';
			#### posts api ###
			
		$_html .= '<li><a href="javascript:void(0)" onclick="tabs_custom(6)" id="tab-menu-6"><img src="../modules/'.$this->name.'/views/img/btn/ico-help.gif" />'.$this->l('Help / Documentation').'</a></li>
		<li><a href="http://addons.prestashop.com/en/2_community-developer?contributor=189784" target="_blank"><img src="../modules/'.$this->name.'/views/img/mitrocops-logo.png"  />&nbsp;'.$this->l('Other Mitrocops Modules').'</a></li>

		</ul>
		';
    	$_html .= '<div style="clear:both"></div>';
		$_html .= '<div class="reviewsadv-content">
						<div class="menu-content" id="tabs-1">'.$this->_welcome().'</div>';
		$_html .= '<div class="menu-content" id="tabs-3">'.$this->_newsSettings().'</div>';
		$_html .= '<div class="menu-content" id="tabs-4">'.$this->_moderateNews().'</div>';
		$_html .= '<div class="menu-content" id="tabs-5">'.$this->_UrlRewriteSettings().$this->_sitemap().'</div>';
		
		#### posts api ###
		include_once(dirname(__FILE__).'/classes/postshelp.class.php');
		$postshelp = new postshelp();

		$_html .= '<div class="menu-content" id="tabs-8">'.$postshelp->postsSettings(array('translate'=>$this->_translate)).'</div>';
		#### posts api ###
		
		$_html .= '<div class="menu-content" id="tabs-6">'.$this->_help_documentation().'</div>';
		$_html .= '<div style="clear:both"></div>';
		$_html .= '</div>';
		
		
		$_html .= '</fieldset>';
    	
    	return $_html;
    }
    
    private function _UrlRewriteSettings(){
    	$_html = '';
    	
    	$_html .= '<h3 class="title-block-content"><i class="fa fa-link fa-lg"></i>&nbsp;'.$this->l('SEO URL Rewriting, Sitemap Settings').'</h3>';
    	
    	
    	$_html .= '<form method="post" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'">';
    	
    	
    	$_html .= '<table style="width:100%">';
    	
    	
    	$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Enable or Disable URL rewriting').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="radio" value="1" id="text_list_on" name="rew_on" onclick="enableOrDisable(1)"
							'.(Tools::getValue('rew_on', Configuration::get($this->name.'rew_on')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t"> 
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>
					
					<input type="radio" value="0" id="text_list_off" name="rew_on"  onclick="enableOrDisable(0)"
						   '.(!Tools::getValue('rew_on', Configuration::get($this->name.'rew_on')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
					
					<p class="clear">'.$this->l('Enable only if your server allows URL rewriting (recommended)').'.</p>
				';
		$_html .= '<script type="text/javascript">
			    	function enableOrDisable(id)
						{
						if(id==0){
							$("#block-url-settings").hide(200);
						} else {
							$("#block-url-settings").show(200);
						}
							
						}
					</script>';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '</table>';
    	
    	$_html .= '<div id="block-url-settings" '.(Configuration::get($this->name.'rew_on')==1?'style="display:block"':'style="display:none"').'>';
    	
    	if(version_compare(_PS_VERSION_, '1.5', '>')){
    	$_html .= $this->_hint15();
    		 
    	} else{
    	$_html .= $this->_hint();
    	}	
    	
    	$_html .= '</div>';
    	
    	$_html .= '<p class="center" style="padding: 10px; margin-top: 10px;">
					<input type="submit" name="submit_urlrewriting" value="'.$this->l('Update settings').'" 
                		   class="button"  />
                	</p>';
    	
    	
    	$_html .= '<br/><br/>';
    	
    	/*$_html .= '<fieldset>
					<legend>'.$this->l('Tools').'</legend>';
    	
    	
    	$_html .= '<p>
                     <input type="submit" value="'.$this->l('Regenerate Google sitemap').'" name="submitsitemap" class="button"> 
                     &nbsp; <a target="_blank" href="'._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml">
                     			'._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml
                     		</a> 
                    </p>';
    	
    	$_html .= '<p class="hint clear" style="display: block; font-size: 11px; width: 95%; margin-top:20px;position:relative">
                            '.$this->l('To declare news sitemap xml, add this line at the end of your robots.txt file').': <br><br>
							  <strong>
								Sitemap '._PS_BASE_URL_.__PS_BASE_URI__.'upload/'.$this->name.'/news.xml
							 </strong>
                            </p>
                ';
    	
    	$_html .= '</fieldset>';
    	
    	$_html .= '</form>';*/
    	return $_html;
    }
    
    private function _hint(){
    	$_html = '';
    	
    	$_html .= '<p style="display: block; font-size: 11px; width: 95%; margin-bottom:20px;position:relative" class="hint clear">
    	<b style="color:#585A69">'.$this->l('If url rewriting doesn\'t works, check that this above lines exist in your current .htaccess file, if no, add it manually on top of your .htaccess file').':</b>
    	<br/><br/>
    	<b>
    	<code>
		RewriteRule ^(.*)news/([0-9a-zA-Z-_]+)/?$ /modules/blocknewsadv/news-item.php?id=$2 [QSA,L] 
		</code>
		</b>
		<br/>
		<b>
		<code>
		RewriteRule ^(.*)news/?$ /modules/blocknewsadv/news.php [QSA,L] 
		</code>
		</b>
		<br/><br/>
		</p>';
    	
    	return $_html;
    }
    
    private function _hint15(){
    	$_html = '';
    	
    	$_html .= '<p style="display: block; font-size: 11px; width: 95%; margin-bottom:20px;position:relative" class="hint clear">
    	<b style="color:#585A69">'.$this->l('If url rewriting doesn\'t works, check that this above lines exist in your current .htaccess file, if no, add it manually on top of your .htaccess file').':</b>
    	<br/><br/>
    	<b>
    	<code>
		RewriteRule ^(.*)news/([0-9a-zA-Z-_]+)/?$ /modules/blocknewsadv/news-item.php?id=$2 [QSA,L] 
		</code>
		</b>
		<br/>
		<b>
		<code>
		RewriteRule ^(.*)news/?$ /modules/blocknewsadv/news.php [QSA,L] 
		</code>
		</b>
		<br/><br/>
		</p>';
    	
    	return $_html;
    }
    
    
    private function _newsSettings(){
    	
    	
    	
    	$_html = '';
    	
    	$_html .= '<h3 class="title-block-content"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;'.$this->l('News Settings').'</h3>';
    	
    	
    	$_html .= '<form method="post" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'">';
    			
    	$_html .= '<style type="text/css">table.news-table-settings td{padding:3px}</style>';
    	
		$_html .= '<table style="width:100%" class="news-table-settings">';
		
		if(version_compare(_PS_VERSION_, '1.5', '>')){
		$_html .= '<tr>';
		$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
		
		$_html .= '<b>' . $this->l ( 'MULTISHOP in ADMIN PANEL, tab "Moderate News". Separates differents shop news in ADMIN PANEL depended on the shop selected by the admin' ) . ':</b>';
		
		$_html .= '</td>';
		$_html .= '<td class="r-set">';
		$_html .= '
		<input type="radio" value="1" id="text_list_on" name="switch_s"
		' . (Tools::getValue ( 'switch_s', Configuration::get ( $this->name . 'switch_s' ) ) ? 'checked="checked" ' : '') . '>
		<label for="dhtml_on" class="t">
		<img alt="' . $this->l ( 'Enabled' ) . '" title="' . $this->l ( 'Enabled' ) . '" src="../img/admin/enabled.gif">
		</label>
		<input type="radio" value="0" id="text_list_off" name="switch_s"
		' . (! Tools::getValue ( 'switch_s', Configuration::get ( $this->name . 'switch_s' ) ) ? 'checked="checked" ' : '') . '>
		<label for="dhtml_off" class="t">
		<img alt="' . $this->l ( 'Disabled' ) . '" title="' . $this->l ( 'Disabled' ) . '" src="../img/admin/disabled.gif">
		</label>
			
		';
		$_html .= '</td>';
		$_html .= '</tr>';
		}
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Position Block Last News').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		
    	
    	$_html .= '<table style="width:66%;">
	    				<tr>
	    					<td style="width: 33%">'.$this->l('Left column').'</td>
	    					<td style="width: 33%">'.$this->l('Right Column').'</td>
	    					
	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="leftnews" '.((Tools::getValue($this->name.'leftnews', Configuration::get($this->name.'leftnews')) ==1)?'checked':'').'  value="1"/>
	    					</td>
	    					<td>
	    						<input type="checkbox" name="rightnews" '.((Tools::getValue($this->name.'rightnews', Configuration::get($this->name.'rightnews')) ==1)?'checked':'') .' value="1"/>
	    					</td>
	    					
	    				</tr>
	    				<tr>
	    					<td>'.$this->l('Home').'</td>
	    					<td>'.$this->l('Footer').'</td>
	    					
	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="homenews" '.((Tools::getValue($this->name.'homenews', Configuration::get($this->name.'homenews')) ==1)?'checked':'').' value="1"/>
	    					</td>
	    					<td>
	    						<input type="checkbox" name="footernews" '.((Tools::getValue($this->name.'footernews', Configuration::get($this->name.'footernews')) ==1)?'checked':'').' value="1"/>
	    					</td>
	    					
	    				</tr>
	    				
	    			</table>';
    	
    	
		$_html .= '</td>';
		$_html .= '</tr>';


        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Position Block News Archives').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';


        $_html .= '<table style="width:66%;">
	    				<tr>
	    					<td style="width: 33%">'.$this->l('Left column').'</td>
	    					<td style="width: 33%">'.$this->l('Right Column').'</td>

	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="arch_left_n" '.((Tools::getValue($this->name.'arch_left_n', Configuration::get($this->name.'arch_left_n')) ==1)?'checked':'').'  value="1"/>
	    					</td>
	    					<td>
	    						<input type="checkbox" name="arch_right_n" '.((Tools::getValue($this->name.'arch_right_n', Configuration::get($this->name.'arch_right_n')) ==1)?'checked':'') .' value="1"/>
	    					</td>

	    				</tr>
	    				<tr>
	    					<td>'.$this->l('Footer').'</td>
	    					<td>&nbsp;</td>

	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="arch_footer_n" '.((Tools::getValue($this->name.'arch_footer_n', Configuration::get($this->name.'arch_footer_n')) ==1)?'checked':'').' value="1"/>
	    					</td>
	    					<td>
	    					&nbsp;
	    					</td>

	    				</tr>

	    			</table>';


        $_html .= '</td>';
        $_html .= '</tr>';



        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Position Block Search News').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';


        $_html .= '<table style="width:66%;">
	    				<tr>
	    					<td style="width: 33%">'.$this->l('Left column').'</td>
	    					<td style="width: 33%">'.$this->l('Right Column').'</td>

	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="search_left_n" '.((Tools::getValue($this->name.'search_left_n', Configuration::get($this->name.'search_left_n')) ==1)?'checked':'').'  value="1"/>
	    					</td>
	    					<td>
	    						<input type="checkbox" name="search_right_n" '.((Tools::getValue($this->name.'search_right_n', Configuration::get($this->name.'search_right_n')) ==1)?'checked':'') .' value="1"/>
	    					</td>

	    				</tr>
	    				<tr>
	    					<td>'.$this->l('Footer').'</td>
	    					<td>&nbsp;</td>

	    				</tr>
	    				<tr>
	    					<td>
	    						<input type="checkbox" name="search_footer_n" '.((Tools::getValue($this->name.'search_footer_n', Configuration::get($this->name.'search_footer_n')) ==1)?'checked':'').' value="1"/>
	    					</td>
	    					<td>
	    					&nbsp;
	    					</td>

	    				</tr>

	    			</table>';


        $_html .= '</td>';
        $_html .= '</tr>';


        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';


        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
        $_html .= '<b>'.$this->l('Enable or Disable Like Functional').':</b>';
         $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="radio" value="1" id="text_list_on" name="is_like"
							'.((Configuration::get($this->name.'is_like') ==1) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t">
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>

					<input type="radio" value="0" id="text_list_off" name="is_like"
						   '.((Configuration::get($this->name.'is_like') ==0) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
        $_html .= '<b>'.$this->l('Enable or Disable UnLike Functional').':</b>';
        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="radio" value="1" id="text_list_on" name="is_unlike"
							'.((Configuration::get($this->name.'is_unlike') ==1) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t">
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>

					<input type="radio" value="0" id="text_list_off" name="is_unlike"
						   '.((Configuration::get($this->name.'is_unlike') ==0) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
        $_html .= '</td>';
        $_html .= '</tr>';
		
		$_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in the Block Last News').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="number_ni"  
			               value="'.Tools::getValue('number_ni', Configuration::get($this->name.'number_ni')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Image width in the block "Last News"').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="text" name="n_block_img_w"  
			               value="'.Tools::getValue('n_block_img_w', Configuration::get($this->name.'n_block_img_w')).'"
			               >&nbsp;px
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Display date in the block "Last News"').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="checkbox" value="1" name="b_display_date" '.((Tools::getValue($this->name.'b_display_date', Configuration::get($this->name.'b_display_date')) ==1)?'checked':'').'>';
        $_html .= '</td>';
		$_html .= '</tr>';


        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Display images in the block "Last News"').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="checkbox" value="1" name="block_display_img" '.((Tools::getValue($this->name.'block_display_img', Configuration::get($this->name.'block_display_img')) ==1)?'checked':'').'>';
        $_html .= '</td>';
        $_html .= '</tr>';
		
		$_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('News per Page in the list view').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="news_page"  
			               value="'.Tools::getValue('news_page', Configuration::get($this->name.'news_page')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Image width in the lists items').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="text" name="lists_img_w"  
			               value="'.Tools::getValue('lists_img_w', Configuration::get($this->name.'lists_img_w')).'"
			               >&nbsp;px
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Display date in the lists items').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="checkbox" value="1" name="l_display_date" '.((Tools::getValue($this->name.'l_display_date', Configuration::get($this->name.'l_display_date')) ==1)?'checked':'').'>';
        $_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Image width on the news page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="text" name="item_img_w"  
			               value="'.Tools::getValue('item_img_w', Configuration::get($this->name.'item_img_w')).'"
			               >&nbsp;px
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Display date on the news page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '<input type="checkbox" value="1" name="i_display_date" '.((Tools::getValue($this->name.'i_display_date', Configuration::get($this->name.'i_display_date')) ==1)?'checked':'').'>';
        $_html .= '</td>';
		$_html .= '</tr>';


        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Active Social share buttons').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="checkbox" value="1" name="is_soc_but" '.((Tools::getValue($this->name.'is_soc_but', Configuration::get($this->name.'is_soc_but')) ==1)?'checked':'').'>';
        $_html .= '</td>';
        $_html .= '</tr>';


        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';


        $data_img_sizes = array();

        $available_types = ImageType::getImagesTypes('products');

        foreach ($available_types as $type){

            $id = $type['name'];
            $name = $type['name'].' ('.$type['width'].' x '.$type['height'].')';

            $data_item_size = array(
                'id' => $id,
                'name' => $name,
            );

            array_push($data_img_sizes,$data_item_size);


        }

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Image size for related products').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .= '<select class="select" name="img_size_rp"
							id="img_size_rp">';
        foreach($data_img_sizes as $image) {
            $_html .= '<option ' . (Tools::getValue('img_size_rp', Configuration::get($this->name . 'img_size_rp')) == $image['id'] ? 'selected="selected" ' : '') . ' value="'.$image['id'].'">' . $image['name'] . '</option>
						';
        }
        $_html .= '</select>';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Truncate product description').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="text" name="item_rp_tr"
			               value="'.Tools::getValue('item_rp_tr', Configuration::get($this->name.'item_rp_tr')).'"
			               >&nbsp;'.$this->l('chars').'
				';
        $_html .= '</td>';
        $_html .= '</tr>';



        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Image width in the related news block on the news page').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="text" name="rp_img_width"
			               value="'.Tools::getValue('rp_img_width', Configuration::get($this->name.'rp_img_width')).'"
			               >&nbsp;px
				';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';



        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
        $_html .= '<b>'.$this->l('Enable or Disable Facebook comments').':</b>';
        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="radio" value="1" id="text_list_on" name="is_comments"
							'.((Configuration::get($this->name.'is_comments') ==1) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t">
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>

					<input type="radio" value="0" id="text_list_off" name="is_comments"
						   '.((Configuration::get($this->name.'is_comments') ==0) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Numbers of comments visible').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="text" name="number_fc"
			               value="'.Tools::getValue('number_fc', Configuration::get($this->name.'number_fc')).'"
			               >
				';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0;vertical-align: top">';

        $_html .= '<b>'.$this->l('Facebook Application Id').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="text" name="appid" style="width:50%"
			               value="'.Tools::getValue('appid', Configuration::get($this->name.'appid')).'"
			               >
				<p class="clear">'.$this->l('To configure the Facebook Application Id read').
                                    '&nbsp;<a style="text-decoration:underline;font-weight:bold" href="../modules/'.$this->name.'/readme.pdf" target="_blank">readme.pdf</a> ,&nbsp;'.
                                 $this->l('which is located in the folder  with the module.').'</p>';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0;vertical-align: top">';

        $_html .= '<b>'.$this->l('My App').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
				<a target="_blank" href="http://www.facebook.com/apps/application.php?id='.Configuration::get($this->name.'appid').'"
				    class="button">'.$this->l('Check My App').'</a>
                &nbsp;&nbsp;
                <a target="_blank" href="http://www.facebook.com/developers/editapp.php?app_id='.Configuration::get($this->name.'appid').'&amp;view=web"
                class="button">'.$this->l('Configure App').'</a>
                &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/setup" class="button">'.$this->l('Create an App').'</a>
				';
        $_html .= '</td>';
        $_html .= '</tr>';



        $admin_img = "";
        $admin_explode = explode(",", Configuration::get($this->name.'appadmin'));
        if(sizeof($admin_explode)>0) {
            foreach ($admin_explode as $admin_explode) {
                if(Tools::strlen($admin_explode)==0) continue;

                $admin_img .= '<a href="http://www.facebook.com/profile.php?&id=' . $admin_explode . '" target="_blank">
								<img class="fb-admins" width="50"
									 src="https://graph.facebook.com/' . $admin_explode . '/picture?type=large">
							</a>';
            }
        }

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0;vertical-align: top">';

        $_html .= '<b>'.$this->l('Moderators').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '
					<input type="text" name="appadmin" style="width:50%"
			               value="'.Tools::getValue('appadmin', Configuration::get($this->name.'appadmin')).'"
			               >
				<p class="clear">'.$this->l('Id administrators, separated by commas').'<br/>'.
            $this->l('Add a facebook accounts ID for beeing comments moderators. ID Can be found on').
            '&nbsp;<a style="text-decoration:underline;font-weight:bold" href="http://findmyfbid.com/" target="_blank">http://findmyfbid.com/</a>'.
            ''.(Tools::strlen($admin_explode)>0?'<br/><br/>':'').$admin_img.'</p>';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:10px 20px 0 0;vertical-align: top">';

        $_html .= '<b>'.$this->l('Moderate Facebook comments').':</b>';

        $_html .= '</td>';




        $_html .= '<td style="text-align:left;padding-top:10px">';
        $_html .=  '

                <a target="_blank" href="http://developers.facebook.com/tools/comments?id='.Configuration::get($this->name.'appid').'&amp;view=edit_settings"
                class="button">'.$this->l('Settings Comments').'</a>
            &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/tools/comments?id='.Configuration::get($this->name.'appid').'&amp;view=queue"
                class="button">'.$this->l('Moderate Comments').'</a>
            &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/tools/comments?id='.Configuration::get($this->name.'appid').'"
                class="button">'.$this->l('Check new comments').'</a>


				';
        $_html .= '</td>';
        $_html .= '</tr>';


        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';


        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Image width in the block "Latest News" on home page').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="text" name="items_w_h"
			               value="'.Tools::getValue('items_w_h', Configuration::get($this->name.'items_w_h')).'"
			               >&nbsp;px
				';
        $_html .= '</td>';
        $_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Truncate text in the block "Latest News" on home page').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="text" name="item_h_tr"
			               value="'.Tools::getValue('item_h_tr', Configuration::get($this->name.'item_h_tr')).'"
			               >&nbsp;'.$this->l('chars').'
				';
        $_html .= '</td>';
        $_html .= '</tr>';


		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in Block Last News on Home page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="hnumber_ni"  
			               value="'.Tools::getValue('hnumber_ni', Configuration::get($this->name.'hnumber_ni')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';

        $_html .= '<tr>';
        $_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';

        $_html .= '<b>'.$this->l('Display date in Block Last News on Home page').':</b>';

        $_html .= '</td>';
        $_html .= '<td style="text-align:left">';
        $_html .=  '<input type="checkbox" value="1" name="h_display_date" '.((Tools::getValue($this->name.'h_display_date', Configuration::get($this->name.'h_display_date')) ==1)?'checked':'').'>';
        $_html .= '</td>';
        $_html .= '</tr>';




        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
        $_html .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	$_html .= '<b>'.$this->l('Enable or Disable RSS Feed').':</b>';
    	
    	$_html .= '<script type="text/javascript">
			    	function enableOrDisableRSS(id)
						{
						if(id==0){
							$("#block-rss-settings").hide(200);
						} else {
							$("#block-rss-settings").show(200);
						}
							
						}
					</script>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="radio" value="1" id="text_list_on" name="rsson" onclick="enableOrDisableRSS(1)"
							'.(Tools::getValue('rsson', Configuration::get($this->name.'rsson')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t"> 
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>
					
					<input type="radio" value="0" id="text_list_off" name="rsson" onclick="enableOrDisableRSS(0)"
						   '.(!Tools::getValue('rsson', Configuration::get($this->name.'rsson')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		$_html .= '</table>';
		
		
		
		$_html .= '<div id="block-rss-settings" '.(Configuration::get($this->name.'rsson')==1?'style="display:block"':'style="display:none"').'>';
    	
		$_html .= '<table style="width:100%" class="news-table-settings">';
		
		
		$_html .= '<tr>';
		$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
		$divLangName = "rssname¤srssdesc";
		
    	// Title of your RSS Feed
		
		$_html .= '<b>'.$this->l('Title of your RSS Feed').':</b>';
    			
		$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
    	
    	$_html .= '<div class="margin-form1">';
		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$rssname = Configuration::get($this->name.'rssname'.'_'.$id_lng);
	    	
	    	
			$_html .= '	<div id="rssname_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="rssname_'.$language['id_lang'].'" 
								  name="rssname_'.$language['id_lang'].'" 
								  value="'.htmlentities(Tools::stripslashes($rssname), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			$_html .= '';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'rssname');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
		$_html .= '</td>';
		$_html .= '</tr>';	
		
		
		
    	// Description of your RSS Feed
    	
		
		$_html .= '<tr>';
		$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
		
    	$_html .= '<b>'.$this->l('Description of your RSS Feed').':</b>';
    			
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
    	
    	$_html .= '<div class="margin-form1">';
		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$rssdesc = Configuration::get($this->name.'rssdesc_'.$id_lng);
	    	
	    	
			$_html .= '	<div id="srssdesc_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

							 <input type="text" style="width:400px"   
								  id="rssdesc_'.$language['id_lang'].'" 
								  name="rssdesc_'.$language['id_lang'].'" 
								  value="'.htmlentities(Tools::stripslashes($rssdesc), ENT_COMPAT, 'UTF-8').'"/>
								  
					</div>';
	    	}
			$_html .= '';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'srssdesc');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
		$_html .= '</td>';
		$_html .= '</tr>';		
    	// Description of your RSS Feed
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in RSS Feed').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="number_rssitems"  
			               value="'.Tools::getValue('number_rssitems', Configuration::get($this->name.'number_rssitems')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
    	$_html .= '</table>';
    	
    	$_html .= '</div>';
			
			$_html .= '<p class="center" style="padding: 10px; margin-top: 10px;">
					<input type="submit" name="news_settings" value="'.$this->l('Update settings').'" 
                		   class="button"  />
                	</p>';
					


		
			
		
		$_html .= '</form>';
		return $_html;
    }
    
    public function _moderateNews($data = null){
    
    $currentIndex = $this->context->currentindex;
    $cookie = $this->context->cookie;


    $currentIndex = isset($data['currentindex'])?$data['currentindex']:$currentIndex;
    $controller = isset($data['controller'])?$data['controller']:'AdminModules';

    $token = isset($data['token'])?$data['token']:Tools::getAdminToken($controller.(int)(Tab::getIdFromClassName($controller)).(int)($cookie->id_employee));

    $_html = '';
    	
    $_html .= '<h3 class="title-block-content"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;'.$this->l('Moderate News').'</h3>';
    
    	
    include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
		$obj_blocknews = new blocknewsadvfunctions();
		
		$i=0;
		$paging = '';
		$related_products = 0;
		$related_posts = 0;
		$id = 0;
		
		if(Tools::isSubmit("edit_item")){
			
			//$divLangName = "ccontent¤title";
			$divLangName = "ccontent¤title¤seokeywords¤seodescription";
			
			$_data = $obj_blocknews->getItem(array('id'=>(int)Tools::getValue("id")));
			
			$id = $_data['item'][0]['id'];
			$img = $_data['item'][0]['img'];
			$status = $_data['item'][0]['status'];
            $iscomments = $_data['item'][0]['is_comments'];
			$data_content = isset($_data['item']['data'])?$_data['item']['data']:'';
			$time_add = $_data['item'][0]['time_add'];
            $author = $_data['item'][0]['author'];
			$related_products = $_data['item'][0]['related_products'];
			$related_posts=$_data['item'][0]['related_posts'];
			
			$_html .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">';
    		
    		$_html .= '<label>'.$this->l('Title:').'</label>
    					<div class="margin-form">';
    		
			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$title = isset($data_content[$id_lng]['title'])?$data_content[$id_lng]['title']:"";
			
			$_html .= '	<div id="title_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="title_'.$language['id_lang'].'" 
								  name="title_'.$language['id_lang'].'" 
								  value="'.htmlentities(Tools::stripslashes($title), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'title');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
    		$_html .= '<div style="clear:both"></div>';
							
			$_html .= '</div>';
			
			
			
			if(Configuration::get($this->name.'rew_on') == 1){
			// identifier
			$current_lng =  $cookie->id_lang;
			$seo_url = isset($data_content[$current_lng]['seo_url'])?$data_content[$current_lng]['seo_url']:"";
	   
		    	
			$_html .= '<label>'.$this->l('Identifier (SEO URL)').'</label>';
	    	
	    	$_html .= '<div class="margin-form">';
	    	
				
				$_html .= '
							<input type="text" style="width:400px"   
									  id="seo_url" 
									  name="seo_url" 
									  value="'.$seo_url.'"/>
							<p>(eg: domain.com/news/identifier)</p>
							';
		    $_html .=  '</div>';
			}
			
			
			$_html .= '<label>'.$this->l('SEO Keywords').'</label>';
    			
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_keywords = isset($data_content[$id_lng]['seo_keywords'])?$data_content[$id_lng]['seo_keywords']:"";
	   
		    	
				$_html .= '	<div id="seokeywords_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seokeywords_'.$language['id_lang'].'" 
									  name="seokeywords_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(Tools::stripslashes($seo_keywords), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seokeywords');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
			
			$_html .= '<label>'.$this->l('SEO Description').'</label>';
	    			
	    	
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_description = isset($data_content[$id_lng]['seo_description'])?$data_content[$id_lng]['seo_description']:"";
	       	
				$_html .= '	<div id="seodescription_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seodescription_'.$language['id_lang'].'" 
									  name="seodescription_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(Tools::stripslashes($seo_description), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seodescription');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
		
    		
    		$_html .= '<label>'.$this->l('Logo Image').'</label>
    			
    				<div class="margin-form">
					<input type="file" name="news_image" id="news_image" />
					<p>Allow formats *.jpg; *.jpeg; *.png; *.gif.</p>';

    		if(Tools::strlen($img)>0){
	    	$_html .= '<div id="post_images_list">';
	    		$_html .= '<div style="float:left;margin:10px" id="post_images_id">';
	    		$_html .= '<table width=100%>';
	    		
	    		$_html .= '<tr><td align="left">';
	    		$_html .= '<input type="radio" checked name="post_images"/>';
	    		
	    		$_html .= '</td>';
	    		
	    		$_html .= '<td align="right">';
	    		
	    			$_html .= '<a href="javascript:void(0)" title="'.$this->l('Delete').'"  
	    						onclick = "delete_img('.$id.');"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>
	    					';
	    		
	    		$_html .= '</td>';
	    		
	    		$_html .= '<tr>';
	    		$_html .= '<td colspan=2>';
	    		if($this->_is_cloud){
                    $_html .= '<img src="../modules/'.$this->name.'/upload/'.$img.'" style="width:50px;height:50px"/>';
                } else {
                    $_html .= '<img src="../upload/'.$this->name.'/'.$img.'" style="width:50px;height:50px"/>';
                }

	    		$_html .= '</td>';
	    		$_html .= '</tr>';
	    		
	    		$_html .= '</table>';
	    		
	    		$_html .= '</div>';
	    	//}
	    	$_html .= '<div style="clear:both"></div>';
	    	$_html .= '</div>';
	    	}
	    	
	    	$_html .= '</div>';
    		
    		
    		if(defined('_MYSQL_ENGINE_')){
	    		
    		$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form" >';
    		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
	    	$id_lng = (int)$language['id_lang'];
	    	$content = isset($data_content[$id_lng]['content'])?$data_content[$id_lng]['content']:"";
			
			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;'; 
	    	
			if($this->_is16 == 1){ $_html .= 'width:80%;'; }
							 
			$_html .= ' ">

						<textarea class="rte" cols="25" rows="10" ';
			if($this->_is16 == 0){
				$_html .= 'style="width:400px"';
			}  
								$_html .= '  id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(Tools::stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
	    	}
	    	
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
	    	
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
	    	}else{
	    		
	    		$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form">';
	    					
	    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    	$languages = Language::getLanguages(false);
		    	$divLangName = "ccontent";
		    	
		    	foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
	    		$content = isset($data_content[$id_lng]['content'])?$data_content[$id_lng]['content']:"";
			
				$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
	
							<textarea class="rte" cols="25" rows="10" style="width:400px"   
									  id="content_'.$language['id_lang'].'" 
									  name="content_'.$language['id_lang'].'">'.htmlentities(Tools::stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>
	
						</div>';
		    	}
				ob_start();
				$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
				$displayflags = ob_get_clean();
				$_html .= $displayflags;
		    	
				$_html .= '<div style="clear:both"></div>';
				
				$_html .= '</div>';
	    	}
	    	
	    	
	    	
		#### related products ###
			$_html .= '<label>'.$this->l('Related Products').'</label>';
    	
			$accessories = (($related_products!=0) ? $obj_blocknews->getProducts($related_products) : array());

            $_html .= '<div class="margin-form">'; 

            $_html .= '<div id="divAccessories">';
            foreach ($accessories as $accessory)
                $_html .= $accessory['name'] . (!empty($accessory['reference']) ? ' (' . $accessory['reference'] . ')' : '') 
                . ' <span class="delAccessory" name="' . $accessory['id_product'] .
                 '" style="cursor:pointer;"><img src="../img/admin/delete.gif" class="middle" alt="Delete" /></span><br />';
                
            $_html .= '</div>';
            
            $_html .= '<input type="hidden" name="inputAccessories" id="inputAccessories" value="';
            foreach ($accessories as $accessory) {
                $_html .= $accessory['id_product'] . '-';
            } $_html .= '" />	
                       <input type="hidden" name="nameAccessories" id="nameAccessories" value="';
            foreach ($accessories as $accessory) {
                 $_html .= $accessory['name'] . '¤';
            } $_html .= '" />';

            $_html .= '<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:'.(($this->_is15 == 0)?'550px':'600px').'">
                
                             <input type="text" value="" id="product_autocomplete_input" style="width:300px" />';
              $_html .= '<p class="clear">' . $this->l('Begin typing the first letters of the product name, then select the product from the drop-down list') . '</p>
                        </div>';
		
		
		$_html .=  '</div>';
		
		$_html .= '<div style="clear:both"></div>';
		
		$_html .= '<script type="text/javascript">
		 $(\'document\').ready( function() {
			 if($(\'#divAccessories\').length){
	        	initAccessoriesAutocomplete();
	        	$(\'#divAccessories\').delegate(\'.delAccessory\', \'click\', function(){ delAccessory($(this).attr(\'name\')); });
	   		 }
   		 });
		</script>';
		
		### realted products ###
		
		
		### related posts ####
		
		$_data_cat  = $obj_blocknews->getRelatedPosts(array('admin'=>1,'id'=>$id)); 
		
    	$_html .= '<label>'.$this->l('Related posts').'</label>
    					<div class="margin-form">';
		
    	$_html .= '
		<div style="height:140px; overflow-x:hidden; overflow-y:scroll; padding:0;" class="margin-form">
		
		<table cellspacing="0" cellpadding="0" style="min-width: '.(($this->_is15 == 0)?'550px':'600px').'" class="table">
            <tr>
				<th style="width:40px;"></th>
				<th style="width:30px;">ID</th>
				<th>'.$this->l('Title').'</th>
				<th>'.$this->l('Lang').'</th>
            </tr>';
            
			$y=0;	
			$related_posts = explode(",",$related_posts);
			foreach($_data_cat['related_posts'] as $_item){
				$name = isset($_item['title'])?$_item['title']:'';
				$id_pr = isset($_item['id'])?$_item['id']:'';
				
				$ids_lng = isset($_item['ids_lng'])?$_item['ids_lng']:array();
				$lang_for_related_posts = array();
				foreach($ids_lng as $lng_id){
					$data_lng = Language::getLanguage($lng_id);
					$lang_for_related_posts[] = $data_lng['iso_code']; 
				}
				$lang_for_related_posts = implode(",",$lang_for_related_posts);
				
				if(Tools::strlen($name)==0) continue;
				
		       $_html .= '
		       		<tr class="'.(($y%2==0)?'':'alt_row').'">
						<td>
							<input type="checkbox" value="'.$id_pr.'"  id="groupRelated_'.$id_pr.'"
								   class="groupBox" name="ids_related_posts[]"
								   '.(in_array($id_pr,$related_posts)?'checked="checked"':'').' />
						</td>
						<td>'.$id_pr.'</td>
						<td><label class="t" for="groupRelated_'.$id_pr.'">'.$name.'</label></td>
						<td>'.$lang_for_related_posts.'</td>
					</tr>';
		       $y++;
			}
	

      	 $_html .= '
       	</table>
									
		</div>';
		
		
		$_html .=  '</div>';
		
		### related posts ####

            $_html .= '<label>'.$this->l('Enable Facebook Comments').'</label>
				<div class = "margin-form" style="padding: 0pt 0pt 10px 130px;">';

            $_html .= '<select name="item_iscomments">
					<option value=1 '.(($iscomments==1)?"selected=\"true\"":"").'>'.$this->l('Enabled').'</option>
					<option value=0 '.(($iscomments==0)?"selected=\"true\"":"").'>'.$this->l('Disabled').'</option>
				   </select>';


            $_html .= '</div>';
	    	
	    	
	    	$_html .= '<label>'.$this->l('Status').'</label>
				<div class = "margin-form">';
				
			$_html .= '<select name="item_status" style="width:100px">
						<option value=1 '.(($status==1)?"selected=\"true\"":"").'>'.$this->l('Enabled').'</option>
						<option value=0 '.(($status==0)?"selected=\"true\"":"").'>'.$this->l('Disabled').'</option>
					   </select>';
				
				
			$_html .= '</div>';
			
			
		

		#### publication date ####	
			
		$date_tmp = '';
    	if(isset($time_add)){
    	$date_tmp = strtotime($time_add);
    	$date_tmp = date('Y-m-d H:i:s',$date_tmp);
    	} else {
    		$date_tmp = date('Y-m-d H:i:s');
    	}
    	
    	$_html .= '<div class="clear"></div>';
    	$_html .= $obj_blocknews->displayDateField('time_add', $date_tmp, $this->l('Publication date'), $this->l('Format : YYYY-MM-DD HH:MM:SS'));
    	
    	//if(version_compare(_PS_VERSION_, '1.5', '>')){
    	$_html .= '<script type="text/javascript">
    	$(\'document\').ready( function() {
    	
    	
	    	if ($(".datepicker").length > 0){
	    	
	    	var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs; 
			
	           $(".datepicker").datepicker({ prevText: \'\', nextText: \'\', dateFormat: \'yy-mm-dd\'+time});
	       	}
       	});
    	</script>';
    	//}
    	
    	#### publication date ####
			
			
		if($this->_is15){
	    	// shop association
	    	$_html .= '<div class="clear"></div>';
	    	$_html .= '<label>'.$this->l('Shop association').':</label>';
	    	$_html .= '<div class="margin-form" style="padding: 0pt 0pt 10px 130px;">';
	
			$_html .= '<table width="50%" cellspacing="0" cellpadding="0" class="table">
							<tr>
								<th>'.$this->l('Shop').'</th>
							</tr>';
			$u = 0;
			
			$shops = Shop::getShops();
			$shops_tmp = explode(",",isset($_data['item'][0]['id_shop'])?$_data['item'][0]['id_shop']:"");
			
			$count_shops = sizeof($shops);
			foreach($shops as $_shop){
				$id_shop = $_shop['id_shop'];
				$name_shop = $_shop['name'];
				 $_html .= '<tr>
							<td>
								<img src="../img/admin/lv2_'.((($count_shops-1)==$u)?"f":"b").'.png" alt="" style="vertical-align:middle;">
								<label class="child">';
			 
				
					$_html .= '<input type="checkbox"  
									   name="cat_shop_association[]" 
									   value="'.$id_shop.'" '.((in_array($id_shop,$shops_tmp))?'checked="checked"':'').' 
									   class="input_shop" 
									   />
									'.$name_shop.'';
					
					$_html .= '</label>
							</td>
						</tr>';
			 $u++;
			}
		
			$_html .= '</table>';
				
			$_html .= '</div>';
																	
	    	}
    	// shop association
    	
    		$_html .= '<label>&nbsp;</label>
						<div class = "margin-form"  style="margin-top:10px">
						<input type="submit" name="cancel_item" value="'.$this->l('Cancel').'" 
                		   class="button"  />&nbsp;&nbsp;&nbsp;
						<input type="submit" name="update_item" value="'.$this->l('Update').'" 
                		   class="button"  />
                		  </div>';
    		
    		$_html .= '</form>';
    		
		} else {
			
			$divLangName = "ccontent¤title¤seokeywords¤seodescription";
			$name = "";
			$content = "";
			$time_add = null;
            $author = "";
			
    		$_html .= '<a href="javascript:void(0)" onclick="$(\'#add-question-form\').show(200);$(\'#link-add-question-form\').hide(200)"
    					id="link-add-question-form"	
					  style="border: 1px solid #F05F5D; padding: 5px; margin-bottom: 10px; display: block; font-size: 16px; color: #F05F5D; text-align: center; font-weight: bold; text-decoration: underline;"
					  >'.$this->l('Add New Item').'</a>';
    		
    		$_html .= '<div style="border: 1px solid rgb(222, 222, 222);padding-top:10px;display:none" id="add-question-form">';
			$_html .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">';
    		
    		$_html .= '<label>'.$this->l('Title').'</label>
    					<div class="margin-form">';
    		
    		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	
			$_html .= '	<div id="title_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="title_'.$language['id_lang'].'" 
								  name="title_'.$language['id_lang'].'" 
								  value="'.htmlentities(Tools::stripslashes($name), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'title');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
    		
			$_html .= '</div>';
			
			if(Configuration::get($this->name.'rew_on') == 1){
			// identifier
			$current_lng =  $cookie->id_lang;
			$seo_url = "";
		    	
			$_html .= '<label>'.$this->l('Identifier (SEO URL)').'</label>';
	    	
	    	$_html .= '<div class="margin-form">';
	    	
				
				$_html .= '
							<input type="text" style="width:400px"   
									  id="seo_url" 
									  name="seo_url" 
									  value="'.$seo_url.'"/>
							<p>(eg: domain.com/news/identifier)</p>
							';
		    $_html .=  '</div>';
			}
			
			
			$_html .= '<label>'.$this->l('SEO Keywords').'</label>';
    			
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_keywords = "";
		    	
				$_html .= '	<div id="seokeywords_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seokeywords_'.$language['id_lang'].'" 
									  name="seokeywords_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(Tools::stripslashes($seo_keywords), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seokeywords');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
			
			$_html .= '<label>'.$this->l('SEO Description').'</label>';
	    			
	    	
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_description = "";
		    	
				$_html .= '	<div id="seodescription_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seodescription_'.$language['id_lang'].'" 
									  name="seodescription_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(Tools::stripslashes($seo_description), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seodescription');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
    		
    		$_html .= '<label>'.$this->l('Logo Image').'</label>
    			
    				<div class="margin-form">
					<input type="file" name="news_image" id="news_image" />
					<p>'.$this->l('Allow formats').' *.jpg; *.jpeg; *.png; *.gif.</p>';
    	
	    	$_html .= '</div>';
    		
    		if(defined('_MYSQL_ENGINE_')){
	    	
    			
    		$_html .= '<label>'.$this->l('Content').'</label>
	    					<div class="margin-form" >';
    			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){

			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;';

	    	if($this->_is16 == 1){ $_html .= 'width:80%;'; }
	    	
			$_html .= '"
							 >

						<textarea class="rte" cols="25" rows="10" ';

			if($this->_is16 == 0){
				$_html .= 'style="width:400px"';
			} 
			
			$_html .= 'id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(Tools::stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
	    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			$_html .= '</div>';
			
	    	}else{
	    		
	    	$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form" >';
    			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language)

			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<textarea class="rte" cols="25" rows="10" style="width:400px"   
								  id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(Tools::stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			$_html .= '</div>';
	    	}
	    	
	    	
	    #### related products ###
			$_html .= '<label>'.$this->l('Related Products').'</label>';
    	
			$accessories = (($related_products!=0) ? $obj_blocknews->getProducts($related_products) : array());

            $_html .= '<div class="margin-form">'; 

            $_html .= '<div id="divAccessories">';
            foreach ($accessories as $accessory)
                $_html .= $accessory['name'] . (!empty($accessory['reference']) ? ' (' . $accessory['reference'] . ')' : '') 
                . ' <span class="delAccessory" name="' . $accessory['id_product'] .
                 '" style="cursor:pointer;"><img src="../img/admin/delete.gif" class="middle" alt="Delete" /></span><br />';
                
            $_html .= '</div>';
            
            $_html .= '<input type="hidden" name="inputAccessories" id="inputAccessories" value="';
            foreach ($accessories as $accessory) {
                $_html .= $accessory['id_product'] . '-';
            } $_html .= '" />	
                       <input type="hidden" name="nameAccessories" id="nameAccessories" value="';
            foreach ($accessories as $accessory) {
                 $_html .= $accessory['name'] . '¤';
            } $_html .= '" />';

            $_html .= '<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:'.(($this->_is15 == 0)?'550px':'600px').'">
                
                             <input type="text" value="" id="product_autocomplete_input" style="width:300px" />';
              $_html .= '<p class="clear">' . $this->l('Begin typing the first letters of the product name, then select the product from the drop-down list') . '</p>
                        </div>';
		
		
		$_html .=  '</div>';
		
		$_html .= '<div style="clear:both"></div>';
		
		$_html .= '<script type="text/javascript">
		 $(\'document\').ready( function() {
			 if($(\'#divAccessories\').length){
	        	initAccessoriesAutocomplete();
	        	$(\'#divAccessories\').delegate(\'.delAccessory\', \'click\', function(){ delAccessory($(this).attr(\'name\')); });
	   		 }
   		 });
		</script>';
		
		### realted products ###
		
		
		### related posts ####
		
		$_data_cat  = $obj_blocknews->getRelatedPosts(array('admin'=>1,'id'=>$id)); 
		
    	$_html .= '<label>'.$this->l('Related posts').'</label>
    					<div class="margin-form">';
		
    	$_html .= '
		<div style="height:140px; overflow-x:hidden; overflow-y:scroll; padding:0;" class="margin-form">
		
		<table cellspacing="0" cellpadding="0" style="min-width: '.(($this->_is15 == 0)?'550px':'600px').'" class="table">
            <tr>
				<th style="width:40px;"></th>
				<th style="width:30px;">ID</th>
				<th>'.$this->l('Title').'</th>
				<th>'.$this->l('Lang').'</th>
            </tr>';
            
			$y=0;	
			$related_posts = explode(",",$related_posts);
			foreach($_data_cat['related_posts'] as $_item){
				$name = isset($_item['title'])?$_item['title']:'';
				$id_pr = isset($_item['id'])?$_item['id']:'';
				
				$ids_lng = isset($_item['ids_lng'])?$_item['ids_lng']:array();
				$lang_for_related_posts = array();
				foreach($ids_lng as $lng_id){
					$data_lng = Language::getLanguage($lng_id);
					$lang_for_related_posts[] = $data_lng['iso_code']; 
				}
				$lang_for_related_posts = implode(",",$lang_for_related_posts);
				
				if(Tools::strlen($name)==0) continue;
				
		       $_html .= '
		       		<tr class="'.(($y%2==0)?'':'alt_row').'">
						<td>
							<input type="checkbox" value="'.$id_pr.'"  id="groupRelated_'.$id_pr.'"
								   class="groupBox" name="ids_related_posts[]"
								   '.(in_array($id_pr,$related_posts)?'checked="checked"':'').' />
						</td>
						<td>'.$id_pr.'</td>
						<td><label class="t" for="groupRelated_'.$id_pr.'">'.$name.'</label></td>
						<td>'.$lang_for_related_posts.'</td>
					</tr>';
		       $y++;
			}
	

      	 $_html .= '
       	</table>
									
		</div>';
		
		
		$_html .=  '</div>';
		
		### related posts ####

            $_html .= '<label>'.$this->l('Enable Facebook Comments').'</label>
				<div class = "margin-form" style="padding: 0pt 0pt 10px 130px;">';

            $_html .= '<select name="item_iscomments">
					<option value=1 selected="true">'.$this->l('Enabled').'</option>
					<option value=0>'.$this->l('Disabled').'</option>
				   </select>';


            $_html .= '</div>';
	    	
	    	
		$_html .= '<label>'.$this->l('Status').'</label>
				<div class = "margin-form" style="padding: 0pt 0pt 10px 130px;">';
				
		$_html .= '<select name="item_status">
					<option value=1 selected="true">'.$this->l('Enabled').'</option>
					<option value=0>'.$this->l('Disabled').'</option>
				   </select>';
			
				
		$_html .= '</div>';
			
			
			
		#### publication date ####	
			
		$date_tmp = '';
    	if(isset($time_add)){
    	$date_tmp = strtotime($time_add);
    	$date_tmp = date('Y-m-d H:i:s',$date_tmp);
    	} else {
    		$date_tmp = date('Y-m-d H:i:s');
    	}
    	
    	$_html .= '<div class="clear"></div>';
    	$_html .= $obj_blocknews->displayDateField('time_add', $date_tmp, $this->l('Publication date'), $this->l('Format : YYYY-MM-DD HH:MM:SS'));
    	
    	//if(version_compare(_PS_VERSION_, '1.5', '>')){
    	$_html .= '<script type="text/javascript">
    	$(\'document\').ready( function() {
    	
    	
	    	if ($(".datepicker").length > 0){
	    	
	    	var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs; 
			
	           $(".datepicker").datepicker({ prevText: \'\', nextText: \'\', dateFormat: \'yy-mm-dd\'+time});
	       	}
       	});
    	</script>';
    	//}
    	
    	#### publication date ####
    		
			
		if($this->_is15){
	    	// shop association
	    	$_html .= '<div class="clear"></div>';
	    	$_html .= '<label>'.$this->l('Shop association').':</label>';
	    	$_html .= '<div class="margin-form" style="padding: 0pt 0pt 10px 130px;">';
	
			$_html .= '<table width="50%" cellspacing="0" cellpadding="0" class="table">
							<tr>
								<th>'.$this->l('Shop').'</th>
							</tr>';
			$u = 0;
			
			$shops = Shop::getShops();
			$shops_tmp = explode(",",isset($_data['post'][0]['id_shop'])?$_data['post'][0]['id_shop']:"");
			
			$count_shops = sizeof($shops);
			foreach($shops as $_shop){
				$id_shop = $_shop['id_shop'];
				$name_shop = $_shop['name'];
				 $_html .= '<tr>
							<td>
								<img src="../img/admin/lv2_'.((($count_shops-1)==$u)?"f":"b").'.png" alt="" style="vertical-align:middle;">
								<label class="child">';
			 
				
					$_html .= '<input type="checkbox"  
									   name="cat_shop_association[]" 
									   value="'.$id_shop.'" '.((in_array($id_shop,$shops_tmp))?'checked="checked"':'').' 
									   class="input_shop" 
									   />
									'.$name_shop.'';
					
					$_html .= '</label>
							</td>
						</tr>';
			 $u++;
			}
		
			$_html .= '</table>';
				
			$_html .= '</div>';
																	
	    	}
    	// shop association		
			
    	
    		$_html .= '<label>&nbsp;</label>
						<div class = "margin-form"  style="margin-top:10px">
						<input type="button" value="'.$this->l('Cancel').'" 
                		   class="button"  
                		   onclick="$(\'#link-add-question-form\').show(200);$(\'#add-question-form\').hide(200);" 
                		   />&nbsp;&nbsp;&nbsp;
						<input type="submit" name="submit_item" value="'.$this->l('Save').'" 
                		   class="button"  />
                		  </div>';
    		
    		$_html .= '</form>';
    		$_html .= '</div>';
		
    		$_html .= '<br/>';
    		
			$_html .= '<table class = "table" width = 100%>
			<tr>
				<th width="5%">'.$this->l('ID').'</th>';
			
			if($this->_is15){
					$_html .= '<th width=100>'.$this->l('Shop').'</th>';
    			}
				$_html .= '<th width=50>'.$this->l('Language').'</th>';
			
			$_html .= '<th width="10%">'.$this->l('Image').'</th>
				<th>'.$this->l('Title').'</th>
				<th>'.$this->l('Count likes').'</th>
				<th width="20%">'.$this->l('Date').'</th>
				<th width="5%">'.$this->l('Enable Facebook Comments').'</th>
				<th width="5%">'.$this->l('Status').'</th>
				<th width = "7%">'.$this->l('Action').'</th>
			</tr>';
			
			$start = (int)Tools::getValue("pageitems");
			$_data = $obj_blocknews->getItems(array('admin'=>1,'start' => $start,'step'=>$this->_step));
			$_items = $_data['items'];
			//echo "<pre>"; var_dump($_items); 
			$paging = $obj_blocknews->PageNav($start,$_data['count_all'],$this->_step, 
											array('admin' => 1,'currentIndex'=>$currentIndex,
												  'token' => '&configure='.$this->name.'&token='.$token,
												  'item' => 'items'
											));
			
			if(sizeof($_items)>0){
				
				foreach($_items as $_item){
					$i=1;		
					$id = $_item['id'];
					$title = $_item['title'];
					$img = $_item['img'];
					$status = $_item['status'];
					$date = $_item['time_add'];
                    $count_likes = $_item['count_likes'];
                    $is_comments = $_item['is_comments'];
					
					$ids_lng = isset($_item['ids_lng'])?$_item['ids_lng']:array();
					$lang_for_item = array();
					foreach($ids_lng as $lng_id){
						$data_lng = Language::getLanguage($lng_id);
						$lang_for_item[] = $data_lng['iso_code']; 
					}
					$lang_for_item = implode(",",$lang_for_item);
					
					if($this->_is15){
						$ids_shops = explode(",",$_item['id_shop']);
						
						$shops = Shop::getShops();
						$name_shop = array();
						foreach($shops as $_shop){
							$id_shop_lists = $_shop['id_shop'];
							if(in_array($id_shop_lists,$ids_shops))
								$name_shop[] = $_shop['name'];
						}
						
						$name_shop = implode(",<br/>",$name_shop);
					}
			
					$_html .= 
						'<tr>
						<td style = "color:black;">'.$id.'</td>';
					
					if($this->_is15){
						$_html .= '<td style = "color:black;">'.$name_shop.'</td>';
					}
					$_html .= '<td style = "color:black;">'.$lang_for_item.'</td>';
					
					$_html .= '<td style = "color:black;" align="center">';
                    if($this->_is_cloud){
                        $logo_img_path = '../modules/'.$this->name.'/upload/';
                    } else {
                        $logo_img_path = '../upload/'.$this->name.'/';
                    }
					if($img)
						$_html .= '<img src="'.$logo_img_path.$img.'" style="width:40px;height:40px" />';
					else
                        $_html .= '---';
						
					$_html .= '</td>
						<td style = "color:black;">'.$title.'</td>
						<td style = "color:black;">'.$count_likes.'</td>';
					$_html .= '<td style = "color:black;">'.$date.'</td>';


                    if($is_comments)
                        $_html .= '<td align="center"><img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif"></td>';
                    else
                        $_html .= '<td align="center"><img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif"></td>';

					if($status)
						$_html .= '<td align="center"><img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif"></td>';
					else
						$_html .= '<td align="center"><img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif"></td>';
				
			
					$_html .= '<td>
				
								 <input type = "hidden" name = "id" value = "'.$id.'"/>
								 <a href="'.$currentIndex.'&configure='.$this->name.'&token='.$token.'&edit_item&id='.(int)($id).'&pageitems='.$start.'" title="'.$this->l('Edit').'"><img src="'._PS_ADMIN_IMG_.'edit.gif" alt="" /></a>
								 <a href="'.$currentIndex.'&configure='.$this->name.'&token='.$token.'&delete_item&id='.(int)($id).'&pageitems='.$start.'" title="'.$this->l('Delete').'"  onclick = "javascript:return confirm(\''.$this->l('Are you sure you want to remove this item?').'\');"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>';
								 $_html .= '</form>
							 </td>';
					$_html .= '</tr>';
				}
			} else {
			$_html .= '<tr><td colspan="8" style="text-align:center;font-weight:bold;padding:10px">'.$this->l('Items not found').'</td></tr>';	
			}
			
			$_html .= '</table>';
		}
			
		     if($i!=0){
		    	$_html .= '<div style="margin:5px">';
		    	$_html .= $paging;
		    	$_html .= '</div>';
		    	}
		   return $_html;
    }
    
 private function _help_documentation(){
     $_html = '';

     if(version_compare(_PS_VERSION_, '1.6', '>')){
         $_html .= '<div class="panel">

				<div class="panel-heading"><i class="fa fa-question-circle fa-lg"></i>&nbsp;'.$this->l('Help / Documentation').'</div>';
     } else {
         $_html .= '<h3 class="title-block-content">'.$this->l('Help / Documentation').'</h3>';
     }

     $_html .= '<b style="text-transform:uppercase">'.$this->l('MODULE DOCUMENTATION ').':</b>&nbsp;<a target="_blank" href="../modules/'.$this->name.'/readme.pdf" style="text-decoration:underline;font-weight:bold">readme.pdf</a>
                <br/><br/><br/><br/>'.
         '<b style="text-transform:uppercase">'.$this->l('GOOGLE RICH SNIPPETS TEST TOOL ').':</b>&nbsp;<a target="_blank" href="https://developers.google.com/structured-data/testing-tool/" style="text-decoration:underline;font-weight:bold">https://developers.google.com/structured-data/testing-tool/</a>
            <br/><br/><br/><br/>'.
         '<b style="text-transform:uppercase">'.$this->l('Snippets based on the ').':</b>
         &nbsp;
         <b>'.$this->l('Rich Snippets for Articles').'</b>
         '.$this->l('More info').'&nbsp;<a target="_blank" href="https://developers.google.com/structured-data/rich-snippets/articles"
         style="text-decoration:underline;font-weight:bold">https://developers.google.com/structured-data/rich-snippets/articles</a>

    		';
     if(version_compare(_PS_VERSION_, '1.6', '>')){
         $_html .= '</div>';
     }

     $_html .= $this->_customhookhelp();

     return $_html;
    }



    private function _customhookhelp(){
        $_html  = '';

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '<div class="panel">

		<div class="panel-heading"><i class="fa fa-question-circle fa-lg"></i>&nbsp;'.$this->l('Frequently Asked Questions').'</div>';
        } else {

            $_html .= '<fieldset>
		<legend><img src="../modules/'.$this->name.'/views/img/ico_help.gif" />'.$this->l('Frequently Asked Questions').'</legend>

		';
        }

        if(version_compare(_PS_VERSION_, '1.5', '>')){

            $_html .= '<div class="row ">

                       ';

            $_html .= '<div class="span">
                          <p>
                             <span style="font-weight: bold; font-size: 15px;" class="question">
                             	- <b style="color:red">'.$this->l('CUSTOM HOOK HELP:').'</b> '.$this->l('How I can show Block Last News, Block News Archives, Block Search News on a single page (CMS or other places for example) ?').'
                             </span>
                             <br/><br/>
                             <span style="color: black;" class="answer">
                             	   '.$this->l('You just need to add a line of code to the tpl file of the page where you want to add the Block Last News, Block News Archives, Block Search News').':
                                   <br/><br/>
                                   <pre>{hook h=\'newsMitrocops\'}</pre>
                              </span>

                         </p>
                       </div><br/><br/>';
        }




        if(version_compare(_PS_VERSION_, '1.5', '>')){
            $_html .= '</div>';
        }

        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $_html .= '</div>';
        } else {
            $_html .= '</fieldset>';
        }

        return $_html;
    }
    
    
 private function _welcome(){
     $data = '';
     if(version_compare(_PS_VERSION_, '1.6', '>')){
         $data .= '<div class="panel">

			<div class="panel-heading"><i class="fa fa-home fa-lg"></i>&nbsp;'.$this->l('Welcome').'</div>';
     } else {
         $data .= '<h3 class="title-block-content">'.$this->l('Welcome').'</h3>';
     }

    	$data .=   $this->l('Welcome and thank you for purchasing the module.').
    			'<br/><br/>'
    			.$this->l('The module allows users add news on site.').
    			'<br/><br/>'
    			.$this->l('To configure module please read').'&nbsp;<b><a style="text-decoration:underline" id="tab-menu-6" onclick="tabs_custom(6)" href="javascript:void(0)">'.$this->l('Help / Documentation').'</a></b>
    			<br/><br/>';

     if(version_compare(_PS_VERSION_, '1.6', '>')){
         $data .= '</div>';
     }

     return $data;
    }
    
	
     public function _jsandcss(){
    	$_html = '';
     if(version_compare(_PS_VERSION_, '1.6', '>')){
    	$_html .=  '<link rel="stylesheet" media="screen" type="text/css" href="../modules/'.$this->name.'/views/css/prestashop16.css" />';
    		
    	}
    	$_html .= '<link rel="stylesheet" href="../modules/'.$this->name.'/views/css/menu.css" type="text/css" />';
    	$_html .= '<script type="text/javascript" src="../modules/'.$this->name.'/views/js/menu.js"></script>';

         $_html .= '<link rel="stylesheet" href="../modules/'.$this->name.'/views/css/font-custom.min.css" type="text/css" />';


    	
    	
     
      	$cookie = $this->context->cookie;
    
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		
		if(defined('_MYSQL_ENGINE_') && Tools::substr(_PS_VERSION_,0,3) != '1.5'){
		$_html .=  '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>';
			$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>';
			if(version_compare(_PS_VERSION_, '1.6.0.11', '<')){
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
				} else {
					$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/admin/tinymce.inc.js"></script>';
				}
				
			
		$_html .= '
		<script type="text/javascript">id_language = Number('.$defaultLanguage.');</script>';
		} 
		
		if(version_compare(_PS_VERSION_, '1.5', '>')  || 
			!defined('_MYSQL_ENGINE_')){
			
			if(version_compare(_PS_VERSION_, '1.5', '>')){
				$_html .=  '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>';
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>';
			
				if(version_compare(_PS_VERSION_, '1.6.0.11', '<')){
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
				} else {
					$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/admin/tinymce.inc.js"></script>';
				}
				
			} else {
				$_html .=  '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>';
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
				';
			}
			
			
			
		$_html .= '<script type="text/javascript">
					tinyMCE.init({
						mode : "specific_textareas",
						theme : "advanced",
						editor_selector : "rte",';
		if(version_compare(_PS_VERSION_, '1.5', '>')){
			 $_html .= 'skin:"cirkuit",';
		}
			$_html  .=  'editor_deselector : "noEditor",';
			
			if(version_compare(_PS_VERSION_, '1.6', '<')){
			$_html .=  'plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
						//Theme options
						theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : false,
					';
			}else{
			$_html .= 'toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		   			   plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor",
		   			   ';
			}
		
						
						
		  $_html .=	   'content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
						document_base_url : "'.__PS_BASE_URI__.'",';
		  if(!defined('_MYSQL_ENGINE_')){
		  $_html .=		'width: "550",';
		  } else {
		  	if(version_compare(_PS_VERSION_, '1.5', '>'))
		  		$_html .=		'width: "650",';
		  	else
		  		$_html .= 'width: "400",';
		  }
		  
		  $_html .=	    'height: "440",
						font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
						// Drop lists for link/image/media/template dialogs
						template_external_list_url : "lists/template_list.js",
						external_link_list_url : "lists/link_list.js",
						external_image_list_url : "lists/image_list.js",
						media_external_list_url : "lists/media_list.js",';
			
			if(version_compare(_PS_VERSION_, '1.5', '>')){
			$_html .= 	'elements : "nourlconvert,ajaxfilemanager",
						 file_browser_callback : "ajaxfilemanager",';
			} else {
			$_html .= 	'elements : "nourlconvert",';
			}
			
			$_html .=	'entity_encoding: "raw",
						convert_urls : false,
						language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"
						
					});
		</script>';
		
		}
		
     	if(version_compare(_PS_VERSION_, '1.6', '>')){
			$_html .= $this->context->controller->addJqueryUI(array('ui.core', 'ui.datepicker'));
		} elseif(version_compare(_PS_VERSION_, '1.5', '>') && version_compare(_PS_VERSION_, '1.6', '<')){
            $_html .= '<link href="'.__PS_BASE_URI__.'js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
						<link href="'.__PS_BASE_URI__.'js/jquery/ui/themes/base/jquery.ui.core.css" rel="stylesheet" type="text/css" media="all" />
						<link href="'.__PS_BASE_URI__.'js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />';

            $_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/jquery.ui.core.min.js"></script>
						<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/jquery.ui.datepicker.min.js"></script>
						<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/ui/i18n/jquery.ui.datepicker-en.js"></script>';
        }else {
            $_html .= '<script type="text/javascript">

					var formProduct;

					var accessories = new Array();

					</script>';

            $_html .= '<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />

			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>';


            $_html .= '<link href="../modules/'.$this->name.'/backward_compatibility/datepicker14/css/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
						<link href="../modules/'.$this->name.'/backward_compatibility/datepicker14/css/jquery.ui.core.css" rel="stylesheet" type="text/css" media="all" />
						<link href="../modules/'.$this->name.'/backward_compatibility/datepicker14/css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />';

            $_html .= '<script type="text/javascript" src="../modules/'.$this->name.'/backward_compatibility/datepicker14/js/jquery.ui.core.min.js"></script>
						<script type="text/javascript" src="../modules/'.$this->name.'/backward_compatibility/datepicker14/js/jquery.ui.datepicker.min.js"></script>
						<script type="text/javascript" src="../modules/'.$this->name.'/backward_compatibility/datepicker14/js/jquery.ui.datepicker-en.js"></script>';


        }
		
		
    	
		
		
		
		
		$_html .= '<style type="text/css">';
		
		if(version_compare(_PS_VERSION_, '1.6', '>')){
			$_html .= '.nobootstrap{min-width:inherit!important}
			.nobootstrap fieldset, .nobootstrap legend{
			background-color:white!important;
			border-radius: 4px;
		}
		 
		.nobootstrap .display-form input,
		.nobootstrap .display-form select{
		border-radius: 4px;
		}
		 
		.nobootstrap .display-form .update-button{
		border:0px!important
		}
		 
		 
		.displayed_flag {
		float: left;
		margin: 4px 0 0 4px;
		}
		 
		.language_flags {
		display: none;
		float: left;
		background: #FFF;
		margin: 4px;
		padding: 8px;
		width: 80px;
		border: 1px solid #555;
		}
		.pointer {
		cursor: pointer;
		}
		 
		.display-form .alert-success {
		background-color: #dff0d8;
		border-color: #d6e9c6;
		color: #3c763d;
		padding:10px;
		}
		.display-form .alert-danger
		{
		background-color: #f2dede;
		border-color: #ebccd1;
		color: #a94442;
		padding:10px;
		}
		 
		';
			 
			 
		} else {
			$_html .= '.update-button{border: 1px solid #EBEDF4;}';
		}
		$_html .= '</style>';
		
		
    	return $_html;
    }	
    
    
	public function translateText(){
		return array('seo_text'=> $this->l('News'),
					 'page'=>$this->l('Page'));
	}
	
	public function renderTplItems() {
		return Module::display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/front/news.tpl');
	}
	

	public function renderTplItem(){
		return Module::display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/front/news-item.tpl');
	}
	
	public function tools_redirect($url){
		
	 	Tools::redirect($url);
	    
		
	}
	
	public function json_encode_ps13($data){
		
	 	return Tools::jsonEncode($data);
	    
		
	}


    public function translateItems(){
        return array('page'=>$this->l('Page'),
                    'message_like' => $this->l('You have already voted for this news!'),
        );
    }

    public function getIdLang(){
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);
        return $id_lang;
    }
	
    

}
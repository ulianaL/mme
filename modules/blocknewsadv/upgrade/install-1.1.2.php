<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
/*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

function upgrade_module_1_1_2($module)
{
	$name_module = 'blocknewsadv';

    // add new values
    Configuration::updateValue($name_module.'block_display_img', 1);

    Configuration::updateValue($name_module.'search_left_n', 1);
    Configuration::updateValue($name_module.'arch_left_n', 1);
    Configuration::updateValue($name_module.'search_right_n', 1);
    Configuration::updateValue($name_module.'arch_right_n', 1);
    Configuration::updateValue($name_module.'search_footer_n', 1);
    Configuration::updateValue($name_module.'arch_footer_n', 1);

    Configuration::updateValue($name_module.'is_like', 1);
    Configuration::updateValue($name_module.'is_unlike', 1);

    Configuration::updateValue($name_module.'items_w_h', 150);
    Configuration::updateValue($name_module.'item_h_tr', 250);
    Configuration::updateValue($name_module.'h_display_date', 1);

    Configuration::updateValue($name_module.'is_soc_but', 1);

    if(version_compare(_PS_VERSION_, '1.5', '>')) {
        $medium_default = "medium"."_"."default";
        Configuration::updateValue($name_module . 'img_size_rp', $medium_default);
    } else {
        Configuration::updateValue($name_module . 'img_size_rp', 'medium');
    }
    Configuration::updateValue($name_module.'item_rp_tr', 75);

    Configuration::updateValue($name_module.'rp_img_width', 150);

    Configuration::updateValue($name_module.'is_comments', 1);
    Configuration::updateValue($name_module.'number_fc', 5);







    // update existings variables
    Configuration::updateValue($name_module.'n_block_img_w', 75);
    Configuration::updateValue($name_module.'hnumber_ni', 3);
    Configuration::updateValue($name_module.'item_img_w', 500);





    $module->createAdminTabs15();

    // add routes only if prestashop > 1.6
    if(version_compare(_PS_VERSION_, '1.6', '>')){
        $module->registerHook('ModuleRoutes');
    }

    if(version_compare(_PS_VERSION_, '1.6', '>')){
        $module->registerHook('DisplayBackOfficeHeader');
    }

    // add new table in database
    $module->createLikePostTable();


    ### add field is_comments in ps_blocknewsadv table ####

    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'blocknewsadv`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('is_comments', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'blocknewsadv` ADD `is_comments` int(11) NOT NULL default \'1\'')) {
                return false;
            }

        }
    }


    return true;
}
?>
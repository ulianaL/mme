<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
/*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

function upgrade_module_1_1_9($module)
{

    if(version_compare(_PS_VERSION_, '1.5', '>')) {
        $module->registerHook('newsMitrocops');

    }


    return true;
}
?>
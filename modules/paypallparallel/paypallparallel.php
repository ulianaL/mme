<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Paypallparallel extends PaymentModule
{
    protected $config_form = true;

    public function __construct()
    {
        $this->name = 'paypallparallel';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Luis Leitao';
       

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('paypallparallel');
        $this->description = $this->l('paypallparallel  paypallparallel ');

        $this->limited_countries = array('US');

        $this->limited_currencies = array('USD');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

       /*
	   if (in_array($iso_code, $this->limited_countries) == false)
        {
            $this->_errors[] = $this->l('This module is not available in your country');
            return false;
        }*/

        Configuration::updateValue('PAYPALLPARALLEL_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('actionPaymentCCAdd') &&
            $this->registerHook('actionPaymentConfirmation') &&
            $this->registerHook('displayPayment') &&
            $this->registerHook('displayPaymentReturn') &&
            $this->registerHook('displayPaymentTop');
    }

    public function uninstall()
    {
        Configuration::deleteByName('PAYPALLPARALLEL_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitPaypallparallelModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPaypallparallelModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'PAYPALLPARALLEL_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'PAYPALLPARALLEL_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'PAYPALLPARALLEL_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'PAYPALLPARALLEL_LIVE_MODE' => Configuration::get('PAYPALLPARALLEL_LIVE_MODE', true),
            'PAYPALLPARALLEL_ACCOUNT_EMAIL' => Configuration::get('PAYPALLPARALLEL_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'PAYPALLPARALLEL_ACCOUNT_PASSWORD' => Configuration::get('PAYPALLPARALLEL_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
		
	  /*
	  $pro_ob = New product(71);
	  print "<pre>";
	  print_R($pro_ob->additional_shipping_cost);
      print "</pre>";
	   */
	   
	   
	   
	  if(isset($_GET['pay']))
	  $this->apicall(); 
	
		
		
		
		
		$this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        
		    
		$currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
            return false;

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        if ($this->active == false)
            return;

        $order = $params['objOrder'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR'))
            $this->smarty->assign('status', 'ok');

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    public function hookActionPaymentCCAdd()
    {
        /* Place your code here. */
    }

    public function hookActionPaymentConfirmation()
    {
        /* Place your code here. */
    }

    public function hookDisplayPayment($params)
    {
          $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
            return false;

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    public function hookDisplayPaymentReturn()
    {
        /* Place your code here. */
    }

    public function hookDisplayPaymentTop()
    {
        /* Place your code here. */
    }
	
	
	public function apicall(){
		
	    $datap['USER'] = 'raindropsinfotechbiz_api1.gmail.com';
		$datap['PWD'] = '1381926891';
		$datap['SIGNATURE'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AZyaC1CS1vdZkhtpb79s1Bg96IQB';
		$datap['METHOD'] = 'SetExpressCheckout';
		$datap['VERSION'] = '124';
		$datap['LOCALECODE'] = 'us_US';
		$datap['LOGOIMG'] = 'http://mme.world/img/medical-marijuana-exchange-logo-1492869365.jpg';
		$datap['CANCELURL'] = 'http://mme.world/order';

		
		
		$product_of_cart = Context::getContext()->cart->getProducts(true,false,null,true);
		
		
		
		foreach ($product_of_cart as &$proo){
			
		   
		
			$product_obj = new Product($proo['id_product']);
			$proo['id_owner'] = $product_obj->id_owner;
			$cus_obj = New customer($product_obj->id_owner);
		    $proo['owner_mail'] = $cus_obj->email;
		}
		
		
		$id_owner = array();
		foreach ($product_of_cart as $key => $row)
		{
			$id_owner[$key] = $row['id_owner'];
		}
		array_multisort($id_owner, SORT_DESC, $product_of_cart);
	

	    $shipping_cost_by_owner = 0;
		$last_owner = 9999999999;
		$count_owner = -1;
		foreach($product_of_cart as $pro){

			if ($last_owner != $pro['id_owner']){
				$total_per_owner = 0;
				$count_owner++;
				$product_count =-1;
                $shipping_cost_by_owner = 0;
				
          

				
			}else{
				
			}
			$last_owner = $pro['id_owner'];
						$product_count++;
				        $total_per_owner += $pro['price_wt']*$pro['quantity'];
						$total_per_owner += $pro['additional_shipping_cost']*$pro['quantity'];
						
						
						if($pro['id_owner'] == 0){
							$email_to_pay = "info@medicalmarijuanaexchange.com";
						}else{
							$email_to_pay =$pro['owner_mail'];
						}
						//user ---1111111
						$datap['PAYMENTREQUEST_'.$count_owner.'_AMT'] = $total_per_owner;
						$datap['PAYMENTREQUEST_'.$count_owner.'_CURRENCYCODE'] = 'USD';
						$datap['PAYMENTREQUEST_'.$count_owner.'_PAYMENTACTION'] = 'Sale';
						$datap['PAYMENTREQUEST_'.$count_owner.'_ITEMAMT'] = $total_per_owner;
						$datap['PAYMENTREQUEST_'.$count_owner.'_SELLERPAYPALACCOUNTID'] =  $email_to_pay;
						$datap['PAYMENTREQUEST_'.$count_owner.'_PAYMENTREQUESTID'] =  $count_owner;
						
						//products111 user ---1111111
						$datap['L_PAYMENTREQUEST_'.$count_owner.'_NAME'.$product_count] = $pro['name'];
						$datap['L_PAYMENTREQUEST_'.$count_owner.'_DESC'.$product_count] = strip_tags($pro['description_short']);
						$datap['L_PAYMENTREQUEST_'.$count_owner.'_QTY'.$product_count] = $pro['quantity'];
						$datap['L_PAYMENTREQUEST_'.$count_owner.'_AMT'.$product_count] = $pro['price_wt'];
						
						
						$shipping_cost_by_owner += $pro['additional_shipping_cost']*$pro['quantity'];
						// add total shipping cost !!
						$datap['L_PAYMENTREQUEST_'.$count_owner .'_NAME'.($product_count+1)] = 'Shipping Cost';
						$datap['L_PAYMENTREQUEST_'.$count_owner .'_DESC'.($product_count+1)] = '';
						$datap['L_PAYMENTREQUEST_'.$count_owner .'_QTY'.($product_count+1)] = 1;
						$datap['L_PAYMENTREQUEST_'.$count_owner .'_AMT'.($product_count+1)] = $shipping_cost_by_owner;
							
						
						
				
			
				print "<div style='background:red;'>".$count_owner."</div>";
				print "<pre>";
				//print_R(Context::getContext()->cart); 
				print_R($pro); 
				print "</pre>";	
				
				
			
		}
		$cusss_obj = new customer((int)Context::getContext()->customer->id);
		
		//print_r($cusss_obj);
		$customer_secure = $cusss_obj->secure_key;
		
		//print $customer_secure ; exit;
		$datap['RETURNURL'] = 'http://mme.world/module/paypallparallel/validation?cart_id='.(int)Context::getContext()->cart->id.'&customer_id='.Context::getContext()->customer->id."&count_owners=".$count_owner."&secure_key=".$customer_secure;

		print "<pre style='background:rgba(29, 128, 0, 0.2);'>";
		print_R($datap); 
		print "</pre>";
		
		
		
		
		//exit(__FILE__);
		
		
		
	
		
		/*
		//user ---1111111
		$datap['PAYMENTREQUEST_0_AMT'] = 100;
		$datap['PAYMENTREQUEST_0_CURRENCYCODE'] = 'BRL';
		$datap['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
		$datap['PAYMENTREQUEST_0_ITEMAMT'] = 100;
		$datap['PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID'] = 'luisleitaoaudio@gmail.com';
		$datap['PAYMENTREQUEST_0_PAYMENTREQUESTID'] = '0';
		
		
		//products111 user ---1111111
		$datap['L_PAYMENTREQUEST_0_NAME0'] = 'Exemplo';
		$datap['L_PAYMENTREQUEST_0_DESC0'] = 'producto 0';
		$datap['L_PAYMENTREQUEST_0_QTY0'] = 1;
		$datap['L_PAYMENTREQUEST_0_AMT0'] = 50;
		
		//product 22  user ---1111111
		$datap['L_PAYMENTREQUEST_0_NAME1'] = 'Exemplo';
		$datap['L_PAYMENTREQUEST_0_DESC1'] = 'producto 0';
		$datap['L_PAYMENTREQUEST_0_QTY1'] = 1;
		$datap['L_PAYMENTREQUEST_0_AMT1'] = 50;
		
		
		
		
		
		
		
		
		
		//user ---22222
		$datap['PAYMENTREQUEST_1_AMT'] = 100;
		$datap['PAYMENTREQUEST_1_CURRENCYCODE'] = 'BRL';
		$datap['PAYMENTREQUEST_1_PAYMENTACTION'] = 'Sale';
		$datap['PAYMENTREQUEST_1_ITEMAMT'] = 100;
		$datap['PAYMENTREQUEST_1_SELLERPAYPALACCOUNTID'] = 'luisleitaoaudio@gmail.com';
		$datap['PAYMENTREQUEST_1_PAYMENTREQUESTID'] = '1';
		
		
		//products.222 user ---22222
		$datap['L_PAYMENTREQUEST_1_NAME0'] = 'Exemplo';
		$datap['L_PAYMENTREQUEST_1_DESC0'] = 'producto 0';
		$datap['L_PAYMENTREQUEST_1_QTY0'] = 1;
		$datap['L_PAYMENTREQUEST_1_AMT0'] = 50;
		
		//product 22 user ---22222
		$datap['L_PAYMENTREQUEST_1_NAME1'] = 'Exemplo';
		$datap['L_PAYMENTREQUEST_1_DESC1'] = 'producto 0';
		$datap['L_PAYMENTREQUEST_1_QTY1'] = 1;
		$datap['L_PAYMENTREQUEST_1_AMT1'] = 50;
		*/
		
		
		
		
		
		$curl = curl_init();
		 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($datap));
		 
		$response =    curl_exec($curl);
		 
		curl_close($curl);
		 
		$nvp = array();
		 
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
			$query = array(
				'cmd'    => '_express-checkout',
				'token'  => $nvp['TOKEN']
			);
			$redirectURL = sprintf('https://www.sandbox.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
			
			
			if(isset($_GET['r'])){
			header('Location: ' . $redirectURL);
			}else{
				//print_r($nvp);exit;
				
			}
			
		} else {
			//Opz, alguma coisa deu errada.
			//Verifique os logs de erro para depuração.
		} 
		
		print "<script> alert('".$nvp['L_LONGMESSAGE0']."')</script>"; 
		print_R($nvp);
		exit;
		return $response;
		
		
	}
}

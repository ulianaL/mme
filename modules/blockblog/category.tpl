{capture name=path}
<span class="navigation_page">{$meta_title}</span>
{/capture}

{capture name=path}
<span class="navigation_page">
<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
<a itemprop="url" href="" title="Shop">
<span itemprop="title">Blog</span>
</a>
</span>
<span class="navigation-pipe">&gt;</span>{$meta_title}</span>
{/capture}

<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<!-- <pre>{$items|print_r}</pre> -->

<script>
   if (typeof(stockdio_events) == "undefined") {
      stockdio_events = true;
      var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
      var stockdio_eventer = window[stockdio_eventMethod];
      var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
      stockdio_eventer(stockdio_messageEvent, function (e) {
         if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
            eval(e.data.method);
         }
      },false);
   }
</script>

{*{include file="$tpl_dir./breadcrumb.tpl"}*}

<div class="blog-section paddingTB60 custom_blog_list">
	<div class="container">
		{if $category_id == "373"}
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="global_dropdown">
					<span class="selected_location_title">COUNTRIES</span> 
					<img src="/themes/default-bootstrap/img/play-button.png">
				</div>
				<div class="global_dropdown_list">
					<a href="/blog/category/australia-medical-marijuana-">Australia</a>
					<a href="/blog/category/brazil-medical-marijuana">Brazil</a>
					<a href="/blog/category/germany">Germany</a>
					<a href="/blog/category/greece-medical-marijuana">Greece</a>
					<a href="/blog/category/israel-medical-marijuana">Israel</a>
					<a href="/blog/category/mexico-marijuana-and-hemp-news">Mexico</a>
					<a href="/blog/category/spain-medical-marijuana">Spain</a>
					<a href="/blog/category/england-medical-marijuana">UK</a>
				</div>
			</div>
		</div>
		{elseif $category_id == "380" || $category_id == "378"}
		{elseif $category_id == "361"}
		<div class="left_width">
		{else}
		<div class="row">
			<div class="site-heading text-center">
				<h3>{$meta_title}</h3>

				{*<p  style="font-size: 18px;margin-bottom: 5px;"><strong>{l s='Posts' mod='blockblog'}  ( <span id="count_items_top" style="color: #333;">{$count_all}</span> )</strong></p>*}
				<div class="border"></div>
			</div>
		</div>
		{/if}
		<div class="social_for_state">
			{if $twitter_link != ''}
				<a href="{$twitter_link}" target="_blank">
					<i class="fa fa-twitter-square"></i>
				</a>
			{/if}
			{if $pinterest_link != ''}
				<a href="{$pinterest_link}" target="_blank">
					<i class="fa fa-pinterest"></i>
				</a>
			{/if}
			{if $tumblr_link != ''}
				<a href="{$tumblr_link}" target="_blank">
					<i class="fa fa-tumblr-square"></i>
				</a>
			{/if}
			{if $facebook_link != ''}
				<a href="{$facebook_link}" target="_blank">
					<i class="fa fa-facebook-square"></i>
				</a>
			{/if}
		</div>
		<div class="row text-center">
		
			{foreach from=$posts item=post name=myLoop}
		
			<div class="col-sm-6 col-md-4">
				<div class="blog-box">
					<div class="blog-box-image" data-id="{$post.id}">
						{if strlen($post.img)>0}
							{* {if $blockblogurlrewrite_on == 1}
								{if $post.seo_url|strstr:'http'}
									<a style="    width: 100%;" href="{$post.seo_url}" 
							   	   target="_blank" title="{$post.title|escape:'htmlall':'UTF-8'}">
								{else}
									<a style="    width: 100%;" href="{$base_dir}blog/post/{$post.seo_url}" 
							   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
								{/if}
							{else}
								{if $post.seo_url|strstr:'http'}
									<a style="    width: 100%;" href="{$post.seo_url}" 
								   	   target="_blank" title="{$post.title|escape:'htmlall':'UTF-8'}">
								{else}
									<a style="    width: 100%;"  href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" 
								   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
						   	    {/if}
							{/if} *}
							<a href="{$base_dir}blog/post/{$post.id|escape:'htmlall':'UTF-8'}">
								<img src="{$base_dir}upload/blockblog/{$post.img}" title="{$post.title|escape:'htmlall':'UTF-8'}" 
									 alt="{$post.title|escape:'htmlall':'UTF-8'}"
									class="img-responsive imgcatbblog" style="    min-width: 100%;"
								/>
							</a>
						{/if}
					{********
						<img src="https://images.pexels.com/photos/6384/woman-hand-desk-office.jpg?w=940&h=650&auto=compress&cs=tinysrgb" class="img-responsive" alt="">
					********}
					</div>
					<div class="blog-box-content">
						<h4>
							{if $blockblogurlrewrite_on == 1}
								{* {if $post.seo_url|strstr:'http'}
								<a class="short_description_link" href="{$post.seo_url}" target="_blank" title="{$post.title|escape:'htmlall':'UTF-8'}">
									{$post.title}
								</a>
								{else}
								<a class="short_description_link" href="{$base_dir}blog/post/{$post.seo_url}"  title="{$post.title|escape:'htmlall':'UTF-8'}">
									{$post.title}
								</a>
								{/if} *}
								<a class="short_description_link" href="{$base_dir}blog/post/{$post.id|intval}" title="{$post.title|escape:'htmlall':'UTF-8'}">{$post.title}</a>
								<div class="blog_box_content_hideed">
									<p>
										{strip}
											{$post.content|truncate:250:"...":true}
										{/strip}
									</p>
									<p>
										{$post.time_add|date_format:"%B %e, %Y"}
									</p>
								</div>
							{else}
								{if $post.seo_url|strpos:"http"}
								<a class="short_description_link_2" href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" 
						   	  			 title="{$post.title|escape:'htmlall':'UTF-8'}">
										{$post.title}
									</a>
								{else}
									<a class="short_description_link_2" href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" 
						   	  			 title="{$post.title|escape:'htmlall':'UTF-8'}">
										{$post.title}
									</a>
									{/if}
							{/if}
						</h4>
						{* <p   style="    font-weight: 800!important;
    color: #333333;
    font-style: italic;"><i class="fa fa-calendar" aria-hidden="true"></i>
{$post.time_add|date_format:"%D"}, &nbsp;<i class="fa fa-comment" aria-hidden="true"></i>
{$post.count_comments} {l s='comments' mod='blockblog'}</p>

						<p>{strip}{$post.content|truncate:200:"...":true} {/strip}</p>
						<a href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" class="btn btn-default site-btn">Read More</a> *}

					</div>
				</div>
				
			</div> <!-- End Col -->	
			{/foreach}
			
		</div>

		<div class="row">
			<div class="col-12 pagination">
				{$paging}
			</div>
		</div>
		{if $category_id == "361"}
			</div>
			<div class="right_width">
				<div id="stocks_news">
				  	<div class="stocks_news_title">Latest news</div>
				  	<div id="news_list">
				  		<div class="single_news_block">News Loading...</div>
				  	</div>
			  		<div class="single_news_block">
			  			<a class="more_news" href="/latest-news">More News<i class="fas fa-chevron-right"></i></a>
			  		</div>
				</div>
			</div>
			<script type="text/javascript">
			$( document ).ready(function(){

			    var markets = ['ACAN', 'NYSENasdaq:ACB','AGTK', 'AMFE', 'AMMJ', 'NYSENasdaq:APHA', 'ATTBF', 'AXIM', 'BLOZF', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CURR', 'CVSI', 'DIGP', 'DIRV',  'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GNBT', 'GRCU', 'GRNH', 'GRWC', 'GRWG', 'GTSO', 'HLIX', 'HLSPY', 'HVST', 'HYYDF', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYHI', 'NDEV', 'NMUS', 'NSPDF', 'OGRMF', 'OWCP', 'PKPH', 'PMCB', 'PNPL', 'POTN', 'PRMCF', 'PUFXF', 'QRSRF', 'REFG', 'RMHB', 'SRNA', 'TAUG', 'NYSENasdaq:CGC','WCIG', 'ZDPY'];

			    var news = [];
			    var j = 0;
			    var i = 0;
			    getnews();
			    function getnews() {
			      if (i <= markets.length-1) {
			        i++;
			        $.ajax({
			          url: 'https://api.stockdio.com/data/financial/info/v1/getNewsEx?app-key=9F294CB158A94C73B7D84E38BC128E54&nItems=2&includeImage=false&includeDescription=false&stockExchange=OTCMKTS&symbol='+markets[i],
			          context: document.body
			        }).done(function (r) {
			            news_one = r['data']['news']['values'];
			            news = $.merge( $.merge( [], news ), news_one);
			            if(news.length >= 10){
			              shownews();
			            }
			            
			            getnews();
			        }).fail(function (e) {
			           console.log('my fail code');
			           getnews();
			        });
			        j++;
			        if (j == markets.length-1) {
			          shownews();
			        }
			      }
			    }
			   function shownews() {
			    news.sort(function(a,b){
			      return new Date(b[0]) - new Date(a[0]);
			    });
			    console.log(news);
			    news_list = '';
			    for (var i = 0; i <= 9; i++) {
			      news_list += '<div class="single_news_block"><a class="singlr_title" href="'+news[i][2]+'" target="_blank">'+news[i][1]+'</a> <span class="singl_date">'+moment(news[i][0]).fromNow()+' | '+news[i][5]+'</span></div>';
			    }
			    $('#news_list').html(news_list);
			   }  
			});
			</script>
		{/if}
	</div>
</div>



{********
<div id="page_nav" class="pages">
{$paging}
</div>
*****}
<script type="text/javascript" src="/themes/default-bootstrap/js/global.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/hoverIntent.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/superfish-modified.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/blocktopmenu.js"></script>

<script type="text/javascript">
	function go_page_blog(page, item_id)
	{
		var location = window.location;

		window.location.href = location.protocol + '//' +
			location.host +
			location.pathname +
			'?page=' + page;
	}

	$('.global_dropdown').click(function(){
	  $('.global_dropdown_list').toggle();
	  $('.global_dropdown img').toggleClass( "global_dropdown_opened" );

	})
</script>


<style type="text/css">
	.right_width{
		padding-left: 20px;
	}
	#stocks_news{
		margin-top: 0 !important;
	}
	.pagination .pages > span.nums > b {
	    background: none;
		color: #6ec31c;
		padding: 0px !important;
	}
	.pagination .pages > span.nums > a{
		padding: 0px !important;
		background: none;
		color: #827979;
	}
	.pagination .pages > span.nums > a:hover {
		background: none !important;
		color: #827979 !important;
	}
	.blog-box-image a img {
	    max-height: 276px !important;
	    max-width: none !important;
	}
</style>

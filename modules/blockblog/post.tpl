{capture name=path}
{$meta_title}
{/capture}

{*{include file="$tpl_dir./breadcrumb.tpl"}*}
<div class="row">
	
	<div id="left_column" class="column col-xs-12 col-md-3">{$HOOK_LEFT_COLUMN}
		{if !empty($leftbanner)}
		<div class="banners_section">
			{if $leftbanner.type == 2}
			<div class="publi {if $leftbanner.size == '4'}fixed_video_banner{/if}">
				<a target="_blank" href="{if strpos($leftbanner.url,'http') !== false}{$leftbanner.url}{else}http://{$leftbanner.url}{/if}">
					{if $leftbanner.size == '4'}
						<a  class = "go_to_campain" target="_blank" href="{if strpos($leftbanner.url,'http') !== false}{$leftbanner.url}{else}http://{$leftbanner.url}{/if}">Go to Campaign</a>
						<span class="remove_video">&times;</span>
		              {include file="$tpl_dir./video_banner.tpl" banner=$leftbanner}
		              <script type="text/javascript">
		              	if (Cookies.get('showed_video1')) {

							Cookies.set('showed_video2', 1);
						}else{

						   Cookies.set('showed_video1', {$leftbanner.id_banner});
						}
		              	
		              	console.log(Cookies.get('showed_video1'));
		              	console.log(Cookies.get('showed_video2'));
		              </script>
		            {else}
		              <img class="publi-img" src="{getBuildBannerUrl($leftbanner.banner_img)}" />
		            {/if}
				</a>
			</div>
			{elseif $leftbanner.type == 1} 
			<div class="publi {if $leftbanner.size == '4'}fixed_video_banner{/if}">
				<a target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$leftbanner.id_camp}">
					{if $leftbanner.size == '4'}
					<a class = "go_to_campain" target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$leftbanner.id_camp}">Go to Campaign</a>
					<span class="remove_video">&times;</span>
		              {include file="$tpl_dir./video_banner.tpl" banner=$leftbanner}
		              <script type="text/javascript">
		              	if (Cookies.get('showed_video1')) {

							Cookies.set('showed_video2', 1);
						}else{

						   Cookies.set('showed_video1', {$leftbanner.id_banner});
						}
		              	
		              	console.log(Cookies.get('showed_video1'));
		              	console.log(Cookies.get('showed_video2'));
		              </script>
		            {else}
		              <img class="publi-img" src="{getBuildBannerUrl($leftbanner.banner_img)}" />
		            {/if}
				</a>
			</div>
			{/if}
		</div>
		{/if}
	</div>
	
	<div class="col-md-9">
		<!-- {$posts|print_r} -->
	{foreach from=$posts item=post name=myLoop}
			
		
		
			<!-----title ----->
			<h3 class="tittleblogpostbig">{$meta_title}</h3>
			
			<div class="row">
			<!------state+------>
			<div class="col-md-6">
			<span class="blogpostauthor">{$post.bl_author}</span>
			<span class="blogpostdate">{$post.time_add|date_format:"%B %e, %Y"}</span>
			{*{if $is_active == 1}
			<span class="tittleblogpostbigcaT">{l s='Posted in' mod='blockblog'}
			 
			{foreach from=$category_data item=category_item name=catItemLoop}
			{if isset($category_item.title)}
			{if $blockblogurlrewrite_on == 1}
			
			<a  class="highlight" href="{$base_dir}blog/category/{$category_item.seo_url}"
			title="{$category_item.title}">{$category_item.title}
			</a>
			{else}
			
			<a class="highlight" href="{$base_dir}modules/blockblog/blockblog-category.php?category_id={$category_item.id}"
			title="{$category_item.title}">{$category_item.title}
			</a>
			{/if}
			{if count($post.category_ids)>1}
			{if $smarty.foreach.catItemLoop.first},&nbsp;{elseif $smarty.foreach.catItemLoop.last}&nbsp;{else},&nbsp;{/if}
			{/if}
			{/if}
			{/foreach}
			</span>
			{/if}*}
			</div>
			<!----end-state+------>
			
			
			<!-------date---------->
			{*<div class="col-md-6">
				<p class="datetopodtpodtblog"><i class="fa fa-calendar" aria-hidden="true"></i> {$post.time_add|date_format:"%D"}</p>
			</div>*}
			<!------end---date--->
		</div>	
		
			<!-------big pic ---->
			{if strlen($post.img)>0}
				<div class="parentimgblogpostbig">
					<img class="imgblogpostbig" src="/upload/blockblog/{$post.img}" alt="{$post.title|escape:'htmlall':'UTF-8'}" title="{$post.title|escape:'htmlall':'UTF-8'}" />		
				</div>
			{/if}
		
		
		<div class="row">	
			<!-------content ---------->
			<div class="col-md-12 commentbodyblogB">
				{***
				<span class="firstlcommentbodyblogB">
				{$post.content|strip_tags|regex_replace:'/[^a-zA-Z]/':''|substr:0:1}
				</span>
				****}
				{assign var="post_content" value=$post.content|replace:'<p> </p>':''|nl2br}
				{* {$post.content} *}
				{$post_content nofilter}
				<p>{$post_content|truncate:500:'...' nofilter}</p>
                <br/>
                <p>
                    <i style="font-style: italic;">{if strlen($post_content) > 500}{l s='- read the rest of the article'}{else}{l s='- read the original article'}{/if} <a href="{$post.seo_url}" style="color: #59b849; font-weight: 400 !important;" target="_blank">{l s='here'}</a></i>
                </p>
			</div>
			<!-------end content ---------->
			
		</div>
		<div class="row">
			<h4></h4>
			<div class="sharebuttonsblockblog col-md-12">
					
					<a href="https://plus.google.com/share?url=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" rel="nofollow" target="_blank" title="Twitter">
					       <img  class="imgsocialtoblogpost" src="{$img_dir}social/google-plus.png" alt="Google plus">
					</a>
					

					<a href="http://twitter.com/?status={$meta_title|escape:'htmlall':'UTF-8'}%0A%0Ahttp://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI} {$posts.0.hashtags|escape:'url'} {$posts.0.mention_name|escape:'url'}" rel="nofollow" target="_blank" title="Twitter">
					       <img  class="imgsocialtoblogpost" src="{$img_dir}social/twitter.png" alt="Twitter">
					</a>
					<a href="http://www.facebook.com/share.php?u=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" rel="nofollow" target="_blank" title="Facebook">
					      <img class="imgsocialtoblogpost" src="{$img_dir}social/facebook.png" alt="Facebook">
					</a>
					<a href="http://www.myspace.com/Modules/PostTo/Pages/?u=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" rel="nofollow" target="_blank" title="MySpace">
					       <img class="imgsocialtoblogpost" src="{$img_dir}social/myspace.png" alt="MySpace">
					</a>
					<a href="https://www.google.com/bookmarks/mark?op=add&amp;bkmk=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}&amp;title={$meta_title|escape:'htmlall':'UTF-8'}" rel="nofollow" target="_blank" title="Google Bookmarks">
					      <img class="imgsocialtoblogpost" src="{$img_dir}social/google.png" alt="Google Bookmarks">
					</a>
					<a href="http://bookmarks.yahoo.com/toolbar/savebm?opener=tb&amp;u=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}&amp;t={$meta_title|escape:'htmlall':'UTF-8'}" rel="nofollow" target="_blank" title="Yahoo! Bookmarks">
					       <img class="imgsocialtoblogpost" src="{$img_dir}social/yahoo.png" alt="Yahoo! Bookmarks">
					</a>
					<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}&amp;title={$meta_title|escape:'htmlall':'UTF-8'}" rel="nofollow" target="_blank" title="LinkedIn">
					       <img class="imgsocialtoblogpost" src="{$img_dir}social/linkedin.png" alt="LinkedIn">
					</a>
					
				
					
					<a href="http://reddit.com/submit?url=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}&amp;title={$meta_title|escape:'htmlall':'UTF-8'}" rel="nofollow" target="_blank" title="Reddit">
					      <img  class="imgsocialtoblogpost" src="{$img_dir}social/reddit.png" alt="Reddit">
					</a>
					<a href="http://www.stumbleupon.com/submit?url=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}&amp;title={$meta_title|escape:'htmlall':'UTF-8'}" rel="nofollow" target="_blank" title="StumbleUpon">
						    <img class="imgsocialtoblogpost" src="{$img_dir}social/stumbleupon.png" src="{$module_dir}/i/share/1292323491.png" alt="StumbleUpon">
					</a>
						<a href="http://digg.com/submit?phase=2&amp;url=http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" rel="nofollow" target="_blank" title="Digg">
						   <img class="imgsocialtoblogpost" src="{$img_dir}social/digg.png" alt="Digg">
					</a>

					<a href="https://pinterest.com/pin/create/button/?url=https://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}&media=https://mme.world/upload/blockblog/{$post.img}&description={$post.title|escape:'htmlall':'UTF-8'}"
                                   class="pin-it-button" count-layout="horizontal">
					       <img class="imgsocialtoblogpost" src="{$img_dir}social/pinterest.png" alt="LinkedIn">
					</a>
			</div>	
		</div>
		{if $count_all>0} 
		<div class="row">

		
		{********
			<pre>
			{$comments|@print_r}
			</pre>
			
		**********}	
			<div class="col-md-12">
			  <div class="commentrowskkjk">
				<h4 class="addcommentmvfdvj">Comments ( <span style="color: rgb(51, 51, 51);font-weight:800!important" id="count_items_top">{$count_all}</span> )</h4>
				{foreach from=$comments item=comment name=myLoop}
				<section class="comments">
					<article class="comment">
					  <a class="comment-img" href="#non">
						<img src="{$img_dir}account_friend_human.png" alt="" width="50" height="50">
					  </a>
					  <div class="comment-body">
						<div class="text">
						  <p>{$comment.comment|escape:'htmlall':'UTF-8'}</p>
						</div>
						<p class="attribution">by <a href="#non">{$comment.name|escape:'htmlall':'UTF-8'}</a> at {$comment.time_add|escape:'htmlall':'UTF-8'}</p>
					  </div>
					</article>
					
				  </section>
				  {/foreach}
			  </div>
			</div>
			
		</div>
		{/if}
		<div class="row">
			<div class="col-md-12">
				{if $post.is_comments == 0}
				<div style="padding:10px;text-align:center;font-weight:bold">
					{l s='Comments are Closed for this post' mod='blockblog'}
				</div>
				{else}
				
				<div id="succes-review2" style="display:none;"   class="alert alert-success">
					{l s='Your comment  has been sent successfully. Thanks for comment!' mod='blockblog'}
				</div>
				{/if}	
			</div>
		</div>
		<div class="row">
			
			<div class="addrevighmy">
				<form class="form-horizontal">
					 <h4 class="addcommentmvfdvj">Add Comment</h4>
					 <div class="form-group">
						<label class="control-label col-sm-2" for="name-review">Name:</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name-review" name="name-review" >
						</div>
					  </div>
					 

					 <div class="form-group">
						<label class="control-label col-sm-2" for="email-review">Email:</label>
						<div class="col-sm-10">
						  <input type="email" class="form-control" id="email-review" name="email-review" >
						</div>
					  </div>
					 

					 <div class="form-group">
						<label class="control-label col-sm-2" for="email-review">Message:</label>
						<div class="col-sm-10">
						   <textarea class="form-control" name="text-review" id="text-review"></textarea>
						</div>
					  </div>
					 
					 
					  <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						  <a  href="javascript:void(0)" class="btn btn-default pull-right" onclick="add_comment({$post.id});" >Add Comment</a>
						</div>
					  </div>
					</form>
			</div>
		</div>
		
		
		
    {/foreach}
    <div class="blog_ss_story">
		<div class="blog_ss_title">Sponsored stories</div>
		<div class="row">
			{foreach $rand_keys item=key}
				<div class="ss_single_story col-md-4 col-sm-4">
					<a href="/sponsored-story?id={$stories.$key.id_sponsoredstories}">
						<div class="ss_storyimg">
							<img src="/modules/medicalmarijuanaexchangedirectory/storyimg/{$stories.$key.image}">
						</div>
						<div class="blog_story_title">{$stories.$key.title}</div>
					</a>
				</div>
			{/foreach}
		</div>
	</div>
	</div>
</div>


<script type="text/javascript">
baseDir = 'https://{$smarty.server.HTTP_HOST}/';
	{literal}


function go_page_blog_comments(page,item_id){
	
	
	$('#blog-list-comments').css('opacity',0.5);
	$.post(baseDir+'modules/blockblog/ajax.php', {
		action:'pagenavcomments',
		page : page,
		item_id : item_id
	}, 
	function (data) {
		if (data.status == 'success') {
		
		$('#blog-list-comments').css('opacity',1);
		
		$('#blog-list-comments').html('');
		$('#blog-list-comments').prepend(data.params.content);
		
		$('#page_nav').html('');
		$('#page_nav').prepend(data.params.page_nav);
		
		
		
	    } else {
			$('#blog-list-comments').css('opacity',1);
			alert(data.message);
		}
		
	}, 'json');

}


function trim(str) {
	   str = str.replace(/(^ *)|( *$)/,"");
	   return str;
	   }

function add_comment(id_post){
	

	var _name_review = $('#name-review').val();
	var _email_review = $('#email-review').val();
	var _text_review = $('#text-review').val();

	//clear errors
	$('#name-review').removeClass('error_testimonials_form');
	$('#email-review').removeClass('error_testimonials_form');
	$('#text-review').removeClass('error_testimonials_form');
	
	if(trim(_name_review).length == 0){
		$('#name-review').addClass('error_testimonials_form');
		alert("{/literal}{l s='Please, enter the Name.' mod='blockblog'}{literal}");
		return;
	}
	
	if(trim(_email_review).length == 0){
		$('#email-review').addClass('error_testimonials_form');
		alert("{/literal}{l s='Please, enter the Email.' mod='blockblog'}{literal}");
		return;
	}

	if(trim(_text_review).length == 0){
		$('#text-review').addClass('error_testimonials_form');
		alert("{/literal}{l s='Please, enter the Message.' mod='blockblog'}{literal}");
		return;
	}
		
	$('#add-review-form').css('opacity','0.5');
	$.post(baseDir+'modules/blockblog/ajax.php', 
			{action:'addcomment',
			 name:_name_review,
			 email:_email_review,
			 id_post:id_post,
			 text_review:_text_review
			 }, 
	function (data) {
		if (data.status == 'success') {

				$('#name-review').val('');
				$('#email-review').val('');
				$('#text-review').val('');

				$('#add-review-form').hide();
				$('#succes-review2').show();
				
			
			
			$('#add-review-form').css('opacity','1');
			
			
		} else {
			
			var error_type = data.params.error_type;
			
			if(error_type == 1){
				$('#name-review').addClass('error_testimonials_form');
				alert("{/literal}{l s='Please, enter the Name.' mod='blockblog'}{literal}");
			} else if(error_type == 2){
				$('#email-review').addClass('error_testimonials_form');
				alert("{/literal}{l s='Please enter a valid email address. For example johndoe@domain.com.' mod='blockblog'}{literal}");
			} else if(error_type == 3){
				$('#text-review').addClass('error_testimonials_form');
				alert("{/literal}{l s='Please, enter the Message.' mod='blockblog'}{literal}");
			} else {
				alert(data.message);
			}
			$('#add-review-form').css('opacity','1');
			
		}
	}, 'json');
	
	
}
</script>
{/literal}


<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/global.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/hoverIntent.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/superfish-modified.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/blocktopmenu.js"></script>

<script type="text/javascript" async defer data-pin-custom="true" src="
//assets.pinterest.com/js/pinit.js"></script>
<?php
//error_reporting(E_ALL|E_STRICT);

$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blockblog';
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
$name_module = 'blockblog';
$category_id = isset($_REQUEST['category_id'])?$_REQUEST['category_id']:0;


include_once(dirname(__FILE__).'/blog.class.php');
$obj_blog = new blog();

if(Configuration::get($name_module.'urlrewrite_on') == 1 && is_numeric($category_id)){
	// redirect to seo url
	$seo_url_cat = $obj_blog->getSEOURLForCategory(array('id'=>$category_id));
	Header('Location: /blog/category/'.$seo_url_cat.'');
}

$category_id = $obj_blog->getTransformSEOURLtoID(array('id'=>$category_id));

$_info_cat = $obj_blog->getCategoryItem(array('id' => $category_id));
$title = isset($_info_cat['category'][0]['title'])?$_info_cat['category'][0]['title']:'';
$seo_description = isset($_info_cat['category'][0]['seo_description'])?$_info_cat['category'][0]['seo_description']:'';
$seo_keywords = isset($_info_cat['category'][0]['seo_keywords'])?$_info_cat['category'][0]['seo_keywords']:''; 

$twitter_link = $_info_cat['category'][0]['twitter_link'];
$pinterest_link = $_info_cat['category'][0]['pinterest_link'];
$tumblr_link = $_info_cat['category'][0]['tumblr_link'];
$facebook_link = $_info_cat['category'][0]['facebook_link'];

$smarty->assign('category_id' , $category_id);
$smarty->assign('meta_title' , $title);
$smarty->assign('meta_description' , $seo_description);
$smarty->assign('meta_keywords' , $seo_keywords);
$smarty->assign('twitter_link' , $twitter_link);
$smarty->assign('pinterest_link' , $pinterest_link);
$smarty->assign('tumblr_link' , $tumblr_link);
$smarty->assign('facebook_link' , $facebook_link);



$smarty->assign($name_module.'urlrewrite_on', Configuration::get($name_module.'urlrewrite_on'));

// SoftSprint Uliana

$banners = array();
$campains = array();
$top_campains = array();
$left_campains = array();
$right_campains = array();
$country_id = '';
global $cookie;
$context = Context::getContext();
if ( $context->cookie->__isset('change_localization_fun')) 
{
    $city_id = $context->cookie->__get('change_localization_fun');  
}else{
    //Tenho qie melhorar isto .
    $city_id = 9999999999999;   
}            
$sql = 'SELECT country FROM cities WHERE city_id ='.$city_id;
$country = Db::getInstance()->getValue($sql);

if ($country == "US") {

    $country_id = 1;

}elseif($country == "CA"){

    $country_id = 2;
}else{

    $country_id = '';
}

$meta_keywords = $seo_keywords;
$meta_keywords = array_map(function($a){
    return trim($a);
}, explode(' ', $meta_keywords));
$meta_keywords = array_filter($meta_keywords);
if (isset($_COOKIE['showed_video2'])) {
    $sql = 'SELECT id_camp, keytext, '._DB_PREFIX_.'camp.city, toggleclasstree, keystatus, only_finance, '._DB_PREFIX_.'camp.id_banner, '._DB_PREFIX_.'banner.size FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND '._DB_PREFIX_.'camp.country = \''.$country_id.'\' AND used !=0 AND '._DB_PREFIX_.'banner.size != 4';
}elseif (isset($_COOKIE['showed_video1'])) {

    $showed_video1 = $_COOKIE['showed_video1'];
    $sql = 'SELECT id_camp, keytext, city, toggleclasstree, keystatus, only_finance, id_banner FROM '._DB_PREFIX_.'camp WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND country = \''.$country_id.'\' AND used !=0 AND id_banner != '.$showed_video1;
}else{
    $sql = 'SELECT id_camp, keytext, city, toggleclasstree, keystatus, only_finance FROM '._DB_PREFIX_.'camp WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND country = \''.$country_id.'\' AND used !=0';
}
$camps = Db::getInstance()->executeS($sql);

foreach ($camps as $value) {
    if ($value['toggleclasstree'] == 1) {

        $banners[] = $value['id_camp'];

    }elseif($value['keystatus'] == 0){

        if ($value['city'] == $city_id) {

            $banners[] = $value['id_camp'];

        }

    }else{
        if ($meta_keywords) {
            if ($value['city'] == $city_id) {
                $tags_array = explode(',', $value['keytext']);
                foreach ($tags_array as $tag) {
                    if (in_array($tag, $meta_keywords)) {
                        $banners[] = $value['id_camp'];
                        break;
                    }
                }
            }
        }
    }
}

if (!empty($banners)) {
    foreach ($banners as $camp) {
         $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_camp='.$camp.' AND '._DB_PREFIX_.'banner.status = 1 AND '._DB_PREFIX_.'banner.size NOT IN (3, 4)';
         $_banner = Db::getInstance()->getRow($sql);

        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_camp='.$camp.' AND '._DB_PREFIX_.'banner.status = 1 AND ('._DB_PREFIX_.'banner.size = 2 OR '._DB_PREFIX_.'banner.size = 7)';
        $_top_banner = Db::getInstance()->getRow($sql);


        if ($_banner) {
            $campains[] = $_banner;
        }

        if ($_top_banner) {
            $top_campains[] = $_top_banner;
        }

    }

    if (count($top_campains) !== 0) {
        $randx =  rand(0,count($top_campains)-1);
        $topbanner = $top_campains[$randx];
    }else{
        $topbanner = '';
    }


    $randx =  rand(0,count($campains)-1);
    $bottombaner = $campains[$randx];

    if (!empty($topbanner)) {
        if($topbanner['type'] == '2'){
            //if seller is see your banner....
            if((int)Context::getContext()->customer->id != $_banner['customer'] ){
            $camppppp_obj =  New Camp( (int)$topbanner['id_camp'] );
            $camppppp_obj->used = $camppppp_obj->used -1;
            $camppppp_obj->update();
            }     
        }
    }

   
    if($bottombaner['type'] == '2'){
        //if seller is see your banner....
        if((int)Context::getContext()->customer->id != $_banner['customer'] ){
        $camppppp_obj =  New Camp( (int)$bottombaner['id_camp'] );
        $camppppp_obj->used = $camppppp_obj->used -1;
        $camppppp_obj->update();
        }     
    }

    $smarty->assign('campains', $campains);
    $smarty->assign('topbanner', $topbanner);
    $smarty->assign('bottombaner', $bottombaner);
}


 $prem_users = array();
$prem_products = array();
$sql = 'SELECT DISTINCT id_customer FROM '._DB_PREFIX_.'prem  WHERE date_end >= NOW() ORDER BY premtype ASC, date_end DESC';
$prem_users = Db::getInstance()->executeS($sql);
$prem_users = array_map(function($a){
    return $a['id_customer'];
}, $prem_users);

foreach ($prem_users as $user) {
    $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_owner = '.$user.' AND active = 1 AND review=1';
    $res = Db::getInstance()->executeS($sql);
    foreach ($res as $item) {
        $p = new Product($item['id_product'], false, Context::getContext()->language->id);
        $img = Image::getCover($p->id);
        $prem_products[] = array(    
            'name' => $p->name,
            'link' => Context::getContext()->link->getProductLink($p),
            'image' => Context::getContext()->link->getImageLink($p->link_rewrite, $img['id_image'])
        );
    }
    if (count($prem_products) >= 3) { 
        break;
    } 
}
$smarty->assign('prem_products', $prem_products);

if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				if(Configuration::get($name_module.'urlrewrite_on') == 1){
					$smarty->assign('page_name' , "blockblog-posts");
				} else {
					$page_name = str_replace(array('.php', '/'), array('', '-'),$_SERVER['REQUEST_URI']);
					$page_name = 'module'.$page_name;
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				include_once(dirname(__FILE__).'/../../header.php');
			}



$step = Configuration::get($name_module.'perpage_posts');
$_data = $obj_blog->getPosts(array(
    'start'=>Tools::getValue('page', 0),
    'step'=>$step,
    'id'=>$category_id
));


include_once(dirname(__FILE__).'/blockblog.php');
$obj_blockblog = new blockblog();

$_data_translate = $obj_blockblog->translateItems();
$page_translate = $_data_translate['page']; 
$paging = $obj_blog->PageNav(
    Tools::getValue('page', 0),
    $_data['count_all'],
    $step,
    array('category_id'=>$category_id, 'page'=>$page_translate)
);

// strip tags for content
foreach($_data['posts'] as $_k => $_item){
	$_data['posts'][$_k]['content'] = strip_tags($_item['content']);
	
}

$smarty->assign(array('posts' => $_data['posts'], 
					  'count_all' => $_data['count_all'],
                      'posts_per_page' => $step,
					  'paging' => $paging
					  )
				);


if(substr(_PS_VERSION_,0,3) == '1.6'){
	echo $obj_blockblog->renderTplCategory();
} else {
	echo Module::display(dirname(__FILE__).'/blockblog.php', 'category.tpl');
}

if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				include_once(dirname(__FILE__).'/../../footer.php');
			}

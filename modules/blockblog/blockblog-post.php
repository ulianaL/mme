<?php
//error_reporting(E_ALL|E_STRICT);
$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blockblog';
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
$controller->setMedia();
//var_dump($controller->js_files);exit;

$name_module = 'blockblog';
$post_id = isset($_REQUEST['post_id'])?$_REQUEST['post_id']:0;

include_once(dirname(__FILE__).'/blog.class.php');
$obj_blog = new blog();

// if(Configuration::get($name_module.'urlrewrite_on') == 1 && is_numeric($post_id)){
// 	// redirect to seo url
// 	// $seo_url_post = $obj_blog->getSEOURLForPost(array('id'=>$post_id));
// 	// Header('Location: /blog/post/'.$seo_url_post.'');
// }

// $post_id = $obj_blog->getTransformSEOURLtoIDPost(array('id'=>$post_id));


$_info_cat = $obj_blog->getPostItem(array('id' => $post_id,'site'=>1));

if(empty($_info_cat['post'][0]['id']))
	Tools::redirect('/');

$title = isset($_info_cat['post'][0]['title'])?$_info_cat['post'][0]['title']:'';
$seo_description = isset($_info_cat['post'][0]['seo_description'])?$_info_cat['post'][0]['seo_description']:'';
$seo_keywords = isset($_info_cat['post'][0]['seo_keywords'])?$_info_cat['post'][0]['seo_keywords']:''; 

$smarty->assign('meta_title' , $title);
$smarty->assign('meta_description' , $seo_description);
$smarty->assign('meta_keywords' , $seo_keywords);

$new_js = array('/js/jquery/jquery-1.11.0.min.js', '/js/jquery/jquery-migrate-1.2.1.min.js', '/js/jquery/plugins/jquery.easing.js',' /js/tools.js', '/themes/default-bootstrap/js/autoload/10-bootstrap.min.js','/themes/default-bootstrap/js/autoload/15-jquery.total-storage.min.js',
    '/themes/default-bootstrap/js/autoload/15-jquery.uniform-modified.js',
  '/themes/default-bootstrap/js/autoload/jquery.mCustomScrollbar.js',
    '/themes/default-bootstrap/js/autoload/jquery.pickmeup.twitter-bootstrap.js',
   ' /themes/default-bootstrap/js/autoload/jquery.pickmeup.uikit.js',
    '/themes/default-bootstrap/js/autoload/js.cookie.js',
  ' /themes/default-bootstrap/js/autoload/pickmeup.js',
  '/js/jquery/plugins/fancybox/jquery.fancybox.js',
   '/themes/default-bootstrap/js/products-comparison.js');
$smarty->assign('js_files', $new_js);


$smarty->assign($name_module.'urlrewrite_on', Configuration::get($name_module.'urlrewrite_on'));
// SoftSprint Uliana

$banners = array();
$campains = array();
$top_campains = array();
$left_campains = array();
$right_campains = array();
$country_id = '';
global $cookie;
$context = Context::getContext();
if ( $context->cookie->__isset('change_localization_fun')) 
{
    $city_id = $context->cookie->__get('change_localization_fun');  
}else{
    //Tenho qie melhorar isto .
    $city_id = 9999999999999;   
}            
$sql = 'SELECT country FROM cities WHERE city_id ='.$city_id;
$country = Db::getInstance()->getValue($sql);

if ($country == "US") {

    $country_id = 1;

}elseif($country == "CA"){

    $country_id = 2;
}else{

    $country_id = '';
}

$_camp_reviews_was_added = false;
$reviewd_camps = array();

$meta_keywords = $seo_keywords;
$meta_keywords = array_map(function($a){
    return trim($a);
}, explode(' ', $meta_keywords));
$meta_keywords = array_filter($meta_keywords);
if (isset($_COOKIE['showed_video2'])) {
    $sql = 'SELECT id_camp, keytext, '._DB_PREFIX_.'camp.city, toggleclasstree, keystatus, only_finance, '._DB_PREFIX_.'camp.id_banner, '._DB_PREFIX_.'banner.size FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND '._DB_PREFIX_.'camp.country = \''.$country_id.'\' AND used !=0 AND '._DB_PREFIX_.'banner.size != 4';
}elseif (isset($_COOKIE['showed_video1'])) {

    $showed_video1 = $_COOKIE['showed_video1'];
    $sql = 'SELECT id_camp, keytext, city, toggleclasstree, keystatus, only_finance, id_banner FROM '._DB_PREFIX_.'camp WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND country = \''.$country_id.'\' AND used !=0 AND id_banner != '.$showed_video1;
}else{
    $sql = 'SELECT id_camp, keytext, city, toggleclasstree, keystatus, only_finance FROM '._DB_PREFIX_.'camp WHERE ((keytext IS NOT NULL AND keytext != "" AND keystatus = 1) OR keystatus = 0 OR toggleclasstree = 1) AND country = \''.$country_id.'\' AND used !=0';
}
$camps = Db::getInstance()->executeS($sql);

foreach ($camps as $value) {
    if ($value['toggleclasstree'] == 1) {

        $banners[] = $value['id_camp'];

    }elseif($value['keystatus'] == 0){

        if ($value['city'] == $city_id) {

            $banners[] = $value['id_camp'];

        }

    }else{
        if ($meta_keywords) {
            if ($value['city'] == $city_id) {
                $tags_array = explode(',', $value['keytext']);
                foreach ($tags_array as $tag) {
                    if (in_array($tag, $meta_keywords)) {
                        $banners[] = $value['id_camp'];
                        break;
                    }
                }
            }
        }
    }
}

if (!empty($banners)) {
    foreach ($banners as $camp) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_camp='.$camp.' AND '._DB_PREFIX_.'banner.status = 1 AND '._DB_PREFIX_.'banner.size NOT IN (3, 4)';
        $_banner = Db::getInstance()->getRow($sql);

        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_camp='.$camp.' AND '._DB_PREFIX_.'banner.status = 1 AND ('._DB_PREFIX_.'banner.size = 2 OR '._DB_PREFIX_.'banner.size = 7)';
        $_top_banner = Db::getInstance()->getRow($sql);

        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_camp='.$camp.' AND only_finance = 0 AND '._DB_PREFIX_.'banner.status = 1 AND '._DB_PREFIX_.'banner.size IN (3, 4)';
        $_left_banner = Db::getInstance()->getRow($sql);

        if ($_banner) {
            $campains[] = $_banner;
        }

        if ($_top_banner) {
            $top_campains[] = $_top_banner;
        }

        if ($_left_banner) {
            $left_campains[] = $_left_banner;
        }

    }

    if (count($top_campains) !== 0) {
        $randx =  rand(0,count($top_campains)-1);
        $topbanner = $top_campains[$randx];
    }else{
        $topbanner = '';
    }

    if (count($left_campains) !== 0) {
        $randx =  rand(0,count($left_campains)-1);
        $leftbanner = $left_campains[$randx];
    }else{
        $leftbanner = '';
    }



    $randx =  rand(0,count($campains)-1);
    $bottombaner = $campains[$randx];

    if (!empty($topbanner)) {
        if($topbanner['type'] == '2'){
            //if seller is see your banner....
            if((int)Context::getContext()->customer->id != $_banner['customer'] ){
                $camppppp_obj =  New Camp( (int)$topbanner['id_camp'] );
                $camppppp_obj->used = $camppppp_obj->used -1;
                $camppppp_obj->update();

                if (!$_camp_reviews_was_added && !in_array($camppppp_obj->id, $reviewd_camps)) {
                    CampReview::addReview((int)$camppppp_obj->id);
                    $reviewd_camps[] = $camppppp_obj->id;
                }
            }     
        }
    }

    if (!empty($leftbanner)) {
        if($leftbanner['type'] == '2'){
            //if seller is see your banner....
            if((int)Context::getContext()->customer->id != $_banner['customer'] ){
                $camppppp_obj =  New Camp( (int)$leftbanner['id_camp'] );
                $camppppp_obj->used = $camppppp_obj->used -1;
                $camppppp_obj->update();

                if (!$_camp_reviews_was_added && !in_array($camppppp_obj->id, $reviewd_camps)) {
                    CampReview::addReview((int)$camppppp_obj->id);
                    $reviewd_camps[] = $camppppp_obj->id;
                }
            }
        }
    }
   
    if($bottombaner['type'] == '2'){
        //if seller is see your banner....
        if((int)Context::getContext()->customer->id != $_banner['customer'] ){
            $camppppp_obj =  New Camp( (int)$bottombaner['id_camp'] );
            $camppppp_obj->used = $camppppp_obj->used -1;
            $camppppp_obj->update();

            if (!$_camp_reviews_was_added && !in_array($camppppp_obj->id, $reviewd_camps)) {
                CampReview::addReview((int)$camppppp_obj->id);
                $reviewd_camps[] = $camppppp_obj->id;
            }
        }     
    }
    $_camp_reviews_was_added = true;
    $smarty->assign('campains', $campains);
    $smarty->assign('topbanner', $topbanner);
    $smarty->assign('leftbanner', $leftbanner);
    $smarty->assign('bottombaner', $bottombaner);
}


 $prem_users = array();
$prem_products = array();
$sql = 'SELECT DISTINCT id_customer FROM '._DB_PREFIX_.'prem  WHERE date_end >= NOW() ORDER BY premtype ASC, date_end DESC';
$prem_users = Db::getInstance()->executeS($sql);
$prem_users = array_map(function($a){
    return $a['id_customer'];
}, $prem_users);

foreach ($prem_users as $user) {
    $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_owner = '.$user.' AND active = 1 AND review=1';
    $res = Db::getInstance()->executeS($sql);
    foreach ($res as $item) {
        $p = new Product($item['id_product'], false, Context::getContext()->language->id);
        $img = Image::getCover($p->id);
        $prem_products[] = array(    
            'name' => $p->name,
            'link' => Context::getContext()->link->getProductLink($p),
            'image' => Context::getContext()->link->getImageLink($p->link_rewrite, $img['id_image'])
        );
    }
    if (count($prem_products) >= 3) { 
        break;
    } 
}
$smarty->assign('prem_products', $prem_products);

if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				if(Configuration::get($name_module.'urlrewrite_on') == 1){
					$smarty->assign('page_name' , "blockblog-post");
				} else {
					$page_name = str_replace(array('.php', '/'), array('', '-'),$_SERVER['REQUEST_URI']);
					$page_name = 'module'.$page_name;
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				include_once(dirname(__FILE__).'/../../header.php');
			}

########### category info ##################
$ids_cat = $_info_cat['post'][0]['category_ids'];
$category_data = array();
foreach($ids_cat as $k => $cat_id){
	$_info_ids = $obj_blog->getCategoryItem(array('id' => $cat_id));
	
	$is_active = 1;
	if(empty($_info_ids['category'][0]['title']))
		$is_active = 0;	
	$category_data[] = @$_info_ids['category'][0];
}
//var_dump($category_data);
########## end category info ###############


$step = Configuration::get($name_module.'perpage_posts');
$_data = $obj_blog->getComments(array('start'=>0,'step'=>$step,'id'=>$post_id));

include_once(dirname(__FILE__).'/blockblog.php');
$obj_blockblog = new blockblog();

$_data_translate = $obj_blockblog->translateItems();
$page_translate = $_data_translate['page']; 

$paging = $obj_blog->PageNav(0,$_data['count_all'],$step,array('post_id'=>$post_id,'page'=>$page_translate));


$smarty->assign(array('comments' => $_data['comments'], 
					  'count_all' => $_data['count_all'],
					  'paging' => $paging
					  )
				);

				
$smarty->assign(array('posts' => $_info_cat['post'],
					  'category_data' => $category_data,
					  'is_active' => $is_active
					  )
				);

//get stories
  $stories = (new Sponsoredstories)->getStories("WHERE status=1 AND used > 0");
  if (count($stories) >= 3) {
      $rand_keys = array_rand($stories, 3);
  }elseif (count($stories) == 2){
      $rand_keys = array(0,1);
  }elseif (count($stories) == 1){
      $rand_keys = array(0);
  }
  $smarty->assign('stories', $stories); 
  $smarty->assign('rand_keys', $rand_keys);


if(substr(_PS_VERSION_,0,3) == '1.6'){
	echo $obj_blockblog->renderTplPost();
} else {
	echo Module::display(dirname(__FILE__).'/blockblog.php', 'post.tpl');
}


if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				include_once(dirname(__FILE__).'/../../footer.php');
			}
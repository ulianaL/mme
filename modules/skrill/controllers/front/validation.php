<?php
/**
* 2015 Skrill
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*  @author    Skrill <contact@skrill.com>
*  @copyright 2015 Skrill
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Skrill
*/

require_once(dirname(__FILE__).'/../../core/core.php');

class SkrillValidationModuleFrontController extends ModuleFrontController
{
    protected $orderConfirmationUrl = 'index.php?controller=order-confirmation&id_cart=';

    /**
     * this function run when we access the controller.
     */
    public function postProcess()
    {
        $cartId = (int)Tools::getValue('cart_id');
        $orderId = Order::getOrderByCartId($cartId);

        if ($orderId) {
            $transactionId = Tools::getValue('transaction_id');
            $this->validateOrder($cartId, $transactionId);
        } else {
            $this->redirectPaymentReturn();
        }
    }

    /**
     * to validate order status and redirect to success or failed page.
     *
     * @param int $cartId
     * @param string $transactionId
     * @return void
     */
    protected function validateOrder($cartId, $transactionId)
    {
        $order = $this->module->getOrderByTransactionId($transactionId);

        if (empty($order) || empty($order['order_status'])) {
            $this->redirectPaymentReturn();
        } elseif ($order['order_status'] == $this->module->failedStatus) {
            $paymentResponse = $this->module->convertSerializeToArray($order['payment_response']);
            $errorStatus = SkrillPaymentCore::getSkrillErrorMapping($paymentResponse['failed_reason_code']);
            $this->redirectPaymentReturn($errorStatus);
        } else {
            $this->redirectSuccess($cartId);
        }
    }

    protected function redirectPaymentReturn($returnMessage = "")
    {
        Tools::redirect(
            $this->context->link->getModuleLink(
                'skrill',
                'paymentReturn',
                array(
                     'secure_key' => $this->context->customer->secure_key,
                     'skrillErrorMessage' => $returnMessage
                ),
                true
            )
        );
    }

    protected function redirectSuccess($cartId)
    {
        Tools::redirect(
            $this->orderConfirmationUrl.
            $cartId.
            '&id_module='.(int)$this->module->id.
            '&key='.$this->context->customer->secure_key
        );
    }
}

{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row moduleconfig-header">
		<div class="col-xs-5 text-right">
			<img src="{$module_dir|escape:'html':'UTF-8'}views/img/worldfirst.png" />
		</div>
		<div class="col-xs-5 text-center">
			<br><br><br><h4>{l s='PrestaShop official payment partner' mod='worldfirst'}</h4>
		</div>
	</div>
	<hr />
	<div class="moduleconfig-content">
		<div class="row">
			<div class="col-xs-12">
				<p>
					<h1><strong>{l s='World First – International Payments for Your Business' mod='worldfirst'}</strong></h1><br>
				</p>
			</div>
			<div class="col-xs-12">
				<p>
					<h2><strong>{l s='What this module does for you' mod='worldfirst'}</strong></h2>
					<h4><strong>{l s='World First helps you transfer money overseas at fantastic exchange rates which could save you up to 2.8% compared to using a bank*. If you make payments to international suppliers, this will simply help reduce your costs. And if you receive payments in foreign currencies through your online store or via online marketplaces, you can collect these funds in local currency receiving accounts and repatriate the funds at a great rate too. These are just a few reasons why more than 100,000 customers use World First.' mod='worldfirst'}</strong></h3>
				</p>
					<p class="col-md-12 text-center">
						<a href="{l s='https://www.worldfirst.com/uk/business/?ID=3258&utm_source=Prestashop&utm_medium=partner&utm_campaign=Prestashop+module&utm_content=supplier+payments' mod='worldfirst'}" target="_blank" class="btn btn-primary" id="create-account-btn">{l s='Discover World First' mod='worldfirst'}</a>
					</p>
			</div>
			<div class="col-xs-12">
				<p>
					<h2><strong>{l s='Paying International Suppliers' mod='worldfirst'}</strong></h2><br>
					<ul class="ul-spaced">
						<li>World First gives you great exchange rates, saving you up to 2.8% compared to using a bank*</li>
						<li>Once you’ve registered, arrange payments in three simple steps</li>
						<li>Keep your suppliers happy with the fastest available international transfers – same/next day for certain currencies</li>
						<li>Manage your currency risk with a forward contract and secure an exchange rate up to three years in advance</li>
						<li>Automate your regular international payments quickly and easily</li>
					</ul>
				</p>
				<hr />
			</div>
			<div class="col-xs-12">
				<p>
					<h2><strong>{l s='International Online Marketplaces' mod='worldfirst'}</strong></h2><br>
					<ul class="ul-spaced">
						<li>Plug in free receiving bank accounts available in USD, GBP, EUR, CAD, and JPY to collect disbursements from overseas marketplaces locally</li>
						<li>World First transfers your funds to your home account at great exchange rates, helping you keep more of your money</li>
						<li>Compatible with over 30 marketplaces around the world including Amazon, eBay, Newegg, Cdiscount, Lazada and more</li>
					</ul>
				</p>
				<hr />
			</div>
			<div class="col-xs-12">
				<p>
					<h2><strong>{l s='Payment Service Providers' mod='worldfirst'}</strong></h2><br>
					<ul class="ul-spaced">
						<li>Plug in free USD, GBP and EUR receiving accounts to collect payments in these currencies and avoid costly conversion fees of up to 4%</li>
						<li>World First transfers these funds to your home account at great exchange rates, helping you save money</li>
						<li>Compatible with Stripe, PayPal, Shopify and more</li>
					</ul>
				</p>
			</div>
			<div class="col-xs-12">
				<p>
				<hr />
					<h2><strong>{l s='Features' mod='worldfirst'}</strong></h2><br>
					<ul class="ul-spaced">
						<li>{l s='Award-winning customer service from our e-commerce experts' mod='worldfirst'}</li>
						<li>{l s='24/7 online platform to manage your funds' mod='worldfirst'}</li>
						<li>{l s='Safe and secure – World First is authorised by the Financial Conduct Authority' mod='worldfirst'}</li>
						<li>{l s='Free currency news and updates' mod='worldfirst'}</li>
					</ul>
				</p>
				<hr />
			</div>
				<p class="col-md-12 text-center">
						<a href="{l s='https://www.worldfirst.com/uk/business/?ID=3258&utm_source=Prestashop&utm_medium=partner&utm_campaign=Prestashop+module&utm_content=supplier+payments' mod='worldfirst'}" target="_blank" class="btn btn-primary" id="create-account-btn">{l s='Discover World First' mod='worldfirst'}</a><br><br>
					</h5>{l s='*Calculated from exchange rates and fees obtained using mystery shop data from the FXCompared International Money Transfer Index (IMTI)™ collected on the 17th October 2016, based on the cost of making a transfer of £10,000 in euros excluding the transfer fee.' mod='worldfirst'}</h5><br>
				</p>
			</div>
		</div>
	</div>
</div>

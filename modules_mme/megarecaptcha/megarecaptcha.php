<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Megarecaptcha extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'megarecaptcha';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Luis Leitao';
        $this->need_instance = 1;
        $this->errorsmodule = '';
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Mega recaptcha');
        $this->description = $this->l('Mega recaptcha Mega recaptcha');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MEGARECAPTCHA_LIVE_MODE', false);
      

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MEGARECAPTCHA_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitMegarecaptchaModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);


        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMegarecaptchaModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 6,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-lock"></i>',
                        'desc' => $this->l('Google reCAPTCHA Secret key'),
                        'name' => 'MEGARECAPTCHA_SECRET',
                        'label' => $this->l('Secret Key'),
                    ),
                    array(
                        'col' => 6,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-lock"></i>',
                        'desc' => $this->l('Google reCAPTCHA Site key'),
                        'name' => 'MEGARECAPTCHA_PUBLIC',
                        'label' => $this->l('Site key'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('RECAPTCHA on contact-us'),
                        'name' => 'MEGARECAPTCHA_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Enable RECAPTCHA on contact-us email send.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('RECAPTCHA on Login'),
                        'name' => 'MEGARECAPTCHA_LOGIN_ENABLE',
                        'is_bool' => true,
                        'desc' => $this->l('Enable RECAPTCHA on Login.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('RECAPTCHA on ACCOUNT CREATE'),
                        'name' => 'MEGARECAPTCHA_ACCOUNT_ENABLE',
                        'is_bool' => true,
                        'desc' => $this->l('Enable RECAPTCHA on Account create.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'MEGARECAPTCHA_SECRET' => Configuration::get('MEGARECAPTCHA_SECRET'),
            'MEGARECAPTCHA_PUBLIC' => Configuration::get('MEGARECAPTCHA_PUBLIC'),
            'MEGARECAPTCHA_LIVE_MODE' => Configuration::get('MEGARECAPTCHA_LIVE_MODE'),
            'MEGARECAPTCHA_LOGIN_ENABLE' => Configuration::get('MEGARECAPTCHA_LOGIN_ENABLE'),
            'MEGARECAPTCHA_ACCOUNT_ENABLE' => Configuration::get('MEGARECAPTCHA_ACCOUNT_ENABLE'),
           
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }
    
     /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        
        //$this->context->controller->addJS($this->_path.'/views/js/front.js'.time());
        //$this->context->controller->addCSS($this->_path.'/views/css/front.css?'.time());
    }
    
    public function hookdisplayHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js?v='.time());
        $this->context->controller->addCSS($this->_path.'/views/css/front.css?v='.time());
		
        $MEGARECAPTCHA_PUBLIC_var  = Configuration::get('MEGARECAPTCHA_PUBLIC');
        $MEGARECAPTCHA_LIVE_MODE_var = Configuration::get('MEGARECAPTCHA_LIVE_MODE');
        $MEGARECAPTCHA_LOGIN_ENABLE_var = Configuration::get('MEGARECAPTCHA_LOGIN_ENABLE');
        $MEGARECAPTCHA_ACCOUNT_ENABLE_var = Configuration::get('MEGARECAPTCHA_ACCOUNT_ENABLE');
        $this->context->smarty->assign('MEGARECAPTCHA_PUBLIC_var', $MEGARECAPTCHA_PUBLIC_var);
        $this->context->smarty->assign('MEGARECAPTCHA_LIVE_MODE_var', $MEGARECAPTCHA_LIVE_MODE_var);
        $this->context->smarty->assign('MEGARECAPTCHA_LOGIN_ENABLE_var', $MEGARECAPTCHA_LOGIN_ENABLE_var);
        $this->context->smarty->assign('MEGARECAPTCHA_ACCOUNT_ENABLE_var', $MEGARECAPTCHA_ACCOUNT_ENABLE_var);
        
        return $this->display(__FILE__, '/views/templates/front/reca.tpl');
    }
    
    
    
    
    

    
    
    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }
}

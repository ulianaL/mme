<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class PaypallparallelValidationModuleFrontController extends ModuleFrontController
{
    /**
     * This class should be use by your Instant Payment
     * Notification system to validate the order remotely
     */
    public function postProcess()
    {
        /**
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die;
        }
 
		
		$datap['USER'] = 'raindropsinfotechbiz_api1.gmail.com';
		$datap['PWD'] = '1381926891';
		$datap['SIGNATURE'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AZyaC1CS1vdZkhtpb79s1Bg96IQB';
		$datap['METHOD'] = 'GetExpressCheckoutDetails';
		$datap['VERSION'] = '124';
		$datap['TOKEN'] = $_GET['token'];
		
		
		$curl = curl_init();
		 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($datap));
		 
		$response =    curl_exec($curl);
		 
		curl_close($curl);
		 
		$nvp = array();
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		
		
		if($nvp['ACK'] === 'Success'){
			 $new_nvp = $this->capture_payment($nvp);
			
		}else{
			
			print "payment error";exit;
		}
		
	
		
	  
	 $total_payed = 0;
	 for ($i = 0; $i <= $_GET['count_owners']; $i++) {
		$total_payed +=  $new_nvp['PAYMENTINFO_'.$i.'_AMT'];
	
     }
	  
	 
 
		
		 /**
         * Since it is an example, we choose sample data,
         * You'll have to get the correct values :)
         */
        $cart_id = $_GET['cart_id'];
        $customer_id = $_GET['customer_id'];
        $amount = $total_payed;

        /**
         * Restore the context from the $cart_id & the $customer_id to process the validation properly.
         */
        Context::getContext()->cart = new Cart((int)$cart_id);
        Context::getContext()->customer = new Customer((int)$customer_id);
        Context::getContext()->currency = new Currency((int)Context::getContext()->cart->id_currency);
        Context::getContext()->language = new Language((int)Context::getContext()->customer->id_lang);

        $secure_key = Context::getContext()->customer->secure_key;

        if ($new_nvp['ACK'] === 'Success') {
            $payment_status = Configuration::get('PS_OS_PAYMENT');
            $message = $new_nvp['TOKEN'];
        } else {
            $payment_status = Configuration::get('PS_OS_ERROR');

            /**
             * Add a message to explain why the order has not been validated
             */
            $message = $this->module->l('An error occurred while processing payment');
			$message .= $new_nvp['L_LONGMESSAGE0']; 
        }

        $module_name = $this->module->displayName;
        $currency_id = (int)Context::getContext()->currency->id;
        
		
		
		
		
		
		
		//order validation 
		$this->module->validateOrder($cart_id, $payment_status, $amount, $module_name, $message, array(), $currency_id, false, $secure_key);
		
		
		$summary = Context::getContext()->cart->getSummaryDetails();
		
		foreach($summary['products'] as $prpp){
			if ($prpp['id_product'] == 81){
				$qtqt = $prpp['quantity'];
				$obj_cu = new customer((int)Context::getContext()->customer->id);
				$actualpoint = $obj_cu->points;
				$newpoints = $actualpoint + $qtqt;
				$obj_cu2 = new customer((int)Context::getContext()->customer->id);
				$obj_cu2->points = $newpoints;
				$obj_cu2->update();
			}
		}
		
		/*
		print "<pre>";
		print_r($summary);
		print "</pre>";*/
		//exit;
		
		
		 /**
         * If the order has been validated we try to retrieve it
         */
       
        $cart_id = Tools::getValue('cart_id');
        $secure_key = Tools::getValue('secure_key');

        $cart = new Cart((int)$cart_id);
        $customer = new Customer((int)$cart->id_customer);

	   $order_id = Order::getOrderByCartId((int)$cart->id);

        
		print $order_id."<br>";
		print $secure_key."<br>";
		print $customer->secure_key."<br>";
		
		if ($order_id && ($secure_key == $customer->secure_key)) {
            /**
             * The order has been placed so we redirect the customer on the confirmation page.
             */

            $module_id = $this->module->id;
          
		   Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart_id.'&id_module='.$module_id.'&id_order='.$order_id.'&key='.$secure_key);
        } else {
            /**
             * An error occured and is shown on a new page.
             */
            $this->errors[] = $this->module->l('An error occured. Please contact the merchant to have more informations');
            return $this->setTemplate('error.tpl');
        }
    }

   
   protected function capture_payment($pass_vars){
	   
      //FINAL PAYMENT !!!!!!!! Atencion this will move money !!!!!!
	    
		$datap = $pass_vars;
		$datap['USER'] = 'raindropsinfotechbiz_api1.gmail.com';
		$datap['PWD'] = '1381926891';
		$datap['SIGNATURE'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AZyaC1CS1vdZkhtpb79s1Bg96IQB';
		$datap['METHOD'] = 'DoExpressCheckoutPayment';
		$datap['VERSION'] = '124';
		$datap['TOKEN'] = $_GET['token'];
		
		
		$curl = curl_init();
		 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($datap));
		 
		$response =    curl_exec($curl);
		 
		curl_close($curl);
		 
		$nvp = array();
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		
		
		
	
	   return $nvp;
	   
   }




   protected function isValidOrder()
    {
        /**
         * Add your checks right there
         */
        return true;
    }
	
	
}

<?php
 
class BlockblogAllModuleFrontController extends ModuleFrontController
{
	
	public function init()
	{
		//var_dump($_SERVER['HTTP_REFERER']);exit;
		header('Location: '.$_SERVER['HTTP_REFERER']);
		//Tools::redirect('modules/blockblog/blockblog-categories.php');
		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
		//$this->addJqueryPlugin(array('thickbox', 'idTabs'));
	}

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		
		parent::initContent();
		
		$this->setTemplate('all.tpl');
		
	}
}
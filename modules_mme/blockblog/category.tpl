{capture name=path}
<span class="navigation_page">{$meta_title}</span>
{/capture}

{capture name=path}
<span class="navigation_page">
<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
<a itemprop="url" href="" title="Shop">
<span itemprop="title">Blog</span>
</a>
</span>
<span class="navigation-pipe">&gt;</span>{$meta_title}</span>
{/capture}

<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>

{*{include file="$tpl_dir./breadcrumb.tpl"}*}







<div class="blog-section paddingTB60 custom_blog_list">
	<div class="container">
		{if $category_id == "373"}
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="global_dropdown">
					<span class="selected_location_title">GLOBAL</span> 
					<img src="/themes/default-bootstrap/img/play-button.png">
				</div>
				<div class="global_dropdown_list">
					<a href="/blog/category/australia-medical-marijuana-">Australia</a>
					<a href="/blog/category/brazil-medical-marijuana">Brazil</a>
					<a href="/blog/category/australia-medical-marijuana-">Germany</a>
					<a href="/blog/category/greece-medical-marijuana">Greece</a>
					<a href="/blog/category/israel-medical-marijuana">Israel</a>
					<a href="/blog/category/mexico-marijuana-and-hemp-news">Mexico</a>
					<a href="/blog/category/spain-medical-marijuana">Spain</a>
					<a href="/blog/category/england-medical-marijuana">UK</a>
				</div>
			</div>
		</div>
		{elseif $category_id == "380" || $category_id == "378"}
		{else}
		<div class="row">
			<div class="site-heading text-center">
				<h3>{$meta_title}</h3>

				{*<p  style="font-size: 18px;margin-bottom: 5px;"><strong>{l s='Posts' mod='blockblog'}  ( <span id="count_items_top" style="color: #333;">{$count_all}</span> )</strong></p>*}
				<div class="border"></div>
			</div>
		</div>
		{/if}
		<div class="social_for_state">
			{if $twitter_link != ''}
				<a href="{$twitter_link}" target="_blank">
					<i class="fa fa-twitter-square"></i>
				</a>
			{/if}
			{if $pinterest_link != ''}
				<a href="{$pinterest_link}" target="_blank">
					<i class="fa fa-pinterest"></i>
				</a>
			{/if}
			{if $tumblr_link != ''}
				<a href="{$tumblr_link}" target="_blank">
					<i class="fa fa-tumblr-square"></i>
				</a>
			{/if}
			{if $facebook_link != ''}
				<a href="{$facebook_link}" target="_blank">
					<i class="fa fa-facebook-square"></i>
				</a>
			{/if}
		</div>
		<div class="row text-center">
		
			{foreach from=$posts item=post name=myLoop}
			<div class="col-sm-6 col-md-4"> 
				<div class="blog-box">
					<div class="blog-box-image">
						{if strlen($post.img)>0}
						{if $blockblogurlrewrite_on == 1}
							<a style="    width: 100%;" href="{$base_dir}blog/post/{$post.seo_url}" 
						   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
						{else}
							<a style="    width: 100%;"  href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" 
						   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
						{/if}   	   
								<img src="{$base_dir}upload/blockblog/{$post.img}" title="{$post.title|escape:'htmlall':'UTF-8'}" 
									 alt="{$post.title|escape:'htmlall':'UTF-8'}"
									class="img-responsive imgcatbblog" style="    min-width: 100%;"
								/>
							</a>
					{/if}
					{********
						<img src="https://images.pexels.com/photos/6384/woman-hand-desk-office.jpg?w=940&h=650&auto=compress&cs=tinysrgb" class="img-responsive" alt="">
					********}
					</div>
					<div class="blog-box-content">
						<h4>
							{if $blockblogurlrewrite_on == 1}
								<a class="short_description_link" href="{$base_dir}blog/post/{$post.seo_url}" title="{$post.title|escape:'htmlall':'UTF-8'}">
									{$post.title}
								</a>
								<div class="blog_box_content_hideed">
									<p>
										{strip}
											{$post.content|truncate:250:"...":true}
										{/strip}
									</p>
									<p>
										{$post.time_add|date_format:"%B %e, %Y"}
									</p>
								</div>
							{else}
									<a class="short_description_link_2" href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" 
						   	  			 title="{$post.title|escape:'htmlall':'UTF-8'}">
										{$post.title}
									</a>
							{/if}
						</h4>
						{* <p   style="    font-weight: 800!important;
    color: #333333;
    font-style: italic;"><i class="fa fa-calendar" aria-hidden="true"></i>
{$post.time_add|date_format:"%D"}, &nbsp;<i class="fa fa-comment" aria-hidden="true"></i>
{$post.count_comments} {l s='comments' mod='blockblog'}</p>

						<p>{strip}{$post.content|truncate:200:"...":true} {/strip}</p>
						<a href="{$base_dir}modules/blockblog/blockblog-post.php?post_id={$post.id}" class="btn btn-default site-btn">Read More</a> *}

					</div>
				</div>
				
			</div> <!-- End Col -->	
			{/foreach}
			
		</div>

		<div class="row">
			<div class="col-12 pagination">
				{$paging}
			</div>
		</div>
	</div>
</div>



{********
<div id="page_nav" class="pages">
{$paging}
</div>
*****}
<script type="text/javascript" src="/themes/default-bootstrap/js/global.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/hoverIntent.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/superfish-modified.js"></script>
<script type="text/javascript" src="/themes/default-bootstrap/js/modules/blocktopmenu/js/blocktopmenu.js"></script>

<script type="text/javascript">
	function go_page_blog(page, item_id)
	{
		var location = window.location;

		window.location.href = location.protocol + '//' +
			location.host +
			location.pathname +
			'?page=' + page;
	}

	$('.global_dropdown').click(function(){
	  $('.global_dropdown_list').toggle();
	  $('.global_dropdown img').toggleClass( "global_dropdown_opened" );

	})
</script>

<style type="text/css">
	.pagination .pages > span.nums > b {
	    background: none;
		color: #6ec31c;
		padding: 0px !important;
	}
	.pagination .pages > span.nums > a{
		padding: 0px !important;
		background: none;
		color: #827979;
	}
	.pagination .pages > span.nums > a:hover {
		background: none !important;
		color: #827979 !important;
	}
</style>

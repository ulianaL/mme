{**
* Luis Leitao
*
* Module Prestashop 
*
*  Module gfont
*
*  @author    Luis Leitao    <luisleitao>
*  @copyright LuisLeitao LuisLeitao
*  @license   http://opensource.org/licenses/afl-3.0.php   Academic Free License (AFL 3.0)


*}


<style id="gfontfront">
@font-face {  
font-family: google; 
src: url({$activefonturl|replace:'http':'https'}); }
</style>

<style>

h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{
  font-family: 'Libre Baskerville', serif;
  color: #474646;
}
</style>  
{**
* Luis Leitao
*
* Module Prestashop 
*
*  Module gfont
*
*  @author    Luis Leitao    <luisleitao>
*  @copyright LuisLeitao LuisLeitao
*  @license   http://opensource.org/licenses/afl-3.0.php   Academic Free License (AFL 3.0)


*}


<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='Preview Font' mod='allgooglefonts'}</h3>
	<h2 style="font-family: fonttt;    min-height: 116px;">
		​‌{literal}
		A​‌B​‌C​‌Ć​‌Č​‌D​‌Đ​‌E​‌F​‌G​‌H​‌I​‌J​‌K​‌L​‌M​‌N​‌O​‌P​‌Q​‌R​‌S​‌Š​‌T​‌U​‌V​‌W​‌X​‌Y​‌Z​‌Ž​‌a​‌b​‌c​‌č​‌ć​‌d​‌đ​‌e​‌f​‌g​‌h​‌i​‌j​‌k​‌l​‌m​‌n​‌o​‌p​‌q​‌r​‌s​‌š​‌t​‌u​‌v​‌w​‌x​‌y​‌z​‌ž​‌ਆ​‌ਈ​‌ਊ​‌ਏ​‌ਐ​‌ਓ​‌ਔ​‌ਕ​‌ਖ​‌ਗ​‌ਘ​‌ਙ​‌ਚ​‌ਛ​‌ਜ​‌ਝ​‌ਞ​‌ਟ​‌ਠ​‌ਡ​‌ਢ​‌ਣ​‌ਤ​‌ਥ​‌ਦ​‌ਧ​‌ਨ​‌ਪ​‌ਫ​‌ਬ​‌ਭ​‌ਮ​‌ਯ​‌ਰ​‌ਲ​‌ਲ਼​‌ਵ​‌ਸ਼​‌ਸ​‌ਹ​‌ੲ​‌ੳ​‌ੴ​‌Ă​‌Â​‌Ê​‌Ô​‌Ơ​‌Ư​‌ă​‌â​‌ê​‌ô​‌ơ​‌ư​‌1​‌2​‌3​‌4​‌5​‌6​‌7​‌8​‌9​‌0​‌੦​‌੧​‌੨​‌੩​‌੪​‌੫​‌੬​‌੭​‌੮​‌੯​‌੧​‌‘​‌?​‌’​‌“​‌!​‌”​‌(​‌%​‌)​‌[​‌#​‌]​‌{​‌@​‌}​‌/​‌&​‌<​‌-​‌+​‌÷​‌×​‌=​‌>​‌®​‌©​‌$​‌€​‌£​‌¥​‌¢​‌:​‌;​‌,​‌.​‌*​‌₹
	    ​‌{/literal}
	</h2>
	<div style="min-height: 40px; margin: 0 auto;.">
		<button  onclick="remove();" style="float: left;"  class="btn btn-default">
		<i class="icon-minus"></i>
		</button>
		
		<div style="float: left;" >
			<span style="font-family: fonttt;font-size: 25px;text-align: center;min-width:300px;float: left;" id="namee"></span>
		</div>
		
		<button  style="float: left;" onclick="add();" class="btn btn-default">
		<i class="icon-plus"></i>
		</button>
	</div>
	

	
	
</div>

<style class="okk">

</style>

<script>

fontlist =  {$fonts|escape:'quotes':'UTF-8'}
url = fontlist[{$active|escape:'htmlall':'UTF-8'}]['files']['regular'];
inicialid = {$active|escape:'htmlall':'UTF-8'};
name = fontlist[inicialid]['family'];

{literal}
okk = '@font-face {  font-family: fonttt; src: url('+url+'); }';
{/literal}

$('.okk').text(okk);
$('#namee').text(name);
$('#fontt').val(inicialid);



function add()
{
inicialid++;
url = fontlist[inicialid]['files']['regular'];
name = fontlist[inicialid]['family'];
{literal}
okk = '@font-face {  font-family: fonttt; src: url('+url+'); }';
{/literal}

$('.okk').text(okk);
$('#namee').text(name);
$('#fontt').val(inicialid);

}

function remove()
{
	if (inicialid != 0)
	{
	inicialid--;
	url = fontlist[inicialid]['files']['regular'];
	name = fontlist[inicialid]['family'];
	{literal}
	okk = '@font-face {  font-family: fonttt; src: url('+url+'); }';
	{/literal}

	$('.okk').text(okk);
	$('#namee').text(name);
	$('#fontt').val(inicialid);
	}
}



$( document ).ready(function() {
$( "#fontt" ).change(function() {
		
		
		selected   = $('#fontt').val() ;
		url = fontlist[selected]['files']['regular'];
		name = fontlist[selected]['family'];

		{literal}
		okk = '@font-face {  font-family: fonttt; src: url('+url+'); }';
		{/literal}

		$('.okk').text(okk);
		$('#namee').text(name);
		
});




 });
</script>








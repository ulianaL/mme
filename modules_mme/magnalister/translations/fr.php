<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{magnalister}prestashop>magnalister_666311f430c7791d0c7d2e3eeb138d23'] = 'magnalister';
$_MODULE['<{magnalister}prestashop>magnalister_67935e2056976693feaf0653726c288e'] = 'Connectez magnalister à Prestashop et vendez sur Amazon, Cdiscount, PriceMinister, eBay';
$_MODULE['<{magnalister}prestashop>configure_666311f430c7791d0c7d2e3eeb138d23'] = 'magnalister';
$_MODULE['<{magnalister}prestashop>configure_6dcf6c728c1cfd0a73880ce2b8f3f892'] = 'PrestaShop official Marketplace partner';
$_MODULE['<{magnalister}prestashop>configure_659b9b6c975f647e41a842f94b654d7b'] = '//www.youtube.com/embed/EyeuzqHi5L8';
$_MODULE['<{magnalister}prestashop>configure_e98eed5d8fed809d9d07b0ae383482a8'] = 'Téléchargez vos produits, importez vos commandes ou synchronisez automatiquement les stocks et les prix... Et plus encore !';
$_MODULE['<{magnalister}prestashop>configure_6f1f95fca2766c15b44fafa0951bb123'] = 'Testez magnalister gratuitement et sans engagement pendant 30 jours.  Après votre inscription et l\'enregistrement de votre boutique, vous recevrez une clé d\'activation personnelle, la passphrase, qui vous permettra d\'activer le logiciel et de le tester dans sa totalité pendant 30 jours consécutifs sans aucun frais « caché ».';
$_MODULE['<{magnalister}prestashop>configure_7510d6cf2ab2a309e923a488424a9f22'] = 'Le certificat fournit au moins ces 5 garanties au consommateur:';
$_MODULE['<{magnalister}prestashop>configure_9431a48354486c3121e7945303771fd5'] = 'Savez-vous, que 78% des décisions d\'achats se font en consultant l’avis des consommateurs sur Amazon, Cdiscount, PriceMinister, eBay et autres.';
$_MODULE['<{magnalister}prestashop>configure_128f7f889e49cf1a63f0f53ac73b56e2'] = 'Les places de marché sont devenues incontournables, car elles génèrent une visibilité accrue ;  pour vous, une source de débouchés plus importants et une augmentation radicale de vos chances de vendre.';
$_MODULE['<{magnalister}prestashop>configure_ec74a708516044b61a6bd48f121f0dd5'] = 'Utilisez votre Prestashop avec le module Magnalister et faites de votre magasin un système de distribution, d\'inventaire et de gestion des commandes.';
$_MODULE['<{magnalister}prestashop>configure_e767b1137979cbceea023dab78c9864b'] = 'https://www.magnalister.com/fr/prestashop-interface-magnalister/?campaign=PrestaShopAddonsFR';
$_MODULE['<{magnalister}prestashop>configure_97c28047f3f8085e557c5e31219df0e6'] = 'Découvrez magnalister';

<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
/*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');


$name = "blocknewsadv";
$token = Tools::getValue('token');

if(md5($name._PS_BASE_URL_) == $token){

    include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
    $blocknewsadvfunctions_obj = new blocknewsadvfunctions();

    $blocknewsadvfunctions_obj->generateSitemap();


} else {
    echo 'Error: Access denien! Invalid token!';
}



/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */


function blocknewsadv_like_news(id,like){

    $('.block-item-like-'+id).css('opacity',0.5);

    $.post(baseDir+'modules/blocknewsadv/process.php', {
            action:'like',
            id : id,
            like : like
        },
        function (data) {
            if (data.status == 'success') {

                $('.block-item-like-'+id).css('opacity',1);

                var count = data.params.count;
                if(like == 1){
                    $('.block-item-like-'+id).html('');
                    $('.block-item-like-'+id).append('<i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');

                    if($('.block-item-like1-'+id)){
                        $('.block-item-like1-'+id).html('');
                        $('.block-item-like1-'+id).append('<i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');
                    }
                } else {
                    $('.block-item-unlike-'+id).html('');
                    $('.block-item-unlike-'+id).append('<i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');

                    if($('.block-item-unlike1-'+id)){
                        $('.block-item-unlike1-'+id).html('');
                        $('.block-item-unlike1-'+id).append('<i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');
                    }
                }


            } else {
                $('.block-item-like-'+id).css('opacity',1);
                alert(data.message);
            }

        }, 'json');
}

    function show_arch(id,column){
        for(i=0;i<100;i++){
            $('#arch'+i+column).hide(200);
        }
        $('#arch'+id+column).show(200);

    }

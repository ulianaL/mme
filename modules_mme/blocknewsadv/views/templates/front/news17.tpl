{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}

{extends file='page.tpl'}

{block name="page_content"}

    {include file="modules/blocknewsadv/views/templates/front/news.tpl"}

{/block}

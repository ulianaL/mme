{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}
 
 {if $blocknewsadvnews_m == 1}




     <div id="blocknewsadvblock_block_left"
          class="block {if $blocknewsadvis17 == 1}block-categories{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} blocknewsadv-block {if $blocknewsadvis_ps15 == 0}margin-top-10{/if}"
             >
         <h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase h6{/if} {if $blocknewsadvrsson == 0}rss-home-block{/if}">

             <div {if $blocknewsadvrsson == 1}class="float-left"{/if}>
                 <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='blocknewsadv'}"
                         >{l s='Latest News' mod='blocknewsadv'}</a>

             </div>
             {if $blocknewsadvrsson == 1}
                 <div class="float-left margin-left-10">
                     <a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/rss.php" title="{l s='RSS Feed' mod='blocknewsadv'}" target="_blank">
                         <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/img/feed.png" alt="{l s='RSS Feed' mod='blocknewsadv'}" />
                     </a>
                 </div>
             {/if}

             <div class="clear"></div>
         </h4>
         <div class="block_content block-items-data">
             {if count($blocknewsadvitems_home)>0}
                 <div class="items-articles-block">

                     {foreach from=$blocknewsadvitems_home item=items name=myLoop}
                         {foreach from=$items.data item=item name=myLoop}


                             <div class="current-item-block">

                                 {if $blocknewsadvblock_display_img == 1}
                                     {if strlen($item.img)>0}
                                         <div class="block-side">
                                             <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                                  title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />
                                         </div>
                                     {/if}
                                 {/if}

                                 <div class="block-content">
                                     <a class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                        href="{if $blocknewsadvrew_on == 1}
                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                     {else}
                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                     {/if}
                                    "
                                             >{$item.title|escape:'htmlall':'UTF-8'}</a>
                                     <div class="item-text-content">{$item.content|strip_tags|substr:0:$blocknewsadvitem_h_tr|escape:'quotes':'UTF-8'}{if strlen($item.content)>$blocknewsadvitem_h_tr}...{/if}</div>

                                     <div class="clr"></div>

                                     {if $blocknewsadvh_display_date == 1}
                                         <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                     {/if}

                                     <span class="float-right comment block-item-like">
                                {if $blocknewsadvis_like == 1}
                                    {if $item.is_liked_news}
                                        <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                                {else}
                                    <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                    <a onclick="blocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                       href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                    </span>
                                    {/if}
                                {/if}
                                         &nbsp;
                                         {if $blocknewsadvis_unlike == 1}
                                             {if $item.is_unliked_news}
                                                 <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                {else}
                                <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                             {/if}
                                         {/if}
                            </span>


                                     <div class="clr"></div>
                                 </div>
                             </div>





                         {/foreach}
                     {/foreach}

                 </div>


                 <div class="prfb-clear"></div>
             {else}
                 <div class="block-no-items">
                     {l s='News not found.' mod='blocknewsadv'}
                 </div>
             {/if}
         </div>
     </div>

{/if}


{if $blocknewsadvarch_m == 1}
    <div id="blocknewsadvarch_block_left" class="block {if $blocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} search_items" >
        <h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase{/if}">{l s='News Archives' mod='blocknewsadv'}</h4>

        <div class="block_content{if $blocknewsadvis17 == 1}17{/if}">
            {if sizeof($blocknewsadvarch)>0}
                <ul class="bullet">
                    {foreach from=$blocknewsadvarch item=items key=year name=myarch}
                        <li><a class="arch-category" href="javascript:void(0)"
                               onclick="show_arch({$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'},'left')">{$year|escape:'htmlall':'UTF-8'}</a></li>
                        <div id="arch{$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'}left"
                             {if $smarty.foreach.myarch.first}{else}class="display-none"{/if}
                                >
                            {foreach from=$items item=item name=myLoop1}
                                <li class="arch-subcat">
                                    <a class="arch-subitem" href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}{if $blocknewsadvrew_on == 1}?{else}&{/if}y={$year|escape:'htmlall':'UTF-8'}&m={$item.month|escape:'htmlall':'UTF-8'}">
                                        {$item.time_add|date_format:"%B"|escape:'htmlall':'UTF-8'}&nbsp;({$item.total|escape:'htmlall':'UTF-8'})
                                    </a>
                                </li>
                            {/foreach}
                        </div>
                    {/foreach}
                </ul>
            {else}
                {l s='There are not Archives yet.' mod='blocknewsadv'}
            {/if}

        </div>

    </div>
{/if}

{if $blocknewsadvsearch_m == 1}
    <div id="blocknewsadvsearch_block_left" class="block {if $blocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $blocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} search_items" >
        <h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase{/if}">{l s='Search News' mod='blocknewsadv'}</h4>
        <form method="get" action="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            <div class="block_content{if $blocknewsadvis17 == 1}17{/if}">
                <input type="text" value="" class="search_items {if $blocknewsadvis17 == 1}search-blocknewsadv17{/if} {if $blocknewsadvis_ps15 == 0}search_text{/if}" name="search" >
                <input type="submit" value="go" class="button_mini {if $blocknewsadvis17 == 1}button-mini-blocknewsadv{/if} {if $blocknewsadvis_ps15 == 0}search_go{/if}"/>
                {if $blocknewsadvis_ps15 == 0}<div class="clear"></div>{/if}
            </div>
        </form>
    </div>
{/if}
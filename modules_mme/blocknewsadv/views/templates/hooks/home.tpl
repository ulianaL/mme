{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}
 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.green.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
 


<div class="lnebg">
<div class="hdnam">
	<a href="/news">
		<span>Recent Articles</span>
	</a>
</div>


<div class="home_articles_arrows">
	<span class="moreb">
     <img src="/themes/default-bootstrap/img/articles_arrow_left.png">   
    </span>
	<span class="lessb">
     <img src="/themes/default-bootstrap/img/home_articles_right.png">   
    </span>
</div>


<div class="clearfix"></div>
</div>

 
 

 




<div {if $blocknewsadvis15 != 0 && $blocknewsadvis17 == 0}id="left_column"{/if}>

   
{*****
		<h4 class="title_block {if $blocknewsadvis17 == 1}text-uppercase h6{/if} {if $blocknewsadvrsson == 0}rss-home-block{/if}">
	
            <div {if $blocknewsadvrsson == 1}class="float-left"{/if}>
                <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='blocknewsadv'}"
                        >{l s='Latest News' mod='blocknewsadv'}</a>

            </div>
	
			
			
            {if $blocknewsadvrsson == 1}
                <div class="float-left margin-left-10">
                    <a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/rss.php" title="{l s='RSS Feed' mod='blocknewsadv'}" target="_blank">
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/img/feed.png" alt="{l s='RSS Feed' mod='blocknewsadv'}" />
                    </a>
                </div>
            {/if}
*****}	
	
	
	
            <div class="clear"></div>
        </h4>
        <div class="block_content block-items-data">
            {if count($blocknewsadvitems_home)>0}
                    <div class="items-articles-block">

                        
						
						
						 <div class="owl-carousel">
						{foreach from=$blocknewsadvitems_home item=items name=myLoop}
                            {foreach from=$items.data item=item name=myLoop}

                    <div class="item">
                    <div class="current-item-block">

                        {if $blocknewsadvblock_display_img == 1}
                            {if strlen($item.img)>0}
                                
								
								
								<div class="block-side"  style="    width: 100%!important;    ">
                                    <a href="{if $blocknewsadvrew_on == 1}
                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                     {else}
                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                     {/if}
                                    ">
									
                                    <div class="news_img" style="background-image: url({$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'});">
                                        <div class="block-content">
                                            <span class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                               href="{if $blocknewsadvrew_on == 1}
                                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                                     {else}
                                                        {$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                                     {/if}
                                                    "
                                                    >{$item.title|escape:'htmlall':'UTF-8'}</span>

                            
                                            {if $blocknewsadvh_display_date == 1}
                                                <span class="block-item-date">{$item.time_add|date_format:"%B %e, %Y"|escape:'htmlall':'UTF-8'}</span>
                                            {/if}

                                        </div>

                                    </div>
                                </a>
								</div>
                            {/if}
                        {/if}

                        
                    </div>




				</div>
                {/foreach}
            {/foreach}
			</div>
			
			
			
			
			
			
			
			
                </div>


            <div class="prfb-clear"></div>
            {else}
            <div class="block-no-items">
                {l s='News not found.' mod='blocknewsadv'}
            </div>
            {/if}
        </div>
    </div>
</div>

<script>

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            loop:false
        }
    }
})


var owl = $('.owl-carousel');
owl.owlCarousel();
// Go to the next item
$('.lessb').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.moreb').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
})

</script>
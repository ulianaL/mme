{*
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */
 *}

<div class="blocknewsadv-clear"></div>
<br/>
{if $blocknewsadvfooternews == 1}

  {if $blocknewsadvis16 == 1}
          {if $blocknewsadvis17 == 1}
            <div class="col-xs-12 col-sm-3 wrapper links">
          {else}
            <section class="blocknewsadvnews_block_footer footer-block col-xs-12 col-sm-3">
          {/if}
    {else}
	<div id="blocknewsadvposts_block_footer"
         class="block footer-block footer-block-class blockmanufacturer blocknewsadv-block"
         style="width:{if $blocknewsadvis_ps15 == 0}190{else}200{/if}px;">
    {/if}

    <h4 class="title_block {if $blocknewsadvis17 == 1}h3 hidden-sm-down{/if}">

        <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='blocknewsadv'}"
                >{l s='Latest News' mod='blocknewsadv'}</a>

        {if $blocknewsadvrsson == 1}
            <a  class="margin-left-left-10" href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/rss.php" title="{l s='RSS Feed' mod='blocknewsadv'}" target="_blank">
                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/blocknewsadv/views/img/feed.png" alt="{l s='RSS Feed' mod='blocknewsadv'}" />
            </a>
        {/if}

    </h4>
    {if $blocknewsadvis17 == 1}
        <div data-toggle="collapse" data-target="#blocknewsadvnews_block_footer" class="title clearfix hidden-md-up">
            <span class="h3">{l s='Latest News' mod='blocknewsadv'}</span>
                        <span class="pull-xs-right">
                          <span class="navbar-toggler collapse-icons">
                            <i class="material-icons add">&#xE313;</i>

                          </span>
                        </span>
        </div>
    {/if}

    <div class="block_content block-items-data toggle-footer {if $blocknewsadvis17 == 1}collapse{/if}" {if $blocknewsadvis17 == 1}id="blocknewsadvnews_block_footer"{/if}>

        {if count($blocknewsadvitemsblock) > 0}
            <div class="items-articles-block">

                {foreach from=$blocknewsadvitemsblock item=items name=myLoop1}
                    {foreach from=$items.data item=item name=myLoop}

                        <div class="current-item-block">
                            {if $blocknewsadvblock_display_img == 1}
                                {if strlen($item.img)>0}
                                    <div class="block-side">
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$blocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                             title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />

                                    </div>
                                {/if}
                            {/if}

                            <div class="block-content">
                                <a class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                   href="{if $blocknewsadvrew_on == 1}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}{else}{$blocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}{/if}"
                                        >{$item.title|escape:'htmlall':'UTF-8'}</a>

                                <div class="clr"></div>
                                {if $blocknewsadvb_display_date == 1}
                                    <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                {/if}
                                <span class="float-right comment block-item-like">

                            {if $blocknewsadvis_like == 1}
                            {if $item.is_liked_news}
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                            {/if}
                            {/if}
                                    &nbsp;

                            {if $blocknewsadvis_unlike == 1}
                            {if $item.is_unliked_news}
                                        <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="blocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                            {/if}
                            {/if}

                        </span>

                                <div class="clr"></div>
                            </div>
                        </div>

                    {/foreach}
                {/foreach}
                <p class="block-view-all">
                    <a href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='View all news' mod='blocknewsadv'}"
                       class="button {if $blocknewsadvis17 == 1}btn btn-default button button-small-blocknewsadv{/if} "
                            ><b>{l s='View all news' mod='blocknewsadv'}</b></a>
                </p>

            </div>
        {else}
            <div class="block-no-items">
                {l s='News not found.' mod='blocknewsadv'}
            </div>
        {/if}

    </div>

    {if $blocknewsadvis16 == 1}
        {if $blocknewsadvis17 == 1}
            </div>
        {else}
            </section>
        {/if}
    {else}
	    </div>
    {/if}
{/if}


{if $blocknewsadvarch_footer_n == 1}
    {if $blocknewsadvis16 == 1}
        {if $blocknewsadvis17 == 1}
            <div class="col-xs-12 col-sm-3 wrapper links">
        {else}
            <section class="blocknewsadvarch_block_footer footer-block col-xs-12 col-sm-3">
        {/if}
    {else}
        <div id="blocknewsadvarch_block_footer"  class="block footer-block-class footer-block blockmanufacturer"
             style="width:{if $blocknewsadvis_ps15 == 0}190{else}200{/if}px;">
    {/if}


        <h4 class="title_block {if $blocknewsadvis17 == 1}h3 hidden-sm-down{/if}">{l s='News Archives' mod='blocknewsadv'}</h4>

    {if $blocknewsadvis17 == 1}
        <div data-toggle="collapse" data-target="#blocknewsadvarch_block_footer" class="title clearfix hidden-md-up">
            <span class="h3">{l s='News Archives' mod='blocknewsadv'}</span>
                        <span class="pull-xs-right">
                          <span class="navbar-toggler collapse-icons">
                            <i class="material-icons add">&#xE313;</i>

                          </span>
                        </span>
        </div>
    {/if}

        <div class="block_content block-items-data toggle-footer {if $blocknewsadvis17 == 1}collapse{/if}" {if $blocknewsadvis17 == 1}id="blocknewsadvarch_block_footer"{/if}>
            {if sizeof($blocknewsadvarch)>0}
                <ul class="bullet">
                    {foreach from=$blocknewsadvarch item=items key=year name=myarch}
                        <li><a class="arch-category" href="javascript:void(0)"
                               onclick="show_arch({$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'},'footer')">{$year|escape:'htmlall':'UTF-8'}</a></li>
                        <div id="arch{$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'}footer"
                             {if $smarty.foreach.myarch.first}{else}class="display-none"{/if}
                                >
                            {foreach from=$items item=item name=myLoop1}
                                <li class="arch-subcat">
                                    <a class="arch-subitem" href="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}{if $blocknewsadvrew_on == 1}?{else}&{/if}y={$year|escape:'htmlall':'UTF-8'}&m={$item.month|escape:'htmlall':'UTF-8'}">
                                        {$item.time_add|date_format:"%B"|escape:'htmlall':'UTF-8'}&nbsp;({$item.total|escape:'htmlall':'UTF-8'})
                                    </a>
                                </li>
                            {/foreach}
                        </div>
                    {/foreach}
                </ul>
            {else}
                {l s='There are not Archives yet.' mod='blocknewsadv'}
            {/if}

        </div>

   {if $blocknewsadvis16 == 1}
        {if $blocknewsadvis17 == 1}
            </div>
        {else}
            </section>
        {/if}
    {else}
	    </div>
    {/if}
{/if}

{if $blocknewsadvsearch_footer_n == 1}
    {if $blocknewsadvis16 == 1}
        {if $blocknewsadvis17 == 1}
            <div class="col-xs-12 col-sm-3 wrapper links">
        {else}
            <section class="blocknewsadvarch_block_footer footer-block col-xs-12 col-sm-3">
        {/if}
    {else}

        <div id="blocknewsadvsearch_block_footer"
        class="block footer-block footer-block-class-last blockmanufacturer search_blog"
        style="width:{if $blocknewsadvis_ps15 == 0}190{else}200{/if}px;">
    {/if}


        <h4 class="title_block {if $blocknewsadvis17 == 1}h3 hidden-sm-down{/if}">{l s='Search News' mod='blocknewsadv'}</h4>
    {if $blocknewsadvis17 == 1}
        <div data-toggle="collapse" data-target="#blocknewsadvsearch_block_footer" class="title clearfix hidden-md-up">
            <span class="h3">{l s='Search News' mod='blocknewsadv'}</span>
                        <span class="pull-xs-right">
                          <span class="navbar-toggler collapse-icons">
                            <i class="material-icons add">&#xE313;</i>

                          </span>
                        </span>
        </div>
    {/if}

        <div class="block-items-data toggle-footer {if $blocknewsadvis17 == 1}collapse{/if}" {if $blocknewsadvis17 == 1}id="blocknewsadvsearch_block_footer"{/if}>
        <form method="get" action="{$blocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            <div class="block_content">
                <input type="text" value="" class="search_items {if $blocknewsadvis17 == 1}search-blocknewsadv17{/if} {if $blocknewsadvis_ps15 == 0}search_text{/if}" name="search" >
                <input type="submit" value="go" class="button_mini {if $blocknewsadvis17 == 1}button-mini-blocknewsadv{/if} {if $blocknewsadvis_ps15 == 0}search_go{/if}"/>
                {if $blocknewsadvis_ps15 == 0}<div class="clear"></div>{/if}
            </div>
        </form>
        </div>
     {if $blocknewsadvis16 == 1}
        {if $blocknewsadvis17 == 1}
            </div>
        {else}
            </section>
        {/if}
    {else}
	    </div>
    {/if}
{/if}
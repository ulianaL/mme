<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blocknewsadv';
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

$name_module = 'blocknewsadv';

if (version_compare(_PS_VERSION_, '1.5', '<')){
	require_once(_PS_MODULE_DIR_.$name_module.'/backward_compatibility/backward.php');
} else{
	$smarty = Context::getContext()->smarty;
}


include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();


$post_id = Tools::getValue("id");


$post_id = (int) $obj_blocknewshelp->getTransformSEOURLtoID(array('id'=>$post_id));

$_info_cat = $obj_blocknewshelp->getItem(array('id' => $post_id,'site'=>1));

$title = isset($_info_cat['item'][0]['title'])?$_info_cat['item'][0]['title']:'';
$seo_description = isset($_info_cat['item'][0]['seo_description'])?$_info_cat['item'][0]['seo_description']:'';
$seo_keywords = isset($_info_cat['item'][0]['seo_keywords'])?$_info_cat['item'][0]['seo_keywords']:''; 


include_once(dirname(__FILE__).'/blocknewsadv.php');
$obj_blocknews = new blocknewsadv();

if(sizeof($_info_cat['item'])==0){

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
						
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("HTTP/1.1 301 Moved Permanently");


    $data_url = $obj_blocknewshelp->getSEOURLs();
    $news_url = $data_url['news_url'];
    $obj_blocknews->tools_redirect($news_url);

	exit;
}

$smarty->assign('meta_title' , $title);
$smarty->assign('meta_description' , $seo_description);
$smarty->assign('meta_keywords' , $seo_keywords);

$smarty->assign($name_module.'rew_on', Configuration::get($name_module.'rew_on'));

$smarty->assign($name_module.'i_display_date', Configuration::get($name_module.'i_display_date'));
		
if(version_compare(_PS_VERSION_, '1.6', '>')){
 	$smarty->assign($name_module.'is16' , 1);
} else {
 	$smarty->assign($name_module.'is16' , 0);
}

if (version_compare(_PS_VERSION_, '1.5', '>') && version_compare(_PS_VERSION_, '1.6', '<')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				if(version_compare(_PS_VERSION_, '1.5', '<'))
					include_once(dirname(__FILE__).'/../../header.php');
			}


### related products ####
$related_products = $_info_cat['item'][0]['related_products']; 
$data_related_products = $obj_blocknewshelp->getRelatedProducts(array('related_data'=>$related_products));
### related products ####


### related posts ###
$related_posts = $_info_cat['item'][0]['related_posts']; 
$data_related_posts = $obj_blocknewshelp->getRelatedPostsForPost(array('related_data'=>$related_posts,'post_id'=>$post_id));
### related posts ###

$id_lang = $obj_blocknews->getIdLang();

$data_locale = $obj_blocknewshelp->getfacebooklib($id_lang);
$lng_iso =$data_locale['lng_iso'];


$is_ssl = false;
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
    $is_ssl = true;


$smarty->assign(array('posts' => $_info_cat['item'],
					  'related_products'=>$data_related_products,
					  'related_posts'=>$data_related_posts,

                        $name_module.'snip_publisher' => Configuration::get('PS_SHOP_NAME'),
                        $name_module.'snip_width'=>Configuration::get($name_module.'item_img_w'),
                        $name_module.'snip_height'=>Configuration::get($name_module.'item_img_w'),
                        $name_module.'is_soc_but'=>Configuration::get($name_module.'is_soc_but'),

                        $name_module.'item_rp_tr'=>Configuration::get($name_module.'item_rp_tr'),

                        $name_module.'is_comments'=>Configuration::get($name_module.'is_comments'),

                        $name_module.'number_fc'=>Configuration::get($name_module.'number_fc'),
                        $name_module.'lng_iso'=>$lng_iso,

                        $name_module.'appid'=>Configuration::get($name_module.'appid'),

                        $name_module.'is_ssl' => $is_ssl,
					 ));


if(version_compare(_PS_VERSION_, '1.5', '>')){
	
	if(version_compare(_PS_VERSION_, '1.6', '>')){
					
		$obj_front_c = new ModuleFrontController();
		$obj_front_c->module->name = 'blocknewsadv';
		$obj_front_c->setTemplate('news-item.tpl');
		
		$obj_front_c->setMedia();
		
		$obj_front_c->initHeader();
		
		$obj_front_c->initContent();
		
		$obj_front_c->initFooter();
		
		
		$obj_front_c->display();
		
	} else {
		echo $obj_blocknews->renderTplItem();
	}
} else {
	echo Module::display(dirname(__FILE__).'/blocknewsadv.php', 'views/templates/front/news-item.tpl');
}
if (version_compare(_PS_VERSION_, '1.5', '>') && version_compare(_PS_VERSION_, '1.6', '<')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				if(version_compare(_PS_VERSION_, '1.5', '<'))
					include_once(dirname(__FILE__).'/../../footer.php');
			}

?>
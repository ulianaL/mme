<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
ob_start(); 
$status = 'success';
$message = '';

$name_module = 'blocknewsadv';

if (version_compare(_PS_VERSION_, '1.5', '<')){
	require_once(_PS_MODULE_DIR_.$name_module.'/backward_compatibility/backward.php');
} else{
	$smarty = Context::getContext()->smarty;
}

include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();

$action = Tools::getValue('action');

switch ($action){
	case 'deleteimg':
        include_once(dirname(__FILE__).'/blocknewsadv.php');
        $obj_blocknewsadv = new blocknewsadv();

        if($obj_blocknewsadv->is_demo){
            $status = 'error';
            $message = 'Feature disabled on the demo mode!';
        } else {
            $item_id = Tools::getValue('item_id');
            $obj_blocknewshelp->deleteImg(array('id' => $item_id));
        }
	break;
    case 'like':
        $ip = $_SERVER['REMOTE_ADDR'];
        $like = (int)Tools::getValue('like');
        $id = (int)Tools::getValue('id');

        $data = $obj_blocknewshelp->like(array('id'=>$id,'like'=>$like,'ip'=>$ip));
        $status = $data['error'];
        $message = $data['message'];
        $count = $data['count'];
    break;
	default:
		$status = 'error';
		$message = 'Unknown parameters!';
	break;
}


$response = new stdClass();
$content = ob_get_clean();
$response->status = $status;
$response->message = $message;	
if($action == 'like')
    $response->params = array('content' => $content, 'count'=>$count);
else
	$response->params = array('content' => $content);
	
	
	
include_once(dirname(__FILE__).'/blocknewsadv.php');
$obj_tools = new blocknewsadv();

echo $obj_tools->json_encode_ps13($response);

?>
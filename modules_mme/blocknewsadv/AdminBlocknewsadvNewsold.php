<?php
/**
 * mitrocops
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
/*
 *
 * @author    mitrocops
 * @category content_management
 * @package blocknewsadv
 * @copyright Copyright mitrocops
 * @license   mitrocops
 */

ob_start();
	/*@ini_set('display_errors', 'on');	
	define('_PS_DEBUG_SQL_', true);
	define('_PS_DISPLAY_COMPATIBILITY_WARNING_', true);
	error_reporting(E_ALL|E_STRICT);
	*/
class AdminBlocknewsadvnewsold extends AdminTab{

	private $_is15;
	public function __construct()

	{
		$this->module = 'blocknewsadv';
		
		if(version_compare(_PS_VERSION_, '1.5', '>')){
			$this->multishop_context = Shop::CONTEXT_ALL;
			$this->_is15 = 1;
		} else {
			$this->_is15 = 0;
		}
		
		
		parent::__construct();
		
	}
	
 	
	public function addCSS(){
	}
	
	public function addJS(){
	}
	

	
	public function display()
	{
		
		
		echo '<style type="text/css">.warn{display:none!important}
									 #maintab20{display:none!important}
									 .add_new_button{border: 1px solid #DEDEDE; padding: 10px; margin-bottom: 10px; width:10%; display: block; font-size: 16px; color: maroon; text-align: center; font-weight: bold; text-decoration: underline;float:right}
			  .title_breadcrumbs{border: 1px solid #DEDEDE;
			    				color: #000000;
							    display: block;
							    font-size: 16px;
							    font-weight: bold;
							    margin: 0 0 10px 0;
							    padding: 10px;
							    text-align: left;
							    text-decoration: none;
							    width: auto;float:left}
				.clear_both{clear:both}
		</style>';
		

		if(version_compare(_PS_VERSION_, '1.5', '>')){
			echo '<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.css" />

			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>';
		} else {
			echo '<script type="text/javascript">

					var formProduct;

					var accessories = new Array();

				</script>';

			echo '<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'js/autocomplete/jquery.autocomplete.css" />

			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/autocomplete/jquery.autocomplete.js"></script>';
		}
		
		if (version_compare(_PS_VERSION_, '1.6', '<')){
			require_once(_PS_MODULE_DIR_.$this->module.'/backward_compatibility/backward.php');
            $variables14 = variables14_blocknewsadv();
            $currentIndex = $variables14['currentindex'];
        } else {
			$currentIndex = AdminController::$currentIndex;
		}
        // include main class
		require_once(dirname(__FILE__) .  '/blocknewsadv.php');
		// instantiate
		$obj_main = new blocknewsadv();
		
		$tab = 'AdminBlocknewsadvnewsold';
		
		$token = $this->token;
		
		
		
		include_once(dirname(__FILE__).'/classes/blocknewsadvfunctions.class.php');
        $blocknewsadvfunctions_obj = new blocknewsadvfunctions();
		
		echo $obj_main->_jsandcss();




        if (Tools::isSubmit('cancel_item'))
        {
            $page = Tools::getValue("pageitems");
            Tools::redirectAdmin($currentIndex.'&configure='.$this->module.'&token='.$token.'&pageitems='.$page);
        }

        if (Tools::isSubmit('update_item'))
        {
            $id = Tools::getValue("id");

            $languages = Language::getLanguages(false);
            $data_title_content_lang = array();
            $seo_url = Tools::getValue("seo_url");
            $time_add = Tools::getValue("time_add");

            $ids_related_products = Tools::getValue("inputAccessories");

            $ids_related_posts = Tools::getValue("ids_related_posts");


            if($this->_is15){
                $cat_shop_association = Tools::getValue("cat_shop_association");
            } else{
                $cat_shop_association = array(0=>1);
            }

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $title = Tools::getValue("title_".$id_lang);
                $content = Tools::getValue("content_".$id_lang);
                $seokeywords = Tools::getValue("seokeywords_".$id_lang);
                $seodescription = Tools::getValue("seodescription_".$id_lang);

                if(Tools::strlen($title)>0 && Tools::strlen($content)>0)
                {
                    $data_title_content_lang[$id_lang] = array('title' => $title,
                        'content' => $content,
                        'seokeywords' => $seokeywords,
                        'seodescription' => $seodescription,
                        'seo_url' => $seo_url
                    );
                }
            }

            $item_status = Tools::getValue("item_status");
            $post_images = Tools::getValue("post_images");
            $item_iscomments = Tools::getValue("item_iscomments");

            $data = array('data_title_content_lang'=>$data_title_content_lang,
                'id' => $id,
                'iscomments' => $item_iscomments,
                'item_status' => $item_status,
                'post_images' => $post_images,
                'related_products'=>$ids_related_products,
                'ids_related_posts'=>$ids_related_posts,
                'cat_shop_association' => $cat_shop_association,
                'time_add' => $time_add
            );
            if(sizeof($data_title_content_lang)>0)
                $blocknewsadvfunctions_obj->updateItem($data);

            $page = Tools::getValue("pageitems");
            Tools::redirectAdmin($currentIndex.'&configure='.$this->module.'&token='.$token.'&pageitems='.$page);

        }

        if (Tools::isSubmit('submit_item'))
        {
            $languages = Language::getLanguages(false);
            $data_title_content_lang = array();
            $seo_url = Tools::getValue("seo_url");
            $time_add = Tools::getValue("time_add");

            $ids_related_products = Tools::getValue("inputAccessories");

            $ids_related_posts = Tools::getValue("ids_related_posts");


            if($this->_is15){
                $cat_shop_association = Tools::getValue("cat_shop_association");
            } else{
                $cat_shop_association = array(0=>1);
            }

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $title = Tools::getValue("title_".$id_lang);
                $content = Tools::getValue("content_".$id_lang);
                $seokeywords = Tools::getValue("seokeywords_".$id_lang);
                $seodescription = Tools::getValue("seodescription_".$id_lang);

                if(Tools::strlen($title)>0 && Tools::strlen($content)>0)
                {
                    $data_title_content_lang[$id_lang] = array('title' => $title,
                        'content' => $content,
                        'seokeywords' => $seokeywords,
                        'seodescription' => $seodescription,
                        'seo_url' => $seo_url
                    );
                }
            }

            $item_status = Tools::getValue("item_status");
            $item_iscomments = Tools::getValue("item_iscomments");

            $data = array('data_title_content_lang'=>$data_title_content_lang,
                'iscomments' => $item_iscomments,
                'item_status' => $item_status,
                'cat_shop_association' => $cat_shop_association,
                'related_products'=>$ids_related_products,
                'ids_related_posts'=>$ids_related_posts,
                'time_add' => $time_add
            );
            //echo "<pre>"; var_dump($data); exit;
            if(sizeof($data_title_content_lang)>0)
                $blocknewsadvfunctions_obj->saveItem($data);

            $page = (int) Tools::getValue("pageitems");
            Tools::redirectAdmin($currentIndex.'&configure='.$this->module.'&token='.$token.'&pageitems='.$page);
        }


        // delete news item
        if (Tools::isSubmit("delete_item")) {
            if (Validate::isInt(Tools::getValue("id"))) {

                $data = array('id' => Tools::getValue("id"));
                $blocknewsadvfunctions_obj->deleteItem($data);

                $page = Tools::getValue("pageitems");
                Tools::redirectAdmin($currentIndex.'&configure='.$this->module.'&token='.$token.'&pageitems='.$page);
            }
        }
        // delete news item

			

		echo $obj_main->_moderateNews(array('edit'=>2,'currentindex'=>$currentIndex,'controller'=>$tab));

		
	}
		

}

?>


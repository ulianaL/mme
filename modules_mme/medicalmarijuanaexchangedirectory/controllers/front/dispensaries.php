<?php


class MedicalmarijuanaexchangedirectoryDispensariesModuleFrontController extends ModuleFrontController
{

    public function __construct()
    {

        parent::__construct();

        $this->display_column_left = false;

    }
    public function postProcess()
    {
        if (isset($_POST['getstores'])) {
            $delivery = $_POST['delivery'];
            $storefront = $_POST['storefront'];
            $open_now = $_POST['open_now'];
            $medical = $_POST['medical'];
            $recreational = $_POST['recreational'];
            $cityName = $_POST['cityName'];
            $regionName = $_POST['regionName'];
            $shops_list = (new Customer)->getDispensaries(
                $cityName,
                $regionName,
                Tools::getValue('order_delivery', 0),
                Tools::getValue('order_pickup', 0),
                $delivery,
                $storefront,
                Tools::getValue('is_doctor', 0),
                $open_now,
                $medical,
                $recreational,
                Tools::getValue('current_time', null),
                Tools::getValue('polygone', array())
            );
            if (!headers_sent()) {
                header('Content-Type: application/json');
            }
            die(Tools::jsonEncode($shops_list));
        }
    }
     public function initContent()
    {

	   parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';

        $business_type = '';
      
        if (isset($_GET["type"])) {

            $business_type = $_GET["type"];
    
        }else{
            $business_type = 'all';
        }
                  

		$this->context->smarty->assign('business_type', $business_type);
        $this->setTemplate('dispensaries.tpl');
			
    } 

}

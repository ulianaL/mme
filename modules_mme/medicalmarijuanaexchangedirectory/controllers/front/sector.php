<?php
class MedicalmarijuanaexchangedirectorySectorModuleFrontController extends ModuleFrontController
{

 public function initContent()
    {

	   parent::initContent();
        
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $context = $context;
        $languages = Language::getLanguages(true, $this->context->shop->id);
        $this->htmlcat = '';
        if (!count($languages)) {
            return false;
        }
        if (isset($_GET['agtech'])) {
            $sector_id = 1;
            $height = "790px";
        }elseif (isset($_GET['biotechnology'])) {
            $sector_id = 2;
            $height = "1140px";
        }elseif (isset($_GET['consumption-devices'])) {
            $sector_id = 3;
            $height = "450px";
        }elseif (isset($_GET['cultivation-retail'])) {
            $sector_id = 4;
            $height = "3250px";
        }elseif (isset($_GET['hemp-products'])) {
            $sector_id = 5;
            $height = "1170px";
        }elseif (isset($_GET['investing-finance'])) {
            $sector_id = 6;
            $height = "960px";
        }elseif (isset($_GET['marijuana-products'])) {
            $sector_id = 7;
            $height = "960px";
        }elseif (isset($_GET['other-ancillary'])) {
            $sector_id = 8;
            $height = "410px";
        }elseif (isset($_GET['real-estate'])) {
            $sector_id = 9;
            $height = "400px";
        }elseif (isset($_GET['secondary-services'])) {
            $sector_id = 10;
            $height = "930px";
        }elseif (isset($_GET['tech-media'])) {
            $sector_id = 11;
            $height = "820px";
        }else{
            $sector_id = 0;
            $height = "";
        }

        global $cookie;
        $current_language = (int)$cookie->id_lang;

        $sql = 'SELECT * FROM  `'._DB_PREFIX_.'sector` WHERE id_sector = '.$sector_id.'';
        $sector = Db::getInstance()->getRow($sql);
               

		$this->context->smarty->assign('sector', $sector);
        $this->context->smarty->assign('height', $height);
        $this->setTemplate('sector.tpl');
			
    } 

}
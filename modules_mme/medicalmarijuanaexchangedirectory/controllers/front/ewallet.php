<?php
class medicalmarijuanaexchangedirectoryewalletModuleFrontController extends ModuleFrontController
{
	public function initContent()
    {
        parent::initContent();

        $this->hide_left_column = true;
        $this->display_header = true;
        $this->display_footer = true;
        $context = Context::getContext();
        $id_customer = Context::getContext()->customer->id;
        $this->context->smarty->assign('hide_left_column', $this->hide_left_column);

        $sql = 'SELECT * FROM '._DB_PREFIX_.'ewallet WHERE id_customer = '.$id_customer;
        $wallet_data = Db::getInstance()->getRow($sql);

        if (!$wallet_data) {
            $sql = 'SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer.' AND bussiness = 1 ';
            $customer_data = Db::getInstance()->getRow($sql);
            if ($customer_data) {
                $date = strtotime("+21 day");
                $payment_date = date('Y-m-d', $date);
                Db::getInstance()->insert('ewallet', array(
                                'id_customer'  => (int)$customer_data['id_customer'],
                                'total_amount' => '0',
                                'payment_date'     => $payment_date,
                            ));

                $sql = 'SELECT * FROM '._DB_PREFIX_.'ewallet WHERE id_customer = '.$id_customer;
                $wallet_data = Db::getInstance()->getRow($sql);
            }
        }

        $amount = number_format($wallet_data['total_amount'], 2, '.', ' ');

        $next_payment = $wallet_data['payment_date'];
        $next_payment = date("F d, Y", strtotime($next_payment));

        $this->context->smarty->assign('wallet_amount', $amount);
        $this->context->smarty->assign('next_payment', $next_payment);

        $sql = 'SELECT * FROM '._DB_PREFIX_.'phistory WHERE id_customer = '.$id_customer;
        $history_data = Db::getInstance()->ExecuteS($sql);

        $this->context->smarty->assign('history_data', $history_data);
        $this->setTemplate('ewallet.tpl');

    }

    public function update_amount(){

    }
}
<?php
class MedicalmarijuanaexchangedirectorySinglestoryModuleFrontController extends ModuleFrontController
{
	public function initContent()
    {
    	parent::initContent();
    	$id = (int)Tools::getValue('id', 0);
    	$stories_obj = new Sponsoredstories;
		$story = $stories_obj->getStory("WHERE used > 0 AND id_sponsoredstories=".(int)$id);

		if ($story) {
			Sponsoredstories::decrementCounter($id);
		} else {
			$this->redirect_after = '404';
            $this->redirect();
		}

		$this->context->smarty->assign('story', $story); 

		$stories = $stories_obj->getStories("WHERE status=1 AND used > 0 AND id_sponsoredstories != ".(int)$id);
		if (count($stories) >= 3) {
		    $rand_keys = array_rand($stories, 3);
		}elseif (count($stories) == 2){
		    $rand_keys = array(0,1);
		}elseif (count($stories) == 1){
		    $rand_keys = array(0);
		}
		$this->context->smarty->assign('stories', $stories); 
		$this->context->smarty->assign('rand_keys', $rand_keys);

        $this->setTemplate('singlestory.tpl');

    }
}

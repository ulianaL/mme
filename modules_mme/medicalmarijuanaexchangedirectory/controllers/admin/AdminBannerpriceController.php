<?php 
class AdminBannerpriceController extends ModuleAdminController
{
	public function __construct()
	{

	    $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'configuration';

        parent::__construct();

        // List of CMS tabs
        $cms_tab = array(0 => array(
            'id' => 0,
            'name' => $this->l('None')
        ));
        foreach (CMS::listCms($this->context->language->id) as $cms_file) {
            $cms_tab[] = array('id' => $cms_file['id_cms'], 'name' => $cms_file['meta_title']);
        }

        $this->fields_options = array(
            'standart' => array(
                'title' =>    $this->l('Standart banner'),
                'icon' =>    'icon-cogs',
                'fields' =>    array(
                    'CPC_STANDART_BANNER' => array(
                        'title' => $this->l('CPC'),
                        'desc' => $this->l('cost per click'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    ),
                    'CPM_STANDART_BANNER' => array(
                        'title' => $this->l('CPM'),
                        'desc' => $this->l('cost per 1000 impressions'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
            'product' => array(
                'title' =>    $this->l('Product page banner'),
                'icon' =>    'icon-cogs',
                'fields' =>    array(
                    'CPC_PRODUCT_BANNER' => array(
                        'title' => $this->l('CPC'),
                        'desc' => $this->l('cost per click'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    ),
                    'CPM_PRODUCT_BANNER' => array(
                        'title' => $this->l('CPM'),
                        'desc' => $this->l('cost per 1000 impressions'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    ),

                    'MONTHLY_PRODUCT_BANNER' => array(
                        'title' => $this->l('Monthly'),
                        'desc' => $this->l('cost per 30 days'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    ),
                ),
                'submit' => array('title' => $this->l('Save')),
            ),
            'video' => array(
                'title' =>    $this->l('Video banner'),
                'icon' =>    'icon-cogs',
                'fields' =>    array(
                    'CPC_VIDEO_BANNER' => array(
                        'title' => $this->l('CPC'),
                        'desc' => $this->l('cost per click'),
                        'validation' => 'isUnsignedInt',
                        'cast' => 'intval',
                        'type' => 'text',
                        'class' => 'fixed-width-xxl'
                    ),
                    'CPM_VIDEO_BANNER' => array(
                        'title' => $this->l('CPM'),
                        'desc' => $this->l('cost per 1000 impressions'),
                        'validation' => 'isUnsignedInt',
                        'cast' => 'intval',
                        'type' => 'text',
                        'class' => 'fixed-width-xxl'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
            'sponsored_stories' => array(
                'title' =>    $this->l('Sponsored Stories'),
                'icon' =>    'icon-cogs',
                'fields' =>    array(
                    'CPC_SPONSORED_STORIES' => array(
                        'title' => $this->l('CPC'),
                        'desc' => $this->l('cost per click'),
                        'validation' => 'isUnsignedInt',
		                'cast' => 'intval',
		                'type' => 'text',
		                'class' => 'fixed-width-xxl'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
        );
    }
}
<div class="ailments_list">
	<h3>Ailments</h3>
	<div class="left_ailments">
		<ul>
			<li><a href="/blog/category/als_ailment" >ALS</a></li> 
			<li><a href="/blog/category/alzheimers_ailment" >ALZHEIMER'S</a></li>  
			<li><a href="/blog/category/anorexia-" >ANOREXIA -EATING DISORDER</a></li>  
			<li><a href="/blog/category/anti-inflammatory" >ANTI-INFLAMMATORY</a></li>   
			<li><a href="/blog/category/arthritis" >ARTHRITIS</a></li>   
			<li><a href="/blog/category/autism" >AUTISM</a></li>      
			<li><a href="/blog/category/bones--osteoporosis" >OSTEOPOROSIS</a></li>  
			<li><a href="/blog/category/cancer" >CANCER</a></li>
			<li><a href="/blog/category/depression" >DEPRESSION</a></li> 
			<li><a href="/blog/category/diabetes" >DIABETES</a></li>  
			<li><a href="/blog/category/epilepsy" >EPILEPSY</a></li> 
			<li><a href="/blog/category/fibromyalgia" >FIBROMYALGIA</a> </li>
			<li><a href="/blog/category/glaucoma-" >GLAUCOMA</a></li>  
			<li><a href="/blog/category/heart-disease-cardiovasular" >HEART DISEASE</a></li> 
			<li><a href="/blog/category/hiv-aids" >HIV (AIDS)</a></li> 
		</ul>
	</div>
	<div class="right_ailments">
		<ul>
			<li><a href="/blog/category/irritable-bowel-syndrome-ibs-" >IRRITABLE BOWEL SYNDROME</a></li> 
			<li><a href="/blog/category/kidneys" >KIDNEYS</a></li> 
			<li><a href="/blog/category/liver-" >LIVER</a></li>
			<li><a href="/blog/category/migraine-headache" >MIGRAINE HEADACHE</a></li> 
			<li><a href="/blog/category/multiple_sclerosis" >MULTIPLE SCLEROSIS</a></li>
			<li><a href="/blog/category/mental-health" >MENTAL HEALTH</a></li> 
			<li><a href="/blog/category/nausea" >NAUSEA</a></li> 
			<li><a href="/blog/category/neuropathic-pain-neuropathy" >NEUROPATHIC PAIN</a></li>
			<li><a href="/blog/category/obesity-and-the-endocannabinoid-system">OBESITY</a></li> 
			<li><a href="/blog/category/opiod-disorder" >OPIOID DISORDER</a></li> 
			<li><a href="/blog/category/pain" >PAIN</a></li>
			<li><a href="/blog/category/parkinsons-disease" >PARKINSON'S & HUNTINGTON’S DISEASE</a></li> 
			<li><a href="/blog/category/post-traumatic-stress-disorder-ptsd">PTSD</a></li>
			<li><a href="/blog/category/sleep-insomnia-" >SLEEP APNEA</a></li>
			<li><a href="/blog/category/vision-glaucoma" >VISION (Glaucoma)</a></li>
		</ul>
	</div>
</div>
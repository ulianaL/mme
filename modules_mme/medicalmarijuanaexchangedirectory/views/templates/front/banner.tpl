 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
 <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<img id="adv_preloader" style="position: fixed; top: 50%; left: 50%; display: none;" src="/themes/default-bootstrap/img/marijuana__preloader.gif" />
<div class="container">
	{if isset($shop_errors) && $shop_errors}
		<div class="alert alert-danger">
			{if is_array($shop_errors)}
				{foreach from=$shop_errors item="err"}
					<p>{$err}</p>
				{/foreach}
			{else}
				{$shop_errors}
			{/if}
		</div>
	{/if}
	<div class="add_adv_buttons">
		<a class="add_banner_top" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbanner')|escape:'html'}"> Add Banner</a>
		<span class="add_story_top">Add Sponsored Story</span>
	</div>
	<table class ="advmanager">
		<tr>
			<td colspan="4"><span class="table_title">Image Banner</span></td>
		</tr>
		<tr class="head">
			<td style="width: 5%;">#</td>
			<td style="width: 23%;">Name</td>
			<td style="width: 23%;">Status</td>
			<td style="width: 49%;">Banner Image</td>
		</tr>
		{foreach $banners item=ban}
		<tr class="toremove{$ban.id_banner}">
			<td class="id_num">{$ban.id_banner}</td>
			<td>{$ban.name}</td>
			{if $ban.status == 1}
				<td>Active</td>
			{else}
				<td>Pending review</td>
			{/if}
			<td class="ban_img">
				{if $ban.size == 4}
					<a class="fanx" href="{getBuildBannerUrl($ban.banner_img)}">
						<video>
							<source src="{getBuildBannerUrl($ban.banner_img)}">
						</video>
					</a>
				{else}
					<a class="fanx" href="{getBuildBannerUrl($ban.banner_img)}"><img src="{getBuildBannerUrl($ban.banner_img)}" alt=""/></a>
				{/if}
				<p class="remove_bannerx"  onclick="remove_banner({$ban.id_banner})">Remove<p>
			</td>
		</tr>
		<tr class="toremove{$ban.id_banner}">
			<td colspan="4">
				<div class="btocampaddtohideshow btocampaddtohideshow{$ban.id_banner}" onclick="openclosecampadd({$ban.id_banner})"><img class="plus_icon" src="/modules/medicalmarijuanaexchangedirectory/img/plus_icon.png"><span>Add Campaign</span></div>
				<span class="show_campain_btn">SHOW CAMPAIGNS ({$ban.name})</span></td>
		</tr>
		
		<tr class="compain_table">
			<td colspan="4">
				<table class ="advmanager">
					<tr class="camp_head">
						<td>Name</td>
						<td>Url</td>
						<td>CPC/CPM</td>
						{if $ban.size != 5 && $ban.size !=6}
						<td>State</td>
						<td>City</td>
						{else}
						<td>Category</td>
						{/if}
						<td>
							<span style="white-space: nowrap;">Avaliable / Total</span>
						</td>
						<td>
							<span style="white-space: nowrap;">Edit</span>
						</td>
						<td>
							<span style="white-space: nowrap;">Delete</span>
						</td>
					</tr>
					{foreach $ban.camp item=bann}
					<tr id='camp{$bann.id_camp}'>
						<td>{$bann.name}</td>
						<td>
							<a  
							data-content="{if strpos($bann.url,'http') !== false}{$bann.url}{else}http://{$bann.url}{/if}" 
							target="_blank" 
							data-trigger="hover"
							data-toggle="popover"
							class=""
							href="{if strpos($bann.url,'http') !== false}{$bann.url}{else}http://{$bann.url}{/if}" 
							style="padding: 12px 8px 7px 10px;">
								<i style="font-size: 22px;color: black;" class="fa fa-link" aria-hidden="true"></i>
							</a>
						</td>
						
						
						<td >
						{if $bann.type ==1}<span style="padding: 9px 5px;cursor: pointer;" data-content="Cost Per Click" data-trigger="hover" data-toggle="popover">(CPC)</span>{/if}
						{if $bann.type ==2}<span style="padding: 9px 5px;cursor: pointer;" data-content="Cost per mille" data-trigger="hover" data-toggle="popover">(CPM)</span>{/if}
						{if $bann.type ==3}<span style="padding: 9px 5px;cursor: pointer;" data-content="Monthly Subscription" data-trigger="hover" data-toggle="popover">Monthly Subscription</span>{/if}
						</td>
						
						{if $ban.size != 5 && $ban.size !=6}
						<td>{$bann.state_code}</td>
						<td>{$bann.city}</td>
						<td>
							{$bann.used} / {$bann.avaliable}
							{if $bann.used == 0}
								<a href="/edit-campaign?campaign={$bann.id_camp}" class="add_new_clicks" target="_blank">Add More</a>
							{/if}
						</td>
						{else}
						
						<td>{$bann.cat_name}</td>
							{if $bann.type ==3}
								<td {if strtotime($bann.date_end) < $smarty.now}class="color_red"{/if}>to {date('j/n/Y', strtotime($bann.date_end))}</td>
							
							{else}
								<td>
									{$bann.used} / {$bann.avaliable}
									{if $bann.used == 0}
										<a href="/edit-campaign?campaign={$bann.id_camp}" class="add_new_clicks" target="_blank">Add More</a>
									{/if}
								</td>	
							{/if}				
						{/if}
						<td><a href="/edit-campaign?campaign={$bann.id_camp}" target="_blank"><img src="/modules/medicalmarijuanaexchangedirectory/img/edit.png"></a></td>
						<td class="text-center"><span class="remove_story" onclick="remove_campaig({$bann.id_camp}, '{$bann.name}')" href="#"><i class="icon-remove-sign"></i></span></td>
					</tr>
					{/foreach}
					<tr class="stats_row">
						<td>{foreach $ban.camp item=bann}
								<div class="campname_list stats_list{$bann.id_camp}" onclick="getstats({$bann.id_camp}, {$bann.id_banner}, {if $bann.type ==1}'Clicks'{else}'Impressions'{/if} )">{$bann.name}</div>
							{/foreach}</td>
						<td colspan="7">
							<div class="graf">
								<div id="stats{$bann.id_banner}" style="height: 200px; width: 600px"></div>
								<div class="clicks"><span class="green_dot"></span> <div class="stats_payment_type"></div></div>
							</div>
							<div class="stats_info">
								<div><span class="clicks_this_period{$bann.id_banner}"></span><div class="stats_payment_type"></div> this period</div>
								<div><span class="visits_today{$bann.id_banner}"></span>Visits Today</div>
								<div><span class="unique_visits{$bann.id_banner}"></span>Unique Visits</div>
							</div>
							<span class="back_btn_graf">Back</span>
						</td>
					</tr>
					<tr class="camp_bottom">
						<td colspan="8">
							<span class="back_btn">Back</span>
							<span class="show_stats">Show Stats <img class="stats_icon" src="/modules/medicalmarijuanaexchangedirectory/img/stats_icon.png"></span>
							
						</td>
					</tr>
					
				</table>
			</td>
		</tr>
		<tr class="showdd showdd{$ban.id_banner}" style="display:none;">
			<td colspan="6">
				<div>
						<h3 class="add_camp_title">Add Campaign:</h3>
						<span class="close_add">&times</span>
						<div class="form-group">
							<label>Campaign name:</label>
							<input type="text" name="name{$ban.id_banner}" class="form-control namex{$ban.id_banner}">
						</div>
						<div class="form-group">
							<label>Link:</label>
							<input type="url" name="urlx{$ban.id_banner}" class="form-control urlx{$ban.id_banner}">
						</div>
						
						<div class="form-group">
							<label>Type of Campaign:</label>
							<select  style="width: 200px;" type="text" name="typex{$ban.id_banner}" class="form-control typex{$ban.id_banner}">
							<option value="1">Cost Per Click (CPC)</option>
							<option value="2">Cost per thousand impressions(CPM)</option>
							{if $ban.size == 5 || $ban.size == 6}<option value="3">Monthly subscription</option>{/if}
							</select>
						</div>
						
						{if $ban.size != 5 && $ban.size != 6}
						<div  class="keywordtabr  hideshowtypetree{$ban.id_banner}" >
							<div class="form-group">
								<label>Enable Keywords</label>
								<input  class="form-control" id="keystatus{$ban.id_banner}"  type="checkbox" name="my-checkbox">
							</div>
							
						
							
							
							<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
							<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>																									
							<div class="form-group">
								<label>Keywords</label>
								<input type="text"  id="key{$ban.id_banner}" name="keywords" class="form-control" value="" data-role="tagsinput"/> 
							</div>
						</div>
						{/if}
						<style>
						.bootstrap-tagsinput {
								min-width: 100%;
							}
							
							.label-info {
								background-color: #5bbe8f;
								border: 1px solid #367256;
							}
						</style>
						
						{if $ban.size != 5 && $ban.size != 6}
						<div class="form-group"  style="    padding: 14px;background: #fcfcfc;border: 1px solid #cccccc;">
							<div class="checkbox">
							  <label><input class="toggleclasstree{$ban.id_banner}" onchange="toggletypetree({$ban.id_banner});" type="checkbox" name="rotb{$ban.id_banner}">Rotating banner (All country)</label>
							</div>
						</div>

						<div class="form-group"  style="padding: 14px;background: #fcfcfc;border: 1px solid #cccccc;">
							<div class="checkbox">
							  <label><input class="only_finance{$ban.id_banner}" type="checkbox" name="only_finance{$ban.id_banner}">Show on finance page only</label>
							</div>
						</div>
						{/if}

					
						{if $ban.size != 5 && $ban.size != 6}
					
						<div class="form-group" style="max-width:500px;" >
							  <label>Country:</label>
							  <select style="width: 200px;" name="country{$ban.id_banner}" ccccid="{$ban.id_banner}" class="form-control countryselect countryselect{$ban.id_banner}">
								  <option value="1">USA</option>
								  <option value="2">CANADA</option>
							  </select>
						</div>
					
						<div class="form-group hideshowtypetree{$ban.id_banner}" style="max-width:500px;" >
							  <label>State:</label>
							  <select style="width: 200px;" name="statex{$ban.id_banner}" class="form-control  stateselectxx  stateselectx{$ban.id_banner}">
								  {foreach $states item=state}
								  <option value="{$state.state_code}" country="{$state.country}" >{$state.state}</option>
								  {/foreach}
							  </select>
						 </div>
						  
						<div class="spinnerloadcity"  style="opacity: 0;">
						<i class="fa fa-spinner fa-spin " aria-hidden="true"></i>
						</div>
						<div class="form-group loadcity hideshowtypetree{$ban.id_banner}" style="max-width:500px;">
							<label>City:</label>
							<select style="width: 200px;" name="cityx{$ban.id_banner}" class="form-control cityselectx{$ban.id_banner}">
								  {foreach $cities item=city}
								  <option value="{$city.city_id}">{$city.city}</option>
								  {/foreach}
							 </select>
						</div>
						{/if}
						{if $ban.size == 5 || $ban.size == 6}
						<div class="banner_cat{$ban.id_banner}">
							<label>Category:</label>
							{$categories_tree_html}
						</div>
						{else}
						<div style = "display: none;" class="banner_cat{$ban.id_banner}">
							<input type="radio" name="categoryBox" value="" checked="checked">
						</div>
						{/if}

						{if $ban.size == 5 || $ban.size == 6}
							<div class="form-group">
								<label>Quantity<span id="qty-tix-{$ban.id_banner}"></span>:</label>
								<small id="qty-tix-{$ban.id_banner}-help">1 click = {$cpc_product} points</small>
								<input  id="quantityx{$ban.id_banner}" type="number" step="1" value="" min="0" name="quantityx{$ban.id_banner}" class="form-control">
							</div>
						{/if}
						{if $ban.size != 5 && $ban.size != 6}
							<div class="form-group">
								<label>Quantity<span id="qty-tix-{$ban.id_banner}"></span>:</label>
								<small id="qty-tix-{$ban.id_banner}-help">1 click = {$cpc_standart} points</small>
								<input  id="quantityx{$ban.id_banner}" type="number" step="1" value="" min="0" name="quantityx{$ban.id_banner}" class="form-control">
							</div>
						{/if}
					  <div class="pthererrrors"></div>
					 <p style="display:none;" class="alert alert-danger dangernopoints dangernopoints{$ban.id_banner}">Points Avaliable: {$points}</p>
					 <p class="alert alert-success" style="display:none;" id="spancountx{$ban.id_banner}"></p>

						
						
						
	<!-- 													<div class="camp1" style="text-align:center;margin-top:15px"></div>
	-->													
						<p  onclick="addcamp({$ban.id_banner}, {$ban.size})" style="margin: 15px auto;cursor: pointer;background: rgb(91, 190, 143);text-align: center;padding: 5px;color: white;">SUBMIT</p>
				</div>	
			</td>
		</tr>
		<tr>
			
		</tr>
		{/foreach}
		<tr>
			<td colspan="4"> <a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbanner')|escape:'html'}" class="add_banner">Add Banner</a></td>
		</tr>
	</table>

	<table class="advmanager video_banners">
		<tr>
			<td colspan="4"><span class="table_title">Video Banner</span></td>
		</tr>
		<tr class="head">
			<td style="width: 5%;">#</td>
			<td style="width: 23%;">Name</td>
			<td style="width: 23%;">Status</td>
			<td style="width: 49%;">Banner Video</td>
		</tr>
		{foreach $video_banners item=ban}
		<tr class="toremove{$ban.id_banner}">
			<td class="id_num">{$ban.id_banner}</td>
			<td>{$ban.name}</td>
			{if $ban.status == 1}
				<td>Active</td>
			{else}
				<td>Pending review</td>
			{/if}
			<td class="ban_img">
				{if $ban.size == 4}
					<a class="fanx" href="{getBuildBannerUrl($ban.banner_img)}">
						<video>
							<source src="{getBuildBannerUrl($ban.banner_img)}">
						</video>
					</a>
				{else}
					<a class="fanx" href="{getBuildBannerUrl($ban.banner_img)}"><img src="{getBuildBannerUrl($ban.banner_img)}" alt=""/></a>
				{/if}
				<p class="remove_bannerx"  onclick="remove_banner({$ban.id_banner})">Remove<p>
			</td>
		</tr>
		<tr class="toremove{$ban.id_banner}">
			<td colspan="4">
				<div class="btocampaddtohideshow btocampaddtohideshow{$ban.id_banner}" onclick="openclosecampadd({$ban.id_banner})"><img class="plus_icon" src="/modules/medicalmarijuanaexchangedirectory/img/plus_icon.png"><span>Add Campaign</span></div>
				<span class="show_campain_btn">SHOW CAMPAIGNS ({$ban.name})</span></td>
		</tr>
		<tr class="compain_table">
			<td colspan="4">
				<table class ="advmanager">
					<tr class="camp_head">
						<td>Name</td>
						<td>Url</td>
						<td>CPC/CPM</td>
						<td>State</td>
						<td>City</td>
						<td>
							<span style="white-space: nowrap;">Avaliable / Total</span>
						</td>
						<td>
							<span style="white-space: nowrap;">Edit</span>
						</td>
						<td>
							<span style="white-space: nowrap;">Delete</span>
						</td>
					</tr>
					{foreach $ban.camp item=bann}
					<tr id='camp{$bann.id_camp}'>
						<td>{$bann.name}</td>
						<td>
							<a  
							data-content="{if strpos($bann.url,'http') !== false}{$bann.url}{else}http://{$bann.url}{/if}" 
							target="_blank" 
							data-trigger="hover"
							data-toggle="popover"
							class=""
							href="{if strpos($bann.url,'http') !== false}{$bann.url}{else}http://{$bann.url}{/if}" 
							style="padding: 12px 8px 7px 10px;">
								<i style="font-size: 22px;color: black;" class="fa fa-link" aria-hidden="true"></i>
							</a>
						</td>
						
						
						<td >
						{if $bann.type ==1}<span style="padding: 9px 5px;cursor: pointer;" data-content="Cost Per Click" data-trigger="hover" data-toggle="popover">(CPC)</span>{/if}
						{if $bann.type ==2}<span style="padding: 9px 5px;cursor: pointer;" data-content="Cost per mille" data-trigger="hover" data-toggle="popover">(CPM)</span>{/if}
						</td>
						
						
						<td>{$bann.state_code}</td>
						<td>{$bann.city}</td>
						<td>
							{$bann.used} / {$bann.avaliable}
							{if $bann.used == 0}
								<a href="/edit-campaign?campaign={$bann.id_camp}" class="add_new_clicks" target="_blank">Add More</a>
							{/if}
						</td>
						<td><a href="/edit-campaign?campaign={$bann.id_camp}" target="_blank"><img src="/modules/medicalmarijuanaexchangedirectory/img/edit.png"></a></td>
						<td class="text-center"><span class="remove_story" onclick="remove_campaig({$bann.id_camp}, '{$bann.name}')" href="#"><i class="icon-remove-sign"></i></span></td>
					</tr>
					{/foreach}
					<tr class="stats_row">
						<td>{foreach $ban.camp item=bann}
								<div class="campname_list stats_list{$bann.id_camp}" onclick="getstats({$bann.id_camp}, {$bann.id_banner}, {if $bann.type ==1}'Clicks'{else}'Impressions'{/if} )">{$bann.name}</div>
							{/foreach}</td>
						<td colspan="7">
							<div class="graf">
								<div id="stats{$bann.id_banner}" style="height: 200px; width: 600px"></div>
								<div class="clicks"><span class="green_dot"></span> <div class="stats_payment_type"></div></div>
							</div>
							<div class="stats_info">
								<div><span class="clicks_this_period{$bann.id_banner}"></span><div class="stats_payment_type"></div> this period</div>
								<div><span class="visits_today{$bann.id_banner}"></span>Visits Today</div>
								<div><span class="unique_visits{$bann.id_banner}"></span>Unique Visits</div>
							</div>
							<span class="back_btn_graf">Back</span>
						</td>
					</tr>
					<tr class="camp_bottom">
						<td colspan="9">
							<span class="back_btn">Back</span>
							<span class="show_stats">Show Stats <img class="stats_icon" src="/modules/medicalmarijuanaexchangedirectory/img/stats_icon.png"></span>
							
						</td>
					</tr>
					
				</table>
			</td>
		</tr>
		<tr class="showdd showdd{$ban.id_banner}" style="display:none;">
			<td colspan="6" >
				<div>
						<h3>Add Campaign:</h3>
						
						<div class="form-group">
							<label>Campaign name:</label>
							<input type="text" name="name{$ban.id_banner}" class="form-control namex{$ban.id_banner}">
						</div>
						<div class="form-group">
							<label>Link:</label>
							<input type="url" name="urlx{$ban.id_banner}" class="form-control urlx{$ban.id_banner}">
						</div>
						
						
						
						<div class="form-group">
							<label>Type of Campaign:</label>
							<select  style="width: 200px;" type="text" name="typex{$ban.id_banner}" class="form-control typex{$ban.id_banner}">
							<option value="1">Cost Per Click (CPC)</option>
							<option value="2">Cost per thousand impressions(CPM)</option>

							</select>
						</div>
						
						
						<div  class="keywordtabr  hideshowtypetree{$ban.id_banner}" >
							<div class="form-group">
								<label>Enable Keywords</label>
								<input  class="form-control" id="keystatus{$ban.id_banner}"  type="checkbox" name="my-checkbox">
							</div>
							
						
							
							
							<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
							<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>																									
							<div class="form-group">
								<label>Keywords</label>
								<input type="text"  id="key{$ban.id_banner}" name="keywords" class="form-control" value="" data-role="tagsinput"/> 
							</div>
						</div>
						
						<style>
						.bootstrap-tagsinput {
								min-width: 100%;
							}
							
							.label-info {
								background-color: #5bbe8f;
								border: 1px solid #367256;
							}
						</style>
						
						
						
						<div class="form-group"  style="    padding: 14px;background: #fcfcfc;border: 1px solid #cccccc;">
							<div class="checkbox">
							  <label><input class="toggleclasstree{$ban.id_banner}" onchange="toggletypetree({$ban.id_banner});" type="checkbox" name="rotb{$ban.id_banner}">Rotating banner (All country)</label>
							</div>
						</div>

						<div class="form-group"  style="padding: 14px;background: #fcfcfc;border: 1px solid #cccccc;">
							<div class="checkbox">
							  <label><input class="only_finance{$ban.id_banner}" type="checkbox" name="only_finance{$ban.id_banner}">Show on finance page only</label>
							</div>
						</div>
		
						
						<div class="form-group" style="max-width:500px;" >
							  <label>Country:</label>
							  <select style="width: 200px;" name="country{$ban.id_banner}" ccccid="{$ban.id_banner}" class="form-control  countryselect countryselect{$ban.id_banner}">
								  <option value="1">USA</option>
								  <option value="2">CANADA</option>
							  </select>
						</div>
					
						<div class="form-group hideshowtypetree{$ban.id_banner}" style="max-width:500px;" >
							  <label>State:</label>
							  <select  style="width: 200px;" name="statex{$ban.id_banner}" class="form-control  stateselectxx  stateselectx{$ban.id_banner}">
								  {foreach $states item=state}
								  <option value="{$state.state_code}" country="{$state.country}" >{$state.state}</option>
								  {/foreach}
							  </select>
						 </div>
						  
						<div class="spinnerloadcity"  style="opacity: 0;">
						<i class="fa fa-spinner fa-spin " aria-hidden="true"></i>
						</div>
						<div class="form-group loadcity hideshowtypetree{$ban.id_banner}" style="max-width:500px;">
							<label>City:</label>
							<select style="width: 200px;" name="cityx{$ban.id_banner}" class="form-control cityselectx{$ban.id_banner}">
								  {foreach $cities item=city}
								  <option value="{$city.city_id}">{$city.city}</option>
								  {/foreach}
							 </select>
						</div>

						<div class="form-group">
							<label>Quantity <span id="qty-tix-{$ban.id_banner}">clicks</span>:</label>
							<small id="qty-tix-{$ban.id_banner}-help">1 click = {$cpc_video} points</small>
							<input  id="quantityx{$ban.id_banner}" type="number" step="1" value="" min="0" name="quantityx{$ban.id_banner}" class="form-control">
						</div>

					  <div class="pthererrrors"></div>
					 <p style="display:none;" class="alert alert-danger dangernopoints dangernopoints{$ban.id_banner}">Points Avaliable: {$points}</p>
					 <p class="alert alert-success" style="display:none;" id="spancountx{$ban.id_banner}"></p>

						
						
						
	<!-- 													<div class="camp1" style="text-align:center;margin-top:15px"></div>
	-->													
						<p  onclick="addcamp({$ban.id_banner}, {$ban.size})" style="margin: 15px auto;cursor: pointer;background: rgb(91, 190, 143);text-align: center;padding: 5px;color: white;">SUBMIT</p>
				</div>	
			</td>
		</tr>
		{/foreach}
		<tr>
			<td colspan="4"> <a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addbanner')|escape:'html'}" class="add_banner">Add Banner</a></td>
		</tr>
	</table>

	<table class ="advmanager video_banners">
		<tr>
			<td colspan="7"><span class="table_title">Sponsored Stories</span></td>
		</tr>
		<tr class="head">
			<td style="width: 5%;">#</td>
			<td style="width: 20%;">Title</td>
			<td style="width: 10%;">
				<span style="white-space: nowrap;">Available / Total</span>
			</td>
			<td style="width: 25%;">Description</td>
			<td style="width: 30%;">Photo</td>
			<td style="width: 5%;">Edit</td>
			<td style="width: 5%;">Delete</td>
		</tr>
		
		{foreach $stories item=story}
		<tr id="story{$story.id_sponsoredstories}">
			<td>{$story.id_sponsoredstories}</td>
			<td>{$story.title}</td>
			<td class="text-center">
				<span style="white-space: nowrap;">{$story.used|intval} / {$story.avaliable}</span>
				{if $story.used == 0}
				<a href="/edit-story?story={$story.id_sponsoredstories}" class="add_new_clicks" target="_blank">Add More</a>
				{/if}
			</td>
			<td>{$story.description}</td>
			<td>
				<img src="/modules/medicalmarijuanaexchangedirectory/storyimg/{$story.image}">
			</td>
			<td><a href="/edit-story?story={$story.id_sponsoredstories}" target="_blank"><img src="/modules/medicalmarijuanaexchangedirectory/img/edit.png"></a></td>
			<td class="text-center"><span class="remove_story" onclick="remove_story({$story.id_sponsoredstories})" href="#"><i class="icon-remove-sign"></i></span></td>
		</tr>
		{/foreach}
		<tr>
			<td colspan="7">
				<span class="add_story">Add a sponsored story</span>
				<div class="add_story_section">
					<div class="add_story_title">Add a Sponsored Story</div>
					<form class="add_story_form" action="#" method="post" enctype="multipart/form-data">
						<label>Title</label>
						<input required="required" type="text" id="story_title" name="story_title" autocomplete="off">
						<label>Clicks (one click cost {$cpc_stories} points)</label>
						<input required="required" type="number" id="story_clicks" name="story_clicks" step="1" min="0" autocomplete="off">
						<div id="story_points" class="alert alert-success"><span>0</span> Points</div>
						<label>Upload a picture (only 900x500px size)</label>
						<div class="story_photo_block">
							<span class="filename"><img src="/modules/medicalmarijuanaexchangedirectory/img/add_photo_img.png"></span>
							<div class="upload_photo_btn">Upload photo</div>
						</div>
						
						<input style="display: none;" type="file" name="fileToUpload" id="fileToUpload">
						<label>Description</label>
						<textarea id="add_description" name="add_description"></textarea>
						<div class="form_buttons">
							<input class="ss_add" type="submit" name="addsponsoredstories" value="Add a sponsored story">
							<span class="ss_cansel">Cancel</span>
						</div>
					</form>
				</div>
			</td>
		</tr>
	</table>
	
</div>
<style>
.forcehide{
	display:none!important;
}
.add_story_section{
	display: none;
}
</style>
<script>
	$(function(){
		$(".categories-tree-html").tree('collapseAll');
		$(".categories-tree-html").find(":input").each(
			function(){
				if (this.value == '1438') {
					$(this).closest('ul.tree').find('label.tree-toggler').first().trigger('click');
				}
			});
	});
	
	var id = '';
	function remove_story(id){
		var remove = confirm('Are you sure you want to remove this story?');
		if (remove) {
			$.ajax({
			   type: "POST",
			   data: {
			   	remove_story: id
			   },
			   dataType: 'json',
			   success: function(msg){
			   	$('#story'+id).remove();
			   	alert('Story Removed!');
			   }
			});
		}else{
			return false;
		}


	}

	function remove_campaig(id, name){
		var remove = confirm('Are you sure you want to remove campaign "'+name+'"?');
		if (remove) {
			$.ajax({
			   type: "POST",
			   data: {
			   	remove_campaig: id
			   },
			   dataType: 'json',
			   success: function(msg){
			   	$('#camp'+id).remove();
			   	alert('Campaign Removed!');
			   }
			});
		}else{
			return false;
		}


	}

var cpc_stories = {$cpc_stories|intval};

$("#story_clicks").on('input',function(){
	var clicks = $('#story_clicks').val();
	var points = clicks * cpc_stories;
	$('#story_points span').text(points);
    $('#story_points').show();
});
$(".add_story_form").submit( function( e ) {
    var form = this;
    e.preventDefault(); 
    var fileInput = $(this).find("input[type=file]")[0],
    file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            if( width == 900 && height == 500 ) {
                form.submit();
            }
            else {
                alert('Error on picture size! Should be 900x500px. Your picture size '+width+'x'+height)
            }
        };
    }
    else { //No file was input or browser doesn't support client side reading
        form.submit();
    }

});

$('.show_stats').click(function(){
  $(this).closest('.compain_table').find('.stats_row td div').first().click();
  $(this).closest('.compain_table').find('.stats_row').show();
  $(this).closest('.camp_bottom').hide();

});
$('.back_btn_graf').click(function(){
  $(this).closest('.stats_row').hide();
  $(this).closest('.stats_row').next('.camp_bottom').show();
});
//campaign
function getstats(id, banner_id, type){		
	$('.campname_list').removeClass('stats_active');
	$('.stats_list'+id).addClass('stats_active');
	$('.stats_payment_type').text(type);
	$.ajax({
	   type: "POST",
	   data: {
	   	camp_stats_id: id
	   },
	   dataType: 'json',
	   success: function(msg){
	   	if (msg.reviews == '') {
	   		$('#stats'+banner_id).html("<h3>There are no statistics for this campaign</h3>");
	   		$('.clicks_this_period'+banner_id).text("0");
	   		$('.visits_today'+banner_id).text("0");
	   		$('.unique_visits'+banner_id).text("0");
	   	}else{

	   		$('.clicks_this_period'+banner_id).text(msg.total);
	   		$('.visits_today'+banner_id).text(msg.today_visits);
	   		$('.unique_visits'+banner_id).text(msg.unique_visits);
	   		$('#stats'+banner_id).empty();
			new Morris.Area({
			  // ID of the element in which to draw the chart.
			  element: 'stats'+banner_id,
			  // Chart data records -- each entry in this array corresponds to a point on
			  // the chart.
			  data: msg.reviews,
			  // The name of the data record attribute that contains x-values.
			  xkey: 'day',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: ['value'],

			  parseTime: false,
			  lineColors:['#71A10B'],
			  fillOpacity: 0.26,
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: ['Value']
			});
	   	}
	     
	   }
	});
	
}
	
$('.ss_cansel').click(function(){
  $('.add_story').show();
  $('.add_story_section').hide();
});

$('.add_story_top').click(function(){
  $('.add_story').hide();
  $('.add_story_section').show();
  $('html, body').animate({
      scrollTop: $('.add_story_section').offset().top
    }, 500);
});

$('.add_story').click(function(){
  $('.add_story').hide();
  $('.add_story_section').show();
});
$('.upload_photo_btn').click(function(){
  $('#fileToUpload').click();
});
$("#fileToUpload").change(function(){
  $(".filename").text(this.files[0].name);
});

$('.show_campain_btn').click(function() {
	$('.showdd').hide();
	$(this).closest( "tr" ).next(".compain_table").toggle();
});

$('.back_btn').click(function() {
	$(this).closest( ".compain_table" ).hide();
});
///toogle to type 3
function toggletypetree(value){
	
	

	
	
	
	//hideshowtypetree
	if(  $('.toggleclasstree'+value).is(':checked')  ){
		$('.hideshowtypetree'+value).hide();
		$('.hideshowtypetree'+value).addClass('forcehide');
	}else{
		$('.hideshowtypetree'+value).show();
		$('.hideshowtypetree'+value).removeClass('forcehide');
	}
	
}




//us or canada  change state name ....
$('.countryselect').change(function ttttttt(){
    
	//alert( $(this).attr('ccccid')  );
	
	ccccid = $(this).attr('ccccid');
	
	
	if ($(this).val() == 1 ){
		$(".stateselectx"+ccccid+" option[country=CA]").hide();
		$(".stateselectx"+ccccid+" option[country=US]").show();
		$(".stateselectx"+ccccid).val($(".stateselectx"+ccccid+" option[country=US]:first").val());
	}else{
		$(".stateselectx"+ccccid+" option[country=US]").hide();
		$(".stateselectx"+ccccid+" option[country=CA]").show();
		$(".stateselectx"+ccccid).val($(".stateselectx"+ccccid+" option[country=CA]:first").val());
	}
	
	     $('.cityselectx'+ccccid).empty();
		 $('.loadcity').fadeTo( "slow" , 0.5);
		 $('.spinnerloadcity').fadeTo( "slow" , 1);
		 
		 whatstate = $(".stateselectx"+ccccid).val();
		 $.ajax({
				type: "POST",
				url: '#',
				data:'getcitiesfromstate='+whatstate+'&activecity=0', // serializes the form's elements.
				success: function(data)
					{
						//alert(data); // show response from the php script.
						$('.cityselectx'+ccccid).empty();
						$('.cityselectx'+ccccid).append(data);
						$.uniform.update();
						$('.loadcity').fadeTo( "slow" , 1);
						$('.spinnerloadcity').fadeTo( "slow" , 0);
						changetexrtspanonchangeinputs(ccccid)
					}
			   });

	$.uniform.update();
})










function remove_banner(value){


if (confirm('Please note that you will remove the banner and all associated campaigns.You are sure?')) {
		
	
		var url = "#"; // the script where you handle the form input.

		$.ajax({
			   type: "POST",
			   url: url,
			   data: "remove_banner&idtoremove="+value, // serializes the form's elements.
			   success: function(data)
			   {
				   if(data == 'deleteok'+value){
				 //  alert('ooooooooooooooooo');
				   $('.toremove'+value).hide('slow');
				   }else{
				   alert(data); // show response from the php script.
				   }
			   },
			   error: function (xhr, ajaxOptions, thrownError) {
					   console.log(xhr.status);
					   console.log(xhr.responseText);
					   console.log(thrownError);
			   }
			 });

		return false; // avoid to execute the actual submit of the form.




} else {
		//alert('nothung');
}

}


$(document).ready(function(){
	 //popover
	 $('[data-toggle="popover"]').popover();
	 //fancybox
	 $(".fanx").fancybox();
});

//on change inputs
{foreach $banners item=ban}

$('.typex{$ban.id_banner}').on('change', function() {
  if (this.value == 1) {
  	$('#quantityx{$ban.id_banner}').val('1');
	$('#quantityx{$ban.id_banner}').attr("step","1");
  }else if (this.value == 2){
  	 $('#quantityx{$ban.id_banner}').val('1000');
	 $('#quantityx{$ban.id_banner}').attr("step","1000");
  }
});

$('#quantityx{$ban.id_banner}, .stateselectx{$ban.id_banner}, .cityselectx{$ban.id_banner}, .typex{$ban.id_banner}').bind("propertychange change click keyup input paste", function(event){
	changetexrtspanonchangeinputs({$ban.id_banner}, {$ban.size});
});

{/foreach} //

{foreach $video_banners item=ban}

$('.typex{$ban.id_banner}').on('change', function() {
  if (this.value == 1) {
  	$('#quantityx{$ban.id_banner}').val('1');
	$('#quantityx{$ban.id_banner}').attr("step","1");
  }else if (this.value == 2){
  	 $('#quantityx{$ban.id_banner}').val('1000');
	 $('#quantityx{$ban.id_banner}').attr("step","1000");
  }
});

$('#quantityx{$ban.id_banner}, .stateselectx{$ban.id_banner}, .cityselectx{$ban.id_banner}, .typex{$ban.id_banner}').bind("propertychange change click keyup input paste", function(event){
	changetexrtspanonchangeinputs({$ban.id_banner}, {$ban.size});
});

{/foreach}	//




function changetexrtspanonchangeinputs(id, size = ''){
	
	var cpc_standart = {$cpc_standart|intval};
	var cpm_standart = {$cpm_standart|intval};

	var cpc_video = {$cpc_video|intval};
	var cpm_video = {$cpm_video|intval}

	var cpc_product = {$cpc_product|intval};
	var cpm_product = {$cpm_product|intval};

	var monthly_banner = {$monthly_banner|intval};

	if ($('#quantityx'+id).val() > 0 || $('.typex'+id).val() == 3){
	$('#spancountx'+id).show('slow');
	}else{
	$('#spancountx'+id).hide('slow');
	}
	
	//text for count...
		//CPM
	var qunatccxx =  + $('#quantityx'+id).val();
		if (size != 5 && size != 6 && size != 4) {
			total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * cpc_standart).toFixed(2) : (qunatccxx / (1000/cpm_standart)).toFixed(2));
			
		}else if(size == 4){
			total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * cpc_video).toFixed(2) : (qunatccxx / (1000/cpm_video)).toFixed(2));
		}else{
			//total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * 4).toFixed(2) : (qunatccxx / 250).toFixed(2));
			if ($('.typex'+id).val() == 1) {
				total_points = (qunatccxx * cpc_product).toFixed(2);
			}else if($('.typex'+id).val() == 2){
				total_points = (qunatccxx / (1000/cpm_product)).toFixed(2);
			}else{
				total_points = monthly_banner;
			}
		}
		// finalqunatccxx =  qunatccxx*500;
		//CPC
		// qunatccxxcpc = $('#quantityx'+id).val() 
		// finalqunatccxxcpc =  qunatccxx*0.5;
	
	if($('.typex'+id).val() == 1){
		$("#qty-tix-" + id).text('');
		if (size != 5 && size != 6 && size != 4) {
			$("#qty-tix-" + id + '-help').text('1 click = '+cpc_standart+' points');
		}else if(size == 4){
	    	$("#qty-tix-" + id + '-help').text('1 click = '+cpc_video+' points');
	    }else{
			$("#qty-tix-" + id + '-help').text('1 click = '+cpc_product+' points');
		}
		typeccc = "You will have "+ qunatccxx +" Clicks to your link.";

	} else if ( $('.typex'+id).val() == 2){
	    typeccc = "You will have "+  qunatccxx  +" Views in your Banner.";
	    $("#qty-tix-" + id).text(' impressions');

	    if (size != 5 && size != 6 && size != 4) {
	    	$("#qty-tix-" + id + '-help').text('1000 impressions = '+cpm_standart+' points');
	    }else if(size == 4){
	    	$("#qty-tix-" + id + '-help').text('1000 impressions = '+cpm_video+' points');
	    }else{
	    	$("#qty-tix-" + id + '-help').text('1000 impressions = '+cpm_product+' points');
	    }

	   

	}else{
		total_points = monthly_banner;
		typeccc = "The banner will be display  30 days";
	    $("#qty-tix-" + id).hide();
	    $("#quantityx" + id).hide();
	    $('#quantityx'+id).val(1);
	    $("#qty-tix-" + id + '-help').text('1 month = '+monthly_banner+' points');
	    	
	
	}
	
	if (size != 5 && size != 6) {
		$('#spancountx'+id).text('\n - Your banner will be visible in '
			+ $('.stateselectx'+id+'  option:selected').text()
			+ ' / '
			+ $('.cityselectx'+id+'  option:selected').text()
			+ '\n - '+typeccc
			+ '\n - Type of Campaign:'
			+ $('.typex'+id+'  option:selected').text()
			+ '\n  - You will use: '
			+ total_points + ' Points. '
		);
	}else{
		$('#spancountx'+id).text('\n '
		+ '\n - '+typeccc
		+ '\n - Type of Campaign:'
		+ $('.typex'+id+'  option:selected').text()
		+ '\n  - You will use: '
		+ total_points + ' Points. '
	);
	}
	

	$('#spancountx'+id).html($('#spancountx'+id).html().replace(/\n/g,'<br/>'));

	$('.dangernopoints').hide();
	if({$points} < total_points){
		$('.dangernopoints'+id).show('slow');
		$('.dangernopoints'+id).text('You only have {$points} points.')
	}

}



//add campain 
function addcamp(id, size = ''){ 

	var cpc_standart = {$cpc_standart|intval};
	var cpm_standart = {$cpm_standart|intval};

	var cpc_video = {$cpc_video|intval};
	var cpm_video = {$cpm_video|intval};

	var cpc_product = {$cpc_product|intval};
	var cpm_product = {$cpm_product|intval};

	var monthly_banner = {$monthly_banner|intval};
	
	errors = 0;
	errorstxt = '<br></br>';
	
	if($('.namex'+id).val() == ''){
	 errors++;
	 errorstxt += 'Name field cannot be empty.<br>';
	}
	
	if($('.urlx'+id).val() == ''){
	 errors++;
	 errorstxt += 'Link field cannot be empty.<br>';
	}
	
	if ($('.typex'+id).val() != 3) {
		if($('#quantityx'+id).val() == ''){
		 errors++;
		 errorstxt += 'Quantity field cannot be empty.<br>';
		}
		
		
		if(  $('#quantityx'+id).val() < 2){
		 errors++;
		 errorstxt += 'You can not use less then 2 Points.<br>';
		}
	}
	
	
	
	var qunatccxx =  + $('#quantityx'+id).val();
		if (size != 5 && size != 6 && size != 4) {
			total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * cpc_standart).toFixed(2) : (qunatccxx / (1000/cpm_standart)).toFixed(2));
			
		}else if(size == 4){
			total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * cpc_video).toFixed(2) : (qunatccxx / (1000/cpm_video)).toFixed(2));
		}else{
			//total_points = ($('.typex'+id).val() == 1 ? (qunatccxx * 4).toFixed(2) : (qunatccxx / 250).toFixed(2));
			if ($('.typex'+id).val() == 1) {
				total_points = (qunatccxx * cpc_product).toFixed(2);
			}else if($('.typex'+id).val() == 2){
				total_points = (qunatccxx / (1000/cpm_product)).toFixed(2);
			}else{
				total_points = monthly_banner;
			}
		}

	
	if({$points} < total_points || errors > 0){
		
		
		
		if(errors > 0){
			errorstxt2 ='<p style="" class="alert alert-danger">'+errorstxt+'</p>';
		    $('.pthererrrors').html(errorstxt2);
		}
		
		if({$points} < total_points){
			$('.dangernopoints').hide();
			$('.dangernopoints'+id).show('slow');
			$('.dangernopoints'+id).text('You only have {$points} points.')
		}
		
	}else{   
	   var url = "#"; // the script where you handle the form input.
   
	   
	   
		$.ajax({
			   type: "POST",
			   url: url,
			   data: "addcampainandtest&id="+id
			   +"&name="+$('.namex'+id).val()
			   +"&url="+$('.urlx'+id).val()
			   +"&type="+$('.typex'+id).val()
			   +"&quantity="+$('#quantityx'+id).val()
			   +"&state="+$('.stateselectx'+id).val()
			   +"&city="+$('.cityselectx'+id).val()
			   +"&country="+$('.countryselect'+id).val()
			   +"&key="+$('#key'+id).val()
			   +"&keystatus="+$('#keystatus'+id).is(':checked')
			   +"&toggleclasstree="+$('.toggleclasstree'+id).is(':checked')
			   +"&only_finance="+$('.only_finance'+id).is(':checked')
			   +"&total_points=" + total_points
			   +"&compain_category=" + $('.banner_cat'+id+' input[name="categoryBox"]:checked').val()

			//abcd
			   
			   , 
			   success: function(data)
			   {
				   //alert(data);
				   if(data != 'ok'){
				   $('.pthererrrors').append(data);
				   
				   alert(data); // show response from the php script.
				   }else{
				   location.reload();
				   }
			   }
			 });

		return false; // avoid to execute the actual submit of the form.
	}
}


//state and city 
$(document).ready(function(){
	 
	 
	
	 
	 
	{foreach $banners item=ban}
		activecity = '';
		//activecity = {$customeroptions.city};
		$('.stateselectx{$ban.id_banner}').change(function() {
			 $('.cityselectx{$ban.id_banner}').empty();
			 $('.loadcity').fadeTo( "slow" , 0.5);
			 $('.spinnerloadcity').fadeTo( "slow" , 1);
			 $.uniform.update();
			 whatstate = $(this).val();
			 $.ajax({
					type: "POST",
					url: '#',
					data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
					success: function(data)
						{
							//alert(data); // show response from the php script.
							$('.cityselectx{$ban.id_banner}').empty();
							$('.cityselectx{$ban.id_banner}').append(data);
							$.uniform.update();
							$('.loadcity').fadeTo( "slow" , 1);
							$('.spinnerloadcity').fadeTo( "slow" , 0);
							changetexrtspanonchangeinputs({$ban.id_banner}, {$ban.size})
						}
				   });
					return false;
		});
	{/foreach} //

	{foreach $video_banners item=ban}
		activecity = '';
		//activecity = {$customeroptions.city};
		$('.stateselectx{$ban.id_banner}').change(function() {
			 $('.cityselectx{$ban.id_banner}').empty();
			 $('.loadcity').fadeTo( "slow" , 0.5);
			 $('.spinnerloadcity').fadeTo( "slow" , 1);
			 $.uniform.update();
			 whatstate = $(this).val();
			 $.ajax({
					type: "POST",
					url: '#',
					data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
					success: function(data)
						{
							//alert(data); // show response from the php script.
							$('.cityselectx{$ban.id_banner}').empty();
							$('.cityselectx{$ban.id_banner}').append(data);
							$.uniform.update();
							$('.loadcity').fadeTo( "slow" , 1);
							$('.spinnerloadcity').fadeTo( "slow" , 0);
							changetexrtspanonchangeinputs({$ban.id_banner})
						}
				   });
					return false;
		});
	{/foreach} //
	
	
})

$(".close_add").on('click',function(){
    $(this).closest('.showdd').hide();
});
function  openclosecampadd(a){

	$('.btocampaddtohideshow').show();
	//$('.btocampaddtohideshow'+a).hide();
	$('.compain_table').hide();
	$('.showdd').hide(0);
	$('.showdd'+a).show();
	 // setTimeout(function(){
	 // 		$.uniform.update();
	 // }, 5e2);
	//$("select").uniform();

}

</script>
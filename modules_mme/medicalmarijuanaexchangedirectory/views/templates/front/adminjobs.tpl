
{capture name=path}
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<span class="navigation_page">Jobs</span>
{/capture}


<h2 class="toggle_title bg_ededed">List Jobs</h2>
<div class="toggle_arrea">
	<p class="alert alert-danger alert-danger-jobs" style="display: none;">You do not have a subscription</p>
	<div class="table-responsive">          
	  <table class="table table-responsive">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Name</th>
	        <th>Date Update</th>
	        <th>Date Add</th>
	        <th style="text-align:center;"></th>
	      
	      </tr>
	    </thead>
	    <tbody>
			{foreach $jobs item=job}
			<tr>
				<td>{$job.id_jobs}</td>
				<td>{$job.tittle}</td>
				<td>{$job.date_upd}</td>
				<td>{$job.date_add}</td>
				<td>
					<div style="display: flex;flex-wrap: wrap;justify-content: space-between;">
						<a style="max-width:47%;margin:0; display:block;text-align: center; background: #56B646 !important; border: none !important;" class="button btn_primary btn-block table__buttons bg_56B646" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobs')|escape:'html'}?list=3&edit={$job.id_jobs}">
							<img style="width: 15px;" src="themes/default-bootstrap/img/edit_pencil_white.svg">
						</a>
						<a style="max-width:47%;margin:0;display:block;text-align: center;background: #E04C42 !important;border: none !important" class="button btn_primary btn-block removejobs table__buttons" removej="{$job.id_jobs}">
							<img style="width: 13px;" src="themes/default-bootstrap/img/delete_button_white.svg">
						</a>
						<a  href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'off')|escape:'html'}?id_jobs={$job.id_jobs}" style="max-width:100%;margin:8px auto 0 auto;display:block;text-align: center;    background: #333;border: #333; text-transforn: uppercase" class="button btn_primary btn-block ">
							VIEW APPLICANTS
						</a>
					</div>
				</td>
			{/foreach}
			</tr>
		</tbody>
	  </table>
	</div>
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'jobs')|escape:'html'}?list=3" class="button btn_primary btn-block {if !$active_subscription}no_subscriptions_jobs{/if}" 
	style="width:100%;text-align:center;border: 1px solid #31674d;">Add Job</a>
</div>

<script>
/*
$(document).ready(function(){
$('#left_column').remove();
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');
});*/


//remove JOBS	
$('.removejobs').click(function(){	
removej = $(this).attr('removej');
if (confirm('Are you sure?')) {
    var url = "#"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: 'deletejob='+removej, // serializes the form's elements.
           success: function(data)
           {
             location.reload();
           }
         });

    return false; // avoid to execute the actual submit of the form.
}
})	


</script>
</div>
{* Softsprint Uliana *}
<script type="text/javascript">
	$(".no_subscriptions_jobs").off("click").on("click", function(e) {
  	    e.preventDefault();
  		$(".alert-danger-jobs").show();
		return false;
	});

	$('.toggle_title').click(function(){	
		$(this).parent('.container').find('.toggle_arrea').toggle();

	});
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script type="text/javascript">
	var markets = ['ACAN', 'ACBFF', 'ACCA', 'ACOL', 'ACRL', 'ADVT', 'AERO', 'AFPW', 'AGTK', 'AMFE', 'AMMJ', 'APHQF', 'ATTBF', 'AXIM', 'AZFL', 'BLDV', 'BLOZF', 'BLPG', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CSAX', 'CURR', 'CVSI', 'DEWM', 'DIGP', 'DIRV', 'DPWW', 'EAPH', 'ECIGQ', 'EFFI', 'EMHTF', 'EMMBF', 'ENCC', 'ENDO', 'ENRT', 'ERBB', 'ESPH', 'ETST', 'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GLDFF', 'GNBT', 'GRCU', 'GRNH', 'GRSO', 'GRWC', 'GRWG', 'GTBP', 'GTSO', 'HALB', 'HLIX', 'HLSPY', 'HMKTF', 'HMPQ', 'HVST', 'HYYDF', 'ICCLF', 'ICNM', 'IGPK', 'IMLFF', 'INMG', 'INQD', 'ITNS', 'IVITF', 'KAYS', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LSCG', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJLB', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYDX', 'MYEC', 'MYHI', 'NDEV', 'NGMC', 'NHLE', 'NMUS', 'NRTI', 'NSAV', 'NSPDF', 'NTRR', 'NVGT', 'OGRMF', 'ONCI', 'OWCP', 'PHOT', 'PKPH', 'PLPL', 'PMCB', 'PNPL', 'PNTV', 'POTN', 'PRMCF', 'PRRE', 'PUFXF', 'QRSRF', 'REFG', 'REVI', 'RMHB', 'RSSFF', 'SAGD', 'SING', 'SIPC', 'SLNX', 'SNNC', 'SPLIF', 'SPRWF', 'SRNA', 'STEV', 'STWC', 'TAUG', 'TBPMF', 'TECR', 'THCBF', 'TRTC', 'TWMJF', 'UAMM', 'UBQU', 'UMBBF', 'USMJ', 'VAPE', 'VAPI', 'VAPR', 'VATE', 'VHUB', 'VRTHF', 'WCIG', 'WDRP', 'WTII', 'ZDPY'];

  var news = [];
  var j = 0;
  for (var i = 0; i <= markets.length-1; i++) {
  	$.ajax({
	  	url: 'https://api.stockdio.com/data/financial/info/v1/getNewsEx?app-key=9F294CB158A94C73B7D84E38BC128E54&nItems=3&stockExchange=OTCMKTS&symbol='+markets[i],
	    context: document.body
	}).done(function (r) {
	    news_one = r['data']['news']['values'];
	    news = $.merge( $.merge( [], news ), news_one);
	    j++;
	    if (j == markets.length-1) {
			shownews();
		}
	}).fail(function (e) {
	   console.log('my fail code');
	});
  }

 function shownews() {
	news.sort(function(a,b){
	  return new Date(b[0]) - new Date(a[0]);
	});
	console.log(news);
	news_list = '';
	for (var i = 0; i <= 4; i++) {
		news_list += '<div class="single_news_block"><a class="singlr_title" href="'+news[i][2]+'" target="_blank">'+news[i][1]+'</a> <span class="singl_date">'+moment(news[i][0]).fromNow()+' | '+news[i][5]+'</span></div>';
	}
	$('#news_list').html(news_list);
 }  


</script>

<script>
   if (typeof(stockdio_events) == "undefined") {
      stockdio_events = true;
      var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
      var stockdio_eventer = window[stockdio_eventMethod];
      var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
      stockdio_eventer(stockdio_messageEvent, function (e) {
         if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
            eval(e.data.method);
         }
      },false);
   }
</script>

<div class="left_width">
  {assign var="i" value=0} 
  {foreach from=$items item=news}
    {if $i == 0}
    <div class="first_news">
      <a href="http://medicalmarijuanaexchange.com/modules/blockblog/blockblog-post.php?post_id={$news.id}">
        <img src="http://medicalmarijuanaexchange.com/upload/blockblog/{$news.img}">
        <span>{$news.title}</span>
      </a>
    </div>
    {elseif $i == 1 || $i == 2}
    <div class="next_news">
      <a href="http://medicalmarijuanaexchange.com/modules/blockblog/blockblog-post.php?post_id={$news.id}">
        <img src="http://medicalmarijuanaexchange.com/upload/blockblog/{$news.img}">
        <span>{$news.title}</span>
      </a>
    </div>
    {elseif $i > 5}
    {break}
    {else}
    <div style="clear: both;"></div>
    <div class="news-list">
      
      <a href="http://medicalmarijuanaexchange.com/modules/blockblog/blockblog-post.php?post_id={$news.id}">
        <img src="http://medicalmarijuanaexchange.com/upload/blockblog/{$news.img}">
      </a>
      <a class="news_title" href="http://medicalmarijuanaexchange.com/modules/blockblog/blockblog-post.php?post_id={$news.id}">{$news.title}</a>
      {$news.content|truncate:200:"...":true}
    </div>
    {/if}

  {assign var="i" value=$i+1}
  {/foreach}
  
</div>
<div class="right_width">
  <iframe id="relatedcompanies" frameborder="0" scrolling="no" width="100%" height="268" src="https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?stockExchange=otcmkts&amp;symbol=ACBFF&amp;presetlist=related_indices&amp;limit=6&amp;palette=Financial-Light&amp;width=100%25&amp;height=auto&amp;onlink=true&amp;onload=iframemarkets&amp;template=2451116a-1563-4a96-be20-60554d6748ba&amp;&amp;app-key=9F294CB158A94C73B7D84E38BC128E54"></iframe>

  <div class="stocks_list">
    <iframe id='st_c0b82d488b814ee7882627a227052610' frameBorder='0' scrolling='no' width='100%' height='600' src='https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols=ACAN;ACBFF;ACCA;ACOL;ACRL;ADVT;AERO;AFPW;AGTK;AMFE;AMMJ;APHQF;ATTBF;AXIM;AZFL;BLDV;BLOZF;BLPG;BUDZ;BXNG;CAFS;CANL;CANN;NYSENasdaq:CARA;CBCA;CBDS;CBGI;CBIS;CBMJ;CBNT;CBSC;CCAN;CGRA;CGRW;CHUM;CIIX;CLSH;CMMDF;CNAB;CNBX;CNZCF;CPMD;CRWG;CSAX;CURR;CVSI;DEWM;DIGP;DIRV;DPWW;EAPH;ECIGQ;EFFI;EMHTF;EMMBF;ENCC;ENDO;ENRT;ERBB;ESPH;ETST;EVIO;GBHPF;GBLX;GLAG;GLDFF;GNBT;GRCU;GRNH;GRSO;GRWC;GRWG;GTBP;GTSO;NYSENasdaq:GWPH;HALB;HLIX;HLSPY;HMKTF;HMPQ;HVST;HYYDF;ICCLF;ICNM;NYSENasdaq:IIPR;NYSENasdaq:IGC;IGPK;IMLFF;INMG;INQD;NYSENasdaq:INSY;ITNS;IVITF;KAYS;KGKG;KSHB;LCTC;LDSYF;LSCG;LVVV;MCIG;MCOA;MCPI;MDCL;MDEX;MDRM;MGWFF;MJLB;MJMD;MJNA;MJNE;MNTR;MQPXF;MRPHF;MSRT;MYDX;MYEC;MYHI;NDEV;NGMC;NHLE;NMUS;NRTI;NSAV;NSPDF;NTRR;NVGT;OGRMF;ONCI;OWCP;PHOT;PKPH;PLPL;PMCB;PNPL;PNTV;POTN;PRMCF;PRRE;PUFXF;QRSRF;REFG;REVI;RMHB;RSSFF;SAGD;SING;SIPC;SLNX;NYSENasdaq:SMG;SNNC;SPLIF;SPRWF;SRNA;STEV;STWC;TAUG;TBPMF;TECR;THCBF;NYSENasdaq:TLRY;NYSENasdaq:TRPX;TRTC;TWMJF;UAMM;UBQU;UMBBF;USMJ;VAPE;VAPI;VAPR;VATE;VHUB;NYSENasdaq:VPCO;VRTHF;WCIG;WDRP;WTII;ZDPY;NYSENasdaq:ZYNE&includeLogo=false&palette=Financial-Light&title=Marijuana%20Stocks&linkUrl=https://medicalmarijuanaexchange.com/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=600px'></iframe>
  </div>

  <div id="stocks_news">
  	<div class="stocks_news_title">Latest news</div>
  	<div id="news_list">
  		<div class="single_news_block">News Loading...</div>
  	</div>
  		<div class="single_news_block">
  			<a class="more_news" href="/latest-news">More News<i class="fas fa-chevron-right"></i></a>
  		</div>
  </div>
  {include file="$includedir/financial_right_banner.tpl"}
</div>
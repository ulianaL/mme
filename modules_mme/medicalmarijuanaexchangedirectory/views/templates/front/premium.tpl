{capture name=path}
				<span class="navigation_page">
				Business Memberships

				</span>
{/capture}
				 
				
				

<div class="row">
  <h1  style="order-bottom: 1px solid;
    margin: 15px;
    margin-bottom: 20px;
    padding-bottom: 14px;" class="center">Business Memberships</h1>
 

  
  

<div class="container">
	<div class="row">
    			<div class="col-md-4">
				
					<!-- PRICE ITEM -->
					<div class="panel price panel-red">
						<div class="panel-heading  text-center">
						<h3>Platinum Membership</h3>
						</div>
						<div class="panel-body text-center">
							<p class="lead" style="font-size:30px"><strong>$2500 / month</strong></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-success"></i>Create Business profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Directory Profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Post Job Listing</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Display products in your store</li>
							<li style="font-size:15px;" class="list-group-item"><i class="icon-ok text-success"></i>Featured Placement in Directory ,Jobs, and shop</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>5 twitter promotions in local market</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>4000 Advertising tokens</li>
						</ul>
						<div class="panel-footer">
							
							<a class=" ajax_add_to_cart_button btn btn-lg btn-block btn-danger" href="https://medicalmarijuanaexchange.com/cart?add=1&amp;id_product=138&amp;token={$static_token}" rel="nofollow" title="Add to cart" data-id-product-attribute="0" data-id-product="138" data-minimal_quantity="1">
									<span>BUY NOW!</span>
							</a>
						</div>
					</div>
					<!-- /PRICE ITEM -->
					
					
				</div>
				
				<div class="col-md-4">
				
					<!-- PRICE ITEM -->
					<div class="panel price panel-blue">
						<div class="panel-heading arrow_box text-center">
						<h3>Gold Membership</h3>
						</div>
						<div class="panel-body text-center">
							<p class="lead" style="font-size:30px"><strong>$1000 / year</strong></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-success"></i>Create Business profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Directory Profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Post Job Listing</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Display products in your store</li>
							<li style="font-size:15px;" class="list-group-item"><i class="icon-ok text-success"></i>Deluxe Placement in Directory ,Jobs, and shop</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>2 twitter promotions in local market</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>1500 Advertising tokens</li>
						</ul>
						<div class="panel-footer">
							
							<a class=" ajax_add_to_cart_button btn btn-lg btn-block btn-info" href="https://medicalmarijuanaexchange.com/cart?add=1&amp;id_product=137&amp;token={$static_token}" rel="nofollow" title="Add to cart" data-id-product-attribute="0" data-id-product="137" data-minimal_quantity="1">
									<span>BUY NOW!</span>
							</a>
						</div>
					</div>
					<!-- /PRICE ITEM -->
					
					
				</div>
				
				<div class="col-md-4">
				
					<!-- PRICE ITEM -->
					<div class="panel price panel-green">
						<div class="panel-heading arrow_box text-center">
						<h3>Silver Membership</h3>
						</div>
						<div class="panel-body text-center">
							<p class="lead" style="font-size:30px"><strong>$500 / year</strong></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-success"></i>Create Business profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Directory Profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Post Job Listing</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Display products in your store</li>
							<li style="font-size:15px;" class="list-group-item"><i class="icon-ok text-success"></i>Priority Placement in Directory ,Jobs, and shop</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>600 Advertising tokens</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>-</li>
						</ul>
						<div class="panel-footer">
							
							<a class=" ajax_add_to_cart_button btn btn-lg btn-block btn-success" href="https://medicalmarijuanaexchange.com/cart?add=1&amp;id_product=136&amp;token={$static_token}" rel="nofollow" title="Add to cart" data-id-product-attribute="0" data-id-product="136" data-minimal_quantity="1">
								<span>BUY NOW!</span>
							</a>
						</div>
					</div>
					<!-- /PRICE ITEM -->
					
					
				</div>
				
				
				
				</div>
				
				<div class="row"   style="margin-top:25px;">
				<div class="col-md-4 col-md-offset-2">
				
					<!-- PRICE ITEM -->
					<div class="panel price panel-grey">
						<div class="panel-heading arrow_box text-center">
						<h3>Bronze Membership</h3>
						</div>
						<div class="panel-body text-center">
							<p class="lead" style="font-size:30px"><strong>$200 / Year</strong></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-success"></i>Create Business profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Directory Profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Post Job Listing</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Display products in your store</li>
							<li style="font-size:15px;" class="list-group-item"><i class="icon-ok text-success"></i>Premium placement in Directory ,Jobs, and shop</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>200 Advertising tokens</li>
						</ul>
						<div class="panel-footer">
							<a class=" ajax_add_to_cart_button btn btn-lg btn-block btn-primary" href="https://medicalmarijuanaexchange.com/cart?add=1&amp;id_product=135&amp;token={$static_token}" rel="nofollow" title="Add to cart" data-id-product-attribute="0" data-id-product="135" data-minimal_quantity="1">
								<span>BUY NOW!</span>
							</a>
							
							
						</div>
					</div>
					<!-- /PRICE ITEM -->
					
					
				</div>
				
				<div class="col-md-4">
				
					<!-- PRICE ITEM -->
					<div class="panel price panel-white">
						<div class="panel-heading arrow_box text-center">
						<h3>Basic Membership</h3>
						</div>
						<div class="panel-body text-center">
							<p class="lead" style="font-size:30px"><strong>$0 / Year</strong></p>
						</div>
						<ul class="list-group list-group-flush text-center">
							
							<li class="list-group-item"><i class="icon-ok text-success"></i>Create Business profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Directory Profile</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Post Job Listing</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>Display products in your store</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>-</li>
							<li class="list-group-item"><i class="icon-ok text-success"></i>-</li>
						</ul>
						<div class="panel-footer"  style="    height: 63px;">
							{*****************
							<a class=" ajax_add_to_cart_button btn btn-lg btn-block btn-default" href="https://medicalmarijuanaexchange.com/cart?add=1&amp;id_product=102&amp;token=854d236f87da447508ebb5b2100f6f3c" rel="nofollow" title="Add to cart" data-id-product-attribute="0" data-id-product="102" data-minimal_quantity="1">
								<span>BUY NOW!</span>
							</a>
						******}
						
						</div>
					</div>
					<!-- /PRICE ITEM -->
					
					
			 </div>
				
				
				
				
				
				
				
			</div>
            
</div>
  
  
  
  
  
</div>




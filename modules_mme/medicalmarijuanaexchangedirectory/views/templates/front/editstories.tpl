<h1>Edit Sponsored Story</h1>
<form class="add_story_form" action="#" method="post" enctype="multipart/form-data">
	<label>Title</label>
	<input required="required" type="text" id="story_title" name="story_title" value="{$story.title}">
    {if $story.used == 0}
        <label class="red_color">New Clicks (one click cost {$story_cost} points)</label>
        <input required="required" type="number" id="story_clicks" name="story_clicks" step="1" min="0" autocomplete="off">
        <div id="story_points" class="alert alert-success"><span>0</span> Points</div>
    {else}
    <label>Avaliable clicks: {$story.used}</label>
    {/if}
	<label>Upload a picture (only 900x500px size)</label>
	<div class="ss_picture">
		<img src="/modules/medicalmarijuanaexchangedirectory/storyimg/{$story.image}">
	</div>
	
	<input type="file" name="fileToUpload" id="fileToUpload">
	<label>Description</label>
	<textarea id="add_description" name="add_description">{$story.description}</textarea>
	<div class="form_buttons">
		<input class="ss_add" type="submit" name="updatesponsoredstories" value="Save a sponsored story">
	</div>
</form>

<script type="text/javascript">
    $("#story_clicks").on('input',function(){
    var clicks = $('#story_clicks').val();
    var points = clicks * {$story_cost};
    $('#story_points span').text(points);
    $('#story_points').show();
});
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('.ss_picture img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
	$("#fileToUpload").change(function(){
	  readURL(this);
	});

	$(".add_story_form").submit( function( e ) {
    var form = this;
    e.preventDefault(); 
    var fileInput = $(this).find("input[type=file]")[0],
    file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            if( width == 900 && height == 500 ) {
                form.submit();
            }
            else {
                alert('Error on picture size! Should be 900x500px. Your picture size '+width+'x'+height)
            }
        };
    }
    else { //No file was input or browser doesn't support client side reading
        form.submit();
    }

});
</script>
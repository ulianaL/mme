{capture name=path}
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<span class="navigation_page">Store Options</span>
{/capture}

<div class="container">

	<h2 class="toggle_title bg_ededed">Store Options</h2>
	<div class="toggle_arrea">
			   <div style="border: 1px solid #d6d4d4;padding: 15px;">
				
				<form method="post" action="#" >	
					<div class="form-group">
						<label>Phone Number to receive mobile alerts by SMS (Please leave blank if you do not want to receive sms)</label>
						
						<input type="text" value="{$customeroptions.phonesms}" name="phonesms" class="form-control">
					</div>
					
					<div class="form-group" style="width:100%;">
						<label>Country:</label>
							<select  style="width: 250px;" name="country"  class="form-control countryselect">
								<option value="1">USA</option>
								<option value="2">CANADA</option>
							</select>
					</div>
					
					
					
				      
					  <div class="form-group" style="width:100%;">
						 <label>State</label>
						 <select style="width: 250px;" name="state" class="form-control stateselect">
						  {foreach $states item=state}
						  <option value="{$state.state_code}" country="{$state.country}"   {if $state.state_code == $customeroptions.state}selected="selected"{/if}>{$state.state}</option>
						  {/foreach}
						</select>
					  </div>
					  
					  <div class="form-group" style="width:100%;">
						 <label>City</label>
						 <select style="width: 250px;" name="city" class="form-control cityselect">
						  {foreach $city item=cit}
						  <option  class="state_{$cit.state_code} citiess" value="{$cit.city_id}"   {if isset($edit.0.city)}{if $cit.city_id == $edit.0.city}selected="selected"{/if}{/if}>{$cit.city}</option>
						  {/foreach}
						</select>
					  </div> 
					  
					  
<script>
{if isset($customeroptions.country)}	
    {if $customeroptions.country == 1}
		$(".stateselect option[country=CA]").hide();
		$(".stateselect option[country=US]").show();
		$('.countryselect').val(1);
		
	{else}
		$(".stateselect option[country=US]").hide();
		$(".stateselect option[country=CA]").show();
		$('.countryselect').val(2);
	{/if}
	$.uniform.update();
{/if}




$('.countryselect').change(function(){
	
	if ($(this).val() == 1 ){
		$(".stateselect option[country=CA]").hide();
		$(".stateselect option[country=US]").show();
		$(".stateselect").val($(".stateselect option[country=US]:first").val());
	}else{
		$(".stateselect option[country=US]").hide();
		$(".stateselect option[country=CA]").show();
		$(".stateselect").val($(".stateselect option[country=CA]:first").val());
	}	
	 $.uniform.update();
	 
	 whatstate = $(".stateselect").val();
								$.ajax({
								   type: "POST",
								   url: '#',
								   data:'getcitiesfromstate='+whatstate+'&activecity=0', // serializes the form's elements.
								   success: function(data)
								   {
									   //alert(data); // show response from the php script.
									   $('.cityselect').empty();
									   $('.cityselect').append(data);
									   $.uniform.update();
									   

								   }
								 });
});		





			
					
					
					$(document).ready(function(){
					
						activecity = '';

						
						activecity = {$customeroptions.city};
					

						$('.stateselect').change(function() {
						
						 whatstate = $(this).val();
								$.ajax({
								   type: "POST",
								   url: '#',
								   data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
								   success: function(data)
								   {
									   //alert(data); // show response from the php script.
									   $('.cityselect').empty();
									   $('.cityselect').append(data);
									   $.uniform.update();
									   

								   }
								 });

						return false;
						})


						 whatstate = $('.stateselect').val();
								$.ajax({
								   type: "POST",
								   url: '#',
								   data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
								   success: function(data)
								   {
									   //alert(data); // show response from the php script.
									   $('.cityselect').empty();
									   $('.cityselect').append(data);
									   $.uniform.update();

								   }
								 });
						
												

						})
						 

						</script>
					  
					<div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="medical" {if isset($customeroptions.medical) && $customeroptions.medical == 1}checked{/if}  id="checkbox3" class="styled" type="checkbox">
						   <label for="checkbox3">Medical</label>
					   </div> 
					 </div>

					 <div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="recreational" {if isset($customeroptions.recreational) && $customeroptions.recreational == 1}checked{/if}  id="checkbox4" class="styled" type="checkbox">
						   <label for="checkbox4">Recreational</label>
					   </div> 
					 </div>

					 <div class="form-group">
						<label>
							Payment Options
						</label>
					   <div class="checkbox clearfix">
						   <input name="payment_cash" {if isset($customeroptions.payment_cash) && $customeroptions.payment_cash == 1}checked{/if}  id="checkbox_1" class="styled" type="checkbox" value="1">
						   <label for="checkbox_1">Cash</label>
					   </div>
					   <div class="checkbox clearfix">
						   <input name="payment_credit_card" {if isset($customeroptions.payment_credit_card) && $customeroptions.payment_credit_card == 1}checked{/if}  id="checkbox_2" class="styled" type="checkbox" value="1">
						   <label for="checkbox_2">Credit Card</label>
					   </div> 
					   <div class="checkbox clearfix">
						   <input name="payment_debit_card" {if isset($customeroptions.payment_debit_card) && $customeroptions.payment_debit_card == 1}checked{/if} id="checkbox_3" class="styled" type="checkbox" value="1">
						   <label for="checkbox_3">Debit Card</label>
					   </div> 
					   <div class="checkbox clearfix">
						   <input name="payment_atm" {if isset($customeroptions.payment_atm) && $customeroptions.payment_atm == 1}checked{/if} id="checkbox_4" class="styled" type="checkbox" value="1">
						   <label for="checkbox_4">ATM</label>
					   </div> 
					 </div>

					 <label>Order Type</label>
					 <div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="allowcod" {if isset($customeroptions.allowcod) && $customeroptions.allowcod == 1}checked{/if}  id="checkbox1" class="styled" type="checkbox">
						   <label for="checkbox1">Order Delivery</label>
					   </div> 
					 </div>

					 <div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="allowpick" {if isset($customeroptions.allowpick) && $customeroptions.allowpick == 1}checked{/if} id="checkbox2" class="styled" type="checkbox">
						   <label for="checkbox2">Order Pickup</label>
					   </div>
					   </div>


					 <label>Business type</label>
					<div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="delivery" {if isset($customeroptions.delivery) && $customeroptions.delivery == 1}checked{/if}  id="checkbox6" class="styled" type="checkbox">
						   <label for="checkbox6">Delivery</label>
					   </div> 
					 </div>

					 <div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="is_doctor" {if isset($customeroptions.is_doctor) && $customeroptions.is_doctor == 1}checked{/if}  id="checkbox5" class="styled" type="checkbox">
						   <label for="checkbox5">Doctors</label>
					   </div> 
					 </div>

					   <div class="form-group">
					   <div class="checkbox clearfix">
						   <input name="storefront" {if isset($customeroptions.storefront) && $customeroptions.storefront == 1}checked{/if} id="checkbox7" class="styled" type="checkbox">
						   <label for="checkbox7">Storefront</label>
					   </div>
					   </div>
					   <div class="form-group">
						 <label>Pickup Address</label>
					   
					   		<textarea class="form-control" name="pickaddress" id="pickaddress">{if isset($customeroptions.pickaddress)}{$customeroptions.pickaddress}{/if}</textarea>
					   		<input type="hidden" name="lat" id="lat" value="{if isset($customeroptions.storeLat)}{$customeroptions.storeLat}{/if}">
					   		<input type="hidden" name="lng" id="lng" value="{if isset($customeroptions.storeLng)}{$customeroptions.storeLng}{/if}">
						</div>

						<div class="form-group">
	                        <div id="map" style="width: 100%; height: 450px;"></div>
	                    </div>

						<div class="form-group edit_save_buttons"style="text-align:right;margin:0;margin-top:15px">
							<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'storeinfo')|escape:'html'}" class="btn btn-primary store_btn_edit">Edit store</a>
							<button type="submit" name="submitcarrieroptions" class="btn btn-primary store_btn_save">Save</button>
						</div>
						
				
				</form>
			</div>
		</div>

{literal}
<script type="text/javascript">
	var map, pos, geocoder, _marker;

	$(document).ready(function () {
        $("#pickaddress").on("keyup change", function (e) {

            switch (e.keyCode) {
                //case 8:  // Backspace
                case 9:  // Tab
                case 13: // Enter
                case 37: // Left
                case 38: // Up
                case 39: // Right
                case 40: // Down
                    break;

                default:

                    clearTimeout($.data(this, 'timer'));
                    var wait = setTimeout(updateMapPin, 500);
                    $(this).data('timer', wait);
            }

        });

        $('[name="city"], [name="state"], [name="country"]').on('change', updateMapPin);
    });

    function updateMapPin() {
        var address = $("#pickaddress").val() +
        	' ' + $('[name="city"]').find('option:selected').text() +
        	' ' + $('[name="state"]').find('option:selected').text() +
        	' ' + $('[name="country"]').find('option:selected').text();
        console.log(address);
        //clear backspace
        address = address.replace(/\s+/g, ' ');
        // check backspace
        if (address.trim().length == 0) {
            return false;
        }
        codeAddress(address.trim());
    }

    function codeAddress(adr) {
        if (typeof _marker !== "undefined") {
            _marker.setMap(null);
        }

        if (adr != undefined)
            var address = adr;
        else
            return false;

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                // var icon = '/img/partners/delivery-pin-2.png';
                // if ($("#PartnerChildEditBusinessType").val() == 'storefront')
                //     icon = '/img/partners/storefront-pin-2.png';
                var point = results[0].geometry.location;
                var contentString = point;
                //map.setCenter(point);
                if (_marker && _marker.setMap)
                    _marker.setMap(null);
                _marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: point,
                    title: 'My Store',
                    // icon: icon
                });

                updateMarkerPosition(point);

                //map.setZoom(12);
                map.setCenter(point);

//                google.maps.event.addListener(_marker, 'drag', function () {
//                    updateMarkerPosition(_marker.getPosition());
//                });

                google.maps.event.addListener(_marker, 'dragend', function () {
                    updateMarkerPosition(_marker.getPosition());
                    // checkComapnyPinValues();
                });

                google.maps.event.trigger(_marker, "click");

            }
        });
    }

	function initMap() {
		geocoder = new google.maps.Geocoder();
		var address = 'Los Angeles, USA';
		if ($('#lat').val() && $('#lng').val()) {
			pos = {
	            lat: +$('#lat').val(),
	            lng: +$('#lng').val()
	        };
	        initializeMap(pos);
	        setMarkerLatLng(pos);
		} else if (address.trim().length > 0) {
			geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    pos = {
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng()
                    };
                    var ac_length = results[0].address_components.length;
                    initializeMap(pos);
                    setMarkerLatLng(pos);
                } else {
                    pos = {lat: 34.0522342, lng: -118.2436849};
                    initializeMap(pos);
                }
            });
		} else {
			pos = {lat: 34.0522342, lng: -118.2436849};
            initializeMap(pos);
		}
	}
	function initializeMap(pos) {
		var zoom = 12;
		map = new google.maps.Map(document.getElementById('map'), {
            center: pos, //{lat: 34.0522342, lng: -118.2436849},
            zoom: zoom,
            streetViewControl: false,
//            zoomControl: true,
//                zoomControlOptions: {
//                    style: google.maps.ZoomControlStyle.SMALL,
//                    position: google.maps.ControlPosition.RIGHT_TOP
//                }
            mapTypeControlOptions: {
                // mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        });
	}

	function setMarkerLatLng(point) {
        if (typeof _marker !== "undefined") {
            _marker.setMap(null);
        }

        if ((point.lat == undefined) || (point.lng == undefined)) {
            return false;
        }

        // var icon = '/img/partners/delivery-pin-2.png';
        // if ('<?php echo $this->Form->data['PartnerChildEdit']['businessType']; ?>' == 'storefront')
            // icon = '/img/partners/storefront-pin-2.png';

        if (_marker && _marker.setMap)
            _marker.setMap(null);
        _marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: point,
            // title: $("#PartnerChildCompanyName").val(),
            title: 'My Store',
            // icon: icon
        });

        //map.setZoom(12);
        map.setCenter(point);

//                google.maps.event.addListener(_marker, 'drag', function () {
//                    updateMarkerPosition(_marker.getPosition());
//                });

        google.maps.event.addListener(_marker, 'dragend', function () {
            updateMarkerPosition(_marker.getPosition());
            // checkComapnyPinValues();
        });

        google.maps.event.trigger(_marker, "click");
    }

    function updateMarkerPosition(latLng) {
        $('#lat').val(latLng.lat());
        $('#lng').val(latLng.lng());
    }
</script>
{/literal}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy2w3YOVYTM_wRkMbKOY2Vl75Zb9w-dAM&libraries=places&callback=initMap" async defer></script>

<div class="custom_breadcrumps font_futura">
	<a class="font_16px" href="//medicalmarijuanaexchange.com/">
		Home
	</a>
	<a class="font_16px">
		Directory
	</a>
</div>
{capture name=path}
	<span class="navigation_page">DIRECTORY ({if $active_directory == 'allusa'}All USA{elseif $active_directory == 'allcanada'}All Canada{else}{$cityinfos.cityName}, {$cityinfos.regionName}{/if})</span>
{/capture}




<div class="directory_filters">
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front')}?allusa=1" class="{if $active_directory == 'allusa'}active_directory{/if}"> 
	<span>Search USA</span>
	</a>
	<span class="black_circle"></span>
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front')}?allcanada=1" class="{if $active_directory == 'allcanada'}active_directory{/if}">
	<span>Search Canada</span>
	</a>
	<span class="black_circle"></span>
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front')}" class="{if $active_directory == 'selectedcity'}active_directory{/if}">
	<span>Search Selected City ({$cityinfos.cityName}, {$cityinfos.regionName})</span>
	</a>
	
</div>



<div class="container">
	<div class="show_mob_list">Show categories list</div>
	<div class="row">
		<div class="col-sm-4 col-md-3 lftpan">
			{include file="$menucat"}
		</div><!---md-3--->
		
		
		
		
		<div class="col-sm-8 col-md-9 rtp">
			
			
			{*****IF IS PREMIUM SYSTEM ****}
			{foreach from=$results item=result}
			{if $result.review == 1}
				 {if $result.pay == 1}
					{if $result.doorderandpay == 1}
						<div class="row" style="position:relative;padding: 10px;margin:0;margin-top:15px;border-bottom:1px solid #e3e3e3;3px solid #000000">
								<div class="col-md-5">
								{if $result.logo != ""}
								<img class="" src="http://medicalmarijuanaexchange.com/modules/medicalmarijuanaexchangedirectory/upload/{$result.logo}"/>
								{/if}
								</div>
								<div class="col-md-7">
									<div class="loop__text">
									<div class="loop__main">
										<a href="{$linkview}&viewid={$result.id_directory}" class="loop__title ellipsis" style="word-wrap: break-word;">
										<span class="directories_title">{$result.name}</span>
										</a>
										<div class="directories_description" style="word-wrap: break-word;    margin-top: 5px;    text-align: justify;">
										{$result.about|truncate:150:"...":true}
										</div>
									</div>
									<div class="loop__button" style="margin-top: 5px;">
									<a href="{$linkview}&viewid={$result.id_directory}" class="view_profile">
									<span class="button__text" >View profile</span>
									</a>
									</div>
									</div>
								</div>
						</div>
					{/if}
				{/if}
			{/if}
			{/foreach}
			
			
			
			{*****IF IS normAL SYSTEM ****}
			{foreach from=$results item=result}
				{if $result.review == 1}
					{if $result.pay == 0}

					<div class="row" style="padding: 10px;margin:0;margin-top:15px;border-bottom:1px solid #e3e3e3;">
						<div class="col-md-5">
							{if $result.logo != ""}
							<img class="" src="http://medicalmarijuanaexchange.com/modules/medicalmarijuanaexchangedirectory/upload/{$result.logo}"/>
							{/if}
						</div>
							<div class="col-md-7">
								<div class="loop__text">
								<div class="loop__main">
									<a href="{$linkview}&viewid={$result.id_directory}" class="loop__title ellipsis" style="word-wrap: break-word;">
									<span class="directories_title">{$result.name}</span>
									</a>
									<div class="directories_description" style="word-wrap: break-word;    margin-top: 5px;    text-align: justify;">
									{$result.about|truncate:150:"...":true}
									</div>
								</div>
								<div class="loop__button" style="margin-top: 5px;">
								<a href="{$linkview}&viewid={$result.id_directory}" class="view_profile">
								<span class="button__text" >View profile</span>
								</a>
								</div>
								</div>
							</div>
					</div>
					{/if}
				{/if}		
			{/foreach}		
				
		</div><!---md-9--->		
	</div>
</div>


<style>
.loop__text {
    padding-top: 8px;
}
img.feat {
    width: 85px;
    position: absolute;
    right: 20px;
   z-index: 99999999;
    color: #00a161;
}
</style>
<script type="text/javascript">
	 $('.show_mob_list').on('click', function(){
        $(this).toggleClass('active');
        $('.lftpan').toggle();
    });
</script>

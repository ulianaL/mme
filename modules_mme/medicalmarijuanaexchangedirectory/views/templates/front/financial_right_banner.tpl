 {if !empty($rightbanner)}
    <div class="right_banners">
    {if $rightbanner.type == 2}
      <div class="publi {if $rightbanner.size == '4'}fixed_video_banner{/if}">
        <a target="_blank" href="{if strpos($rightbanner.url,'http') !== false}{$rightbanner.url}{else}http://{$rightbanner.url}{/if}">
          {if $rightbanner.size == '4'}
            <a  class = "go_to_campain" target="_blank" href="{if strpos($rightbanner.url,'http') !== false}{$rightbanner.url}{else}http://{$rightbanner.url}{/if}">Go to Campaign</a>
            <span class="remove_video">&times;</span>
            {include file="$tpl_dir./video_banner.tpl" banner=$rightbanner}
            <script type="text/javascript">
              if (Cookies.get('showed_video1')) {

                Cookies.set('showed_video2', 1);
              }else{

                 Cookies.set('showed_video1', {$rightbanner.id_banner});
              }
            </script>
          {else}
            <img class="publi-img" src="{getBuildBannerUrl($rightbanner.banner_img)}" />
          {/if}
        </a>
      </div>
    {elseif $rightbanner.type == 1} 
        <div class="publi {if $rightbanner.size == '4'}fixed_video_banner{/if}">
          <a class = "go_to_campain" target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$rightbanner.id_camp}">
            {if $rightbanner.size == '4'}
              <a target="_blank" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'linkcount')|escape:'html'}?campurl={$rightbanner.id_camp}">Go to Campaign</a>
              <span class="remove_video">&times;</span>
              {include file="$tpl_dir./video_banner.tpl" banner=$rightbanner}
              <script type="text/javascript">
                if (Cookies.get('showed_video1')) {

                  Cookies.set('showed_video2', 1);
                }else{

                   Cookies.set('showed_video1', {$rightbanner.id_banner});
                }
              </script>
            {else}
              <img class="publi-img" src="{getBuildBannerUrl($rightbanner.banner_img)}" />
            {/if}
          </a>
        </div>
    {/if}
    </div>
  {/if}
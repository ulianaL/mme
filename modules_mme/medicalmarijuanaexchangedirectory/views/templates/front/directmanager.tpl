<div class="container">
	<h2 class="toggle_title">Directory manager</h2>
	<div class="toggle_arrea">
		<div  style="    border: 1px solid #d6d4d4;padding: 15px;">
			<table class="table" style="border: 1px solid #d6d4d4;">
				<thead>
				<tr>
					<th>#</th>
					<th>Premium</th>
					<th>Logo</th>
					<th>Name</th>
					<th style="min-width: 200px!important;">Category</th>
					<th>About</th>
					{*<th>Biography</th>*}
					<th>Approved</th>
					<th>Edit</th>
					
					
			    </tr>
				</thead>
				<tbody>
				
				{foreach $directory_rows item=di}
				<tr {if $di.pay == 1} style="background: #fcfcfc;"{/if} >
					<td>{$di.id_directory}</td> 
					
					<td>
						{if $di.pay == 1}
						<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/enabled.gif"/>
						{else}
						<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/disabled.gif"/>
						{/if}
					</td> 
					
					<td>
						<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/upload/{$di.logo}" />
					</td>
					<td>{$di.name}</td>
					<td>
						{if $di.cat == 1}Accounting Firms{/if}
	                    {if $di.cat == 2}Advertising / Marketing{/if}
	                    {if $di.cat == 29}Affiliate Programs{/if}
	                    {if $di.cat == 3}Apparel &amp; Accessories{/if}
	                    {if $di.cat == 4}Breeders, Genetics, or Clones{/if}
	                    {if $di.cat == 5}Consulting{/if}
	                    {if $di.cat == 6}Detoxification{/if}
	                    {if $di.cat == 7}Dispensaries, Collectives and Wellness Centers{/if}
	                    {if $di.cat == 8}Doctors &amp; Referral Services{/if}
	                    {if $di.cat == 9}Education{/if}
	                    {if $di.cat == 10}Extraction Companies{/if}
	                    {if $di.cat == 11}Glass Companies{/if}
	                    {if $di.cat == 12}Government Agencies{/if}
	                    {if $di.cat == 13}Hemp-Based Products{/if}
	                    {if $di.cat == 14}Hospitality &amp; Tourism{/if}
	                    {if $di.cat == 15}Infused Product Companies{/if}
	                    {if $di.cat == 16}Investment{/if}
	                    {if $di.cat == 17}Legal{/if}
	                    {if $di.cat == 18}Lighting{/if}
	                    {if $di.cat == 19}Manufacturers{/if}
	                    {if $di.cat == 30}Non Profit{/if}
	                    {if $di.cat == 20}Organizations &amp; Non-Profits{/if}
	                    {if $di.cat == 21}Public Companies{/if}
	                    {if $di.cat == 22}Publications/Resources{/if}
	                    {if $di.cat == 23}Research / Testing Labs{/if}
	                    {if $di.cat == 24}Retailers{/if}
	                    {if $di.cat == 25}Security / Insurance{/if}
	                    {if $di.cat == 26}Seed Banks{/if}
	                    {if $di.cat == 27}Soil, Composts &amp; Nutrients{/if}
	                    {if $di.cat == 28}Technology{/if}
	                    {if $di.cat == 31}Web Design{/if}
					</td>
					<td>{$di.about|truncate:100:"...":false}</td>
					{*<td>{$di.bio|truncate:100:"....":false}</td>*}
					<td>
						{if $di.review == 1}
							<img style="margin:0 auto; display:blockbackground: white; padding: 6px; border: 1px solid #333;" src="{$img_ps_dir}admin/enabled.gif"/>
						{else}
							<img style="margin:0 auto; display:blockbackground: white;padding: 6px;border: 1px solid #333;" src="{$img_ps_dir}admin/disabled.gif"/>
						{/if}
					</td>
					
					<td>
						{if $di.pay == 1}
							<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addpay')|escape:'html'}?editrowx={$di.id_directory}" class="directortableeditbu">Edit</a>
						{else}
							<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addfree')|escape:'html'}?editrowx={$di.id_directory}" class="directortableeditbu">Edit</a>
						{/if}
							<br>
							<a href="#" style="background:red;" class="directortableeditbu" href="#"   onclick="removedirectoryid({$di.id_directory});return false;" >Delete</a>
					</td>
					
					{if $di.pay == 1 && $active_subscription == 0}
					<tr>
						<td colspan="9"  style="padding:0;">
							<p class="alert alert-danger"  style="margin:0;">Your products are not visible in store. Please purchase a subscription. 
							<a class="button" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'premium')}">Buy now.</a><a></a></p>
						</td>
					</tr>
					{/if}
				</tr>
				
				
				
				{/foreach}
				
				
				</tbody>
			 </table>
		
			 <div class="clearfix">
			<!--  <a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addfree')|escape:'html'}" class="addtodirectonmy">Add Free listing</a> -->
			 <a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addpay')|escape:'html'}" class="addtodirectonmy">Add Premium Listing</a>
			 </div>
			 
		</div>
	</div>
</div>


<script>
function removedirectoryid(value){


	if (confirm("Are you sure?") == true) {
		window.location.replace('{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'mypro')|escape:'html'}?removedirectoryx='+value);
	} else {
		alert('cancel');
	}
}
</script>


{*****
<pre>
{$directory_rows|@print_r}
</pre>
*****}


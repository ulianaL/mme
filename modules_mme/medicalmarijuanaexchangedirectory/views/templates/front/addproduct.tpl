{if $edit or $addpro}


			{if $addpro}
				{capture name=path}
				<span class="navigation_page">Add Product</span>
				{/capture}
			{else}
				{capture name=path}
				<span class="navigation_page">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a itemprop="url" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addproduct')|escape:'html'}" title="Shop">
				<span itemprop="title">My Store</span>
				</a>
				</span>
				<span class="navigation-pipe">&gt;</span>
				Edit Product - {$pro->name.1}</span>
				{/capture}
			{/if}


<script>
$(document).ready(function(e) {
	$('.hiddeonload').fadeIn( 500, function() {
		// Animation complete
	  });
});
</script>
			
			
			
			
<div class="container hiddeonload"  style="display:none">
	<div class="col-sm-12 rtp">
		<form action="#" method="post" enctype="multipart/form-data">
			
			{if !$addpro}
			<h2>Edit Product</h2>
			{else}
			<h2>Add Product</h2>
			{/if}
			
			
			
			
			
			
			<div class="form-group" style="background:red;padding: 15px;">
					   <div class="checkbox clearfix">
						   <input name="ismedical"  id="checkbox33" class="styled" type="checkbox" {if isset($pro->ismedical)}{if $pro->ismedical == 1}checked{/if}{else}checked{/if}>
						   <label for="checkbox33" style="color:white;font-size:15px">This product is Medical?</label>
					   </div> 
			</div>
			
			{*<div class="form-group">
				<label>Shipping cost:</label>
				<input type="text" value="{if isset($pro->additional_shipping_cost)}{$pro->additional_shipping_cost}{/if}" name="additional_shipping_cost" class="form-control"/>
			</div>*}
			
			<div class="form-group">
				<label>Name</label>
				<input type="text" value="{if isset($pro->name.1)}{$pro->name.1}{/if}" name="name" class="form-control"/>
			</div>
			
			
			<div class="form-group">
				<label>Reference</label>
				<input type="text" value="{if isset($pro->reference)}{$pro->reference}{/if}" name="reference" class="form-control"/>
			</div>
			
			<div class="form-group">
				<label>Price</label>
				<div class="input-group-addon">$USD</div>
				<input type="text" value="{if isset($pro->price)}{$pro->price}{/if}" name="price" class="form-control"/>
			</div>
			{if isset($user) && $user->dispensaries == '1'}
				<div class="form-group attributes-box">
					<label>{l s='Attributes'}</label>
					<div>
						{* <pre>{print_r($user)}</pre> *}
						<input type="hidden" name="attribute_type" value="weight">
						<input type="hidden" name="attribute_unit" value="">
						{* <label>
							<input type="radio" name="attribute_type" value="weight"> <span>{l s='Weight'}</span>
						</label> *}
						{* <label>
							<input type="radio" name="attribute_type" value="size"> <span>{l s='Size'}</span>
						</label> *}

						<ul class="attibutes-tab-box">
							<li class="{if $pro->attribute_unit == 'oz' || !$pro->attribute_unit}active{/if}" onclick="showAttributesList('oz')">{l s='Oz'}</li>
							<li class="{if $pro->attribute_unit == 'g'}active{/if}" onclick="showAttributesList('g')">{l s='G'}</li>
							<li class="{if $pro->attribute_unit == 'item'}active{/if}" onclick="showAttributesList('item')">{l s='Item'}</li>
						</ul>

						<div class="weight-box" style="display: block;">
							<table>
								{* <thead>
									<tr>
										<th>{l s='Value'}</th>
										<th style="width: 243px">{l s='Custom value'}</th>
										<th>{l s='Additional price'}</th>
										<th></th>
									</tr>
								</thead> *}
								<tbody>

									{foreach from=$weight_attribues item="_type" key="weight_type"}
										{foreach from=$_type item="_attr"}
											<tr class="weight-{$weight_type}">
												<td>{$_attr.name}</td>
												<td>
													<input type="text" name="weight_price_value[{$_attr.id_attribute}]" class="form-control" value="{if isset($combinations[$_attr.id_attribute])}{$combinations[$_attr.id_attribute]['price']}{elseif isset($attrinute_prices[$_attr.id_attribute])}{$attrinute_prices[$_attr.id_attribute]}{/if}">
												</td>
											</tr>
										{/foreach}
									{/foreach}
									{* <tr>
										<td>
											<select name="weight_attr[]" class="attr-value" onchange="showCustomFiled(this);">
												<option value="custom">{l s='Custom value'}</option>
												{foreach from=$weight_attributes item=attr}
													<option value="{$attr.id_attribute|intval}">{$attr.name}</option>
												{/foreach}
											</select>
										</td>
										<td>
											<input type="text" name="weight_value[]" class="form-control custom-value">
										</td>
										<td>
											<input type="number" name="weight_price_value[]" class="form-control price-value" value="0.0" step="0.1" min="0">
										</td>
										<td>
											<a href="#" class="btn btn-default" onclick="addAttributeRow('.weight-box', this);return false;" title="{l s='Add new value'}">+</a>
										</td>
									</tr> *}
									<tr class="weight-item">
										<td>{l s='per Item'}</td>
										<td>
											<input type="text" name="weight_price_value[price]" class="form-control" value="{if isset($pro) && is_object($pro) && $pro->price}{$pro->price}{/if}">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						{* <div class="size-box" style="display: none;">
							<table>
								<thead>
									<tr>
										<th>{l s='Value'}</th>
										<th style="width: 243px">{l s='Custom value'}</th>
										<th>{l s='Additional price'}</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<select name="size_attr[]" class="attr-value" onchange="showCustomFiled(this);">
												<option value="custom">{l s='Custom value'}</option>
												{foreach from=$size_attributes item=attr}
													<option value="{$attr.id_attribute|intval}">{$attr.name}</option>
												{/foreach}
											</select>
										</td>
										<td>
											<input type="text" name="size_value[]" class="form-control custom-value">
										</td>
										<td>
											<input type="number" name="size_price_value[]" class="form-control price-value" value="0.0" step="0.1" min="0">
										</td>
										<td>
											<a href="#" class="btn btn-default" onclick="addAttributeRow('.size-box', this);return false;" title="{l s='Add new value'}">+</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div> *}

						{* <p class="alert alert-warning">
							{l s='If you select "Custom value" and do not write this in special field, this attribute will be ignored by system.'}
						</p> *}
					</div>

					<script type="text/javascript">
						function showAttributesList (type) {
							$(".weight-box tbody tr").hide();
							$("tr.weight-" + type).show();
							$('input[name="attribute_unit"]').val(type);
						}
						function addAttributeRow (selector, el) {
							var tr = $(el).closest('tr').first().clone(),
								box = $(selector + ' table tbody').first();
							box.append(tr);
							tr.find('input').val('').show()
							tr.find('select').val('custom');
							tr.find('td').last().find('.btn-danger').length == 0 && tr.find('td').last().append($('<a href="#" class="btn btn-danger" onclick="$(this).closest(\'tr\').remove();return false;" title="Remove this attribute">&times;</a>'));
						}
						function showCustomFiled(el) {
							el.value == 'custom' ?
								$(el).closest('tr').first().find('.custom-value').show() :
									$(el).closest('tr').first().find('.custom-value').hide();
						}
						$(function(){
							$(".attibutes-tab-box li").on('click', function (e) {
								$(this).parent().find('li.active').removeClass('active');
								$(this).addClass('active');
							});
							$(".attibutes-tab-box li.active").trigger('click');
							$('[name="attribute_type"]').on('change', function() {
								var attribute_type = $('[name="attribute_type"]:checked').val();
								if (attribute_type == 'weight') {
									$(".weight-box").show();
									$(".size-box").hide();
								} else if (attribute_type == 'size') {
									$('.size-box').show();
									$('.weight-box').hide();
								}
							});
						});
					</script>

					<style type="text/css">
						.attributes-box {
							    background-color: #fafafa;
						}
						.attributes-box > div > label input[type="radio"] {
							margin-top: 5px;
							/*display: flex;
							align-items: center;*/
						}

						.attributes-box select{
							    display: block;
						    width: 100%;
						    height: 34px;
						    padding: 6px 12px;
						    font-size: 14px;
						    line-height: 1.42857143;
						    color: #555;
						    background-color: #fff!important;
						    background-image: none;
						    border: 1px solid #ccc;
						    border-radius: 4px;
						    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
						    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
						    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
						    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
						    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
						}
						.attibutes-tab-box {
							    width: 300px;
						    display: flex;
						    justify-content: flex-start;
						    align-items: center;
						}
						.attibutes-tab-box li {
							    flex-basis: 33%;
						    text-align: center;
						    border: 1px solid #56b646;
						    border-top-width: 2px;
						    border-bottom-width: 2px;
						    cursor: pointer;
						        color: #000;
    						line-height: 27px;
						}
						.attibutes-tab-box li.active {
							color: #FFF;
							background: #56b646;
						}
						.attibutes-tab-box li:first-child {
							border-left-width: 2px;
						    border-top-left-radius: 5px;
						    border-bottom-left-radius: 5px;
						}
						.attibutes-tab-box li:last-child {
							border-right-width: 2px;
						    border-top-right-radius: 5px;
						    border-bottom-right-radius: 5px;
						}
						.weight-box tbody tr {
							display: none;
						}
					</style>
				</div>
			{/if}
			
			<script src="//tinymce.cachefly.net/4.3/tinymce.min.js"></script>
			<script>
			
			tinymce.init({
			selector:'#textarea',
		
			});
			</script>
			{if isset($user) && $user->dispensaries == '1'}
				<div class="form-group">
					<label>Brand</label>
					<input type="text" value="{if isset($pro->brand)}{$pro->brand}{/if}" name="brand" class="form-control"/>
				</div>
				<div class="form-group">
					<label>THC</label>
					<input type="text" value="{if isset($pro->thc)}{$pro->thc}{/if}" name="thc" class="form-control"/>
				</div>
				<div class="form-group">
					<label>CBD</label>
					<input type="text" value="{if isset($pro->cbd)}{$pro->cbd}{/if}" name="cbd" class="form-control"/>
				</div>
				<div class="form-group">
					<label>Strain</label>
					<input type="text" value="{if isset($pro->strain)}{$pro->strain}{/if}" name="strain" class="form-control"/>
				</div>
			{/if}
			
			<div class="form-group">
			<label>Short description:</label>
			<textarea name="description_short" id="textarea">{if isset($pro->description_short.1)}{$pro->description_short.1}{/if}</textarea>	
			</div>
			
			<script>
			
			tinymce.init({
			selector:'#textarea2',
			plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
    ],
			});
			</script>
			
			<div class="form-group">
			<label>Description:</label>
			<textarea name="description" id="textarea2">{if isset($pro->description.1)}{$pro->description.1}{/if}</textarea>	
			</div>
			
			
			
			
			
			
			
			
			  {**************************
			<div class="form-group ">
				<label class="control-label">{l s='Categories'}</label>
				<div class="col-md-12 cats" style="border: 1px solid #d6d4d4;">
						{$htmlcat|escape:"quotes":'UTF-8'}
				</div>
			</div>
			*******************}
	

{* <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script> *}
{* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" /> *}


<div class="cat2"  style="    background: #fafafa;padding: 25px; margin: 21px 0;">
	<h2>Categories:</h2>
	{$categories_tree_html}
	{if false}
		{literal}
		<div class="tree" id="full">
			<ul>
				
				<li  id="226" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Equipment
					<ul>
						<li id="252" data-jstree='{ "selected" : false, "type" : "folder" }'>Climate Control
							<ul>
								<li id="266" data-jstree='{ "selected" : false, "type" : "folder" }'>Fan & Filters</li>
								<li id="267" data-jstree='{ "selected" : false, "type" : "folder" }'>Blade Fans</li>
								<li id="268" data-jstree='{ "selected" : false, "type" : "folder" }'>Ducting, Vents, & Silencers</li>
								<li id="269" data-jstree='{ "selected" : false, "type" : "folder" }'>CO2 Supplies</li>
								<li id="270" data-jstree='{ "selected" : false, "type" : "folder" }'>Controllers</li>
							</ul>
						</li>
						<li id="253" data-jstree='{ "selected" : false, "type" : "folder" }'>Complete Grow Packages</li>
						<li id="254" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Lights
							<ul>
								<li id="271" data-jstree='{ "selected" : false, "type" : "folder" }'>400 W Grow Lights Kit</li>
								<li id="272" data-jstree='{ "selected" : false, "type" : "folder" }'>600 W Grow Lights Kit</li>
								<li id="273" data-jstree='{ "selected" : false, "type" : "folder" }'>1000 W Grow Lights Kit</li>
								<li id="274" data-jstree='{ "selected" : false, "type" : "folder" }'>LED Grow Lights</li>
								<li id="275" data-jstree='{ "selected" : false, "type" : "folder" }'>Fluorescents Grow Lights</li>
								<li id="276" data-jstree='{ "selected" : false, "type" : "folder" }'>Ballast & Bulbs</li>
								<li id="277" data-jstree='{ "selected" : false, "type" : "folder" }'>Lighting Accesories</li>
							</ul>					
						</li>
						<li id="255" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Essentials
							<ul>
								<li id="278" data-jstree='{ "selected" : false, "type" : "folder" }'>Reflective Materials</li>
								<li id="279" data-jstree='{ "selected" : false, "type" : "folder" }'>Pesticides</li>
								<li id="280" data-jstree='{ "selected" : false, "type" : "folder" }'>Pots & Containers
									<ul>
										<li id="305" data-jstree='{ "selected" : false, "type" : "folder" }'>Prune Pots</li>
										<li id="306" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Bags</li>
										<li id="307" data-jstree='{ "selected" : false, "type" : "folder" }'>Peat Pots</li>
										<li id="308" data-jstree='{ "selected" : false, "type" : "folder" }'>Thermoformed Pots</li>
										<li id="309" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Lids</li>
										<li id="310" data-jstree='{ "selected" : false, "type" : "folder" }'>Reservoirs & Trays</li>
									</ul>							
								</li>
								<li id="281" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Mediums</li>
								<li id="282" data-jstree='{ "selected" : false, "type" : "folder" }'>Meters & Testers</li>
								<li id="283" data-jstree='{ "selected" : false, "type" : "folder" }'>Sprayers</li>
								<li id="284" data-jstree='{ "selected" : false, "type" : "folder" }'>Timers</li>
								<li id="285" data-jstree='{ "selected" : false, "type" : "folder" }'>Trimming Shears & Scalpels</li>
								<li id="286" data-jstree='{ "selected" : false, "type" : "folder" }'>Measuring Utensils</li>
								<li id="287" data-jstree='{ "selected" : false, "type" : "folder" }'>Microscopes</li>
								<li id="288" data-jstree='{ "selected" : false, "type" : "folder" }'>Sticky Traps</li>
								<li id="289" data-jstree='{ "selected" : false, "type" : "folder" }'>Weather Stations</li>
								<li id="290" data-jstree='{ "selected" : false, "type" : "folder" }'>Plant Support</li>
							</ul>
						</li>
						<li id="256" data-jstree='{ "selected" : false, "type" : "folder" }'>Grow Tents</li>
						<li id="257" data-jstree='{ "selected" : false, "type" : "folder" }'>Harvest
							<ul>
								<li id="300" data-jstree='{ "selected" : false, "type" : "folder" }'>Leaf Trimmers</li>
								<li id="301" data-jstree='{ "selected" : false, "type" : "folder" }'>Drying Racks</li>
								<li id="302" data-jstree='{ "selected" : false, "type" : "folder" }'>Extraction Bags, Screens</li>
								<li id="303" data-jstree='{ "selected" : false, "type" : "folder" }'>Zipper Washing Bag</li>
								<li id="304" data-jstree='{ "selected" : false, "type" : "folder" }'>Bags & Storage</li>
							</ul>					
						</li>
						<li id="258" data-jstree='{ "selected" : false, "type" : "folder" }'>Hydroponics
							<ul>
								<li id="298" data-jstree='{ "selected" : false, "type" : "folder" }'>Hydroponic Systems</li>
								<li id="299" data-jstree='{ "selected" : false, "type" : "folder" }'>Water Treatment Units</li>
							</ul>					
						</li>
						<li id="259" data-jstree='{ "selected" : false, "type" : "folder" }'>Nutrients
							<ul>
								<li id="291" data-jstree='{ "selected" : false, "type" : "folder" }'>Feeding Packages</li>
								<li id="292" data-jstree='{ "selected" : false, "type" : "folder" }'>Base Nutrients</li>
								<li id="293" data-jstree='{ "selected" : false, "type" : "folder" }'>Bloom Boosters</li>
								<li id="294" data-jstree='{ "selected" : false, "type" : "folder" }'>Additives & Supplements</li>
								<li id="295" data-jstree='{ "selected" : false, "type" : "folder" }'>Biological Inoculants</li>
								<li id="296" data-jstree='{ "selected" : false, "type" : "folder" }'>Finishing Solution</li>
								<li id="297" data-jstree='{ "selected" : false, "type" : "folder" }'>Sweeteners</li>
							</ul>					
						</li>
						<li id="260" data-jstree='{ "selected" : false, "type" : "folder" }'>Propagation
							<ul>
								<li id="261" data-jstree='{ "selected" : false, "type" : "folder" }'>Cultilene Rockwool</li>
								<li id="262" data-jstree='{ "selected" : false, "type" : "folder" }'>Accel-A-Root Starter Plugs</li>
								<li id="263" data-jstree='{ "selected" : false, "type" : "folder" }'>Neoprene Inserts</li>
								<li id="264" data-jstree='{ "selected" : false, "type" : "folder" }'>Heating Mats</li>
								<li id="265" data-jstree='{ "selected" : false, "type" : "folder" }'>Cloning Trays, Gels</li>
							</ul>					
						</li>
						<li id="561" data-jstree='{ "selected" : false, "type" : "folder" }'>Used Equiptment</li>
					</ul>
				</li>
				
				
				<li  id="250" data-jstree='{ "selected" : false, "type" : "folder" }'>Accessories
					<ul>
						<li id="315" data-jstree='{ "selected" : false, "type" : "folder" }'>Extraction</li>
						<li id="317" data-jstree='{ "selected" : false, "type" : "folder" }'>Storage</li>
						<li id="318" data-jstree='{ "selected" : false, "type" : "folder" }'>Cleaning Tools</li>
						<li id="319" data-jstree='{ "selected" : false, "type" : "folder" }'>Grinders</li>
						<li id="320" data-jstree='{ "selected" : false, "type" : "folder" }'>Safe Cans</li>
						<li id="321" data-jstree='{ "selected" : false, "type" : "folder" }'>Scales</li>
						<li id="322" data-jstree='{ "selected" : false, "type" : "folder" }'>Cleansing Aids</li>
						<li id="323" data-jstree='{ "selected" : false, "type" : "folder" }'>Carrying Cases</li>
						<li id="324" data-jstree='{ "selected" : false, "type" : "folder" }'>Ash Trays/ Trays</li>
						<li id="325" data-jstree='{ "selected" : false, "type" : "folder" }'>Papers & Blunt Wraps</li>
						<li id="326" data-jstree='{ "selected" : false, "type" : "folder" }'>Water Pipes</li>
						<li id="327" data-jstree='{ "selected" : false, "type" : "folder" }'>Pipes</li>
						<li id="328" data-jstree='{ "selected" : false, "type" : "folder" }'>Vaporizers</li>
						<li id="330" data-jstree='{ "selected" : false, "type" : "folder" }'>Incense</li>
						<li id="331" data-jstree='{ "selected" : false, "type" : "folder" }'>Books</li>
						<li id="332" data-jstree='{ "selected" : false, "type" : "folder" }'>Dvds</li>
						<li id="333" data-jstree='{ "selected" : false, "type" : "folder" }'>Magazine</li>
						<li id="1249" data-jstree='{ "selected" : false, "type" : "folder" }'>Clothing
							<ul>
								<li id="1250" data-jstree='{ "selected" : false, "type" : "folder" }'>Womens Clothing</li>
								<li id="1251" data-jstree='{ "selected" : false, "type" : "folder" }'>Mens Clothing</li>
							</ul>					
						</li>
					</ul>
				</li>
				
				
				<li  id="564" data-jstree='{ "selected" : false, "type" : "folder" }'>Hemp
					<ul>
						<li id="1434" data-jstree='{ "selected" : false, "type" : "folder" }'>Products</li>
						<li id="1435" data-jstree='{ "selected" : false, "type" : "folder" }'>Service</li>
					</ul>
				</li>
				
				
				<li  id="1232" data-jstree='{ "selected" : false, "type" : "folder" }'>Wellness & Holistic
					<ul>
						<li id="1428" data-jstree='{ "selected" : false, "type" : "folder" }'>Herbalist</li>
						<li id="1429" data-jstree='{ "selected" : false, "type" : "folder" }'>Naturopathy</li>
						<li id="1430" data-jstree='{ "selected" : false, "type" : "folder" }'>Yoga</li>
						<li id="1431" data-jstree='{ "selected" : false, "type" : "folder" }'>Reiki</li>
						<li id="1432" data-jstree='{ "selected" : false, "type" : "folder" }'>Spa Treatments</li>
					</ul>
				</li>
				
				
				<li  id="1326" data-jstree='{ "selected" : false, "type" : "folder" }'>Travel</li>
				
				
				<li  id="1437" data-jstree='{ "selected" : false, "type" : "folder" }'>Wholesale</li>
				
				
				<li  id="1402" data-jstree='{ "selected" : false, "type" : "folder" }'>Dispensaries
					<ul>
						<li id="1433" data-jstree='{ "selected" : false, "type" : "folder" }'>Delivery</li>
						<li id="1436" data-jstree='{ "selected" : false, "type" : "folder" }'>Walk-in</li>
					</ul>
				</li>
				
				
				<li  id="1427" data-jstree='{ "selected" : false, "type" : "folder" }'>Schools and training</li>

				
				<li  id="1426" data-jstree='{ "selected" : false, "type" : "folder" }'>Business Services</li>
				
				
				<li  id="1420" data-jstree='{ "selected" : false, "type" : "folder" }'>Events</li>


				<li  id="1423" data-jstree='{ "selected" : false, "type" : "folder" }'>Doctors</li>
				
				
				<li  id="1409" data-jstree='{ "selected" : false, "type" : "folder" }'>Medical Products & Recreational Products
					<ul>
						<li id="1410" data-jstree='{ "selected" : false, "type" : "folder" }'>Indica</li>
						<li id="1411" data-jstree='{ "selected" : false, "type" : "folder" }'>Sativa</li>
						<li id="1412" data-jstree='{ "selected" : false, "type" : "folder" }'>Hybrid</li>
						<li id="1413" data-jstree='{ "selected" : false, "type" : "folder" }'>Concentrate</li>
						<li id="1414" data-jstree='{ "selected" : false, "type" : "folder" }'>Edible</li>
						<li id="1415" data-jstree='{ "selected" : false, "type" : "folder" }'>Topicals</li>
						<li id="1418" data-jstree='{ "selected" : false, "type" : "folder" }'>Preroll</li>
						<li id="1419" data-jstree='{ "selected" : false, "type" : "folder" }'>Wax</li>
						<li id="1422" data-jstree='{ "selected" : false, "type" : "folder" }'>Drink</li>
						<li id="1442" data-jstree='{ "selected" : false, "type" : "folder" }'>Tincture</li>
						<li id="1443" data-jstree='{ "selected" : false, "type" : "folder" }'>Gear</li>
					</ul>
				</li>
				
			</ul>
		</div>
		{/literal}
	{/if}
	
	
	
	
	
		
<input  type="hidden" name="newcatinput" value="{if !$addpro}{foreach from=$cats item=ca}{$ca}#{/foreach}{/if}"  id="newcatinput">

				
</div>






		{* {if !$addpro}
			<script>
			function fillcategories(){
			
			
			{foreach from=$cats item=ca}
		        $('#full').jstree(true).check_node([{$ca}]);
				
				
				$('#full').jstree(true)._open_to({$ca});

			{/foreach}
			
			}
			
			
			
			
			</script>
		{/if} *}	
		
	{literal}	
<script>
/*
down vote
In the last version (3.0), the API was changed.

If you need just array of selected IDs (like in examples in this node), it is now very easy:

var selectedElmsIds = $('#tree').jstree("get_selected");
If you need to iterate over the selected elements, you just need to pass additional "true" parameter.
var selectedElmsIds = [];
var selectedElms = $('#tree').jstree("get_selected", true);
$.each(selectedElms, function() {
    selectedElmsIds.push(this.id);
});
	
*/	
	
	
// $('#full').jstree({
//  plugins : ["checkbox","sort","types","wholerow"],
//  "types" : { "file" : { "icon" : "jstree-file" } },
//   "ui": {
           
//             "selected_parent_close":false
//         },
//  checkbox: {       
//       cascade_to_disabled  : true, 
//       three_state : false, // to avoid that fact that checking a node also check others
//       whole_node : false,  // to avoid checking the box just clicking the node 
//       tie_selection : false // for checking without selecting and selecting without checking
//     },
//     'core': {
//             'multiple': false,
//         },
//  	'checkbox' : {            
//             'deselect_all': true,
//              'three_state' : false, 
//         }
//  })
//  .on("check_node.jstree uncheck_node.jstree", function(e, data) {
      
//     obj = $("#full").jstree(true).get_checked()
//     var str = '';
//     for (var p in obj) {
//         if (obj.hasOwnProperty(p)) {
          
// 			str += obj[p]+'#';
//         }
//     }
//      $('#newcatinput').val(str);
  
//   })
//   .on("changed.jstree", function(e, data) {
//      $('#newcatinput').val( $("#full").jstree(true).get_checked()[0])


//   //alert('test change value')
//   })
  
  
  
  {/literal}{* {if !$addpro}.on('ready.jstree'), fillcategories(){/if} *}{literal} 
  
  
  
</script>
{/literal}




	   
			
		
	
{if $cookie->id_customer == '77'}	
	<div class="form-group">
		<label>Your store prduct link</label>
		<input type="text" value="{if isset($pro->own_store_link)}{$pro->own_store_link}{/if}" name="own_store_link" class="form-control">
	</div>
{/if}	

	
{include file="$includedir/mapform.tpl"}	

	
<script type="text/javascript" async src="{$js_dir}plugin2.js"></script>



 



 
 
 
 


			
			<div class="form-group">
				<label>AVAILABLE QUANTITIES FOR SALE</label>
				<input type="text" value="{if isset($stock)}{$stock}{/if}" name="qt" class="form-control"/>
			</div>
			
			
			
			
		

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/all.fine-uploader/fine-uploader-gallery.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/all.fine-uploader/fine-uploader.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/all.fine-uploader/fine-uploader-new.css" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/all.fine-uploader/fine-uploader-gallery.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/jquery.fine-uploader/jquery.fine-uploader.js"></script>



			{if !$addpro}
				<div class="form-group">
					<label>PICTURES: <span class="size_info" style="font-size: 15px;margin-left: 10px;font-weight: normal;">(recomended size of images 500x500px)</span></label>
						<div class="" style="position: relative;min-height: 200px;max-height: 490px;overflow-y: hidden;width: inherit;border-radius: 6px;border: 1px dashed #ccc;background-color: #fafafa;padding: 20px;">
						{foreach from=$propics item=pics}
							<div class="col-md-3">
								<img src="http://{$pics.link}" class="img-responsive"/>
								<i idpicture="{$pics.imaid}" style="font-size: 26px;position: absolute; bottom: 7px;left: 20px;color: red;" class="fa fa-trash removepic" aria-hidden="true"></i>
							</div>
						{/foreach}
					</div>
				</div>
			{/if}
			
			

<!-- Fine Uploader Gallery template   24/09/2018 lll

    ====================================================================== -->
    <script type="text/template" id="qq-template-gallery">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                
				
				
				
				
				<li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                        Retry
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
						<span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>


    <!-- Fine Uploader DOM Element
    ====================================================================== -->
    <div id="fine-uploader-gallery"></div>

    <!-- Your code to create an instance of Fine Uploader and bind to the DOM/template
    ====================================================================== -->
    <script>
        
		$('#fine-uploader-gallery').fineUploader({
            template: 'qq-template-gallery',
            request: {
               {if $addpro || $edit}
			   endpoint: "{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addproduct')|escape:'html'}?addproduct&unique={$uniqueId}"
				{else}
				 endpoint: "#"
				{/if}

		   },
            /*
			thumbnails: {
                placeholders: {
                    waitingPath: 'https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.14.2/all.fine-uploader/placeholders/waiting-generic.png',
                    notAvailablePath: '/source/placeholders/not_available-generic.png'
                }
            },*/
			
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            },
			callbacks: {
				onError: function(id, name, errorReason, xhrOrXdr) {
					alert(qq.format("Error on file number {} - {}.  Reason: {}", id, name, errorReason));
				}
			},
			debug: true,
        });
		
		
$('.removepic').click(function(){	
if (confirm('Are you sure?')) {
    var url = "#"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: 'delete='+$(this).attr('idpicture'), // serializes the form's elements.
           success: function(data)
           {
            //   alert(data); // show response from the php script.
			   $("[idpicture='"+data+"']").parent().hide('slow', function(){ $("[idpicture='"+data+"']").remove(); });
           }
         });

    return false; // avoid to execute the actual submit of the form.
}
});	

</script>


	<input type="hidden" name="uniqueId" value="{if isset($uniqueId)}{$uniqueId}{/if}"/>
	{if !$addpro}
	<input class="button button_large" name="editproduct" value="SAVE" type="submit"/>
	{else}
	<input id="addproductinput" class="button button_large" name="addproductinput" value="ADD" type="submit"/>
	{/if}
		</form>
	</div>
</div>
{*SoftSprint Uliana*}
<script type="text/javascript">
	$('#addproductinput').click(function(){
		if ($('.filename').text() == 'No file selected') {
			alert("Please upload the product image!");
			return false;
		}
	});
	
</script>









{else}


{capture name=path}
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<span class="navigation_page">My products</span>
{/capture}

<div class="allmystore">
  <h2 class="toggle_title font_baskervile">My products</h2>
  <div class="toggle_arrea">
  <div class="table-responsive">
  <table class="table table-responsive">
    <thead class="products_table_header">
      <tr>
        <th>ID</th>
        <th>Photo</th>
        <th>Link</th>
        <th>Reference</th>
        <th>Category</th>
        <th>Base price</th>
        <th>Quantity</th>
        <th>Featured</th>
		
        <th style="max-width: 40px;text-align:center;">Status</th>
        <th style="min-width: 55px;text-align:center;">{*Approved*}</th>
		<th style="max-width: 55px;text-align:center;" >{*Edit*}</th>
      </tr>
    </thead>
	<tbody>
		{***$products|@print_r****}
		{foreach from=$products item=pro}
			<tr style="border-top:1px solid #DADADA;">
				<td>
					{$pro.id_product}
				</td>
       			<td>
				   <img style="height: 87px;margin:0 auto; display:block;" src="//{$pro.imgcover}" />
				</td>
       			<td>
					<a href="{$link->getProductLink($pro.id_product)}">
						Link
					</a>
				</td>
				<td>
					{$pro.reference}
				</td>
				<td>
					{$pro.catname}
				</td>
  	    		<td>
				  ${$pro.price|number_format:2:",":"."}
				</td>
        		<td>
					{$pro.quantity}
				</td>
        
				<td>
					{if $pro.active == 0}
						{*<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/disabled.gif"/>*}
						<img  style="margin:0 auto; display:block; width: 10px;" src="{$img_ps_dir}admin/cross_red.svg"/>
					{else}
						{*<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/enabled.gif"/>*}
						<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/confirmed_chek_green.svg"/>
					{/if}
				</td> 
		
				<td>
					{if $pro.review == 0}
						{*<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/disabled.gif"/>*}
						<img  style="margin:0 auto; display:block; width: 10px;" src="{$img_ps_dir}admin/cross_red.svg"/>
					{else}
						{*<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/enabled.gif"/>*}
						<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/confirmed_chek_green.svg"/>
					{/if}
				</td>
				<td class="table_buttons_block">
					<a class="button btn_primary btn-block table__buttons" href="/index.php?fc=module&amp;module=medicalmarijuanaexchangedirectory&amp;controller=addproduct&amp;edit={$pro.id_product}">
						<img src="themes/default-bootstrap/img/edit_pencil_white.svg">
					</a>
					<a removepro="{$pro.id_product}" class="button btn_primary btn-block removepro table__buttons">
						<img src="themes/default-bootstrap/img/delete_button_white.svg">
					</a>
				</td>
				<td>
					<a class="button btn_primary btn-block calendar_button" onclick="openclosecal({$pro.id_product})" style="text-align:center;">
						Featured on<br>Home page
					</a>
					<div style="text-align: center;background: black;display:none" class="rnge range_{$pro.id_product}">
		
		</div>
		
		<div style="display:none;" class="form-group col-md-12 backgroundbbb   chofeatoopenclose chofeatoopenclose{$pro.id_product}">
			<h4 class="texttoselecttypeofii">Please select categorie:</h4>
			<select  style="width:150px" class="form-control" id="typeoffeatureseproducts{$pro.id_product}" name="typeoffeatureseproducts">
				
				<option value="1">Deals</option>
				<option value="2">Deals Nearby</option>
				<option value="3">Delivery</option> 
				<option value="4">Dispensary Storefronts</option>
				<option value="5">Doctors</option>
				<option value="6">Featured Brands</option>
				
				
				<option value="7">Medical</option>
				<option value="8">Events</option>
				

	




			</select>
		</div>
		
		<div class="datetofea datetofea_{$pro.id_product}"  style="display:none;background: #333; text-align: center;padding: 5px;color:white;">
		Please choose dates.
		</div>
		
		
		
		
		
		
		<div class="bbn bbn_{$pro.id_product}" sdate="0" enddate="0" onclick="submitpoints(this,{$pro.id_product})" needpoints="0"  style="cursor: pointer;display:none;background: rgb(91, 190, 143); text-align: center;padding: 5px;color:white;">
		Submit
		</div>
		
	
		
		
<script>	
$(document).ready(function(){
					
			
			pickmeup('.range_{$pro.id_product}', {
						flat : true,
						mode : 'range'
					})

					
			

             $('.range_{$pro.id_product}').on('pickmeup-change', function (e) {
				    //console.log(e.originalEvent); 
					
					
					
					//dates bettwen selection!!
					var date1 = e.originalEvent.detail.formatted_date[0]
					var date2 = e.originalEvent.detail.formatted_date[1]
	
	

					var date10 = e.originalEvent.detail.date[0]
					var date20 = e.originalEvent.detail.date[1]
					var timeDiff = Math.abs(date20.getTime() - date10.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))+1; 
					//alert(diffDays);

					
					
					
					var htmlhh = formatAmericanDate(date1)+' to '+formatAmericanDate(date2) +'<br>'+diffDays+' Days / Points';
				
					
					//calc need points 
					
					
					
					
					if ({$points} < diffDays){
						 htmlhh += '<br>You only have <b style="font-weight:900!important">{$points}</b>';
						 htmlhh += ' point avaliable';
						 htmlhh += '<br><a style="color:red" href="{$link->getProductLink(81)}">Buy points now.</a>';
					}
					
					//alert('.bbn_{$pro.id_product}')
					$('.bbn_{$pro.id_product}').attr('needpoints',diffDays);
					
					
					//sdate="0" enddate="0"
					$('.bbn_{$pro.id_product}').attr('sdate',date1);
					$('.bbn_{$pro.id_product}').attr('enddate',date2);
					
					
					
					$('.datetofea_{$pro.id_product}').html(htmlhh);
  
				})
				
				
				
				
				
				
					
})	
</script>
					<a class="button btn_primary btn-block calendar_button" onclick="openclosecal2({$pro.id_product})" style="text-align:center;">
						Featured on<br>Products page
					</a>
				</td>
			</tr>
			<tr id="new-category-feature-box{$pro.id_product|intval}" style="">
				<td class="" colspan="11" style="padding: 15px">
					<div class="row">
						<div class="col-sm-3">
							<div style="text-align: center;background: black;display:none" class="rnge2 range2_{$pro.id_product}"></div>
						</div>
			
						<div class="col-sm-9">
							<div style="display:none;" class="form-group col-md-12 chofeatoopenclose2 chofeatoopenclose2{$pro.id_product}">
								<h4 class="">Please select category:</h4>
								{$categories_tree_html}
							</div>

							<div class="datetofea2 datetofea2_{$pro.id_product}"  style="display:none;background: #333; text-align: center;padding: 5px;color:white;">Please choose dates.</div>
							<div class="bbn2 bbn2_{$pro.id_product}" sdate="0" enddate="0" onclick="submitpoints2(this, {$pro.id_product|intval})" needpoints="0"  style="cursor: pointer;display:none;background: rgb(91, 190, 143); text-align: center;padding: 5px;color:white;">Submit</div>

							<script type="text/javascript">
								$(document).ready(function() {
									pickmeup('.range2_{$pro.id_product}', {
										flat : true,
										mode : 'range'
									});
						            $('.range2_{$pro.id_product}').on('pickmeup-change', function (e) {
										var date1 = e.originalEvent.detail.formatted_date[0],
											date2 = e.originalEvent.detail.formatted_date[1],
											date10 = e.originalEvent.detail.date[0],
											date20 = e.originalEvent.detail.date[1],
											timeDiff = Math.abs(date20.getTime() - date10.getTime()),
											diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))+1,
											htmlhh = formatAmericanDate(date1) + ' to ' + formatAmericanDate(date2) +'<br>'+diffDays+' Days / Points';
											formatAmericanDate(date1);
										if ({$points|intval} < diffDays){
											 htmlhh += '<br>You only have <b style="font-weight:900!important">{$points|intval}</b>';
											 htmlhh += ' point avaliable';
											 htmlhh += '<br><a style="color:red" href="{$link->getProductLink(81)}">Buy points now.</a>';
										}
										$('.bbn2_{$pro.id_product}').attr('needpoints', diffDays);
										$('.bbn2_{$pro.id_product}').attr('sdate',date1);
										$('.bbn2_{$pro.id_product}').attr('enddate',date2);
										$('.datetofea2_{$pro.id_product}').html(htmlhh);
									});
								});
							</script>
						</div>
					</div>
				</td>
			</tr>
			{if count($pro.feat) != 0}
				<tr>
					<td data-id="{$pro.id_product}" class="featured__table collapse_table_header" colspan="11">
						Featured on Home page
						<img src="/themes/default-bootstrap/img/down-arrow_white.svg">
					</td>
				</tr>
				<tr>
					<td colspan="11" class="collapse_table_content collapse_table_content_{$pro.id_product}">
						<table>
							<thead>
								<tr>
									<th colspan="1">Start Date</th>
									<th colspan="1">End Date</th>
									<th colspan="1">Used Points</th>
									<th colspan="3">Category</th>
								</tr>
							</thead>
							<tbody>
								{foreach $pro.feat item=fea}
									<tr>
										<th colspan="1">
											{date('n/j/Y', strtotime($fea.start))}
										</th>
										<th colspan="1">
											{date('n/j/Y', strtotime($fea.end))}
										</th>
										<th colspan="1">
											{$fea.pointsused}
										</th>
										{if $fea.categoriehome == 1}
											<th colspan="3" >
												Deals
											</th>
										{/if}
										{if $fea.categoriehome == 2}
											<th colspan="3" >
												Deals Nearby
											</th>
										{/if}
										{if $fea.categoriehome == 3}
											<th colspan="3" >
												Delivery
											</th>
										{/if}
										
										{if $fea.categoriehome == 4}
											<th colspan="3" >
												Dispensary Storefronts
											</th>
										{/if}
										{if $fea.categoriehome == 5}
											<th colspan="3" >
												Doctor
											</th>
										{/if}
										{if $fea.categoriehome == 6}
											<th colspan="3" >
												Featured Brands
											</th>
										{/if}
										{if $fea.categoriehome == 7}
											<th colspan="3" >
												Medical
											</th>
										{/if}
										{if $fea.categoriehome == 8}
											<th colspan="3" >
												Events
											</th>
										{/if}
									</tr>
								{/foreach}
							</tbody>
						</table>
					</td>
				</tr>
			{/if}

			{if isset($category_features[$pro.id_product])}
				<tr>
					<td data-id="{$pro.id_product}" class="featured__table collapse_table_category_feature" colspan="11">
						Featured on Products page
						<img src="/themes/default-bootstrap/img/down-arrow_white.svg">
					</td>
				</tr>
				<tr>
					<td colspan="11" class="collapse_table_content collapse_table_category_feature_content_{$pro.id_product}">
						<table>
							<thead>
								<tr>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Used Points</th>
									<th>Category</th>
									<th style="width: 30px;"></th>
								</tr>
							</thead>
							<tbody>
								{foreach from=$category_features[$pro.id_product] item="category_feature"}
									<tr>
										<th>{date('n/j/Y', strtotime($category_feature.start))}</th>
										<th>{date('n/j/Y', strtotime($category_feature.end))}</th>
										<th>{$category_feature.points}</th>
										<th>{$category_feature.category_name}</th>
										<th>
											<a href="#" onclick="removeCategoryFeature(this, {$category_feature.id_category_product_feature|intval}); return false;" style="font-size: 20px; line-height: 18px;" title="{l s='Delete'}">&times;</a>
										</th>
									</tr>
								{/foreach}
							</tbody>
						</table>
					</td>
				</tr>
			{/if}
		{/foreach} 
	</tbody>
  </table>






  {*<table class="table table-responsive">
    <thead class="products_table_header">
      <tr>
        <th>ID</th>
        <th>Photo</th>
        <th>Link</th>
        <th>Reference</th>
        <th>Category</th>
        <th>Base price</th>
        <th>Quantity</th>
        <th>Featured</th>
		
        <th style="max-width: 40px;text-align:center;">Status</th>
        <th style="max-width: 55px;text-align:center;">Approved</th>
		<th style="max-width: 55px;text-align:center;" >Edit</th>
      </tr>
    </thead>
    <tbody>
     {***$products|@print_r****}
    {*{foreach from=$products item=pro}

	 <tr style="border-top:1px solid #DADADA;">
        <td>{$pro.id_product}</td>
       
	    <td><img style="height: 87px;margin:0 auto; display:block;" src="//{$pro.imgcover}" /></td>
       

	   <td>
	   <a href="{$link->getProductLink($pro.id_product)}">Link</a>
	   </td>
		
		<td>{$pro.reference}</td>
        <td>{$pro.catname}</td>
       
  	    <td>${$pro.price|number_format:2:",":"."}</td>
        <td>{$pro.quantity}</td>
        
		<td>
			{if $pro.active == 0}
			<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/disabled.gif"/>
			{else}
			<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/enabled.gif"/>
			{/if}
		</td> 
		
		<td>
			{if $pro.review == 0}
			<img  style="margin:0 auto; display:block" src="{$img_ps_dir}admin/disabled.gif"/>
			{else}
			<img style="margin:0 auto; display:block" src="{$img_ps_dir}admin/enabled.gif"/>
			{/if}
		</td>
		
		<td>
			<a style="max-width:85%;margin:0 auto; display:block;text-align: center;" class="button btn_primary btn-block table__buttons" href="/index.php?fc=module&module=medicalmarijuanaexchangedirectory&controller=addproduct&edit={$pro.id_product}">
				<img src="themes/default-bootstrap/img/edit_pencil_white.svg" />
			</a>
			<a removepro="{$pro.id_product}" style="max-width:85%;margin:0 auto; display:block;text-align: center;background: rgb(217, 92, 92);margin-top: 10px;" class="button btn_primary btn-block removepro table__buttons">
				<img src="themes/default-bootstrap/img/delete_button_white.svg" />
			</a>
		</td>
		
		<td>
		<a class="button btn_primary btn-block" onclick="openclosecal({$pro.id_product})" style="text-align:center;">Featured on<br>Home page</a>
		<div style="text-align: center;background: black;display:none" class="rnge range_{$pro.id_product}">
		
		</div>
		
		<div style="display:none;" class="form-group col-md-12 backgroundbbb   chofeatoopenclose chofeatoopenclose{$pro.id_product}">
			<h4 class="texttoselecttypeofii">Please select categorie:</h4>
			<select  style="width:150px" class="form-control" id="typeoffeatureseproducts{$pro.id_product}" name="typeoffeatureseproducts">
				
				<option value="1">Deals</option>
				<option value="2">Deals Nearby</option>
				<option value="3">Delivery</option> 
				<option value="4">Dispensary Storefronts</option>
				<option value="5">Doctors</option>
				<option value="6">Featured Brands</option>
				
				
				<option value="7">Medical</option>
				<option value="8">Events</option>
				

	




			</select>
		</div>
		
		<div class="datetofea datetofea_{$pro.id_product}"  style="display:none;background: #333; text-align: center;padding: 5px;color:white;">
		Please choose dates.
		</div>
		
		
		
		
		
		
		<div class="bbn bbn_{$pro.id_product}" sdate="0" enddate="0" onclick="submitpoints(this,{$pro.id_product})" needpoints="0"  style="cursor: pointer;display:none;background: rgb(91, 190, 143); text-align: center;padding: 5px;color:white;">
		Submit
		</div>
		
	
		
		
<script>	
$(document).ready(function(){
					
			
			pickmeup('.range_{$pro.id_product}', {
						flat : true,
						mode : 'range'
					})

					
			

             $('.range_{$pro.id_product}').on('pickmeup-change', function (e) {
				    //console.log(e.originalEvent); 
					
					
					
					//dates bettwen selection!!
					var date1 = e.originalEvent.detail.formatted_date[0]
					var date2 = e.originalEvent.detail.formatted_date[1]
	
	

					var date10 = e.originalEvent.detail.date[0]
					var date20 = e.originalEvent.detail.date[1]
					var timeDiff = Math.abs(date20.getTime() - date10.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))+1; 
					//alert(diffDays);

					
					
					
					var htmlhh = date1+' to '+date2 +'<br>'+diffDays+' Days / Points';
				
					
					//calc need points 
					
					
					
					
					if ({$points} < diffDays){
						 htmlhh += '<br>You only have <b style="font-weight:900!important">{$points}</b>';
						 htmlhh += ' point avaliable';
						 htmlhh += '<br><a style="color:red" href="{$link->getProductLink(81)}">Buy points now.</a>';
					}
					
					//alert('.bbn_{$pro.id_product}')
					$('.bbn_{$pro.id_product}').attr('needpoints',diffDays);
					
					
					//sdate="0" enddate="0"
					$('.bbn_{$pro.id_product}').attr('sdate',date1);
					$('.bbn_{$pro.id_product}').attr('enddate',date2);
					
					
					
					$('.datetofea_{$pro.id_product}').html(htmlhh);
  
				})
				
				
				
				
				
				
					
})	
</script>
		
		</td>
		
    </tr>
	
	
	
	{if count($pro.feat) != 0}
		<tr>
			<td data-id="{$pro.id_product}" class="featured__table collapse_table_header" colspan="11">
				Featured on Home page
				<img src="/themes/default-bootstrap/img/down-arrow_white.svg" />
			</td>
		</tr>
		<thead class="collapse_table_content collapse_table_content_{$pro.id_product}">
		  <tr>
			<th colspan="1" >Start Date</th>
			<th colspan="1" >End Date</th>
			<th colspan="1" >Used Points</th>
			<th colspan="3" >Categorie</th>
		  </tr>
		</thead>
		<tbody class="collapse_table_content collapse_table_content_{$pro.id_product}">
			{foreach $pro.feat item=fea}
				<tr class="collapse_table_content collapse_table_content_{$pro.id_product}">
					<th colspan="1">
						{$fea.start}
					</th>
					<th colspan="1">
						{$fea.end}
					</th>
					<th colspan="1">
						{$fea.pointsused}
					</th>
					{if $fea.categoriehome == 1}
						<th colspan="3" >
							Deals
						</th>
					{/if}
					{if $fea.categoriehome == 2}
						<th colspan="3" >
							Deals Nearby
						</th>
					{/if}
					{if $fea.categoriehome == 3}
						<th colspan="3" >
							Delivery
						</th>
					{/if}
					
					{if $fea.categoriehome == 4}
						<th colspan="3" >
							Dispensary Storefronts
						</th>
					{/if}
					{if $fea.categoriehome == 5}
						<th colspan="3" >
							Doctor
						</th>
					{/if}
					{if $fea.categoriehome == 6}
						<th colspan="3" >
							Featured Brands
						</th>
					{/if}
					{if $fea.categoriehome == 7}
						<th colspan="3" >
							Medical
						</th>
					{/if}
					{if $fea.categoriehome == 8}
						<th colspan="3" >
							Events
						</th>
					{/if}
				</tr>
			{/foreach}
		</tbody>



		{*<tr class="collapse_table_content collapse_table_content_{$pro.id_product}">
			{foreach $pro.feat item=fea}
				<tr>
					<th colspan="1">{$fea.start}</th>
					<th colspan="1" > {$fea.end}</tr>
					<th colspan="1" > {$fea.pointsused}</th>
					{if $fea.categoriehome == 1}
					<th colspan="3" >Deals</th>
					{/if}
					{if $fea.categoriehome == 2}
					<th colspan="3" >Deals Nearby</th>
					{/if}
					{if $fea.categoriehome == 3}
					<th colspan="3" >Delivery</th>
					{/if}
					
					{if $fea.categoriehome == 4}
					<th colspan="3" >Dispensary Storefronts</th>
					{/if}
					{if $fea.categoriehome == 5}
					<th colspan="3" >Doctor</th>
					{/if}
					{if $fea.categoriehome == 6}
					<th colspan="3" >Featured Brands</th>
					{/if}
					{if $fea.categoriehome == 7}
					<th colspan="3" >Medical</th>
					{/if}
					{if $fea.categoriehome == 8}
					<th colspan="3" >Events</th>
					{/if}
				</tr>
			{/foreach}
		</tr>*}
	{*{/if}


	  
	 {/foreach} 
    </tbody>
  </thead>
  </table>*}
  </div>
<div class="under_products_table_buttons">
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'addproduct')|escape:'html'}?addproduct" class="button btn_primary btn-block">
		Add Product
	</a>
	<a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'importcsv')|escape:'html'}" class="button btn_primary btn-block">
		Import products from CSV file
	</a>
</div>

</div>



<script>
function submitpointsFDS(values){
	

	
	typeofcategoriejava  =  $('#typeoffeaturese').val();
	needpoint = $(values).attr('needpoints');
	sdate = $(values).attr('sdate');
	enddate = $(values).attr('enddate');
	var url = "#"; // the script where you handle the form input.
	$.ajax({
           type: "POST",
           url: url,
           data: 'submitpointsFDS'+'&needpoint='+needpoint
				   +'&sdate='+sdate
				   +'&typeofcategoriejava='+typeofcategoriejava
				   +'&enddate='+enddate, 
           success: function(data)
           {
               if( data.indexOf('Thank you') >= 0){
			   
			   jQuery.fancybox({
					afterClose  : function() {
						location.reload();

					},
					'modal' : false,
					'content' : data,
					 
				});
				}else{
				jQuery.fancybox({
			
					'modal' : false,
					'content' : data,
					 
				});
				}
           }
         });
}
//pointsFDS



function submitpoints(se,productid){
	 
	 
	 //%%
	 typeoffeatureseproducts = $('#typeoffeatureseproducts'+productid).val();
	 needpoint = $(se).attr('needpoints');
	 
	 //sdate="0" enddate="0"
	 sdate = $(se).attr('sdate');
	 enddate = $(se).attr('enddate');
	
	//alert(needpoint);
	//alert(productid);
	var url = "#"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: 'submitpoints'+'&needpoint='+needpoint
		   +'&productid='+productid
		   +'&typeoffeatureseproducts='+typeoffeatureseproducts
		   +'&sdate='+sdate+'&enddate='+enddate, // serializes the form's elements.
           success: function(data)
           {
               if( data.indexOf('Thank you') >= 0){
			   
			   jQuery.fancybox({
					afterClose  : function() {
						location.reload();

					},
					'modal' : false,
					'content' : data,
					 
				});
				}else{
				jQuery.fancybox({
			
					'modal' : false,
					'content' : data,
					 
				});
				}
           }
         });
	
	
	
}

function submitpoints2(el, productid) {
	var box = $("#new-category-feature-box" + productid),
		button = $(el),
		category_input = box.find('#products-categories-list input:checked').first();

	if (category_input.length == 0 || +category_input.val() <= 0) {
		alert('Please select category');
		return;
	}

	if (box.length != 1) {
		return;
	}

	if (button.attr('sdate') == '0' || button.attr('enddate') == '0') {
		alert('Please select date');
		return;
	}

	$.ajax({
		type: "POST",
		url: '#',
		data: {
			submitProductCategoryFeature: 1,
			start: button.attr('sdate'),
			end: button.attr('enddate'),
			id_category: category_input.val(),
			id_product: productid
		},
		cache: false,
		async: true,
		dataType: 'json',
		success: function(data)
		{
			if (typeof data.hasError != 'undefined' && data.hasError) {
				jQuery.fancybox({
					'modal' : false,
					'content' : '<p>' + data.message + '</p>'
				});
			} else if (typeof data.status != 'undefined' && data.status == 'ok') {
				jQuery.fancybox({
					afterClose  : function() {
						location.reload();
					},
					'modal' : false,
					'content' : '<p>' + data.message + '</p>'
				});
			} else {
				jQuery.fancybox({
					'modal' : false,
					'content' : '<p>Unknown response</p>'
				});
			}
   //         if( data.indexOf('Thank you') >= 0){
		   
			//    jQuery.fancybox({
			// 		afterClose  : function() {
			// 			location.reload();

			// 		},
			// 		'modal' : false,
			// 		'content' : data,
					 
			// 	});
			// }else{
			// 	jQuery.fancybox({
			
			// 		'modal' : false,
			// 		'content' : data,
					 
			// 	});
			// }
       	},
       	error: function(xhr, textStatus, errorThrown) {
       		jQuery.fancybox({
				'modal' : false,
				'content' : '<p>' + "TECHNICAL ERROR:<br/>Details:<br/>&nbsp;&nbsp;Error thrown: " + (typeof xhr.responseText != 'undefined' ? xhr.responseText : '') + "<br/>&nbsp;&nbsp;" + 'Text status: ' + textStatus + '</p>'
			});
		}
     });
}

function removeCategoryFeature(el, id_feature) {
	if (!confirm('Are you sure ?')) {
		return;
	}

	$.ajax({
		type: "POST",
		url: '#',
		data: {
			deleteCategoryFeature: 1,
			id: id_feature
		},
		cache: false,
		async: true,
		dataType: 'json',
		success: function(data)
		{
			if (typeof data.hasError != 'undefined' && data.hasError) {
				jQuery.fancybox({
					'modal' : false,
					'content' : '<p>' + data.message + '</p>'
				});
			} else if (typeof data.status != 'undefined' && data.status == 'ok') {
				// jQuery.fancybox({
				// 	afterClose  : function() {
				// 		location.reload();
				// 	},
				// 	'modal' : false,
				// 	'content' : '<p>' + data.message + '</p>'
				// });
				$(el).closest('tr').remove();
			} else {
				jQuery.fancybox({
					'modal' : false,
					'content' : '<p>Unknown response</p>'
				});
			}
       	},
       	error: function(xhr, textStatus, errorThrown) {
       		jQuery.fancybox({
				'modal' : false,
				'content' : '<p>' + "TECHNICAL ERROR:<br/>Details:<br/>&nbsp;&nbsp;Error thrown: " + (typeof xhr.responseText != 'undefined' ? xhr.responseText : '') + "<br/>&nbsp;&nbsp;" + 'Text status: ' + textStatus + '</p>'
			});
		}
    });	
}

function formatAmericanDate (date) {
	var a = date.split('-');
	console.log(a);
	return (new Array(+a[1], +a[0], a[2])).join('/');
}

$('.collapse_table_header').on('click', function(){
	var id = $(this).attr('data-id');
	$('.collapse_table_category_feature').removeClass('active');
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$('.collapse_table_content_' + id).slideUp();
	} else {
		$('.collapse_table_header').removeClass('active');
		$(this).addClass('active');
		$('.collapse_table_content').slideUp();
		$('.collapse_table_content_' + id).slideDown();
	}
});
$('.collapse_table_category_feature').on('click', function(){
	var id = $(this).attr('data-id');
	$('.collapse_table_header').removeClass('active');
	if($(this).hasClass('active')) {
		$(this).removeClass('active');
		$('.collapse_table_category_feature_content_' + id).slideUp();
	} else {
		$('.collapse_table_category_feature').removeClass('active');
		$(this).addClass('active');
		$('.collapse_table_content').slideUp();
		$('.collapse_table_category_feature_content_' + id).slideDown();
	}
});
</script>

</div>

</div>
{/if}









<script>
/*
$(document).ready(function(){
$('#left_column').remove();
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');
});*/

$('.removepro').click(function(){
removewhat = $(this).attr('removepro');
//alert(removewhat);

remove_obj = $(this);
if (confirm('Are you sure?')) {
    var url = "#"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: 'deletepro='+removewhat, // serializes the form's elements.
           success: function(data)
           {
               //alert(data); // show response from the php script.
			   if (data != 'error'){
				 $(remove_obj).parent().parent().remove();
			   }else{
				alert(data);
			   }
			   
			   //$("[idpicture='"+data+"']").parent().hide('slow', function(){ $("[idpicture='"+data+"']").remove(); });
           }
         });

    return false; // avoid to execute the actual submit of the form.
}





})





//remove JOBS	
$('.removejobs').click(function(){	
removej = $(this).attr('removej');
if (confirm('Are you sure?')) {
    var url = "#"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: 'deletejob='+removej, // serializes the form's elements.
           success: function(data)
           {
             location.reload();
           }
         });

    return false; // avoid to execute the actual submit of the form.
}
})	


</script>
</div>
{* Softsprint Uliana *}
<script type="text/javascript">
	$(".no_subscriptions_jobs").off("click").on("click", function(e) {
  	    e.preventDefault();
  		$(".alert-danger-jobs").show();
		return false;
	});

	$('.toggle_title').click(function(){	
		$(this).parent('.container').find('.toggle_arrea').toggle();
	});
</script>


<script type="text/javascript">
	$(function() {
		if ($(".categories-tree-html").length > 0) {
			$(".categories-tree-html").tree('collapseAll');

			$(".categories-tree-html").find(":input").each(
				function()
				{
					if (this.value == '1438') {
						$(this).closest('ul.tree').find('label.tree-toggler').first().trigger('click');
					}
				}
			);
			{if isset($cats) && $cats && is_array($cats)}
				if ($("#products-categories-list-selected").length > 0) {
					{foreach from=$cats item="_c"}
						var radio = $('#products-categories-list-selected input[value="{$_c|intval}"]');
						// radio.parent().addClass("tree-selected");
						radio.parents('ul.tree').each(function(){
							$(this).show();
							$(this).prev().find('.icon-folder').removeClass('icon-folder').addClass('icon-folder-open');
						});
					{/foreach}
				}
			{/if}
		}
	});
</script>


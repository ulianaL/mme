<h1>Job Offers "{$job.tittle}"</h1>
<h4>Job description</h4>
<p style="font-family: 'Roboto', sans-serif">{$job.description}:</p>

<div class="row">    
	<div class="col-md-12">
    <table class="table table-striped custab">
    <thead>
        <tr>
            <th>ID</th>
            <th>State</th>
            <th>City</th>
            <th>Zip code</th>
            <th>Date Apply</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Job Titles</th>
            <th>Resume</th>
           
        </tr>
    </thead>
          {foreach $apps item=app}
			<tr>
                <td>{$app.id_appjob}</td>
                <td>{$app.state}</td>
                <td>{$app.city}</td>
                <td>{$app.zipcode}</td>
                <td>{$app.data_apply}</td>
                <td>{$app.customer->firstname} {$app.customer->lastname}</td>
                <td>{$app.customer->email}</td>
                <td>{$app.phone}</td>
                
                
				
				<td>
				  {if $app.jobtitles == 1}Accounting-Finance{/if}       
				  {if $app.jobtitles == 2}Administrative-Clerical{/if} 
				  {if $app.jobtitles == 3}Banking-Mortgage Professionals{/if}  
				  {if $app.jobtitles == 4}Building Construction{/if}     
				  {if $app.jobtitles == 5}Biotech-R&amp;D-Science{/if}  
				  {if $app.jobtitles == 6}Business-Strategic Management{/if} 
				  {if $app.jobtitles == 7}Creative-Design{/if}  
				  {if $app.jobtitles == 8}Customer Support{/if}   
				  {if $app.jobtitles == 9}Editorial Writing{/if}    
				  {if $app.jobtitles == 10}Education{/if}  
				  {if $app.jobtitles == 11}Engineering{/if}       
				  {if $app.jobtitles == 12}Food Services-Hospitality{/if} 
				  {if $app.jobtitles == 13}Human Resources{/if}     
				  {if $app.jobtitles == 14}Installation-Maintenance-Repair{/if}  
				  {if $app.jobtitles == 15}IT-Software Development{/if}  
				  {if $app.jobtitles == 16}Legal{/if}   
				  {if $app.jobtitles == 17}Logistics-Transportation{/if}  
				  {if $app.jobtitles == 18}Manufacturing-Production{/if}  
				  {if $app.jobtitles == 19}Marketing-Product{/if}     
				  {if $app.jobtitles == 20}Medical-Health{/if}   
				  {if $app.jobtitles == 21}Other{/if}      
				  {if $app.jobtitles == 22}Project-Program Management{/if}  
				  {if $app.jobtitles == 23}Quality Assurance-Safety{/if}    
				  {if $app.jobtitles == 24}Real Estate{/if}   
				  {if $app.jobtitles == 25}Sales-Retail{/if}      
				  {if $app.jobtitles == 26}Security-Protective Services{/if}   
			  
				</td>
				<td style="text-align: justify;word-wrap: break-word!important;max-width: 260px;">
					{if $app.resume_file && file_exists(implode('', [$upload_dir, $app.resume_file])) }
						<a href="?id_jobs={$app.id_jobs|intval}&download_cv={$app.resume_file}">{l s='download CV'}</a><br/>
					{/if}
					{$app.resume|nl2br}
				</td>
				
				
            </tr>
          {/foreach} 
    </table>
    </div>
</div>







{****
<pre>
{$apps|@print_r}
</pre>

<pre>
{$job|@print_r}
</pre>
*****}



<style>
.breadcrumb {
    display: none;
}
#left_column{
	display: none;
}
</style>

<script>
$(document).ready(function(){
$('#center_column').removeClass('col-sm-9');
$('#center_column').addClass('col-sm-12');

});
</script>
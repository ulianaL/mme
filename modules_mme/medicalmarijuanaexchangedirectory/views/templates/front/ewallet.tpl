{capture name=path}
<span class="navigation_page">My Wallet</span>
{/capture}


<center><h2>Wallet Balance: <strong>${$wallet_amount}</strong></h2></center>
<h4>Next payment: <strong>{$next_payment}</strong></h4>
<br>
<table class="table">
	<thead>
		<tr>
			<td colspan="2" style="background: #777;color: white;">My Payment History</td>
		</tr>
		<tr>
			<th>Date</th>
			<th>Total Amount</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$history_data item=history}
		<tr>
			<td>{$history['payment_date']}</td>
			<td>${$history['total']}</td>
		</tr>
		{/foreach}
	</tbody>
</table>
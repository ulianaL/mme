{capture name=path}
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<span class="navigation_page">Add banner</span>
{/capture}

<style type="text/css">
	div.checker{
		display: inline-block;
	}
	.params{
		display: block;
	}
</style>

<h2>Add banner</h2>



{foreach $erros item=error}
<p class="alert alert-danger">{$error}</p>
{/foreach}

<form action="#" method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label>Banner Name*</label>
		<input type="text" name="name" value="" class="form-control" required="">
	</div>
	<!------
	1. Medium Rectangle: 300×250
	2. Leaderboard: 728×90
	3. Wide Skyscraper: 160×600
	---->
	<div class="form-group">
		<label>Banner type:</label>
		<label class="type_img"><input type="radio" name="banner_type" value="image" checked="checked">Image</label>
		<label class="type_video"><input type="radio" name="banner_type" value="video">Video</label>
	</div> 
	
	<div class="upload_image_section">
		<div class="form-group" style="max-width:500px;" >
			<label>Banner Size:</label>
			<select  name="size" class="form-control">
				<option value="1">Medium Rectangle: 300×250</option>
				<option value="2">Leaderboard: 728×90</option>
				<option value="7">Leaderboard: 1200×150</option>
				<option value="3">Wide Skyscraper: 160×600</option>
				<option value="5">Products Page Top: 900×475</option>
				<option value="6">Products Page Bottom: 900×290</option>
			
			</select>
		</div>
		
		<div class="form-group">
			<label for="fileToUpload">Banner Picture</label>
			{*****
			<div  style="margin-bottom: 9px;">
				<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/upload/{$valuetoform.logo}" />
			</div>
			****}
			<input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file"  aria-describedby="fileHelp" >
	    </div>
	</div>

	<div class="upload_video_section">
		<div class="form-group">
			<label for="videoToUpload">Banner Video (.mp4, .webm or .ogg)</label>
			<input type="file" name="videoToUpload" id="fileToUpload" class="form-control-file"  aria-describedby="fileHelp" >
	    </div>
	   {* <div class="form-group">
	    	<label class="params">Height</label>
	    	<input type="text" name="height">
	    </div>
	    <div class="form-group">
	    	<label class="params">Width</label>
	    	<input type="text" name="width">
	    </div> *}
	    <div class="form-group">
	    	<input type="checkbox" name="autoplay" checked="checked">
	    	<label>Enable Autoplay</label>
	    </div>
	    <div class="form-group">
	    	<input type="checkbox" name="loop" checked="checked">
	    	<label>Enable Loop</label>
	    </div>
	</div>
	
	
	
	
	
	
	<input type="hidden"  name="addnewbannerimage">
	<button type="submit" name="submit" class="btn btn-primary">Submit</button>


</form>
<script type="text/javascript">
	$('input[type=radio][name=banner_type]').change(function() {
	    if (this.value == 'image') {
	        $('.upload_image_section').show();
	        $('.upload_video_section').hide();
	    }
	    else if (this.value == 'video') {
	         $('.upload_video_section').show();
	         $('.upload_image_section').hide();
	    }
	});
</script>
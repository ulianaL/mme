{capture name=path}
	<span class="navigation_page">List your cannabis organization in the directory</span>
{/capture}


<div class="container">
	<div class="row">
		
		<div class="col-sm-9 rtp">
			
			
<form action="#" method="post" enctype="multipart/form-data">
	<h2>List your cannabis organization in the directory</h2>
  
	  <div class="form-group">
		<label for="exampleInputEmail1">Name*</label>
		<input type="text" name="name"  value="{if $irow ==1}{$valuetoform.name}{/if}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
		<small id="emailHelp" class="form-text text-muted"></small>
	  </div>
	
	
	
		<div class="form-group" style="max-width:500px;" >
			<label>Country:</label>
			<select  name="country"  class="form-control countryselect">
				<option value="1">USA</option>
				<option value="2">CANADA</option>
			</select>
		</div>
	
	

	
	<div class="form-group" style="max-width:500px;" >
		<label>State:</label>
		<select  name="state" class="form-control stateselectbb">
			{foreach $states item=state}
			<option value="{$state.state_code}"  country="{$state.country}" {if $irow ==1}{if $valuetoform.state == $state.state_code} selected="selected" {/if}{/if} >{$state.state} ({$state.state_code})</option>
			{/foreach}
		</select>
	</div>
	
	
	<div class="spinnerloadcity"  style="opacity: 0;">
		<i class="fa fa-spinner fa-spin " aria-hidden="true"></i>
	</div>
	<div class="form-group loadcity" style="max-width:500px;">
		<label>City:</label>
		<select  name="city" class="form-control cityselectbb">
			{foreach $cities item=city}
			<option value="{$city.city_id}">{$city.city}</option>
			{/foreach}
		</select>
	</div>
	
  
  
  
  <div class="form-group">
    <label for="exampleSelect1">Category*</label>
    <select name="cat"  class="form-control" id="exampleSelect1" data-placeholder="Select a category..." required>
		<option value="" disabled {if $irow == 0}selected{/if}>Select a category...</option>
		<option value="1" {if $irow ==1}{if $valuetoform.cat == 1} selected{/if}{/if}>Accounting Firms</option>
	    <option value="2" {if $irow ==1}{if $valuetoform.cat == 2} selected{/if}{/if} >Advertising / Marketing</option>
	    <option value="29" {if $irow ==1}{if $valuetoform.cat == 29} selected{/if}{/if}>Affiliate Programs</option>
	    <option value="3" {if $irow ==1}{if $valuetoform.cat == 3} selected{/if}{/if} >Apparel &amp; Accessories</option>
	    <option value="4" {if $irow ==1}{if $valuetoform.cat == 4} selected{/if}{/if} >Breeders, Genetics, or Clones</option>
	    <option value="5" {if $irow ==1}{if $valuetoform.cat == 5} selected{/if}{/if} >Consulting</option>
	    <option value="6" {if $irow ==1}{if $valuetoform.cat == 6} selected{/if}{/if} >Detoxification</option>
	    <option value="7" {if $irow ==1}{if $valuetoform.cat == 7} selected{/if}{/if} >Dispensaries, Collectives and Wellness Centers</option>
	    <option value="8" {if $irow ==1}{if $valuetoform.cat == 8} selected{/if}{/if} >Doctors &amp; Referral Services</option>
	    <option value="9" {if $irow ==1}{if $valuetoform.cat == 9} selected{/if}{/if}>Education</option>
	    <option value="10" {if $irow ==1}{if $valuetoform.cat == 10} selected{/if}{/if}>Extraction Companies</option>
	    <option value="11" {if $irow ==1}{if $valuetoform.cat == 11} selected{/if}{/if} >Glass Companies</option>
	    <option value="12" {if $irow ==1}{if $valuetoform.cat == 12} selected{/if}{/if}>Government Agencies</option>
	    <option value="13" {if $irow ==1}{if $valuetoform.cat == 13} selected{/if}{/if}>Hemp-Based Products</option>
	    <option value="14" {if $irow ==1}{if $valuetoform.cat == 14} selected{/if}{/if}>Hospitality &amp; Tourism</option>
	    <option value="15" {if $irow ==1}{if $valuetoform.cat == 15} selected{/if}{/if}>Infused Product Companies</option>
	    <option value="16" {if $irow ==1}{if $valuetoform.cat == 16} selected{/if}{/if}>Investment</option>
	    <option value="17" {if $irow ==1}{if $valuetoform.cat == 17} selected{/if}{/if}>Legal</option>
	    <option value="18" {if $irow ==1}{if $valuetoform.cat == 18} selected{/if}{/if}>Lighting</option>
	    <option value="19" {if $irow ==1}{if $valuetoform.cat == 19} selected{/if}{/if}>Manufacturers</option>
	    <option value="30" {if $irow ==1}{if $valuetoform.cat == 30} selected{/if}{/if}>Non Profit</option>
	    <option value="20" {if $irow ==2}{if $valuetoform.cat == 20} selected{/if}{/if}>Organizations &amp; Non-Profits</option>
	    <option value="21" {if $irow ==1}{if $valuetoform.cat == 21} selected{/if}{/if}>Public Companies</option>
	    <option value="22" {if $irow ==1}{if $valuetoform.cat == 22} selected{/if}{/if}>Publications/Resources</option>
	    <option value="23" {if $irow ==1}{if $valuetoform.cat == 23} selected{/if}{/if}>Research / Testing Labs</option>
	    <option value="24" {if $irow ==1}{if $valuetoform.cat == 24} selected{/if}{/if} >Retailers</option>
	    <option value="25" {if $irow ==1}{if $valuetoform.cat == 25} selected{/if}{/if}>Security / Insurance</option>
	    <option value="26" {if $irow ==1}{if $valuetoform.cat == 26} selected{/if}{/if}>Seed Banks</option>
	    <option value="27" {if $irow ==1}{if $valuetoform.cat == 27} selected{/if}{/if}>Soil, Composts &amp; Nutrients</option>
	    <option value="28" {if $irow ==1}{if $valuetoform.cat == 28} selected{/if}{/if}>Technology</option>
	    <option value="31" {if $irow ==1}{if $valuetoform.cat == 31} selected{/if}{/if}>Web Design</option>
    </select>
  </div>
  
  <div class="form-group">
    <label for="exampleTextarea">About*</label>
    <textarea class="form-control" name="about" id="exampleTextarea" rows="5" required>{if $irow ==1}{$valuetoform.about}{/if}</textarea>
  </div>
  
   <div class="form-group">
    <label for="exampleTextarea">Company Biography</label>
    <textarea class="form-control" name="bio" id="bio" rows="5" >{if $irow ==1}{$valuetoform.bio}{/if}</textarea>
  </div>
  
  <div class="form-group">
    <label for="fileToUpload">Logo (recomended size of image 325x160px)</label>
	{if $irow == 1}
	<div  style="margin-bottom: 9px;">
	<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/upload/{$valuetoform.logo}" />
	</div>
	{/if}
    <input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  
   <div class="form-group">
    <label for="exampleInputEmail2">Email</label>
    <input type="email" name="email"   value="{if $irow ==1}{$valuetoform.email}{/if}"   class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp2" placeholder="" >
    <small id="emailHelp2" class="form-text text-muted"></small>
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail11">Phone</label>
    <input type="text" name="phone"  value="{if $irow ==1}{$valuetoform.phone}{/if}"  class="form-control" id="exampleInputEmail11" aria-describedby="emailHelp11" placeholder="" >
    <small id="emailHelp11" class="form-text text-muted"></small>
  </div>
  
   <div class="form-group">
    <label for="exampleInputEmail11">Address</label>
    <textarea type="text" name="address"  class="form-control" id="exampleInputEmail11" aria-describedby="emailHelp11" placeholder="" >{if $irow ==1}{$valuetoform.address}{/if}</textarea>
   <small id="emailHelp11" class="form-text text-muted"></small>
  </div>
  
  
  <div class="form-group">
    <label for="exampleInputEmail11">Video Url</label>
    <input type="text" name="videourl"   value="{if $irow ==1}{$valuetoform.videourl}{/if}" class="form-control" id="exampleInputEmail11" aria-describedby="emailHelp11" placeholder="" >
   <small id="emailHelp11" class="form-text text-muted"></small>
  </div>
  
  
  <div class="form-group">
    <label for="exampleInputEmail111">Website</label>
    <input type="text" name="website"  value="{if $irow ==1}{$valuetoform.website}{/if}"  class="form-control" id="exampleInputEmail111" aria-describedby="emailHelp111" placeholder="" >
    <small id="emailHelp111" class="form-text text-muted"></small>
  </div>
  
  <div class="form-group">
    
	<label for="twitter">Twitter</label>
    <div class="input-group">

	<span class="input-group-addon">@</span>
	<input type="text" name="twitter" value="{if $irow ==1}{$valuetoform.twitter}{/if}"   class="form-control" id="twitter" aria-describedby="twitter" placeholder="" >
    </div>
	<small id="twitter" class="form-text text-muted"></small>
  </div>
  
  
  <div class="form-group">
    
	<label for="facebook">Facebook</label>
    <div class="input-group">

	<span class="input-group-addon">https://www.facebook.com/	</span>
	<input type="text" name="facebook" value="{if $irow ==1}{$valuetoform.facebook}{/if}"  class="form-control" id="facebook" aria-describedby="facebook" placeholder="" >
    </div>
	<small id="facebook" class="form-text text-muted"></small>
  </div> 
  
  <div class="form-group">
    
	<label for="youtube">YouTube</label>
    <div class="input-group">

	<span class="input-group-addon">https://www.youtube.com/</span>
	<input type="text" name="youtube"  value="{if $irow ==1}{$valuetoform.youtube}{/if}"  class="form-control" id="youtube" aria-describedby="youtube" placeholder="" >
    </div>
	<small id="youtube" class="form-text text-muted"></small>
  </div>
  
  <div class="form-group">
    
	<label for="instagram">Instagram</label>
    <div class="input-group">

	<span class="input-group-addon">https://instagram.com/</span>
	<input type="text" name="instagram"   value="{if $irow ==1}{$valuetoform.instagram}{/if}"  class="form-control" id="instagram" aria-describedby="instagram" placeholder="" >
    </div>
	<small id="instagram" class="form-text text-muted"></small>
  </div>
  
  
  <div class="form-group">
    
	<label for="pinterest">Pinterest</label>
    <div class="input-group">

	<span class="input-group-addon">https://pinterest.com/</span>
	<input type="text" name="pinterest"   value="{if $irow ==1}{$valuetoform.pinterest}{/if}"    class="form-control" id="pinterest" aria-describedby="pinterest" placeholder="" >
    </div>
	<small id="pinterest" class="form-text text-muted"></small>
  </div>
 
 
  <h1>Image Slider</h1>
  <hr>
  <div class="form-group">
    <label for="image1">Image 1 (recomended size of image 870x220px)</label>
		{if $irow == 1}
		<div  style="margin-bottom: 9px;">
			{if $valuetoform.image1 }
			<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/gal/{$valuetoform.image1}" />
			{/if}
		</div>
		{/if}
    <input type="file" name="image1" id="image1" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  <div class="form-group">
    <label for="image2">Image 2 (recomended size of image 870x220px)</label>
		{if $irow == 1}
		<div  style="margin-bottom: 9px;">
			{if $valuetoform.image2 }
				<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/gal/{$valuetoform.image2}" />
			{/if}
		</div>
		{/if}
    <input type="file" name="image2" id="image2" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  <div class="form-group">
    <label for="image3">Image 3 (recomended size of image 870x220px)</label>
		{if $irow == 1}
		<div  style="margin-bottom: 9px;">
			{if $valuetoform.image3 }
				<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/gal/{$valuetoform.image3}" />
			{/if}
		</div>
		{/if}
    <input type="file" name="image3" id="image3" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  <div class="form-group">
    <label for="image4">Image 4 (recomended size of image 870x220px)</label>
    {if $irow == 1}
		<div  style="margin-bottom: 9px;">
			{if $valuetoform.image4 }
				<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/gal/{$valuetoform.image4}" />
			{/if}
		</div>
		{/if}
		<input type="file" name="image4" id="image4" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  <div class="form-group">
    <label for="image5">Image 5 (recomended size of image 870x220px)</label>
		{if $irow == 1}
		<div  style="margin-bottom: 9px;">
			{if $valuetoform.image5 }
				<img  class="imgdirectonmystore" src="{$base_dir_ssl}/modules/medicalmarijuanaexchangedirectory/gal/{$valuetoform.image5}" />
			{/if}
		</div>
		{/if}
    <input type="file" name="image5" id="image5" class="form-control-file"  aria-describedby="fileHelp">
  </div>
  <input type="hidden" name="submitaddfree">
	{if $irow == 1}
		<input type="hidden" name="submiteditpay">
	{/if}
  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>
	

{**************	
<pre>			
{$valuetoform|@print_r}			
</pre>			
	***********}		
		
		
		</div><!---md-9--->
		
		
		<!---
		<div class="col-sm-3">
		<h1>md3end</h1>
		</div>md-3end--->
		
		
	</div>
</div>


<style>
.loop__text {
    padding: 8px;
}

</style>





<script>
// country from db...
{if $irow ==1}
$(document).ready(function(){
	{if $valuetoform.country == 1}
		$(".stateselectbb option[country=CA]").hide();
		$(".stateselectbb option[country=US]").show();
		$('.countryselect').val(1);
		
	{else}
		$(".stateselectbb option[country=US]").hide();
		$(".stateselectbb option[country=CA]").show();
		$('.countryselect').val(2);
	{/if}
	$.uniform.update();
});
{/if}



//state and city 
$('.countryselect').change(function ttttttt(){
$(".stateselectbb option[country=CA]").hide();
	if ($(this).val() == 1 ){
		$(".stateselectbb option[country=CA]").hide();
		$(".stateselectbb option[country=US]").show();
		$(".stateselectbb").val($(".stateselectbb option[country=US]:first").val());
	}else{
		$(".stateselectbb option[country=US]").hide();
		$(".stateselectbb option[country=CA]").show();
		$(".stateselectbb").val($(".stateselectbb option[country=CA]:first").val());
	}
	
		
		$('.cityselectbb').empty();
		$('.loadcity').fadeTo( "slow" , 0.5);
		$('.spinnerloadcity').fadeTo( "slow" , 1);
		$.uniform.update();
		whatstate = $(".stateselectbb").val();
		$.ajax({
			type: "POST",
			url: '#',
			data:'getcitiesfromstate='+whatstate+'&activecity=0' , // serializes the form's elements.
			success: function(data)
				{
			    	//alert(data); // show response from the php script.
					$('.cityselectbb').empty();
					$('.cityselectbb').append(data);	
					$.uniform.update();
					$('.loadcity').fadeTo( "slow" , 1);
					$('.spinnerloadcity').fadeTo( "slow" , 0);	
				}
		});
});








$(document).ready(function(){
activecity = '';
	
	$('.stateselectbb').change(function() {

		 $('.cityselectbb').empty();
		 $('.loadcity').fadeTo( "slow" , 0.5);
		 $('.spinnerloadcity').fadeTo( "slow" , 1);
		 $.uniform.update();
		 whatstate = $(this).val();
		 $.ajax({
				type: "POST",
				url: '#',
				data:'getcitiesfromstate='+whatstate+'&activecity=' +activecity, // serializes the form's elements.
				success: function(data)
					{
						//alert(data); // show response from the php script.
						$('.cityselectbb').empty();
						$('.cityselectbb').append(data);
						$.uniform.update();
						$('.loadcity').fadeTo( "slow" , 1);
						$('.spinnerloadcity').fadeTo( "slow" , 0);
						
					}
			   });
				return false;
	})


	
	
	{if $irow ==1}
		
        activecity = {$valuetoform.city};
		$('.cityselectbb').empty();
		 $('.loadcity').fadeTo( "slow" , 0.5);
		 $('.spinnerloadcity').fadeTo( "slow" , 1);
		 $.uniform.update();
		 whatstate = $(this).val();
		 $.ajax({
				type: "POST",
				url: '#',
				data:'getcitiesfromstate={$valuetoform.state}&activecity=' +activecity, // serializes the form's elements.
				success: function(data)
					{
						//alert(data); // show response from the php script.
						$('.cityselectbb').empty();
						$('.cityselectbb').append(data);
						$.uniform.update();
						$('.loadcity').fadeTo( "slow" , 1);
						$('.spinnerloadcity').fadeTo( "slow" , 0);
						
					}
			   });
	
    {/if}	
	
	
	
	
})

//END END END state and city...
</script>
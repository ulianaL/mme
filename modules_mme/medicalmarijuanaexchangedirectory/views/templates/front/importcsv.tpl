
 {capture name=path}
    <span class="navigation_page">Import CSV</span>
 {/capture}
 <div class="import_products">
  {if $success_messege !== ''}
  <h3 class="import_success">{$success_messege}</h3>
  {/if}
  {if $import_error !== ''}
  <h3 class="import_error">{$import_error}</h3>
  {/if}
  <div class="import_left_width">
    	<form method="post" enctype="multipart/form-data">
    	<p>Select a CSV file to import:</p>
    	<input type="file" name="file">
    	<input class="submit button btn_primary" type="submit" name="upload_file" value="Next step">
    	</form>
      {if !empty($products_array)}
        <form method="post" enctype="multipart/form-data">
          <div  class="import_table">
          	<table>
              <thead>
                <tr>
                  <td>Product ID</td>
                  <td>Is Medical</td>
                  <td>Name*</td>
                  <td>Category</td>
                  <td>Price</td>
                  <td>Reference #</td>
                  <td>Quantity</td>
                  <td>Short description</td>
                  <td>Description</td>
                  <td>Image URLs (x,y,z...)</td>
                  <td>Your store prduct link (for not cannabis)</td>
                  <td>Country</td>
                  <td>State</td>
                  <td>City</td>
                  {if $user->dispensaries}
                    <td>Attributes group</td>
                    <td>Attributes value</td>
                    <td>Brand</td>
                    <td>THC</td>
                    <td>CBD</td>
                    <td>Strain</td>
                  {/if}
                </tr>
              </thead>
          	{foreach from=$products_array item=product}
          		<tr>
          			 {foreach from=$product item=cols}
          			 <td>{$cols}</td>

          			 {/foreach}
          		</tr>
          	{/foreach}
          	</table>
          </div>
          <input type="submit" name="import" value="Import .CSV data">
      </form>
    {/if}
    </div>
    <div class="import_right_width">
      <div>
        <h3>AVAILABLE FIELDS</h3>
        {if $user->dispensaries}
          <ul>
            <li>Product ID (if you want to update the product)</li>
            <li>Is Medical</li>
            <li>Name*</li>
            <li>Category
            </li>
            <li>Price</li>
            <li>Reference #</li>
            <li>Quantity</li>
            <li>Short description</li>
            <li>Description</li>
            <li>Image URLs (x,y,z...)</li>
            <li>Your store prduct link (for not cannabis)</li>
            <li>Country</li>
            <li>State</li>
            <li>City</li>
            <li>Attributes group (weight)</li>
            <li>Attributes value (1oz,1/2oz,1/4oz...)</li>
            <li>Brand</li>
            <li>THC</li>
            <li>CBD</li>
            <li>Strain</li>
          </ul>
        {elseif $user->bussiness}
          <ul>
            <li>Product ID (if you want to update the product)</li>
            <li>Is Medical</li>
            <li>Name*</li>
            <li>Category
            </li>
            <li>Price</li>
            <li>Reference #</li>
            <li>Quantity</li>
            <li>Short description</li>
            <li>Description</li>
            <li>Image URLs (x,y,z...)</li>
            <li>Your store prduct link (for not cannabis)</li>
            <li>Country</li>
            <li>State</li>
            <li>City</li>
          </ul>
        {/if}
      </div>

      <div>
        {if $user->dispensaries}
          <a class="csv_examle" href="/docs/csv_import/import_products_with_attribues.csv" target="_blank">Download Example CSV file</a>
        {elseif $user->bussiness}
          <a class="csv_examle" href="/docs/csv_import/import_products.csv" target="_blank">
            Download Example CSV file
          </a>
        {/if}
      </div>
    </div>
    
  </div>
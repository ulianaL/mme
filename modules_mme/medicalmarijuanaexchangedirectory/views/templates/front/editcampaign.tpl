<h1>Edit Campaign</h1>
<form class="add_campaign_form" action="#" method="post">
	<label>Title</label>
	<input required="required" type="text" class="campaign_input" name="campaign_title" value="{$campaign.name}">
    {if $campaign.used == 0}
        {if $campaign.type == 1}
            <label class="red_color">New Clicks (one click cost {$price} points)</label>
            <input required="required" type="number" class="campaign_input" id="campaign_clicks" name="campaign_clicks" step="1" min="0" autocomplete="off">
        {elseif  $campaign.type == 2}
             <label class="red_color">New Impressions (1000 impressions cost {$price} points)</label>
            <input required="required" type="number" class="campaign_input" id="campaign_impressions" name="campaign_clicks" step="1" min="0" autocomplete="off">
        {/if}
        <div id="campaign_points" class="alert alert-success"><span>0</span> Points</div>
    {else}
        {if $campaign.type == 1}
            <label>Avaliable clicks: {$campaign.used}</label>
        {elseif  $campaign.type == 2}
            <label>Avaliable impressions: {$campaign.used}</label>
        {else}
            <label>Subscription to: {date('j/n/Y', strtotime($campaign.date_end))}</label>
        {/if}
    {/if}
    <label>Link</label>
    <input type="text" required="required" class="campaign_input" name="campaign_link" value="{$campaign.url}">
	<div class="form_buttons">
		<input class="ss_add" type="submit" name="updatecampaign" value="Save Campaign">
	</div>
</form>
<script type="text/javascript">
     $("#campaign_clicks").on('input',function(){
            var clicks = $('#campaign_clicks').val();
            var points = clicks * {$price};
            $('#campaign_points span').text(points);
            $('#campaign_points').show();
        });

      $("#campaign_impressions").on('input',function(){
            var clicks = $('#campaign_impressions').val();
            var points = clicks * {$price}/1000;
            $('#campaign_points span').text(points);
            $('#campaign_points').show();
        });
</script>
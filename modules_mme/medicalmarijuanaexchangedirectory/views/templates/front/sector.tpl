<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<span class="sector_title">{$sector.title} Sector</span>
<div class="sector_description">{$sector.description}</div>
<div class="sector_info">
	<iframe id='st_c0b82d488b814ee7882627a227052610' frameBorder='0' scrolling='no' width='100%' height="{$height}" src='https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols={$sector.symbols}&includeLogo=false&includeVolume=true&palette=Financial-Light&showBorderAndTitle=false&linkUrl=https://medicalmarijuanaexchange.com/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=100%'></iframe>
</div>
<span class="sector_news_title">Sector News</span>
<div class="sector_news">
	News Loading...
</div>
<script type="text/javascript">
	var str = '{$sector.symbols}';
	var markets = str.split(";");
	var news = [];
  	var j = 0;
  	for (var i = 0; i <= markets.length-1; i++) {
  		$.ajax({
	  		url: 'https://api.stockdio.com/data/financial/info/v1/getNewsEx?app-key=9F294CB158A94C73B7D84E38BC128E54&nItems=3&stockExchange=OTCMKTS&symbol='+markets[i],
	    	context: document.body
		}).done(function (r) {
	    	news_one = r['data']['news']['values'];
	    	news = $.merge( $.merge( [], news ), news_one);
	    	j++;
	    	if (j == markets.length-1) {
				shownews();
			}
		}).fail(function (e) {
	   		console.log('my fail code');
		});
  	}

 	function shownews() {
		console.log(news);
		news.sort(function(a,b){
	  		return new Date(b[0]) - new Date(a[0]);
		});
		console.log(news);
		news_list = '';
		for (var i = 0; i <= news.length-1; i++) {
			news_list += '<div class="single_news_block"><a class="singlr_title" href="'+news[i][2]+'" target="_blank">'+news[i][1]+'</a> <span class="singl_date">'+moment(news[i][0]).fromNow()+' | '+news[i][5]+'</span></div>';
			if (i >= 19) {
				break;
			}
		}
		$('.sector_news').html(news_list);
  		// $(".single_news_block").slice(0, 25).show();
    // 	$("#loadMore").on('click', function (e) {
    //     	e.preventDefault();
    //     	$(".single_news_block:hidden").slice(0, 10).slideDown();
    //     	if ($(".single_news_block:hidden").length == 0) {
    //         	$("#load").fadeOut('slow');
    //     	}
    //     	$('html,body').animate({
    //         	scrollTop: $(this).offset().top
    //     	}, 1500);
    // 	});
 	}
</script>

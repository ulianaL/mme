{capture name=path}
	<span class="navigation_page">Latest news</span>
{/capture}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script type="text/javascript">
	var markets = ['ACAN', 'ACBFF', 'ACCA', 'ACOL', 'ACRL', 'ADVT', 'AERO', 'AFPW', 'AGTK', 'AMFE', 'AMMJ', 'APHQF', 'ATTBF', 'AXIM', 'AZFL', 'BLDV', 'BLOZF', 'BLPG', 'BUDZ', 'BXNG', 'CAFS', 'CANL', 'CANN', 'CBCA', 'CBDS', 'CBGI', 'CBIS', 'CBMJ', 'CBNT', 'CBSC', 'CCAN', 'CGRA', 'CGRW', 'CHUM', 'CIIX', 'CLSH', 'CMMDF', 'CNAB', 'CNBX', 'CNZCF', 'CPMD', 'CRWG', 'CSAX', 'CURR', 'CVSI', 'DEWM', 'DIGP', 'DIRV', 'DPWW', 'EAPH', 'ECIGQ', 'EFFI', 'EMHTF', 'EMMBF', 'ENCC', 'ENDO', 'ENRT', 'ERBB', 'ESPH', 'ETST', 'EVIO', 'GBHPF', 'GBLX', 'GLAG', 'GLDFF', 'GNBT', 'GRCU', 'GRNH', 'GRSO', 'GRWC', 'GRWG', 'GTBP', 'GTSO', 'HALB', 'HLIX', 'HLSPY', 'HMKTF', 'HMPQ', 'HVST', 'HYYDF', 'ICCLF', 'ICNM', 'IGPK', 'IMLFF', 'INMG', 'INQD', 'ITNS', 'IVITF', 'KAYS', 'KGKG', 'KSHB', 'LCTC', 'LDSYF', 'LSCG', 'LVVV', 'MCIG', 'MCOA', 'MCPI', 'MDCL', 'MDEX', 'MDRM', 'MGWFF', 'MJLB', 'MJMD', 'MJNA', 'MJNE', 'MNTR', 'MQPXF', 'MRPHF', 'MSRT', 'MYDX', 'MYEC', 'MYHI', 'NDEV', 'NGMC', 'NHLE', 'NMUS', 'NRTI', 'NSAV', 'NSPDF', 'NTRR', 'NVGT', 'OGRMF', 'ONCI', 'OWCP', 'PHOT', 'PKPH', 'PLPL', 'PMCB', 'PNPL', 'PNTV', 'POTN', 'PRMCF', 'PRRE', 'PUFXF', 'QRSRF', 'REFG', 'REVI', 'RMHB', 'RSSFF', 'SAGD', 'SING', 'SIPC', 'SLNX', 'SNNC', 'SPLIF', 'SPRWF', 'SRNA', 'STEV', 'STWC', 'TAUG', 'TBPMF', 'TECR', 'THCBF', 'TRTC', 'TWMJF', 'UAMM', 'UBQU', 'UMBBF', 'USMJ', 'VAPE', 'VAPI', 'VAPR', 'VATE', 'VHUB', 'VRTHF', 'WCIG', 'WDRP', 'WTII', 'ZDPY'];

  var news = [];
  var j = 0;
  for (var i = 0; i <= markets.length-1; i++) {
  	$.ajax({
	  	url: 'https://api.stockdio.com/data/financial/info/v1/getNewsEx?app-key=9F294CB158A94C73B7D84E38BC128E54&nItems=3&stockExchange=OTCMKTS&symbol='+markets[i],
	    context: document.body
	}).done(function (r) {
	    news_one = r['data']['news']['values'];
	    news = $.merge( $.merge( [], news ), news_one);
	    j++;
	    if (j == markets.length-1) {
			shownews();
		}
	}).fail(function (e) {
	   console.log('my fail code');
	});
  }

 function shownews() {
	console.log(news);
	news.sort(function(a,b){
	  return new Date(b[0]) - new Date(a[0]);
	});
	console.log(news);
	news_list = '';
	for (var i = 0; i <= news.length-1; i++) {
		news_list += '<div class="single_news_block"><a class="singlr_title" href="'+news[i][2]+'" target="_blank">'+news[i][1]+'</a> <span class="singl_date">'+moment(news[i][0]).fromNow()+' | '+news[i][5]+'</span></div>';
	}
	$('#news_list').html(news_list);
  $(".single_news_block").slice(0, 25).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".single_news_block:hidden").slice(0, 10).slideDown();
        if ($(".single_news_block:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
 }  
</script>

<script>
   if (typeof(stockdio_events) == "undefined") {
      stockdio_events = true;
      var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
      var stockdio_eventer = window[stockdio_eventMethod];
      var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
      stockdio_eventer(stockdio_messageEvent, function (e) {
         if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
            eval(e.data.method);
         }
      },false);
   }
</script>

<div class="left_width">
  <div id="stocks_news">
    <div class="stocks_news_title">Latest news</div>
    <div id="news_list">
      <div class="single_loading_block">News Loading...</div>
    </div>
      <a href="#" id="loadMore">Load More</a>
  </div>
</div>
<div class="right_width">
  <iframe id="relatedcompanies" frameborder="0" scrolling="no" width="100%" height="268" src="https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?stockExchange=otcmkts&amp;symbol=ACBFF&amp;presetlist=related_indices&amp;limit=6&amp;palette=Financial-Light&amp;width=100%25&amp;height=auto&amp;onlink=true&amp;onload=iframemarkets&amp;template=2451116a-1563-4a96-be20-60554d6748ba&amp;&amp;app-key=9F294CB158A94C73B7D84E38BC128E54"></iframe>

  <div class="stocks_list">
    <iframe id='st_c0b82d488b814ee7882627a227052610' frameBorder='0' scrolling='no' width='100%' height='600' src='https://api.stockdio.com/visualization/financial/charts/v1/QuoteBoard?app-key=9F294CB158A94C73B7D84E38BC128E54&stockExchange=OTCMKTS&symbols=ACAN;ACBFF;ACCA;ACOL;ACRL;ADVT;AERO;AFPW;AGTK;AMFE;AMMJ;APHQF;ATTBF;AXIM;AZFL;BLDV;BLOZF;BLPG;BUDZ;BXNG;CAFS;CANL;CANN;NYSENasdaq:CARA;CBCA;CBDS;CBGI;CBIS;CBMJ;CBNT;CBSC;CCAN;CGRA;CGRW;CHUM;CIIX;CLSH;CMMDF;CNAB;CNBX;CNZCF;CPMD;CRWG;CSAX;CURR;CVSI;DEWM;DIGP;DIRV;DPWW;EAPH;ECIGQ;EFFI;EMHTF;EMMBF;ENCC;ENDO;ENRT;ERBB;ESPH;ETST;EVIO;GBHPF;GBLX;GLAG;GLDFF;GNBT;GRCU;GRNH;GRSO;GRWC;GRWG;GTBP;GTSO;NYSENasdaq:GWPH;HALB;HLIX;HLSPY;HMKTF;HMPQ;HVST;HYYDF;ICCLF;ICNM;NYSENasdaq:IIPR;NYSENasdaq:IGC;IGPK;IMLFF;INMG;INQD;NYSENasdaq:INSY;ITNS;IVITF;KAYS;KGKG;KSHB;LCTC;LDSYF;LSCG;LVVV;MCIG;MCOA;MCPI;MDCL;MDEX;MDRM;MGWFF;MJLB;MJMD;MJNA;MJNE;MNTR;MQPXF;MRPHF;MSRT;MYDX;MYEC;MYHI;NDEV;NGMC;NHLE;NMUS;NRTI;NSAV;NSPDF;NTRR;NVGT;OGRMF;ONCI;OWCP;PHOT;PKPH;PLPL;PMCB;PNPL;PNTV;POTN;PRMCF;PRRE;PUFXF;QRSRF;REFG;REVI;RMHB;RSSFF;SAGD;SING;SIPC;SLNX;NYSENasdaq:SMG;SNNC;SPLIF;SPRWF;SRNA;STEV;STWC;TAUG;TBPMF;TECR;THCBF;NYSENasdaq:TLRY;NYSENasdaq:TRPX;TRTC;TWMJF;UAMM;UBQU;UMBBF;USMJ;VAPE;VAPI;VAPR;VATE;VHUB;NYSENasdaq:VPCO;VRTHF;WCIG;WDRP;WTII;ZDPY;NYSENasdaq:ZYNE&includeLogo=false&palette=Financial-Light&title=Marijuana%20Stocks&linkUrl=https://medicalmarijuanaexchange.com/stockprofile?exchange={literal}{exchange}%26symbol={symbol}{/literal}&onload=st_c0b82d488b814ee7882627a227052610&height=600px'></iframe>
  </div>

  {include file="$includedir/financial_right_banner.tpl"}

</div>

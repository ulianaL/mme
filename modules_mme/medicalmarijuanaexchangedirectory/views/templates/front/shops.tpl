

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/css-stars.css" />
{* NEW *}
<div class="shop_logo" {if $cust.background_value == 'own'} style="background-image: url({$img_ps_dir}/cust/{$cust.background_image});" {elseif $cust.background_value == 'marijuana'}style="background-image: url(/themes/default-bootstrap/img/mme_bg.jpg);" {elseif $cust.background_value == 'white'}style="background: #ffffff;" {elseif $cust.background_value == 'black'}style="background: #000000;" {/if}>
    <div class="shop_breadcrambs"></div>    
    <img class="shop_logo_img" src="{$img_ps_dir}/cust/{$cust.logopic}">
</div>
<div class="shop_content row">
    <div class="{if $shopOwner->dispensaries == '1'}col-md-12{else}col-md-8{/if}" id="s-left-column">
        <nav class="navbar navbar-default" style="margin:0;">
          <div class="container-fluid">
            <ul class="nav navbar-nav">
                {if $shopOwner->dispensaries == '1'}
                    <li class="active" onclick="showDispensaries();"><a href="#ttt2" data-toggle="tab">MENU</a></li>
                    <li><a href="#ttt1" data-toggle="tab" onclick="hideDispensaries();">DETAILS</a></li>
                {else}
                    <li class="active" onclick="hideDispensaries();"><a href="#ttt1" data-toggle="tab">DETAILS</a></li>
                    <li><a href="#ttt2" data-toggle="tab" onclick="showDispensaries();">MENU</a></li>
                {/if}
              <li><a href="#ttt3" data-toggle="tab" onclick="hideDispensaries();">REVIEWS</a></li>
              <li><a href="#ttt4" data-toggle="tab" onclick="hideDispensaries();">PHOTOS</a></li>
              <li><a href="#ttt5" data-toggle="tab" onclick="hideDispensaries();">VIDEOS</a></li>
            </ul>
          </div>
        </nav>

            <div class="tab-content clearfix" style="">
                <div  class="tab-pane fix__images {if $shopOwner->dispensaries != '1'}active{/if}" id="ttt1">
                    <h3 class="details-title">About Us</h3>
                    <p style="font-size: 14px;">{$cust.aboutus}</p>
                </div>  
                
                <div class="row tab-pane {if $shopOwner->dispensaries == '1'}active{/if}" id="ttt2">
                    {if $shopOwner->dispensaries == '1'}
                        <div class="despensaries_search_container">
            
                            <div class="despensaries_search_input_parent">
                                <img src="/themes/default-bootstrap/img/header-search.png">
                                <input type="text" id="s-product-name" autocomplete="off" placeholder="{l s='Search' mod='medicalmarijuanaexchangedirectory'}" onkeyup="searchProducts(1);">
                            </div>
                        </div>



                        {*<div class="col-md-3" id="s-filter-box">
                            <ul>
                                <li>
                                    <a href="#" data-id="all" onclick="selectCategory(this); return false;">{l s='All products' mod='medicalmarijuanaexchangedirectory'}</a>
                                </li>
                                {foreach from=$productCategories item="_pc"}
                                    <li>
                                        <a href="#" class="s-category-item" data-id="{$_pc.id|intval}" onclick="selectCategory(this); return false;">
                                            {$_pc.name}
                                            <span>({$_pc.count|intval})</span>
                                        </a>
                                    </li>
                                {/foreach}
                            </ul>
                            <style type="text/css">
                                #s-filter-box {

                                }
                                #s-filter-box ul {
                                    display: block
                                }
                                #s-filter-box ul li {
                                    width: 100%;
                                    margin-bottom: 3px;
                                }
                                #s-filter-box ul li a {
                                    width: 100%;
                                    display: block;
                                        padding: 3px 15px;
                                }                               
                                #s-filter-box ul li a.selected {
                                        background: #0a9546;
                                    color: #FFF;
                                }
                                /*#s-filter-box ul li a.selected::after {
                                    content: '×';
                                        float: right;
                                    position: relative;
                                    right: -11px;
                                    font-size: 15px;
                                }*/
                                #s-filter-box ul li a span {

                                }
                            </style>
                        </div>*}
                    {/if}
                    <div  class="col-md-12 shop_menu_content">
                        {if $shopOwner->dispensaries == '1'}
                        <style>
                            .shop_menu_content {
                                display: flex;
                                padding: 0;
                            }

                            .shop_menu_content .shop_menu_filters {
                                display: flex;
                                flex-direction: column;
                                width: 100%;
                                max-width: 25%;
                                margin-bottom: auto;
                                box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px;
                                border-radius: 3px;
                                overflow: hidden;
                            }

                            .shop_menu_content .shop_menu_filters h4 {
                                font-family: 'Futura PT Book';
                                font-size: 23px;
                                text-align: center;
                                padding: 10px 0;
                                margin-bottom: 0;
                            }

                            .shop_menu_content .shop_menu_filters ul {
                                padding: 10px;
                                margin: 0;
                                display: flex;
                                flex-direction: column;
                            }

                            .shop_menu_content .shop_menu_filters ul li {
                                width: 100%;
                                display: flex;
                            }

                            .shop_menu_content .shop_menu_filters ul li a {
                                font-family: 'Futura PT Book';
                                font-size: 19px;
                                padding: 15px 10px;
                                width: 100%;
                                position: relative;
                            }

                            .shop_menu_content .shop_menu_filters ul li a:after,
                            .shop_menu_content .shop_menu_filters ul li a:before {
                                content: '';
                                position: absolute;
                                top: 50%;
                                right: 5px;
                                width: 15px;
                                height: 15px;
                                border: 1px solid;
                                transform: translateY(-50%);
                                border-radius: 4px;
                            }

                            .shop_menu_content .shop_menu_filters ul li a:after {
                                background: red;
                                display: none;
                                z-index: 10;
                            }

                            .shop_menu_content .shop_menu_filters ul li a.all_selected:after,
                            .shop_menu_content .shop_menu_filters ul li a.selected:after,
                            .shop_menu_content .shop_menu_filters ul li a.all_flower_selected:after {
                                display: block;
                                background-color: #56b646;
                                background-image: url(/themes/default-bootstrap/img/menu_tick.svg);
                                background-size: 80% 80%;
                                background-repeat: no-repeat;
                                background-position: center;
                            }

                            @media(max-width: 991px) {
                                .shop_menu_content {
                                    flex-direction: column;
                                }

                                .shop_menu_content .shop_menu_filters {
                                    max-width: 100%;
                                }
                                .despensaries_search_container{
                                    float: none;
                                    width: 90%;
                                    margin: auto;
                                }
                            }
                        </style>
                            <div class="shop_menu_filters">
                                <h4>
                                    Product Category
                                </h4>
                                <ul>
                                    <li>
                                        <a href="#" data-id="all" class="s-category-item all_selected" onclick="selectCategory(this); return false;">{l s='All Products' mod='medicalmarijuanaexchangedirectory'}</a>
                                    </li>

                                    <li>
                                        <a href="#" data-id="all_flower" class="s-category-item" onclick="selectCategory(this); return false;">{l s='All Flower' mod='medicalmarijuanaexchangedirectory'}</a>
                                    </li>
                                {foreach from=$productCategories item="_pc"}
                                    {if $_pc.id == 1410}
                                        <li class="flower">
                                            <a href="#" class="s-category-item" data-id="{$_pc.id|intval}" onclick="selectCategory(this); return false;">
                                                {$_pc.name}
                                                <span>({$_pc.count|intval})</span>
                                            </a>
                                        </li>
                                        {/if}
                                        {if $_pc.id == 1411}
                                        <li class="flower">
                                            <a href="#" class="s-category-item" data-id="{$_pc.id|intval}" onclick="selectCategory(this); return false;">
                                                {$_pc.name}
                                                <span>({$_pc.count|intval})</span>
                                            </a>
                                        </li>
                                        {/if}
                                        {if $_pc.id == 1412}
                                        <li class="flower">
                                            <a href="#" class="s-category-item" data-id="{$_pc.id|intval}" onclick="selectCategory(this); return false;">
                                                {$_pc.name}
                                                <span>({$_pc.count|intval})</span>
                                            </a>
                                        </li>
                                    {/if}
                                {/foreach}
                                {foreach from=$productCategories item="_pc"}
                                    {if $_pc.id !== 1410 && $_pc.id !== 1411 && $_pc.id !== 1412}
                                    <li>
                                        <a href="#" class="s-category-item" data-id="{$_pc.id|intval}" onclick="selectCategory(this); return false;">
                                            {$_pc.name}
                                            <span>({$_pc.count|intval})</span>
                                        </a>
                                    </li>
                                    {/if}
                                {/foreach}
                            </ul>
                            </div>
                        {/if}
                        <div id="prods" style="flex: 1;">
                            {if $shopOwner->dispensaries == '1'}
                                {include file="$tpl_dir./shop-product-list-ajax.tpl" products=$shopProducts class='blocknewproducts tab-pane' id='blocknewproducts'}
                                {$pging}
                            {else}
                                {include file="$tpl_dir./shop-product-list.tpl" products=$new_products class='blocknewproducts tab-pane' id='blocknewproducts'}
                            {/if}
                        </div>
                    </div>  
                </div>
                
                
                
                <!-----  REVIEW  -------->
                <div  class="tab-pane" id="ttt3">
                    <div class="addreviewbu">
                    <span class="title_right_info">Reviews</span>
                    <span class="avr_cust_reviews">BASED ON CUSTOMERS REVIEWS</span>
                    <div>
                        <select id="averageTotal" class="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <script>
                                
                                $(function() {
                                  $('#averageTotal').barrating({
                                    theme: 'css-stars',
                                    readonly: true,
                                    initialRating: {sprintf('%.2f', $averageTotal)}                         
                                  })
                                });
                        </script>
                    </div>
                    <a class="aaddreviewbu" onclick="addreview();return false;" href="#">WRITE A REVIEW</a>
                       <div class="addrevformghj" style="display:none;">
                         <form id="addreviewformb" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label style="text-align:left" class="control-label col-sm-12 titled" >Comment</label>
                                <div class="col-sm-12"> 
                                  <textarea rows="10" type="text" name="comment" class="inputd"  class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label style="text-align: left;" class="control-label col-sm-6 titled" >Staff</label>
                                <select id="star1" name="star1" class="col-sm-8">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5" >5</option>
                                </select>
                            </div>
                            <div class="form-group" >
                                <label style="text-align: left;" class="control-label col-sm-6 titled" >Accessibility</label>
                                <select id="star2" name="star2" class="col-sm-8">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5" >5</option>
                                </select>
                            </div>
                            <div class="form-group" >
                                <label style="text-align: left;" class="control-label col-sm-6 titled" >Quality</label>
                                <select id="star3" name="star3" class="col-sm-8">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5" >5</option>
                                </select>
                            </div>
                            
                            <div class="form-group" >
                                <label style="text-align: left;" class="control-label col-sm-6 titled" >Atmosphere</label>
                                <select id="star4" name="star4" class="col-sm-8">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5" >5</option>
                                </select>
                            </div>
                            
                            <div class="form-group" >
                                <label style="text-align: left;" class="control-label col-sm-6 titled" >Price</label>
                                <select id="star5" name="star5" class="col-sm-8">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5" >5</option>
                                </select>
                            </div>
                            <input type="hidden" name="submitereviewifset">

                            
                          <input type="submit" value="Add Review" class="aaddreviewbu  butohideonaddreview" > 
                         </form>
                    </div>
                    </div>
                    
                    {foreach from=$rev item=re}
                        <div class="itemreview">
                            <div class="row">
                                <div class="col-md-12 startbreak">
                                        <div class="bistartrev">
                                            <select id="big{$re.id_rev}" class="">
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                          <option value="4">4</option>
                                                          <option value="5">5</option>
                                            </select>
                                            <script>
                                                    
                                                    $(function() {
                                                      $('#big{$re.id_rev}').barrating({
                                                            theme: 'css-stars',
                                                            readonly: true,
                                                            initialRating:{math equation="(a+b+c+d+e)/5" a=$re.star1 b=$re.star2 c=$re.star3 d=$re.star4 e=$re.star5 format="%.2f"}
                                                    
                                                          })
                                                    });
                                            </script>
                                        </div>
                                        <h4 class="shop_review_author">{$re.firstname} {$re.lastname}</h4>  
                                    <p style="color: #474646">{$re.comment}</p>
                                    
                                </div>
                            </div>
                        </div>
                    {/foreach}
                    
                    
                    
                    
                </div>

                
                
                <!----pictures---->
                <div  class="tab-pane" id="ttt4">
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.32/owl.carousel.js"></script>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.32/owl.carousel.css" />

                    <span class="title_right_info">Photos</span>

                        {foreach from=$pics item=pic}
                        <div class="item">
                            <a data-fancybox-group="other-views"  class="fancybox" href="{$img_ps_dir}customerspic/{$pic.pic}">
                            <img style="max-height:500px; max-width:100%;" src="{$img_ps_dir}customerspic/{$pic.pic}" >
                            </a>
                        </div>
                        
                        {/foreach}
                </div>
            
            
            <div  class="tab-pane"  id="ttt5">
                
                <span class="title_right_info">Video</span>
                {foreach from=$videos item=video}
                <div class="wi80k0red item">
                <div class="col-md-12 videoWrapper"> 
                
                <iframe width="100%" height="auto" src="{$video.videourl|replace:'watch?v=':'embed/'}"></iframe>
                </div>
                </div>
                {/foreach}

            </div>

    
    
        </div>
    </div>
    <div class="col-md-4" id="s-right-column" {if $shopOwner->dispensaries == '1'}style="display: none;"{/if}>
        <div class="shop_right_info">
            <span class="title_right_info">Work Schedule</span>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Monday</div>
                <div class="hourtexts">{$cust.day2}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Tuesday</div>
                <div class="hourtexts">{$cust.day3}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Wednesday</div>
                <div class="hourtexts">{$cust.day4}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Thursday</div>
                <div class="hourtexts">{$cust.day5}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Friday</div>
                <div class="hourtexts">{$cust.day6}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Saturday</div>
                <div class="hourtexts">{$cust.day7}</div>
            </div>
            <div class="alldays">
                <span class="shop_lightgreendot"></span>
                <div class="daytexts">Sunday</div>
                <div class="hourtexts">{$cust.day1}</div>
            </div>

            {* <div class="shop_connect_btn" onclick="addcontacttochat({$cust.id})">Connect</div> *}
        </div>
        
        <div class="shop_right_info ">
            <span class="title_right_info">Contact Details</span>
            <div class="alldays">
                <span class="shop_greendot"></span>
                <div class="daytexts2">Address</div>
                <div class="hourtexts2">{$cust.address}</div>
            </div>
            <div class="alldays">
                <span class="shop_greendot"></span>
                <div class="daytexts2">Phone</div>
                <div class="hourtexts2">{$cust.phone}</div>
            </div>
            <div class="alldays">
                <span class="shop_greendot"></span>
                <div class="daytexts2">Email</div>
                <div class="hourtexts2">{$cust.emailse}</div>
            </div>
            <div class="alldays">
                <span class="shop_greendot"></span>
                <div class="daytexts2">Website</div>
                <div class="hourtexts2"><a href="{$cust.website2}" target="_black">{$cust.website_host}</a></div>
            </div>
        </div>
        
        <div class="shop_right_info ">
            <span class="title_right_info">Social Media</span>
            <div class="alldays">
                <div class="daytexts3"><a href="{$cust.facebook}" target="_black"><img src="/modules/medicalmarijuanaexchangedirectory/img/facebook-f-brands.png"></a>
                Facebook</div>
            </div>
            <div class="alldays">
                <div class="daytexts3"><a href="{$cust.twitter}" target="_black"><img src="/modules/medicalmarijuanaexchangedirectory/img/twitter-brands.png"></a>Twitter</div>
            </div>
            <div class="alldays">
                <div class="daytexts3"><a href="{$cust.instagram}" target="_black"><img src="/modules/medicalmarijuanaexchangedirectory/img/pinterest-brands.png"></a>Pinterest</div>
            </div>
            
        </div>
        <div class="shop_right_info">
            <ul class="store_options">
                {if $cust.medical == '1'}
                    <li>Medical</li>
                {/if}

                {if $cust.recreational == 1}
                    <li>Recreational</li>
                {/if}

                {if $cust.payment_cash == 1}
                    <li>Cash</li>
                {/if}

                {if $cust.payment_credit_card == 1}
                    <li>Credit Card</li>
                {/if}

                {if $cust.payment_debit_card == 1}
                    <li>Debit Card</li>
                {/if}

                {if $cust.payment_atm == 1}
                    <li>ATM</li>
                {/if}

                {if $cust.allowcod == 1}
                    <li>Order Delivery</li>
                {/if}

                {if $cust.allowpick != ''}
                    <li>Order Pickup</li>
                {/if}
                {if $cust.delivery == 1}
                    <li>Delivery</li>
                {/if}

                {if $cust.is_doctor == 1}
                    <li>Doctors</li>
                {/if}

                {if $cust.storefront == 1}
                    <li>Storefront</li>
                {/if}
            </ul>
        </div>
    </div>
    
</div>
<div style="clear: both;"></div>
{* END NEW *}

<script type="text/javascript">
    function preloader() {
        return '<img style="display: flex; margin: 20px auto; width: 150px;" src="/themes/default-bootstrap/img/marijuana__preloader.gif" />';
    }
    function showDispensaries() {
        {if $shopOwner->dispensaries == '1'}
            $('#s-right-column').hide();
            $('#s-left-column')
                .removeClass('col-md-8')
                .addClass('col-md-12');
            $("#s-filter-box").show();
            // $("#prods").parent().removeClass('col-md-12').addClass('col-md-9');
        {/if}
    }
    function hideDispensaries() {
        {if $shopOwner->dispensaries == '1'}
            $("#s-filter-box").hide();
            $('#s-right-column').show();
            $('#s-left-column')
                .addClass('col-md-8')
                .removeClass('col-md-12');
            // $("#prods").parent().addClass('col-md-12').removeClass('col-md-9');
        {/if}
    }
    function selectCategory(el) {
        if ($(el).data('id') == 'all') {
            $(".s-category-item").removeClass('selected');
            $(".s-category-item").removeClass('all_flower_selected');
            $(el).addClass('all_selected');

        }else if ($(el).data('id') == 'all_flower'){
            $(".s-category-item").removeClass('all_selected');
            $(".s-category-item").removeClass('selected');
            $(el).addClass('all_flower_selected');
        }else {
            $(".s-category-item").removeClass('all_selected');
            $(".s-category-item").removeClass('all_flower_selected');
            $(el).toggleClass('selected');
        }
        searchProducts(1);
    }
    function initPagination() {
        $("#page_nav a").on('click', function(e) {
            e.preventDefault();
            searchProducts($(this).data('page'));
        });
    }
    var currentAjaxInstance;
    function searchProducts(page) {
        var c = [];
        $(".s-category-item").each(function(){
            if ($(this).hasClass('selected')) {
                c.push($(this).data('id'));
            }else if($(this).hasClass('all_flower_selected')) {
                c = ['1410', '1411', '1412'];
            }
        });
        if (currentAjaxInstance) {
            currentAjaxInstance.abort();
        }
        $('#prods').html(preloader());
        currentAjaxInstance = $.ajax({
            url: '/modules/medicalmarijuanaexchangedirectory/ajax.php',
            type: 'POST',
            cache: false,
            async: true,
            data: {
                action: 'searchProductsByOwner',
                id_owner: {$shopOwner->id|intval},
                categories: c,
                p: page||1,
                q: $("#s-product-name").val().trim(),
                ajax: 1
            },
            dataType: 'json',
            success: function(res){
                if (typeof res.hasError != 'undefined' && res.hasError == true) {
                    if (!!$.prototype.fancybox)
                        $.fancybox.open([{
                            type: 'inline',
                            autoScale: true,
                            minHeight: 30,
                            content: '<p class="fancybox-error">' + res.message + '</p>'
                        }], {
                            padding: 0
                        });
                    else
                        alert(res.message);
                } else if (typeof res.status != 'undefined' && res.status == 'ok') {
                    if (res.products.length > 0) {
                        setTimeout(function() {
                            $('#prods').html(res.products_html);
                            $('#prods').append($(res.paging));
                            initPagination();
                        }, 500);
                    } else {
                        $("#prods").html('<p class="alert alert-warning">{l s='No results found' mod='medicalmarijuanaexchangedirectory'}</p>');
                    }
                } else {
                    if (!!$.prototype.fancybox)
                        $.fancybox.open([{
                            type: 'inline',
                            autoScale: true,
                            minHeight: 30,
                            content: '<p class="fancybox-error">Internal error</p>'
                        }], {
                            padding: 0
                        });
                    else
                        alert('Internal error');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (errorThrown == 'abort') {
                    return;
                }
                if (!!$.prototype.fancybox)
                    $.fancybox.open([{
                        type: 'inline',
                        autoScale: true,
                        minHeight: 30,
                        content: '<p class="fancybox-error">' + "TECHNICAL ERROR: <br/><br/>Details:<br/>Error thrown: " + errorThrown + "<br/>" + 'Text: ' + jqXHR.responseText + '</p>'
                    }], {
                        padding: 0
                    });
                else
                    alert("TECHNICAL ERROR: <br/><br/>Details:<br/>Error thrown: " + errorThrown + "<br/>" + 'Text: ' + jqXHR.responseText);
            }
        });
    }
    $(function(){
        initPagination();
    });
</script>

        
<script type="text/javascript">



function addreview(){
$('.addrevformghj').slideToggle( "slow");
$('.jhhbuhb').hide();
}
   
   
   
   $(function() {
      $('#star1').barrating({
        theme: 'css-stars'
      });
   });
   
   $(function() {
      $('#star2').barrating({
        theme: 'css-stars'
      });
   });
   $(function() {
      $('#star3').barrating({
        theme: 'css-stars'
      });
   });
   
   $(function() {
      $('#star4').barrating({
        theme: 'css-stars'
      });
   });
   $(function() {
      $('#star5').barrating({
        theme: 'css-stars'
      });
   });
   
  
</script>




<script>
$(document).ready(function() {
$('.owl-carousel').owlCarousel({
    loop:true,
    
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})

$("a.fancybox").fancybox({
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   2000, 
        'speedOut'      :   1000, 
        'overlayShow'   :   false,
        
            
            
    });
});
</script>






<style>

.content_scene_cat {
  /* border-top: 5px solid #333; */
  
  line-height: 19px;
  margin: 0 0 26px 0;
  
  }
  .content_scene_cat .content_scene_cat_bg {
    padding: 4px 10px 10px 42px;
    background-color: #333 !important;
    }
    @media (max-width: 1199px) {
      .content_scene_cat .content_scene_cat_bg {
        padding: 10px 10px 10px 15px; } }
  .content_scene_cat span.category-name {
    font: 600 42px/51px "Open Sans", sans-serif;
    color: #fff;
    margin-bottom: 12px; }
    @media (max-width: 1199px) {
      .content_scene_cat span.category-name {
        font-size: 25px;
        line-height: 30px; } }
  .content_scene_cat p {
    margin-bottom: 0; }
  .content_scene_cat a {
     }
    .content_scene_cat a:hover {
      text-decoration: underline; }
  .content_scene_cat .content_scene {
    color: #777; }
    .content_scene_cat .content_scene .cat_desc {
      padding-top: 20px; }
      .content_scene_cat .content_scene .cat_desc a {
        color: #777; }
        .content_scene_cat .content_scene .cat_desc a:hover {
          color: #515151; }

</style>




<script>
//made by vipul mirajkar thevipulm.appspot.com
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

$(document).ready(function() {
    $('.custom_select_title').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).parent().find('.custom_select_content').slideUp(200);
        } else {
            $('.custom_select_title').removeClass('active');
            $(this).addClass('active');
            $('.custom_select_content').slideUp(200);
            $(this).parent().find('.custom_select_content').slideDown(200);
        }
    });

    $('.mobile_dispensaries_filters').on('click', function() {
        $('.dispensaries_block_parent_content').slideDown();
    });

    $('.mobile_dispensaries_list').on('click', function() {
        $('.mobie__dispenasaries_lists').slideDown();
    });

    $('.map_close__btn').on('click', function(){
        var el = $(this).attr("data-class");
        $('.' + el).slideUp();
    });
});
</script>
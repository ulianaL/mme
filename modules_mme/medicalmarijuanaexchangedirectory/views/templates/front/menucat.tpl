<div class="blogarea">
	<div><a href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', $requestParamsCategory)}"><h2 class="{if $active_category == 0}active_category{/if}">All Categories</h2></a></div>
	<div class="clearfix"></div>
		<div id="categories_block_left1" class="mainlft">
			<ul>
				<li>
				<a class="{if $active_category == 1}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>1]))}">Accounting Firms ({if isset($count_categories[1])} {$count_categories[1]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 2}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>2]))}">Advertising / Marketing ({if isset($count_categories[2])} {$count_categories[2]} {else} 0 {/if})</a>  
				</li>

				<li>
				<a class="{if $active_category == 29}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>29]))}">Affiliate Programs ({if isset($count_categories[29])} {$count_categories[29]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 3}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>3]))}">Apparel & Accessories ({if isset($count_categories[3])} {$count_categories[3]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 4}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>4]))}">Breeders, Genetics, or Clones ({if isset($count_categories[4])} {$count_categories[4]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 5}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>5]))}">Consulting ({if isset($count_categories[5])} {$count_categories[5]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 6}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>6]))}">Detoxification ({if isset($count_categories[6])} {$count_categories[6]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 7}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>7]))}">Dispensaries, Collectives and Wellness Centers ({if isset($count_categories[7])} {$count_categories[7]} {else} 0 {/if})</a>  
				</li>
				<li>
				<a class="{if $active_category == 8}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>8]))}">Doctors & Referral Services ({if isset($count_categories[8])} {$count_categories[8]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 9}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>9]))}">Education ({if isset($count_categories[9])} {$count_categories[9]} {else} 0 {/if})</a>  
				</li>
				
				<li>
				<a class="{if $active_category == 10}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>10]))}">Extraction Companies ({if isset($count_categories[10])} {$count_categories[10]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 11}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>11]))}">Glass Companies ({if isset($count_categories[11])} {$count_categories[11]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 12}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>12]))}">Government Agencies ({if isset($count_categories[12])} {$count_categories[12]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 13}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>13]))}">Hemp-Based Products ({if isset($count_categories[13])} {$count_categories[13]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 14}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>14]))}">Hospitality & Tourism ({if isset($count_categories[14])} {$count_categories[14]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 15}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>15]))}">Infused Product Companies ({if isset($count_categories[15])} {$count_categories[15]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 16}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>16]))}">Investment ({if isset($count_categories[16])} {$count_categories[16]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 17}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>17]))}">Legal ({if isset($count_categories[17])} {$count_categories[17]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 18}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>18]))}">Lighting ({if isset($count_categories[18])} {$count_categories[18]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 19}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>19]))}">Manufacturers ({if isset($count_categories[19])} {$count_categories[19]} {else} 0 {/if})</a>  
				</li>

				<li>
				<a class="{if $active_category == 30}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>30]))}">Non Profit ({if isset($count_categories[30])} {$count_categories[30]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 20}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>20]))}">Organizations & Non-Profits ({if isset($count_categories[20])} {$count_categories[20]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 21}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>21]))}">Public Companies ({if isset($count_categories[21])} {$count_categories[21]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 22}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>22]))}">Publications/Resources ({if isset($count_categories[22])} {$count_categories[22]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 23}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>23]))}">Research / Testing Labs ({if isset($count_categories[23])} {$count_categories[23]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 24}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>24]))}">Retailers ({if isset($count_categories[24])} {$count_categories[24]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 25}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>25]))}">Security / Insurance ({if isset($count_categories[25])} {$count_categories[25]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 26}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>26]))}">Seed Banks ({if isset($count_categories[26])} {$count_categories[26]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 27}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>27]))}">Soil, Composts &amp; Nutrients ({if isset($count_categories[27])} {$count_categories[27]} {else} 0 {/if})</a>  
				</li>
				
				
				<li>
				<a class="{if $active_category == 28}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>28]))}">Technology ({if isset($count_categories[28])} {$count_categories[28]} {else} 0 {/if})</a>  
				</li>

				<li>
				<a class="{if $active_category == 31}active_category{/if}" href="{$link->getModuleLink('medicalmarijuanaexchangedirectory', 'front', array_merge($requestParamsCategory, ['catid'=>31]))}">Web Design ({if isset($count_categories[31])} {$count_categories[31]} {else} 0 {/if})</a>  
				</li>				
				
			</ul>

		</div>
	<div class="clearfix"></div>
</div>
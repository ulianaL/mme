
<div class="container">
  <h2 class="toggle_title">My orders</h2>
  <div class="toggle_arrea">
  <div class="table-responsive">          
  <table class="table table">
    <thead>
      <tr>
        <th>ID ORDER</th>
        <th>Reference</th>
        <th>Payment</th>
        <th>Date</th>
        <th>Total paid</th>
        <th>Total</th>
    
		
      </tr>
    </thead>
    <tbody>
    
    {foreach from=$myorders item=ord key=k}
	
	<tr style="    background: #5bbe8f;   color: white;" >
       <td>{$ord.order.id}</td>
       <td>{$ord.order.reference}</td>
	   <td>{$ord.order.payment}</td>
       <td>{$ord.order.date_add}</td>
       <td>${$ord.order.total_paid|string_format:"%.2f"}</td>
       <td>${$ord.order.total_paid_tax_incl|string_format:"%.2f"}</td>
    </tr>   
	
			<tr>
			<td></td>
			<td style="background:#ddd; border: 1px solid #999999;" colspan="5" >
			<h4 style="margin:0;">Products:</h4>
			</td>
			</tr>
			{foreach from=$ord.details item=proord}
		    <tr>
			<td style="border: 0;" ></td>
			<td colspan="5" style="background:#f5f5f5; border: 1px solid #999999;">
				 
				<table class="table table" style="margin: 0;">
				 <thead>
				  <tr>
					<th  style="background: #333;color: white;width: 150px;">Payment / Shipping</th>
					<th>Id:</th>
					<th style="    max-width: 150px; width: 150px;">Name:</th>
					<th>Unit price:</th>
					<th>Quantity:</th>
					<th>Total price:</th>
				  </tr>
				</thead>
				<tbody>
				<tr>
					<td style="background:
					{if  $proord.pay == 1}
					#3b7bbf 
					{else if $proord.pay == 2}
					#c2c2c2
					{else}
					#333
					{/if}
					;color: white;">
					
					{if  $proord.pay == 0}
					<p class="pdt">COD (Cash on delivery)<p>
					{/if}
					{if  $proord.pay == 1}
					<p class="pdt">Paypal<p>
					{/if}
					{if  $proord.pay == 2}
					<p class="pdt">Pickup on store<p>
					
					{/if}
					</td>
					
					<td><p class="pdt">{$proord.product_id}<p></td>
					<td><p class="pdt">{$proord.product_name}<p></td>
					<td><p class="pdt">${$proord.unit_price_tax_incl|string_format:"%.2f"}<p></td>
					<td><p class="pdt">X{$proord.product_quantity}<p></td>	
					<td><p class="pdt">${$proord.total_price_tax_incl|string_format:"%.2f"}<p></td>
			   </tr>
			   
			      
			   </tbody>
			   </table>
			   
			</td>
		   </tr>
		   {/foreach} 
		   
		   
		   
		    <tr>
			<td style="border: 0;"></td>
			<td style="background:#ddd; border: 1px solid #999999;" colspan="5" >
			<h4 style="margin:0;">Shipping:</h4>
			</td>
			</tr>
			
			
		    <tr>
			<td style="border: 0;" ></td>
			<td colspan="5" style="background:#f5f5f5; border: 1px solid #999999;">
				 
				<table class="table table" style="margin: 0;">
				 <thead>
				  <tr>
					<th>Total shipping</th>
				  </tr>
				</thead>
				<tbody>
				    <tr>
				
						<td><p class="pdt">${$ord.order.total_shipping|string_format:"%.2f"}<p></td>
				   </tr>
			   </tbody>
			   </table>
			</td>
		   </tr>
		   
		   
			<tr>
				<td style="border: 0;"></td>
				<td style="background:#ddd; border: 1px solid #999999;" colspan="5" >
				<h4 style="margin:0;">Adresses:</h4>
				</td>
			</tr>
		   
		   <tr>
			<td style="border: 0;" ></td>
			<td colspan="5" style="background:#f5f5f5; border: 1px solid #999999;">
				 
				<table class="table table" style="margin: 0;">
				 <thead>
				  <tr>
					<th style="border-right: 1px solid #d6d4d4;">Delivery Address</th>
					<th>Invoice Address</th>
				  </tr>
				</thead>
				<tbody>
				    <tr>
						<td style="border-right: 1px solid #d6d4d4;">
							
							{assign var=address value=$ord.addr1}
							{foreach from=$ord.addr1.ordered name=adr_loop item=pattern}
								{assign var=addressKey value=" "|explode:$pattern}
								<p>
								{foreach from=$addressKey item=key name="word_loop"}
									<span {if isset($addresses_style[$key])} class="{$addresses_style[$key]}"{/if}>
										{$address.formated[$key|replace:',':'']|escape:'html':'UTF-8'}
									</span>
								{/foreach}
								</p>
							{/foreach}
							
						</td>
						
						<td>
							
							{assign var=address value=$ord.addr2}
							{foreach from=$ord.addr2.ordered name=adr_loop item=pattern}
								{assign var=addressKey value=" "|explode:$pattern}
								<p>
								{foreach from=$addressKey item=key name="word_loop"}
									<span {if isset($addresses_style[$key])} class="{$addresses_style[$key]}"{/if}>
										{$address.formated[$key|replace:',':'']|escape:'html':'UTF-8'}
									</span>
								{/foreach}
								</p>
							{/foreach}
							
						</td>	
				   </tr>

			   </tbody>
			   </table>
			</td>
		   </tr>
	 {/foreach} 
    </tbody>
  </table>
  </div>

</div>
</div>

<div class="col-12 lavs_container">
    <div class="col-12 p-0 font_30px font_baskervile font_474646 pt-3 border_top_1px_838080 pt_35px">
    Hemp Laws 
    </div>
    <div class="pt_35px lavs_collaps_container">
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Alabama
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Ala. Code § 2-8-380 to 2-8-383 and § 20-2-2 (2016) 
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates an industrial hemp research program overseen by the Alabama Department of Agriculture and Industries to study hemp.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                The department may coordinate the study with institutions of higher education.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Alaska
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. § 03.05.010
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. § 03.05.100
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. § 03.05.076 to 03.05.079
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. § 11.71.900
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. § 17.20.020
                        </p>
                        <p class="font_18px font_474646">
                           Alaska Stat. §17.38.900 (2018) Senate Bill 6
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the commissioner of natural resources to adopt regulations related to industrial hemp including approved sources or varieties of seed, testing requirements, and establishing isolation distances.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the department to establish fee levels to cover regulatory costs and annually review these fee levels.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows for the creation of a pilot program by an institution of higher education or the Department of Natural Resources.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Defines both industrial hemp and cannabidiol oil. Amends definitions for hashish oil and marijuana.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Clarifies that the addition of industrial hemp to food does not create an adulterated food product.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires a report on or before Dec. 1, 2024.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Arizona
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           SB 1098 (2018)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Authorizes a pilot program for the research, growth, cultivation and marketing of industrial hemp and establishes the Industrial Hemp Trust Fund
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the Arizona Department of Agriculture (AZDA) to adopt rules for the licensing, production and management of hemp and hemp seed, to set fees to fund AZDA’s activities, and to establish an industrial hemp advisory council.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows for commercial hemp production, processing, manufacturing
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Arkansas
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Ark. Stat. Ann. § 2-15-401 et seq. (2017) Act 981
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates the Arkansas Industrial Hemp Program including a 10-year research program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Authorizes the State Plant Board to adopt rules to administer the research program and license growers.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Requires the State Plant Board to provide an annual report starting Dec. 31, 2018
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows the University of Arkansas’s Division of Agriculture and the Arkansas Economic Development Commission to work with the State Plant Board.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a separate program fund, which will include feeds collected and other sources of funding
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                California
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Cal. Food and Agric. Code §81000 to 81010 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows for a commercial hemp program overseen by the Industrial Hemp Advisory Board within the California Department of Food and Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes registration for seed breeders
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                This division will not become operative unless authorized under federal law.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Colorado
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Colo. Rev. Stat. § 35-61-101 to 35-61-109 (2016) Senate Bill 241
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows hemp cultivation for commercial and research purposes to be overseen by the Industrial Hemp Committee under the Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a seed certification program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a grant program for state institutions of higher education to research new hemp seed varieties.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Connecticut
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           2014 Conn. Acts, P.A. #14-191 (Reg. Sess.)
                        </p>
                        <p class="font_18px font_474646">
                           HB 5780
                        </p>
                        <p class="font_18px font_474646">
                           HB 5476
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Created an industrial hemp feasibility study which reported to the state legislature on Jan. 1, 2015
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Delaware
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Del. Code Ann. tit. 3 § 2800 to 2802 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            HB 385
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Establishes an industrial hemp research program overseen by the Delaware Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the department to certify institutions of higher education to cultivate hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Florida
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           S 1726 (Enacted; Effective June 16, 2017) SB 1726
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Directs the Department of Agriculture and Consumer Services to authorize and oversee the development of industrial hemp pilot projects at certain universities. Commercialization projects may be allowed after two years with certain conditions.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Authorizes the universities to develop pilot projects in partnership with public, nonprofit, and private entities;
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires a university to submit a report within two years of establishing a pilot program.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Hawaii
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Hawaii Rev. Stat. § 141A to 141J and § 712 (2016) Senate Bill 2175
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Establishes an industrial hemp pilot program overseen by the Hawaii Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the Board of Agriculture to certify hemp seeds.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Illinois
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Ill. Ann. Stat. ch. 720 § 550/15.2 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates an industrial hemp pilot program which allows the Illinois Department of Agriculture or state institutions of higher education to grow hemp for research purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires institutions of higher education provide annual reports to the department.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Indiana
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Ind. Code Ann. § 15-15-13-1 to 15-15-13-17 (2016) SB 357
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows the production and possession of hemp by licensed growers for commercial and research purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Growers and handlers of hemp seeds must obtain a hemp seed production license.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Nothing in this section allows anyone to violate federal law.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Kansas
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            K.S.A Ch. 62 § 1 - 62 § 2
                        </p>
                        <p class="font_18px font_474646">
                            Kan. Stat Ann. secs. 2-3901 to 2-3902 (2018).
                        </p>
                        <p class="font_18px font_474646">
                            SB 263 (2018)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates the Alternative Crop Research Act (and licensing fee fund) to promote the research and development of industrial hemp.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the Kansas Department of Agriculture (KDA), either alone or in coordination with a state institute of higher education, to cultivate and promote research and development of industrial hemp.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs KDA to oversee annual licensing, establish fees, and promulgate rules and regulations
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows for a pilot program in Russell County, and other counties as determined by KDA.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Kentucky
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Ky. Rev. Stat. Ann. § 260.850 to 260.869 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates an industrial hemp research program and a commercial licensing program to allow hemp cultivation for any legal purpose.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                The commercial growers’ license shall only be allowed subject to the legalization of hemp under federal law.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Growers are required to use certified seeds and may import or resell certified seeds.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Mandates the University of Kentucky Agricultural Experiment Station oversee a five-year hemp research program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates the Industrial Hemp Commission, attached to the Agricultural Experiment Station, to oversee, among other things, the licensing, testing and implementation of regulations and rules related to hemp.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Maine
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Me. Rev. Stat. Ann. tit. 7 § 2231 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            LD 4
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows hemp growing for commercial purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a license for seed distributors.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Maryland
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Md. Agriculture Code Ann. § 14-101 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            HB 443
                        </p>
                        <p class="font_18px font_474646">
                            HB 698
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a license allowing individuals to plant, grow, harvest, possess, process, sell, or buy industrial hemp in Maryland.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Authorizes the Maryland Department of Agriculture or an institution of higher education to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Massachusetts
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Mass. Gen. Laws. Ann. 128 § 116 to 123 (2017)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                EAllows for hemp to be planted, grown, harvested, possessed, bought or sold for research or commercial purposes under the regulation of the Massachusetts Department of Agricultural Resources (MDAR).
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Requires producers and distributors to obtain a license issued by MDAR and for persons utilizing hemp for commercial or research purposes to register with MDAR.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs MDAR and Commissioner of Agriculture to promulgate rules and regulations.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Michigan
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Mich. Comp. Laws § 286.841 to 286.844 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates an industrial hemp research program allowing the Michigan Department of Agriculture and Rural Development and institutions of higher education to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Minnesota
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Minn. Stat. § 18K.01 to 18K.09 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes a commercial hemp licensing program overseen by the Minnesota commissioner of agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Applicants must prove they comply with all federal hemp regulations, meaning that commercial licenses may not be available until federal law changes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows the commissioner to implement an industrial hemp pilot program. Institutions of higher education may apply to participate in this program.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Missouri
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            HB 2034 (2018)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates an industrial hemp agricultural pilot program, in accordance with federal law, to be implemented by the Missouri Department of Agriculture (MDA) to study the growth, cultivation, processing, feeding and marketing.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Creates the Industrial Hemp Fund.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs MDA to promulgate rules, such as establishing permit and registration fees, to implement the program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the Missouri Crop Improvement Association to establish and administer a seed certification program; specifies the food containing industrial hemp may not be considered adulterated.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Montana
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Mont. Code Ann. § 80-18-101 to 80-18-111 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows the Montana Department of Agriculture to implement a commercial hemp licensing program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Requires commercial growers to use certified seeds.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Requires a federal controlled substances registration from the DEA for the affirmative defense against marijuana charges to apply.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Nebraska
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Neb. Rev. Stat. § 2-5701 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows a postsecondary institution or the Nebraska Department of Agriculture to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Nevada
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Nev. Rev. Stat. § 557.010 to 557.080 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            SB 305
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Mandates the Nevada Board of Agriculture implement an industrial hemp pilot program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows institutions of higher education and the Nevada Department of Agriculture to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                New Hampshire
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            N.H. Rev. Stat. Ann. § 433-C:1 to 433-C:3 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            HB 421
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows institutions of higher education to cultivate hemp for research purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                All research must be coordinated with the New Hampshire Department of Agriculture, Markets and Food.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                All research projects must conclude within three years of commencement.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                New Jersey
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            AB 1330 / SB 3145 (2018)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the New Jersey Department of Agriculture to create a pilot program to research industrial hemp cultivation.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Exempts anyone participating in the agricultural pilot program from crimes and penalties relating to the purchase, sale, or cultivation of marijuana.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                New Mexico
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            SB 6 (2017)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the New Mexico Department of Agriculture to adopt rules for the research and development of industrial hemp, including for licensure, law enforcement training, inspection, recordkeeping, fees and compliance processes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Establishes the New Mexico Industrial Hemp Research and Development Fund.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                New York
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            N.Y. Agriculture and Markets Law § 505 to 508 (McKinney 2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows the growth of hemp as part of an agricultural pilot program by the Department of Agriculture and Markets and/or an institution of higher education.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                The commissioner of agriculture and markets may authorize no more than 10 sites for growing hemp as part of a pilot program
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                The commissioner may develop regulations to authorize the acquisition and possession of industrial hemp seeds.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                1 NYCRR 159.2 allows authorized growers to possess, grow and cultivate seeds and hemp plants.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                North Carolina
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            N.C. Gen. Stat. § 106-568.50 to 106-568.54 and § 90-87(16) (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Creates an agricultural hemp pilot program overseen by the North Carolina Industrial Hemp Commission within the North Carolina Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                The commission must collaborate with North Carolina State University and North Carolina A&T State University.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                North Dakota
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            N.D. Cent. Code § 4-41-01 to 4-41-03 and § 4-05.1-05 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            HB 1438
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows hemp cultivation for commercial or research purposes overseen by the North Dakota agricultural commissioner.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Growers must use certified seeds. Licensees may import, resell and plant hemp seeds.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Permits the North Dakota State University-Main Research Center to conduct research on industrial hemp and hemp seeds.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Oklahoma
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            OK ST T. 2 § 3-401-3-410
                        </p>
                        <p class="font_18px font_474646">
                            Title 35
                        </p>
                        <p class="font_18px font_474646">
                            HB 2913 (2018)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Creates the Oklahoma Industrial Hemp Agriculture Pilot Program and revolving fund for the program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows universities, or subcontractors, to cultivate industrial hemp for research and development purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the Oklahoma Department of Agriculture, Food, and Forestry to manage the pilot program, establish a certified seed program, and promulgate rules related to licensing, inspections.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Oregon
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Or. Rev. Stat § 571.300 to § 571.315 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            SB 676
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows individuals registered by the Oregon Department of Agriculture to grow hemp for commercial purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Growers and handlers who intend to sell or distribute seeds must be licensed as seed producers.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Pennsylvania
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                           Pa. Cons. Stat. Ann. tit. 3 § 701 to 710 (Purdon 2016)
                        </p>
                        <p class="font_18px font_474646">
                            HB 967
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Allows institutions of higher education or the Department of Agriculture of the commonwealth to research hemp under an industrial hemp pilot program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               This chapter shall expire if the secretary of agriculture of the Commonwealth determines a federal agency is authorized to regulate hemp.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Rhode Island
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            R.I. Gen. Laws § 2-26-1 to 2-26-9 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            H8232
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Establishes a commercial hemp program overseen by the Department of Business Regulation.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the Division of Agriculture in the Department of Environmental Management to assist the Department of Business Regulation in regulating hemp.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Growers must verify they are using certified seeds.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               The department shall authorize institutions of higher education to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                South Carolina
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            S.C. Code Ann. § 46-55-10 to 46-55-40 (Law. Co-op 2016)
                        </p>
                        <p class="font_18px font_474646">
                            SB 839
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows hemp growth for commercial and research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Tennessee
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Tenn. Code Ann. § 43-26-101 to 43-26-103 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            SB 2495/HB 2445
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows commercial hemp production overseen by the Tennessee Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Directs the commissioner of agriculture to develop licensing rules for processors and distributors.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows institutions of higher education to acquire and study seeds for research and possible certification.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Utah
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Utah Code Ann. § 4-41-101 to 4-41-103 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            House Bill 105
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the Utah Department of Agriculture to grow hemp for research purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires that the department certify institutions of higher education to grow hemp for research purposes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Vermont
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Vt. Stat Ann. tit. 6 § 561-566 (2016)
                        </p>
                        <p class="font_18px font_474646">
                            SB157
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                              Allows for commercial hemp production overseen by the Vermont secretary of agriculture, food and markets.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires the registration form advise applicants that hemp is still listed and regulated as cannabis under the federal Controlled Substances Act.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Virginia
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Va. Code § 3.2-4112 to 3.2-4120 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                              Authorizes research and commercial hemp programs overseen by the Virginia Board of Agriculture and Consumer Services and the Virginia commissioner of agriculture and human services.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               The commissioner must establish separate licenses for the research program and for commercial growers.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Nothing in this chapter allows individuals to violate federal laws.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Washington
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Wash. Rev. Code Ann. § 15.120.005 to 15.120.050
                        </p>
                        <p class="font_18px font_474646">
                            SB 6206
                        </p>
                        <p class="font_18px font_474646">
                            HB 2064
                        </p>
                        <p class="font_18px font_474646">
                            SB 5131
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                              Allows hemp production as part of a research program overseen by the Washington State Department of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires the department establish a seed certification program.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                West Virginia
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            W. Va. Code. § 19-12E-1 to 19-12E-9 (2016)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                              Allows hemp production for commercial purposes by growers licensed by the West Virginia Commissioner of Agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Growers must use seeds which produce plants containing less than 1 percent THC
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Wisconsin
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §94.55
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §94.67
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §97.02
                        </p>
                        <p class="font_18px font_474646">
                            §348.27
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §961.14
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §961.32
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §961.442
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §961.55
                        </p>
                        <p class="font_18px font_474646">
                            Wis. Stat. §973.01 (effective Dec. 2, 2017)
                        </p>
                        <p class="font_18px font_474646">
                            (Also, see 2017 Act 100 or S.B. 119.)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Directs the state Department of Agriculture, Trade and Consumer Protection (DATCP) to establish a state industrial hemp program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Includes GPS coordinates, fee payment and a criminal history search as requirements for licenses.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Directs the DATCP to establish and administer a seed certification program or designate another agency or organization to administer the program.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Requires the DATCP to create a pilot program to study the growth, cultivation and marketing of industrial hemp.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Specifies exemptions from prosecution under the state Uniform Controlled Substances Act.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Amends the definition of agricultural commodity to include industrial hemp.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lavs_collapse_block">
            <div class="lavs_collapse_block_title">
                Wyoming
            </div>
            <div class="lavs_collapse_block_content">
                <div class="lavs_collapse_block_content_container">
                    <div class="lavs_collapse_block_content_citation font_baskervile">
                        <p class="font_22px font_474646">
                            Citation
                        </p>
                        <p class="font_18px font_474646">
                            Wyo. Stat. § 35-7-2101 to 35-7-2107 (effective July 1, 2017)
                        </p>
                    </div>
                    <div class="lavs_collapse_block_content_summary">
                        <p class="summary__title font_baskervile font_22px font_474646">
                            Summary
                        </p>
                        <div class="summary__content_container">
                            <div class="summary_content font_futura font_18px font_474646">
                                Authorizes the planting, growing, harvesting, possession, processing, or sale of industrial hemp for licensed individuals.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Provides for licensing requirements and rule-making authority by the state department of agriculture.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Allows the University of Wyoming and the state department of agriculture to grow industrial hemp for research purposes.
                            </div>
                            <div class="summary_content font_futura font_18px font_474646">
                               Provides an affirmative defense for marijuana possession or cultivation of marijuana for licensed industrial hemp growers.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

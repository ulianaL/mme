{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{if $edit == 0}
<div class="panel">
<table class="table">
  <thead>
    <tr>
      <th>#</th>
	  <th>Logo</th>
      <th>Name</th>
      <th>About</th>
      <th>Bio</th>
      <th>Categorie</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Status</th>
      <th>Edit</th>
      <th>Order OK</th>
	  
     
    </tr>
  </thead>
  
  <tbody>
   

   {foreach from=$results item=result}
   
   <tr {if $result.pay == 1} style="background: #86d241;"{/if}>
      <th scope="row">{$result.id_directory	}</th>
     <td>
	  {if $result.logo != ""}
				<img style="width:50px;" src="{$module_link}/modules/medicalmarijuanaexchangedirectory/upload/{$result.logo}"/>
				{/if}
	  </td>

	 <td>{$result.name}</td>
      <td style="max-width:300px;">{$result.about}</td>
      <td style="max-width:300px;">{$result.bio}</td>
	  <td>{$result.cat}</td>
      <td>{$result.email}</td>
      <td>{$result.phone}</td>
     
	   
	   
	   <td style="  text-align: center;">
	   
	   {if $result.review == 0}
		<a href="{$linkdosub}&updatee=1&idd={$result.id_directory}">
		<img style="width:25px" src="http://icons.iconarchive.com/icons/ampeross/qetto-2/256/no-icon.png">
	   </a >
	   {/if}
	   
	   {if $result.review == 1}
	   <a href="{$linkdosub}&updatee=0&idd={$result.id_directory}">
		<img style="width:25px"  src="http://findicons.com/files/icons/1008/quiet/256/valid.png">
	   </a>
	   {/if}
	   
	   
	   
	   
	   </td>
	   
	   <td>
	   <a href="{$linkdosub}&edit={$result.id_directory}" class="button">Edit</a>
	   
	   </td>

      
	  <td 
	  {if $result.pay == 1}
		  {if $result.doorderandpay == 0}
		  style="background: red;color:white;text-align:center"
		  {else}
		  style="background: green;color:white;text-align:center"
		  {/if}
	  {else}
	  style="background: #828282;color:white;text-align:center"
	  {/if}
	  
	  >
	  {$result.customer}
	  <br>
	   {$result.name}
	  </td>
	  
	  
	  
	  
	  
    </tr>
    {/foreach}
	
	

 
	
	
  </tbody>
</table>	
</div>



{***************************** **************************************}
{else}




{if isset($smarty.get.dev)}
<pre>
{$row|@print_r}
</pre>
{/if}

<div class="panel">
<h1>edit {$toedit}</h1>
	<form action="#" method="post" enctype="multipart/form-data">
	<h2>List your cannabis organization in the directory</h2>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Name*</label>
    <input type="text" name="name" value="{$row.name}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="">
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>

{* added new rows SoftSprint Uliana *}
  <div class="form-group">
    <label for="exampleInputEmail1">Country</label>
	    {if $row.country ==1}
	    <p>USA</p>
	    {else}
	    <p>CANADA</p>
	    {/if}
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">State</label>
	    <p>{$state}</p>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">City</label>
	    <p>{$city}</p>
  </div>

  <div class="form-group">
    <label for="exampleSelect1">Category*</label>
    <select name="cat" class="form-control" id="exampleSelect1" data-placeholder="Select a category..." required="">
							                                    
							<option value="1" {if $row.cat == 1}selected{/if} >Accounting Firms</option>
                            <option value="2" {if $row.cat == 2}selected{/if} >Advertising / Marketing</option>
                            <option value="3" {if $row.cat == 3}selected{/if} >Apparel &amp; Accessories</option>
                            <option value="4" {if $row.cat == 4}selected{/if} >Breeders, Genetics, or Clones</option>
                            <option value="5" {if $row.cat == 5}selected{/if} >Consulting</option>
                            <option value="6" {if $row.cat == 6}selected{/if} >Detoxification</option>
                            <option value="7" {if $row.cat == 7} selected {/if} > Dispensaries, Collectives and Wellness Centers</option>
                            <option value="8" {if $row.cat == 8} selected {/if}>Doctors &amp; Referral Services</option>
                            <option value="9" {if $row.cat == 9} selected {/if}>Education</option>
                            <option value="10" {if $row.cat == 10} selected {/if}>Extraction Companies</option>
                            <option value="11" {if $row.cat == 11} selected {/if} >Glass Companies</option>
                            <option value="12" {if $row.cat == 12} selected {/if} >Government Agencies</option>
                            <option value="13" {if $row.cat == 13} selected {/if} >Hemp-Based Products</option>
                            <option value="14" {if $row.cat == 14} selected {/if} >Hospitality &amp; Tourism</option>
                            <option value="15" {if $row.cat == 15} selected {/if} >Infused Product Companies</option>
                            <option value="16" {if $row.cat == 16} selected {/if} >Investment</option>
                            <option value="17" {if $row.cat == 17} selected {/if} >Legal</option>
                            <option value="18" {if $row.cat == 18} selected {/if} >Lighting</option>
                            <option value="19" {if $row.cat == 19} selected {/if} >Manufacturers</option>
                            <option value="20" {if $row.cat == 20} selected {/if} >Organizations &amp; Non-Profits</option>
                            <option value="21" {if $row.cat == 21} selected {/if} >Public Companies</option>
                            <option value="22" {if $row.cat == 22} selected {/if} >Publications/Resources</option>
                            <option value="23" {if $row.cat == 23} selected {/if} >Research / Testing Labs</option>
                            <option value="24" {if $row.cat == 24} selected {/if} >Retailers</option>
                            <option value="25" {if $row.cat == 25} selected {/if} >Security / Insurance</option>
                            <option value="26" {if $row.cat == 26} selected {/if} >Seed Banks</option>
                            <option value="27" {if $row.cat == 27} selected {/if} >Soil, Composts &amp; Nutrients</option>
                            <option value="28" {if $row.cat == 28} selected {/if} >Technology</option>
                    
    </select>
  </div>
  
   <div class="form-group">
    <label for="exampleTextarea">About*</label>
    <textarea style="width:300px;" class="form-control" name="about" id="exampleTextarea" rows="15" required="">{$row.about}</textarea>
  </div>
  
   <div class="form-group">
    <label for="exampleTextarea">Company Biography</label>
    <textarea style="width:300px;" class="form-control" name="bio" id="bio" rows="15">{$row.bio}</textarea>
  </div>
  
  <div class="form-group">
    <label for="fileToUpload">Logo</label>
    {if $row.logo != ""}
	<img style="max-width: 100px; border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" src="{$module_link}modules/medicalmarijuanaexchangedirectory/upload/{$row.logo}"/>
	{/if}
	<input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file" aria-describedby="fileHelp">
  </div>
  
   <div class="form-group">
    <label for="exampleInputEmail2">Email</label>
    <input type="email" name="email"  value="{$row.email}" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp2" placeholder="">
    <small id="emailHelp2" class="form-text text-muted"></small>
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail11">Phone</label>
    <input type="text" name="phone"  value="{$row.phone}" class="form-control" id="exampleInputEmail11" aria-describedby="emailHelp11" placeholder="">
    <small id="emailHelp11" class="form-text text-muted"></small>
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail11">Video Url</label>
    <input type="text" name="videourl"  value="{$row.videourl}" class="form-control" id="exampleInputEmail11" aria-describedby="emailHelp11" placeholder="">
    <small id="emailHelp11" class="form-text text-muted"></small>
  </div>
 
{if $row.pay == 1} 
			  <div class="form-group">
				<label for="exampleInputEmail111">Address</label>
				<textarea type="text" name="address" value="{$row.address}" class="form-control" id="exampleInputEmail111" aria-describedby="emailHelp111" placeholder="">{$row.address}</textarea>
				<small id="emailHelp111" class="form-text text-muted"></small>
			  </div>
	


<div class="col-md-12">
             <address> {$row.address} </address>
          </div>
{literal}	
 <script>
     $(document).ready(function(){
        $("address").each(function(){
             var embed ="<iframe width='425' height='350' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'  src='https://maps.google.com/maps?&amp;q="+encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
             $(this).html(embed);
       });
     });
 </script> 
{/literal}	






			  
			  
			  <div class="form-group">
				<label for="exampleInputEmail111">Website</label>
				<input type="text" name="website" value="{$row.website}" class="form-control" id="exampleInputEmail111" aria-describedby="emailHelp111" placeholder="">
				<small id="emailHelp111" class="form-text text-muted"></small>
			  </div>
			  
			  <div class="form-group">
				
				<label for="twitter">Twitter</label>
				<div class="input-group">

				<span class="input-group-addon">@</span>
				<input type="text" name="twitter"  value="{$row.twitter}" class="form-control" id="twitter" aria-describedby="twitter" placeholder="">
				</div>
				<small id="twitter" class="form-text text-muted"></small>
			  </div>
			  
			  
			  <div class="form-group">
				
				<label for="facebook">Facebook</label>
				<div class="input-group">

				<span class="input-group-addon">https://www.facebook.com/	</span>
				<input type="text" name="facebook"  value="{$row.facebook}"  class="form-control" id="facebook" aria-describedby="facebook" placeholder="">
				</div>
				<small id="facebook" class="form-text text-muted"></small>
			  </div> 
			  
			  <div class="form-group">
				
				<label for="youtube">YouTube</label>
				<div class="input-group">

				<span class="input-group-addon">https://www.youtube.com/</span>
				<input type="text" value="{$row.youtube}" name="youtube" class="form-control" id="youtube" aria-describedby="youtube" placeholder="">
				</div>
				<small id="youtube" class="form-text text-muted"></small>
			  </div>
			  
			  <div class="form-group">
				
				<label for="instagram">Instagram</label>
				<div class="input-group">

				<span class="input-group-addon">https://instagram.com/</span>
				<input type="text" name="instagram" value="{$row.instagram}" class="form-control" id="instagram" aria-describedby="instagram" placeholder="">
				</div>
				<small id="instagram" class="form-text text-muted"></small>
			  </div>
			  
			  
			  <div class="form-group">
				
				<label for="pinterest">Pinterest</label>
				<div class="input-group">

				<span class="input-group-addon">https://pinterest.com/</span>
				<input type="text"  value="{$row.pinterest}"  name="pinterest" class="form-control" id="pinterest" aria-describedby="pinterest" placeholder="">
				</div>
				<small id="pinterest" class="form-text text-muted"></small>
			  </div>
			 
			 
			  <h1>Image Slider</h1>
			  <hr>
			  <div class="form-group"  style="border-bottom: 1px solid #333;padding-bottom: 18px;">>
				<label for="image1">Image 1</label>
				


				{if $row.image1 != ""}
				<img  style="    max-width: 100px;border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" class="img-responsive" src="{$module_link}/modules/medicalmarijuanaexchangedirectory/gal/{$row.image1}"/>
				{/if}
				<input type="file" name="image1" id="image1" class="form-control-file" aria-describedby="fileHelp">
			  </div>


			  <div class="form-group"   style="border-bottom: 1px solid #333;padding-bottom: 18px;">>
				<label for="image2">Image 2</label>
				{if $row.image2 != ""}
				<img  style="    max-width: 100px;border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" class="img-responsive"  src="{$module_link}/modules/medicalmarijuanaexchangedirectory/gal/{$row.image2}"/>
				{/if}
				<input type="file" name="image2" id="image2" class="form-control-file" aria-describedby="fileHelp">
			  </div>

			  <div class="form-group"   style="border-bottom: 1px solid #333;padding-bottom: 18px;">>
				<label for="image3">Image 3</label>
				{if $row.image3 != ""}
				<img  style="    max-width: 100px;border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" class="img-responsive" src="{$module_link}/modules/medicalmarijuanaexchangedirectory/gal/{$row.image3}"/>
				{/if}
				<input type="file" name="image3" id="image3" class="form-control-file" aria-describedby="fileHelp">
			  </div>
			  
			  <div class="form-group"   style="border-bottom: 1px solid #333;padding-bottom: 18px;">>
				<label for="image4">Image 4</label>
				{if $row.image4 != ""}
				<img  style="    max-width: 100px;border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" class="img-responsive" src="{$module_link}/modules/medicalmarijuanaexchangedirectory/gal/{$row.image4}"/>
				{/if}
				<input type="file" name="image4" id="image4" class="form-control-file" aria-describedby="fileHelp">
			  </div>
			 
			  
			  <div class="form-group"   style="border-bottom: 1px solid #333;padding-bottom: 18px;"> >
				<label for="image5">Image 5</label>
				{if $row.image5 != ""}
				<img  style="    max-width: 100px;border: 4px solid #282b30;margin: 9px;margin-left: 0; border-radius: 11px;" class="img-responsive" src="{$module_link}/modules/medicalmarijuanaexchangedirectory/gal/{$row.image5}"/>
				{/if}
				<input type="file" name="image5" id="image5" class="form-control-file" aria-describedby="fileHelp">
			  </div>
  
{/if}
 
 
 <input type="hidden" name="idtoupdate" value="{$row.id_directory}" />
 <input type="hidden" name="submitaddfree">
  <div class="form-group" style="">
 <label > </label>
 <button type="submit" style="height:50px" name="submit" class="btn btn-primary right">Update</button>
 
 
 
 
 
 </div>

</form>

<div>
<button  id="delete123" style="height:50px;background:red;"   class="btn btn-primary right">DELETE</button>
</div>

<script>
$('#delete123').click(function(event){
     if(!confirm("You are sure? (you will delete id: {$row.id_directory})")){
        event.preventDefault();
      }else{
	     
		 urlx = '{$token}&configure=medicalmarijuanaexchangedirectory&tab_module=administration&deletee={$row.id_directory}';
		// alert(urlx) ;
	   
         window.location = urlx;
	  

	 
	  }
    });
</script>

{* Added Go Back button SoftSprint Uliana *}
<div class="back_button"><a href="javascript:history.back();">Go to Back</a></div>

</div>

{/if}








<style>

</style>

/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/


function initMap() {
    geocoder = new google.maps.Geocoder();
   geoInfo(adr)
   //pos = {lat: 34.0522342, lng: -118.2436849};
   initializeMap(pos);
 
}
function geoInfo(adr) {
    if (adr == undefined)
        return false;

    geocoder.geocode({'address': adr}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
             pos = {
                 lat: results[0].geometry.location.lat(),
                 lng: results[0].geometry.location.lng()
             }
            map.setCenter(results[0].geometry.location);
        }
    });
}
function initializeMap(pos, ac_length) {
    var styles = [
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#060500"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#060500"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural.landcover",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.attraction",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.government",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.medical",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.place_of_worship",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.school",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.sports_complex",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fffefb"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fffefb"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fcfcfc"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.station.bus",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ];

// Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(styles);

    zoom = 11;
    if (ac_length != undefined) {
        zoom = ac_length;
//            if (ac_length == 1) {
//                // Assume country
//                zoom = 8;
//            } else if (ac_length == 2) {
//                // Assume city
//                zoom = 6;
//
//            } else if (ac_length == 14) {
//                // Assume city
//                zoom = 14;
//
//            } else {
//                // Everything else can have a standard zoom level
//                zoom = 9;//11
//            }
    }

    map = new google.maps.Map(document.getElementById('map'), {
        center: pos, //{lat: 34.0522342, lng: -118.2436849},
        zoom: zoom,
        streetViewControl: false,
//            zoomControl: true,
//                zoomControlOptions: {
//                    style: google.maps.ZoomControlStyle.SMALL,
//                    position: google.maps.ControlPosition.RIGHT_TOP
//                }
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    });
    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    google.maps.event.addListener(map, "dragend", function () {
        // for (var i = 0; i < infoWindows.length; i++) {  //I assume you have your infoboxes in some array
        //     infoWindows[i].close();
        // }
        // $(".info-window-for-mobile").hide();
        // mylat = map.getCenter().lat();
        // mylng = map.getCenter().lng();
        // if (map.getBounds() != undefined) {
        //     nelat = map.getBounds().getNorthEast().lat();
        //     nelng = map.getBounds().getNorthEast().lng();
        //     distance_ = {'latf': mylat, 'lngf': mylng, 'latt': nelat, 'lngt': nelng};
        //     distance_ = ((getDistance(distance_) / 1000) / 10); // 2 - pol radiusa 6 - odna 6-aya
        // } else {
        //     distance_ = 10;
        // }

        // distance = {'latf': mylat, 'lngf': mylng, 'latt': posMapCenter.lat, 'lngt': posMapCenter.lng};
        // distance = (getDistance(distance) / 1000);
        // if (distance < distance_)
        //     return false;
        // clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);
    });
    google.maps.event.addListener(map, 'zoom_changed', function () {
        // for (var i = 0; i < infoWindows.length; i++) {  //I assume you have your infoboxes in some array
        //     infoWindows[i].close();
        // }
        // $(".info-window-for-mobile").hide();
        // clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);
    });
    google.maps.event.addListener(map, 'resize', function () {
        // for (var i = 0; i < infoWindows.length; i++) {  //I assume you have your infoboxes in some array
        //     infoWindows[i].close();
        // }
        // $(".info-window-for-mobile").hide();
        // clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);
    });

    google.maps.event.addListenerOnce(map, 'idle', function () {
    	// clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);
    });

    google.maps.event.addListener(map, "click", function (event) {
        for (var i = 0; i < infoWindows.length; i++) {  //I assume you have your infoboxes in some array
            infoWindows[i].close();
        }
        $(".info-window-for-mobile").hide();
  

    });
    
    
}


function assingFilterVariables()
{
    $('#deliveries_check').prop('checked') ? delivery = 1 : delivery = 0;
    $('#storefronts_check').prop('checked') ? storefront = 1 : storefront = 0;
    $('#doctors_check').prop('checked') ? is_doctor = 1 : is_doctor = 0;

    $('#order_pickup_check').prop('checked') ? order_pickup = 1 : order_pickup = 0;
    $('#order_delivery_check').prop('checked') ? order_delivery = 1 : order_delivery = 0;

    $('.open-now-btn').hasClass('active-filter') ? open_now = 1 : open_now = 0;
    $('.medical-btn').hasClass('active-filter') ? medical = 1 : medical = 0;
    $('.recreational-btn').hasClass('active-filter') ? recreational = 1 : recreational = 0;
}

$(document).ready(function(){

    $('.dispensaries_filter_block.check .filter-btn').on('click', function(){
        $(this).toggleClass('active-filter');
    });

    $('.filter-btn').on("click", function () {
   
        // $('#deliveries_check').parent().hasClass('checked') ? delivery = 1 : delivery = 0;
        // $('#storefronts_check').parent().hasClass('checked') ? storefront = 1 : storefront = 0;
        // $('#doctors_check').parent().hasClass('checked') ? is_doctor = 1 : is_doctor = 0;

        // $('#order_pickup_check').parent().hasClass('checked') ? order_pickup = 1 : order_pickup = 0;
        // $('#order_delivery_check').parent().hasClass('checked') ? order_delivery = 1 : order_delivery = 0;

        assingFilterVariables();
        // clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);

    });

    $('.custom_select_content_check_parent input').on("change", function () {
   
        // $('#deliveries_check').parent().hasClass('checked') ? delivery = 1 : delivery = 0;
        // $('#storefronts_check').parent().hasClass('checked') ? storefront = 1 : storefront = 0;
        // $('#doctors_check').parent().hasClass('checked') ? is_doctor = 1 : is_doctor = 0;

        // $('#order_pickup_check').parent().hasClass('checked') ? order_pickup = 1 : order_pickup = 0;
        // $('#order_delivery_check').parent().hasClass('checked') ? order_delivery = 1 : order_delivery = 0;

        assingFilterVariables();
        // clearMarkers();
        updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational);

    });
});

function rad (x) {
    return x * Math.PI / 180;
};

var getDistance = function (distance) {
    var R = 6378137; // Earth's mean radius in meter
    var dLat = rad(distance.latt - distance.latf);
    var dLong = rad(distance.lngt - distance.lngf);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(distance.latf)) * Math.cos(rad(distance.latt)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};
 
function updateFeaturedList(map, cityName, regionName, delivery, storefront, open_now, medical, recreational) {
	var polygone = [],
        distance = 250,
        date = new Date();

    // find distance
    if (map.getCenter() != undefined) {
        mylat = map.getCenter().lat();
        mylng = map.getCenter().lng();
        posMapCenter = {
            lat: mylat,
            lng: mylng
        };
    }
    if (map.getBounds() != undefined) {
        nelat = map.getBounds().getNorthEast().lat();
        nelng = map.getBounds().getNorthEast().lng();

        swlat = map.getBounds().getSouthWest().lat();
        swlng = map.getBounds().getSouthWest().lng();

        nwlat = nelat;
        nwlng = swlng;

        selat = swlat;
        selng = nelng;

        distance = {'latf': mylat, 'lngf': mylng, 'latt': nelat, 'lngt': nelng};
        distance = (getDistance(distance) / 1000);

        polygone.push(nwlng + ' ' + nwlat);
        polygone.push(nelng + ' ' + nelat);
        polygone.push(selng + ' ' + selat);
        polygone.push(swlng + ' ' + swlat);
    }

    console.log(polygone);
    var nb_business_types = parseInt(delivery) + parseInt(storefront) + parseInt(is_doctor);
    $(".busines_types_counter").text(nb_business_types);

    $.ajax({
        type: 'post',
        cache: false,
        async: true,
        data: {
            getstores: 1,
            cityName: cityName,
            regionName: regionName,
            delivery: delivery,
            storefront: storefront,
            open_now: open_now,
            medical: medical,
            recreational: recreational,
            current_time: date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(),
            polygone: polygone,
            order_delivery: order_delivery,
            order_pickup: order_pickup,
            is_doctor: is_doctor
        },
        dataType: 'json',
        success: function(data){
            clearMarkers();
            if(data.length == 0){
                $('.no-featured-list').html('');
            }else{
                var content='';
                for (i = 0; i < data.length; i++) {
                    if (data[i].name !== null) {
                        var type;
                        var shop__image;
                        // var current_work_hours;
                        var icon;
                       
                        if (data[i].delivery == '1') {
                            type = '<span class="delivery_type">Delivery</span>';
                            icon = 'https://medicalmarijuanaexchange.com/img/delivery.png'; // put url to map marker
                            shop__image = '<img src="/themes/default-bootstrap/img/delivery_icon.svg" />';
                        } else if (data[i].storefront == '1') {
                            type = '<span class="storefront_type">Storefront</span>';
                            icon = 'https://medicalmarijuanaexchange.com/img/shop.png'; // put url to map marker
                            shop__image = '<img src="/themes/default-bootstrap/img/cannabis_icon.svg" />';
                        } else if (data[i].is_doctor == '1') {
                            type = '<span class="storefront_type">Doctor</span>';
                            icon = 'https://medicalmarijuanaexchange.com/img/doctor.png'; // put url to map marker
                            shop__image = '<img src="/themes/default-bootstrap/img/doctor-stethoscope.svg" />';
                        } else {
                            type = '<span class="delivery_type">Unknown</span>';
                            icon = undefined;
                        }

                        content += '<div class="company-list-item dispensaries-list-item" data-info="'+data[i].id_customer+'">' +
                        '               <div class="list-image-part disp-header">' +
                                            '<div class="company-logo" style="background-image: url(/img/cust/' + data[i].logopic + ');"></div>' +
                                        '</div>' +
                                        '<div class="list-content-part disp-header">' +
                            
                                            '<a href="/shop?store_id_all=' + data[i].id_customer + '" class="company-name font_futura">' + 
                                                shop__image +
                                                data[i].name + 
                                            '</a>'+
                                             '<div class="bistartrev"><div class="br-wrapper br-theme-css-stars"> <div class="br-widget br-readonly">';

                                             for (j = 1; j <= 5; j++) {
                                             	if (j < data[i].averageTotal) {
                                             		content += '<a href="#" data-rating-value="'+j+'" data-rating-text="'+j+'" class="br-selected"></a>';
                                             	}else{
                                             		content += '<a href="#" data-rating-value="'+j+'" data-rating-text="'+j+'" class="br-fractional"></a>';
                                             	}
                                            	
                                             }
                                            
                                             content += '</div><span>'+data[i].averageTotal+' by '+data[i].rev_quantity+' reviews </span></div></div>';
                                            content += '<div class="font_futura delivery_address">'+
                                                '<img class="dispensaries_location_item" src="/themes/default-bootstrap/img/dark_marker.svg" /> '+
                                                data[i].address +
                                            '</div>'+
                                  
                                            '<div class="font_futura delivery_clock">' +
                                                '<img class="dispensaries_location_item" src="/themes/default-bootstrap/img/dark_clock.svg" /> '+
                                                ((typeof data[i].open != 'undefined' && data[i].open) ? '<b>Open now</b>' : '<b>Closed</b>') +
                                            '</div>' +
                                             '<div class="dispensaries_shop_button">'+
                                                 '<a href="/shop?store_id_all=' + data[i].id_customer + '" target="_blank">View Menu</a>' +
                                             '</div>'+
                                        '</div>'+
                                    '</div>';
                        if (data[i].storeLat && data[i].storeLng) {
                            var marker = new google.maps.Marker({
                                icon: icon, //*** uncomment this for different map icons ***
                                position: new google.maps.LatLng(data[i].storeLat, data[i].storeLng),
                                map: map,
                                //frommmj: data.data[i].frommmj,
                                //businessType: data.data[i].businessType,
                                markerId: data[i].id_customer
                            });

                            var info_w = '<div class="info-window-company">'+
                                            '<div class="window-image-part">' +
                                                '<div class="company-logo" style="background-image: url(/img/cust/' + data[i].logopic + ');"></div>'
                                            +'</div>'
                                            +'<div class="window-content-part disp-header">' 
                            
                                            +'<h1 class="company-name" style="margin-bottom: 0px; min-width: 90px; display: inline-block"><a href="https://medicalmarijuanaexchange.com/shop?store_id_all=' + data[i].id_customer + '">' + data[i].name + '</a></h1>'
                                             +'<div class="bistartrev" style="margin-left: 10px;"><div class="br-wrapper br-theme-css-stars"> <div class="br-widget br-readonly">';

                                                for (j = 1; j <= 5; j++) {
                                                    if (j < data[i].averageTotal) {
                                                        info_w += '<a href="#" data-rating-value="'+j+'" data-rating-text="'+j+'" class="br-selected"></a>';
                                                    }else{
                                                        info_w += '<a href="#" data-rating-value="'+j+'" data-rating-text="'+j+'" class="br-fractional"></a>';
                                                    }
                                                    
                                                }
                                            
                                            info_w += '</div><span>'+data[i].averageTotal+' by '+data[i].rev_quantity+' reviews </span></div></div><div>'+
                                             '<div style="margin-left: 10px; margin-top: 10px;">' +
                                                ((typeof data[i].open != 'undefined' && data[i].open) ? '<b>Open now</b>' : '<b>Closed</b>') +
                                            '</div>'
                                            +'</div>'
                                        +'</div>'
                            var infowindow = new google.maps.InfoWindow();
                            google.maps.event.addListener(marker, 'click', (function (marker, info_w, infowindow) {
                                return function () {
                                    for (var j = 0; j < infoWindows.length; j++) {
                                        infoWindows[j].close();
                                    }
                                    infowindow.setContent(info_w);
                                    //add infowindow to array
                                    infoWindows.push(infowindow);

                                    infowindow.open(map, marker);
                                    
                                };
                            })(marker, info_w, infowindow));
                            markersArray.push(marker);
                        }
                        
                    }
                    
                }
                $('.no-featured-list').html(content);
                initShowInfo();
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            
        }
    });
}


function clearMarkers() {
        // info_Window = new google.maps.InfoWindow();
        // info_Window.close();
        if (markersArray.length > 0) {
            for (i in markersArray) {
                try {
                    markersArray[i].setMap(null);
                } catch (e) {

                }

            }

        markersArray = [];
        markersIdsArray = [];
        objLists = [];
    }
}

// init show info
function initShowInfo() {

    var zIndex = 500;

    $(".company-list-item").off();
    $(".company-list-item").on('click', function () {
        if ($(this).attr('href') != undefined) {
            return true;
        }

        var key = -1;
        var selLi = $(this).attr('data-info');
        for (var i = 0; i < markersArray.length; i++) {
            if (selLi == markersArray[i].markerId) {
                markersArray[i].setAnimation(google.maps.Animation.BOUNCE);

                zIndex++;

                markersArray[i].setZIndex(zIndex);
                setTimeout(function () {
                    markersArray[i].setAnimation(null);
                }, 700);

                google.maps.event.trigger(markersArray[i], 'click');

                if ($(".map-overlay").is(':visible'))
                {
                    $(".panel-controller").click();
                }

                break;
            }

        }
    });
}

<?php


if (!class_exists('Db')) {
    require(dirname(dirname(dirname(__FILE__))).'/config/config.inc.php');
}

/**
 * MAjax
 */
class MAjax
{
    private $db;
    private $context;
    private $productsPerPage = 9;

    function __construct($db)
    {
        $this->db = $db;
        $this->context = Context::getContext();
        $this->context->currency = new Currency(1);
    }

    public function run()
    {
        $action = Tools::getValue('action', '');
        $method = 'process'.ucfirst($action);
        if ($action && method_exists($this, $method)) {
            $this->{$method}();
        }
        $this->_reponse(array(
            'hasError' => true,
            'message' => 'Action not found'
        ));
    }

    private function processSearchProductsByOwner()
    {
        $id_owner = (int)Tools::getValue('id_owner', 0);
        $categories = Tools::getValue('categories', array());
        if (!is_array($categories)) {
            $categories = array();
        }
        $q = ' IFNULL(p.review, 0) <> 0 ';
        $q .= trim(Tools::getValue('q', '')) !== '' ? ' AND pl.name LIKE \'%'.addslashes(trim(Tools::getValue('q', ''))).'%\'' : '';
        $p = (int)Tools::getValue('p', 0) - 1;
        $limit = $this->productsPerPage;

        $products = Product::getproductsshop(
            $id_owner,
            (int) $this->context->language->id,
            $p,
            $limit,
            false,
            null,
            null,
            $this->context,
            $categories,
            array(),
            $q
        );

        $nb_products = (int)Product::getproductsshop(
            $id_owner,
            (int)$this->context->language->id,
            $p,
            $limit,
            true,
            null,
            null,
            $this->context,
            $categories,
            array(),
            $q
        );
        $this->context->smarty->assign(array(
            'class' => 'blocknewproducts tab-pane',
            'id' => 'blocknewproducts',
            'products' => $products,
            'link' => new Link(null, 'https://'),
            'PS_CATALOG_MODE' => (int)Configuration::get('PS_CATALOG_MODE'),
            'priceDisplay' => false
        ));
        // print_r($this->context->smarty);exit;
        $tpl = $this->context->smarty->createTemplate(
            _PS_ALL_THEMES_DIR_ . 'default-bootstrap/shop-product-list-ajax.tpl',
            $this->context->smarty
        );

        $this->_reponse(array(
            'status' => 'ok',
            'products' => $products,
            'products_html' => $tpl->fetch(),
            'nb_products' => $nb_products,
            'paging' => self::PageNav($p + 1, $nb_products, $limit, array())
        ));
    }

    public static function PageNav($p, $count, $step, $_data =null )
    {
        $start = (int)(($p - 1)*$step);
        if($start<0) {
            $start = 0;
        }
        // $start = 1 * 2;
        // $count = 100;
        // $step = 2;

        $_admin = null; //isset($_data['admin'])?$_data['admin']:null;

        $all_items = 1; //isset($_data['all_items'])?$_data['all_items']:'';
        $is_search =  0; //isset($_data['is_search'])?$_data['is_search']:0;

        $is_arch = 0; //isset($_data['is_arch'])?$_data['is_arch']:0;
        $month = 0; //isset($_data['month'])?$_data['month']:0;
        $year = 0; //isset($_data['year'])?$_data['year']:0;

        // $data_url = $this->getSEOURLs();
        $data_url['news_url'] = 'https://medicalmarijuanaexchange.com/shop?store_id_all=46';
        
        $res = '';
        $product_count = $count;

        $delimeter_rewrite = "&";
        // if(Configuration::get('PS_REWRITING_SETTINGS')){
        //     $delimeter_rewrite = "?";
        // }
        
            $res .= '<div class="pages custom_blog_list" id="page_nav">';
            if($_admin){
                // $data_translate = $this->getTranslateText();
                $res .= '<span>page:</span>';
            }
            $res .= '<span class="nums">';
        $start1 = $start;
            for ($start1 = ($start - $step*4 >= 0 ? $start - $step*4 : 0); $start1 < ($start + $step*5 < $product_count ? $start + $step*5 : $product_count); $start1 += $step)
                {
                    $par = (int)($start1 / $step) + 1;
                    if ($start1 == $start)
                        {
                            $res .= '<b>'. $par .'</b>';
                        }
                    else
                        {
                            if($_admin){
                                $currentIndex = $_data['currentIndex'];
                                $token = $_data['token'];
                                $item = $_data['item'];
                                $res .= '<a href="'.$currentIndex.'&page'.$item.'='.($start1 ? $start1 : 0).$token.'" >'.$par.'</a>';
                            } else {
                                /*$res .= '<a href="javascript:void(0)" onclick="go_page_news( '.($start1 ? $start1 : 0).' )">'.$par.'</a>';*/
                                if(Tools::strlen($all_items)>0){

                                    if($is_search == 1){
                                        // search //
                                        $search = isset($_data['search'])?$_data['search']:'';

                                        if(false && Configuration::get($this->_name.'rew_on')==1) {

                                            $items_url = $data_url['news_url'];
                                            //if(version_compare(_PS_VERSION_, '1.6', '<')) {
                                                $p = ($start1 ? '?p='.$par.'&' : '?').'search='.$search;
                                            /*} else {
                                                $p = ($start1 ? '/' . $par : '') . '?search=' . $search;
                                            }*/

                                        } else {

                                            $items_url = $data_url['news_url'];
                                            $p = ($start1 ? $delimeter_rewrite.'p='.$par.'&' : $delimeter_rewrite).'search='.$search;

                                        }

                                        $res .= '<a href="'.$items_url.$p.'" >'.$par.'</a>';
                                        // search //


                                    } elseif($is_arch == 1){

                                        // posts archive ///
                                        if(false && Configuration::get($this->_name.'rew_on')==1) {

                                            $items_url = $data_url['news_url'];
                                            //if(version_compare(_PS_VERSION_, '1.6', '<')) {
                                                $p = ($start1 ? '?p='.$par.'&' : '?').'y='.$year.'&m='.$month;
                                            /*} else {
                                                $p = ($start1 ? '/' . $par : '') . '?y=' . $year . '&m=' . $month;
                                            }*/

                                        } else {

                                            $items_url = $data_url['news_url'];
                                            $p = ($start1 ? $delimeter_rewrite.'p='.$par.'&' : $delimeter_rewrite).'y='.$year.'&m='.$month;

                                        }

                                        $res .= '<a href="'.$items_url.$p.'" >'.$par.'</a>';
                                        // posts archive ///


                                    }else {
                                        // all posts page
                                        if(false && Configuration::get($this->_name.'rew_on')==1) {

                                            $items_url = $data_url['news_url'];
                                            //if(version_compare(_PS_VERSION_, '1.6', '<')) {
                                                $p = ($start1 ? '?p='.$par : '');
                                            /*} else {
                                                $p = ($start1 ? '/'.$par : '');
                                            }*/


                                        } else {

                                            $items_url = $data_url['news_url'];
                                            $p = ($start1 ? $delimeter_rewrite.'p='.$par : '');

                                        }

                                        $res .= '<a href="'.$items_url.$p.'" title="'.$par.'" data-page="'.$par.'">'.$par.'</a>';
                                        // all posts page
                                    }

                                }
                            }
                        }
                }
        
        $res .= '</span>';
        $res .= '</div>';
        
        return $res;
    }

    private function _reponse(array $data = array())
    {
        if (!headers_sent()) {
            header('Content-Type: application/json');
        }
        die(Tools::jsonEncode($data));
    }
}
if (Tools::getValue('ajax', 0) == 1) {
    (new MAjax(Db::getInstance()))->run();
}

<?php
//LLL 12/09/2018
class MedicalmarijuanaexchangedirectoryPrem extends ObjectModel
{
	
	public $id_prem;
	public $id_customer;
	public $id_order;
	public $premtype;
	public $date_add;
	public $date_end; 
	//1 - Bronze Membership $200
    //2 - Silver Membership $500
	//3 - Gold Membership $1000
    //4- Platinum Membership $2500

	
	
	
	
	
	public static $definition = array(
        'table' => 'prem',
        'primary' => 'id_prem',
        'fields' => array(
            
          
            'id_prem' => array('type' => self::TYPE_INT),
            'id_customer' => array('type' => self::TYPE_INT),
            'id_order' => array('type' => self::TYPE_INT),
            'premtype' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_STRING),
            'date_end' => array('type' => self::TYPE_STRING),
            
            
        ),
    );
	
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
      
	     /*Shop::addTableAssociation('classes', array('type' => 'shop'));*/
        parent::__construct($id, $id_lang, $id_shop);
    }
	
	public function update($null_values = false)
    {
		return parent::update($null_values);
	}
	
	
	
}
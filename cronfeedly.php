<?php

if (php_sapi_name() != 'cli') {
    throw new Exception('This script must be run on the command line.');
}

if (!isset($argv[1]) || $argv[1] != 'n46728b4v25d674cq5e4') {
    echo "The secure key is wrong.\n";
    exit;
}

$_GET['debug'] = 1;

require(dirname(__FILE__).'/config/config.inc.php');

// copy(
// 	'https://stocknews.com/cdn-cgi/image/fit=scale-down,format=auto,q=50,onerror=redirect,height=540/https://stocknews.com/wp-content/uploads/2021/03/shutterstock_328054946-1-scaled.jpg',
// 	dirname(__FILE__) . '/upload/000.jpg'
// );
// exit;

(new FeedlyBoards)
    ->init()
    ->loadArticles();

<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



class BannerCore extends ObjectModel
{
    public $id_banner;
    public $name;
    public $banner_img;
    public $customer;
    public $city;
    public $state;
    public $country;
    public $size;
    public $status;
    public $autoplay;
    public $banner_loop; 
	
   
   
    public static $definition = array(
        'table' => 'banner',
        'primary' => 'id_banner',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'name' =>             array('type' => self::TYPE_STRING),
            'banner_img' =>             array('type' => self::TYPE_STRING),
            'customer' =>             array('type' => self::TYPE_INT),
            'city' =>             array('type' => self::TYPE_INT),
            'state' =>             array('type' => self::TYPE_INT),
            'country' =>             array('type' => self::TYPE_INT),
            'size' =>             array('type' => self::TYPE_INT),
            'status' => array('type' => self::TYPE_INT),
            'autoplay' => array('type' => self::TYPE_INT),
            'banner_loop' => array('type' => self::TYPE_INT),
        ),
        
    );

    
   

    public function __construct($id_banner = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
       
		
		parent::__construct($id_banner, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		return parent::add($auto_date = true, $null_values = false);
	}
	
	public function update($null_values = false)
    {
	   return parent::update($null_values);
	}
	
	public function getbanners($where=''){	
		 return Db::getInstance()->executeS("
		 SELECT *
		 FROM `pscl_banner` 
          
		 ".pSQL($where));
	}
	
	
   

}

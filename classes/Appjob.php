<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



class AppjobCore extends ObjectModel
{
    public $id_appjob;
    public $id_jobs;
    public $state; 
    public $city;
    public $data_apply;
    public $customer_id;
    public $jobtitles;
    public $phone;
    public $zipcode;
    public $resume;
    public $resume_file;
	
   
   
    public static $definition = array(
        'table' => 'appjob',
        'primary' => 'id_appjob',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
           'id_jobs' =>             array('type' => self::TYPE_INT),
           'state' =>             array('type' => self::TYPE_STRING),
           'city' =>             array('type' => self::TYPE_STRING),
		   'data_apply' =>           array('type' => self::TYPE_DATE,  'validate' => 'isDate'),
		   'customer_id' =>             array('type' => self::TYPE_INT),
		   'jobtitles' =>             array('type' => self::TYPE_INT),
		   'phone' =>             array('type' => self::TYPE_STRING),
		   'resume' =>             array('type' => self::TYPE_STRING),
           'resume_file' =>             array('type' => self::TYPE_STRING),
		   'zipcode' =>             array('type' => self::TYPE_INT),
        ),
        
    );

    
   

    public function __construct($id_appjob = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
        parent::__construct($id_appjob, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        parent::delete();

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		parent::add($auto_date = true, $null_values = false);
	}
	
	public function update($null_values = false)
    {
	parent::update($null_values);
	}
	
	public static function getappjobs($where = "")
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'appjob` j '
		.$where
		);

    }
	
	public  function getappjob($where = "")
    {
        return Db::getInstance()->getRow('
    		SELECT *
    		FROM `'._DB_PREFIX_.'appjob` j '
    		.pSQL($where)
		);

    }
}

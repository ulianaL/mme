<?php
class SponsoredstoriesCore extends ObjectModel
{
    public $id;
    public $title;
    public $description;
    public $image;
    public $customer;
    public $status;
    public $link;
    public $avaliable;
    public $used;
   
    public static $definition = array(
        'table' => 'sponsoredstories',
        'primary' => 'id_sponsoredstories',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'title' =>             array('type' => self::TYPE_STRING),
            'description' =>       array('type' => self::TYPE_STRING),
            'image' =>             array('type' => self::TYPE_STRING),
            'customer' =>          array('type' => self::TYPE_INT),
            'status' =>            array('type' => self::TYPE_INT),
            'link' =>              array('type' => self::TYPE_STRING),
            'avaliable' =>         array('type' => self::TYPE_INT),
            'used' =>              array('type' => self::TYPE_INT),
        ),
    );

    public function getStories($where=''){  
         return Db::getInstance()->executeS("
         SELECT *
         FROM `pscl_sponsoredstories` 
          
         ".pSQL($where));
    }

    public function getStory($where=''){  
         return Db::getInstance()->getRow("
         SELECT *
         FROM `pscl_sponsoredstories` 
          
         ".pSQL($where));
    }

    public static function incrementCounter($id_sponsoredstories, $count = 1)
    {
        return Db::getInstance()->execute(
            "UPDATE "._DB_PREFIX_."sponsoredstories SET used = used + ".(int)$count . " WHERE id_sponsoredstories=".(int)$id_sponsoredstories
        );
    }

    public static function decrementCounter($id_sponsoredstories, $count = 1)
    {
        return Db::getInstance()->execute(
            "UPDATE "._DB_PREFIX_."sponsoredstories SET used = used - ".(int)$count . " WHERE id_sponsoredstories=".(int)$id_sponsoredstories
        );
    }
}
<?php

class EventregistryCore extends ObjectModel
{
    public $id_eventregistry;
    public $uri;
    public $title;
    public $image;
    public $content;
    public $date;
    public $categories;
    public $city;
    public $country;
    public $category_to_show;
    public $show_in;
    public $url; 
    public $source;
    public $confirm; 
    public $admin_category;
    
    public static $definition = array(
        'table' => 'eventregistry',
        'primary' => 'id_eventregistry',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'uri' =>             array('type' => self::TYPE_STRING),
            'title' =>             array('type' => self::TYPE_STRING),
            'image' =>             array('type' => self::TYPE_STRING),
            'content' =>             array('type' => self::TYPE_STRING),
            'date' =>             array('type' => self::TYPE_STRING),
            'categories' =>             array('type' => self::TYPE_STRING),
            'city' =>             array('type' => self::TYPE_STRING),
            'country' => array('type' => self::TYPE_STRING),
            'category_to_show' => array('type' => self::TYPE_STRING),
            'show_in' => array('type' => self::TYPE_STRING),
            'url' => array('type' => self::TYPE_STRING),
            'source' => array('type' => self::TYPE_STRING),
            'confirm' => array('type' => self::TYPE_INT),
            'admin_category' => array('type' => self::TYPE_STRING),
        ),
        
    );

    
   

    public function __construct($id_eventregistry = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
       
		
		parent::__construct($id_eventregistry, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		return parent::add($auto_date = true, $null_values = false);
	}
	
	public function update($null_values = false)
    {
	   return parent::update($null_values);
	}
	public static function checkDuplicate($uri='', $title=''){
        return Db::getInstance()->getRow('
         SELECT *
         FROM `pscl_eventregistry` 
         WHERE `uri` = ' . $uri . ' OR `title` = "'.$title.'"');
    }
	public static function geteventregistry($where=''){	
		 return Db::getInstance()->executeS("
		 SELECT *
		 FROM `pscl_eventregistry`
         WHERE `confirm` = 1 AND DATE(date) <= CURDATE()
         GROUP BY  `category_to_show` ");
	}
	public static function removeRowEventregistry($id){
        return Db::getInstance()->getRow("
         DELETE
         FROM `pscl_eventregistry` 
         WHERE `id_eventregistry` = ".$id
         );
    }
	
   

}

<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



class ChatCore extends ObjectModel
{
    public $chat_id;
	public $customer;
	public $seller;
	public $msn;
	public $date;
	public $extrartext;
	public $extraint;
	public $chatmain;

   
    public static $definition = array(
        'table' => 'chat',
        'primary' => 'chat_id',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
          
	
			'customer' =>             array('type' => self::TYPE_INT),
			'chatmain' =>             array('type' => self::TYPE_INT),
			'seller' =>             array('type' => self::TYPE_INT),
			'msn' =>             array('type' => self::TYPE_STRING),
			'date' =>             array('type' => self::TYPE_DATE,  'validate' => 'isDate'),
			'extrartext' =>             array('type' => self::TYPE_STRING),
			'extraint' =>             array('type' => self::TYPE_INT),
        ),
        
    );

    
   

    public function __construct($chat_id = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
       
		
		parent::__construct($chat_id, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		parent::add($auto_date = true, $null_values = false);
	}
	
	public function update($null_values = false)
    {
	parent::update($null_values);
	}
	
	public function getChat($maxid = 99999999999999999999,$me=99999999999999,$you=99999999999){	
		 return Db::getInstance()->executeS("
		 SELECT *
		 FROM `pscl_chat` 
         WHERE `chat_id` > ".(int)$maxid);
	}
	
	
   

}

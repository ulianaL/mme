<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



class CampCore extends ObjectModel
{
    public $id_camp;
    public $avaliable;
    public $name;
    public $url;
    public $customer;
    public $type;
    public $quantity;
    public $state;
    public $city;
    public $id_banner;
    public $used;
    public $country;
	
    public $keytext;
    public $keystatus;
    public $toggleclasstree;
    public $only_finance;
    public $id_category;
    public $date_end;
	
   
   
    public static $definition = array(
        'table' => 'camp',
        'primary' => 'id_camp',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'name' =>             array('type' => self::TYPE_STRING),
            'keytext' =>             array('type' => self::TYPE_STRING),
            'url' =>             array('type' => self::TYPE_STRING),
            'keystatus' =>             array('type' => self::TYPE_INT),
            'customer' =>             array('type' => self::TYPE_INT),
            'type' =>             array('type' => self::TYPE_INT),
            'quantity' =>             array('type' => self::TYPE_INT),
            'state' =>             array('type' => self::TYPE_STRING),
            'city' =>             array('type' => self::TYPE_INT),
            'id_banner' =>             array('type' => self::TYPE_INT),  
			'avaliable' =>             array('type' => self::TYPE_INT),
			'used' =>             array('type' => self::TYPE_INT),
			'country' =>             array('type' => self::TYPE_INT),
			'toggleclasstree' =>             array('type' => self::TYPE_INT),
            'only_finance' =>             array('type' => self::TYPE_INT),
            'id_category' =>             array('type' => self::TYPE_STRING),
		    'date_end' =>             array('type' => self::TYPE_DATE),

           
		   
		
			

        ),
        
    );

    
   

    public function __construct($id_camp = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
       

	   parent::__construct($id_camp, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        parent::delete();

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		parent::add($auto_date = true, $null_values = false);
		return true;
	}
	
	public function update($null_values = false)
    {
	parent::update($null_values);
	}
	
	
	
	public function getcamp($where=''){	
		 return Db::getInstance()->executeS("
		 SELECT *
		 FROM `pscl_camp`
		 ".pSQL($where));
	}
}

<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2019 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class CampReviewCore extends ObjectModel
{
    public $id_camp;
    public $ip;
    public $date_add;

    public static $definition = array(
        'table' => 'camp_review',
        'primary' => 'id_camp_review',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'id_camp' =>             array('type' => self::TYPE_INT),
            'ip' =>             array('type' => self::TYPE_STRING),
            'date_add' =>                array('type' => self::TYPE_DATE, 'validate' => 'isDate')
        ),
        
    );
    
    /**
     * Get Ip Adddress
     * @return [varchar] [IP Address]
     */
    public static function getIp() 
    {

        if (!empty($_SERVER['HTTP_CLIENT_IP']) && self::validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    if (self::validate_ip($ip)) {
                        return $ip;
                    }
                }
            } else {
                if (self::validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED']) && self::validate_ip($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];
        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && self::validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && self::validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];
        if (!empty($_SERVER['HTTP_FORWARDED']) && self::validate_ip($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];

        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     **/
    private static function validate_ip($ip)
    {
        $orgIp = $ip;
        if (strtolower($ip) === 'unknown') {
            return false;
        }

        // generate ipv4 network address
        $ip = ip2long($ip);

        // if the ip is set and not equivalent to 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // make sure to get unsigned long representation of ip
            // due to discrepancies between 32 and 64 bit OSes and
            // signed numbers (ints default to signed in PHP)
            $ip = sprintf('%u', $ip);
            // do private network range checking
            if ($ip >= 0 && $ip <= 50331647) return false;
            if ($ip >= 167772160 && $ip <= 184549375) return false;
            if ($ip >= 2130706432 && $ip <= 2147483647) return false;
            if ($ip >= 2851995648 && $ip <= 2852061183) return false;
            if ($ip >= 2886729728 && $ip <= 2887778303) return false;
            if ($ip >= 3221225984 && $ip <= 3221226239) return false;
            if ($ip >= 3232235520 && $ip <= 3232301055) return false;
            if ($ip >= 4294967040) return false;
        }

        if (!is_numeric(str_replace('.', '', $orgIp))) {
            return false;
        }

        return true;
    }

    /**
     * Add new review
     *
     * @param int $id_camp
     *
     * @return boolean
     **/
    public static function addReview($id_camp)
    {
        if (!$id_camp) {
            return false;
        }
        $record = new self();
        $record->id_camp = (int)$id_camp;
        $record->ip = self::getIp();
        $record->date_add = date('Y-m-d H:i:s');
        return $record->add();
    }

    public static function getDataByPeriod($id_camp, $from, $to, $today = null)
    {
        if (is_null($today)) {
            $today = date('Y-m-d H:i:s');
        }
        $res = array(
            'reviews' => array(),
            'total' => (int)Db::getInstance()->getValue(
                "SELECT COUNT(*) FROM "._DB_PREFIX_."camp_review WHERE date_add >= '".addslashes($from)."' AND date_add <= '".addslashes($to)."' AND id_camp=".(int)$id_camp
            ),
            'today_visits' => (int)Db::getInstance()->getValue(
                "SELECT COUNT(*) FROM "._DB_PREFIX_."camp_review WHERE date_add >= '".date('Y-m-d 00:00:00', strtotime($today))."' AND date_add <= '".date('Y-m-d 23:59:59', strtotime($today))."' AND id_camp=".(int)$id_camp
            ),
            'unique_visits' => count(
                Db::getInstance()->executeS(
                    "SELECT ip, COUNT(*) AS `count` FROM "._DB_PREFIX_."camp_review WHERE date_add >= '".addslashes($from)."' AND date_add <= '".addslashes($to)."' AND id_camp=".(int)$id_camp." GROUP BY ip"
                )
            )
        );

        $rows = Db::getInstance()->executeS(
            "SELECT
                DATE_FORMAT(date_add, \"%Y-%m-%d\") AS `date`, COUNT(*) AS `nb_reviews`
            FROM
                "._DB_PREFIX_."camp_review
            WHERE
                date_add >= '".addslashes($from)."' AND
                date_add <= '".addslashes($to)."' AND
                id_camp = ".(int)$id_camp."
            GROUP BY `date`
            ORDER BY `date` ASC"
        );

        if ($rows) {
            $res['reviews'] = array_map(
                function ($a) {
                    return array(
                        'day' => $a['date'],
                        'value' => (int)$a['nb_reviews']
                    );
                },
                $rows
            );
        }

        return $res;
    }

    public static function getDataByMonth($id_camp, $today = null)
    {
        if (is_null($today)) {
            $today = date('Y-m-d H:i:s');
        }
        return self::getDataByPeriod(
            (int)$id_camp,
            date('Y-m-01 00:00:00', strtotime($today)),
            date('Y-m-t 23:59:59', strtotime($today)),
            $today
        );
    }
}

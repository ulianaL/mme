<?php
class Ewallet extends ObjectModel
{
	public $id;

    public $id_customer;

    public $total_amount;

    public $payment_date;

    public static $definition = array(
        'table' => 'ewallet',
        'primary' => 'id',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'total_amount' =>    array('type' => self::TYPE_FLOAT),
            'payment_date' =>    array('type' => self::TYPE_DATE)
        ),
    );

}
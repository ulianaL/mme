<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



class JobsCore extends ObjectModel
{
    public $id_jobs;
    public $tittle;
    public $description;
    public $city;
    public $data_add_jobs;
    public $jobs_categorie_id;
    public $date_add;
    public $date_upd;
    public $insdustry;
    public $jobtype;
    public $state;
    public $customer_id;
    public $country;
	
   
   
    public static $definition = array(
        'table' => 'jobs',
        'primary' => 'id_jobs',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
     
            //'state' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'customer_id' =>             array('type' => self::TYPE_STRING),
            'state' =>             array('type' => self::TYPE_STRING),
            'tittle' =>             array('type' => self::TYPE_STRING),
            'description' =>        array('type' => self::TYPE_STRING),
            'jobs_categorie_id' =>  array('type' => self::TYPE_STRING),
            'city' =>               array('type' => self::TYPE_STRING),
            'country' =>               array('type' => self::TYPE_STRING),
            'data_add_jobs' =>      array('type' => self::TYPE_STRING),
            'insdustry' =>      array('type' => self::TYPE_STRING),
            'jobtype' =>      array('type' => self::TYPE_STRING),
			'date_add' =>           array('type' => self::TYPE_DATE,  'validate' => 'isDate'),
            'date_upd' =>           array('type' => self::TYPE_DATE,  'validate' => 'isDate'),
			

        ),
        
    );

    
   

    public function __construct($id_jobs = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
        parent::__construct($id_jobs, $id_lang, $id_shop);
        
          
    }

    public function delete()
    {
        parent::delete();

 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
		parent::add($auto_date = true, $null_values = false);
	}
	
	public function update($null_values = false)
    {
	parent::update($null_values);
	}
	
	public static function getJobs($where = "")
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'jobs` j '
		.$where
		);

    }
	
	public  function getJob($where = "")
    {
        return Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'jobs` j '
		.pSQL($where)
		);

    }
	
	
   

}

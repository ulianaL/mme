<?php

class CategoryFeatures extends ObjectModel
{
    public $id;
    public $id_category_product_feature;
    public $id_category;
    public $id_product;
    public $start;
    public $end;
    public $points;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'category_product_feature',
        'primary' => 'id_category_product_feature',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'id_category' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_product' =>         array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'start' =>              array('type' => self::TYPE_STRING),
            'end' =>                array('type' => self::TYPE_STRING),
            'points' =>             array('type' => self::TYPE_FLOAT)
        ),
    );

    public static function getProductsForCategory($id_category, $id_lang = null, $limit = null)
    {
        $context = Context::getContext();
        if (is_null($id_lang)) {
            $id_lang = $context->language->id;
        }
        $front = true;
        $active = true;
        $sql = 'SELECT DISTINCT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) AS quantity'.(Combination::isFeatureActive() ? ', IFNULL(product_attribute_shop.id_product_attribute, 0) AS id_product_attribute,
                    product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity' : '').', pl.`description`, pl.`description_short`, pl.`available_now`,
                    pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, image_shop.`id_image` id_image,
                    il.`legend` as legend, m.`name` AS manufacturer_name, cl.`name` AS category_default, \'0\' AS new, product_shop.price AS orderprice
                FROM `'._DB_PREFIX_.'category_product` cp
                LEFT JOIN `'._DB_PREFIX_.'product` p
                    ON p.`id_product` = cp.`id_product`
                '.Shop::addSqlAssociation('product', 'p').
                (Combination::isFeatureActive() ? ' LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
                ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$context->shop->id.')':'').'
                '.Product::sqlStock('p', 0).'
                LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
                    ON (product_shop.`id_category_default` = cl.`id_category`
                    AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                    ON (p.`id_product` = pl.`id_product`
                    AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
                LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
                    ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
                LEFT JOIN `'._DB_PREFIX_.'image_lang` il
                    ON (image_shop.`id_image` = il.`id_image`
                    AND il.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
                    ON m.`id_manufacturer` = p.`id_manufacturer`
                LEFT JOIN ' . _DB_PREFIX_ . 'category_product_feature AS cpf ON (cpf.id_product = p.id_product)
                
                WHERE cpf.`start` <= \''.date('Y-m-d H:i:s').'\'
                    AND cpf.`end` >= \''.date('Y-m-d H:i:s').'\'
                    AND cpf.id_category = '.(int)$id_category.'
                    AND product_shop.`id_shop` = '.(int)$context->shop->id
                    .($active ? ' AND product_shop.`active` = 1' : '')
                    .($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '');
        if (is_numeric($limit) && (int)$limit > 0) {
            $sql .= ' LIMIT '.(int)$limit;
        }
        $result = Db::getInstance()->executeS($sql);
        return Product::getProductsProperties($id_lang, $result);
    }

    public static function getAllByIds(array $ids = array(), $id_lang = null)
    {
        if (null === $id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        return !$ids
            ? array()
            : Db::getInstance()->executeS(
                "SELECT
                    cpf.*,
                    cl.name AS `category_name`
                FROM
                    "._DB_PREFIX_."category_product_feature AS cpf
                LEFT JOIN "._DB_PREFIX_."category_lang AS cl ON (cl.id_category = cpf.id_category AND cl.id_lang = ".(int)$id_lang.")
                WHERE
                    cpf.id_product IN (".implode(', ', $ids).")"
            );
    }
}
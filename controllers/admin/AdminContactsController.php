<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Contact $object
 */
class AdminContactsControllerCore extends AdminController
{
    public function __construct()
    {
        $this->table = 'sponsoredstories';
        $this->className = 'sponsoredstories';
        // $this->module = 'medicalmarijuanaexchangedirectory';
        $this->allow_export = true;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        
        parent::__construct();

        
        
        $this->_select = 'cu.*';
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer cu ON (cu.id_customer = a.customer)';
        
        // $this->fieldImageSettings = array(
  //           'name' => 'banner_img',
  //           'dir' => '../modules/medicalmarijuanaexchangedirectory/bimg'
  //       );
        
        
        $this->fields_list = array(
            'id_banner' => array(
                'title' => $this->l('Id'),
              
                'type' => 'text', 
            ),
            'banner_img' => array(
                'title' => $this->l('Image'),
                
                'type' => 'text', 
                 'callback' => 'imagecallback',###callback image
            ),
            'firstname' => array(
                'title' => $this->l('C. First N.'),
               
                'type' => 'text', 
            ),
            
            'lastname' => array(
                'title' => $this->l('C. Last N.'),
               
                'type' => 'text', 
            ),
            
            'id_customer' => array(
                'title' => $this->l('C. ID'),
                
                'type' => 'text', 
            ),

            // 'state' => array(
            //     'title' => $this->l('Displayed'),
            //     'icon' => array(
            //             0 => '',
            //             1 => '',
            //             'default' => 'disabled.gif'
            //         ),
            //     'type' => 'bool',
            //     'class' => 'fixed-width-xs',
            //     'align' => 'center'
            // ),
            'status' => array(
                'title' => $this->l('Displayed'),
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            )
        );
    }
    
    public function ajaxProcessStatusBanner()
    {
        if (!$id_banner = (int)Tools::getValue('id_banner')) {
            die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
        } else {
            $banner = new Banner((int)$id_banner);
            if (Validate::isLoadedObject($banner)) {
                $banner->status = $banner->status == 1 ? 0 : 1;
                $banner->save() ?
                die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully')))) :
                die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
            }
        }
    }
    
    public function imagecallback($value, $row) {
        // $img_link = 'https://medicalmarijuanaexchange.com/modules/medicalmarijuanaexchangedirectory/bimg/'.$value;
        $img_link = getBuildBannerUrl($value);

        // echo "<pre>"; print_r($row); echo "</pre>"; exit;
        if ($row['size'] == '4') {
            return 'Video banner:<br/>'.$value;
        }
        
        return '<a href=""><img   style="    max-width: 140px;" class="imgm img-thumbnail" src="'.$img_link.'"></a>';
    }
    
    
    
    
    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        //$this->addRowAction('view');

        return parent::renderList();
    }
    
    
    public function initToolbar() {
        parent::initToolbar();

        unset( $this->toolbar_btn['new'] );
    }



    
    public function renderForm()
    {
       if (!($obj = $this->loadObject(true))) {
            return;
        }       
        
        $this->fields_form = array(
           // 'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Editar '),
            ),
            'input' => array(
                
                array(
                    'type' => 'html',
                    'label' => $this->l('id_banner'),
                    'name' => 'id_banner',
                    'html_content' => '<span style="display: inline-block; padding-top: 8px;">'.$obj->id.'</span>'
                    
                ),
    
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Size / Banner type'),
                    'name' => 'size',
                    'options' => array(
                        'query' => array(
                            array(
                                'value' => '4',
                                'name' => 'Video'
                            ),
                            array(
                                'value' => '1',
                                'name' =>'Medium Rectangle: 300×250'
                            ),
                            array(
                                'value' => '2',
                                'name' =>'Leaderboard: 728×90'
                            ),
                            array(
                                'value' => '3',
                                'name' =>'Wide Skyscraper: 160×600'
                            ),

                        ),
                        'id' => 'value',
                        'name' => 'name'
                    )
                    
                ),
                array(
                   'type' => 'file',
                   'label' => $obj->size == '4' ? $this->l('Video') : $this->l('Picture'),
                   'name' => 'banner_img',
                   'display_image' => true,
                   // 'image' => '<img src="https://medicalmarijuanaexchange.com/modules/medicalmarijuanaexchangedirectory/bimg/'.$obj->banner_img.'"/>'
                   'image' => ($obj->size == '4' ? '<video style="width: 200px;">
                                 <source src="' . getBuildBannerUrl($obj->banner_img). '">
                                </video>' : '<img src="'.getBuildBannerUrl($obj->banner_img).'">')
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Autoplay'),
                    'name' => 'autoplay',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'autoplayon',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'autoplayoff',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                    array(
                    'type' => 'switch',
                    'label' => $this->l('Loop'),
                    'name' => 'banner_loop',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'banner_loopon',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'banner_loopoff',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Displayed'),
                    'name' => 'status',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                
            )
        );

        

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );
        
        

        return parent::renderForm();
    }
    
    
    
    
    
    
    public function postProcess()
    {
        if (Tools::getValue('id_banner', 0)) {
            $this->object = $this->loadObject();
        }
        parent::postProcess();
        if (isset($_FILES) && count($_FILES)) {
            // echo "<pre>"; print_r($_FILES); echo "</pre>"; exit;
            $name = 'banner_img';
            if ($_FILES[$name]['name'] != null) {
                $uniquename = time().'.'.pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
                $connection = ssh2_connect(CLOUD_HOST, CLOUD_PORT);
                ssh2_auth_password($connection, CLOUD_USER, CLOUD_PWD);
                $res = ssh2_scp_send($connection, $_FILES[$name]["tmp_name"], CLOUD_IMG_DIR . $uniquename, 0644);
                if (!$res) {
                    $target_dir = _PS_MODULE_DIR_ . "/medicalmarijuanaexchangedirectory/bimg/";
                    move_uploaded_file($_FILES[$name]["tmp_name"],  $target_dir.$uniquename);
                }
                $this->object->banner_img = $uniquename;
                $this->object->save();
                // echo "<pre>";
                // var_dump($this->object);
                // exit();
            }
        }
    }
}

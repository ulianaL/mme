<?php 
class AdminStoryControllerCore extends AdminController
{
	public function __construct()
    {
        $this->table = 'sponsoredstories';
        $this->className = 'Sponsoredstories';
        // $this->module = 'medicalmarijuanaexchangedirectory';
        $this->allow_export = true;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        
        parent::__construct();

        
        
        $this->_select = 'cu.*';
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer cu ON (cu.id_customer = a.customer)';
        
        // $this->fieldImageSettings = array(
  //           'name' => 'banner_img',
  //           'dir' => '../modules/medicalmarijuanaexchangedirectory/bimg'
  //       );
        
        
        $this->fields_list = array(
            'id_sponsoredstories' => array(
                'title' => $this->l('Id'),
              
                'type' => 'text', 
            ),
            'image' => array(
                'title' => $this->l('Image'),
                
                'type' => 'text', 
                 'callback' => 'imagecallback',
            ),
            'firstname' => array(
                'title' => $this->l('C. First N.'),
               
                'type' => 'text', 
            ),
            
            'lastname' => array(
                'title' => $this->l('C. Last N.'),
               
                'type' => 'text', 
            ),
            
            'id_customer' => array(
                'title' => $this->l('C. ID'),
                
                'type' => 'text', 
            ),
            'status' => array(
                'title' => $this->l('Displayed'),
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            )
        );
    }
    
    public function ajaxProcessStatusSponsoredstories()
    {
        if (!$id_sponsoredstories = (int)Tools::getValue('id_sponsoredstories')) {
            die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
        } else {
            $story = new Sponsoredstories((int)$id_sponsoredstories);
            if (Validate::isLoadedObject($story)) {
                $story->status = $story->status == 1 ? 0 : 1;
                $story->save() ?
                die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully')))) :
                die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
            }
        }
    }
    
    public function imagecallback($value, $row) {
         $img_link = '/modules/medicalmarijuanaexchangedirectory/storyimg/'.$value;
    
        
        return '<a href=""><img   style="    max-width: 140px;" class="imgm img-thumbnail" src="'.$img_link.'"></a>';
    }
    
    
    
    
    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        //$this->addRowAction('view');

        return parent::renderList();
    }
    
    
    public function initToolbar() {
        parent::initToolbar();

        unset( $this->toolbar_btn['new'] );
    }



    
    public function renderForm()
    {
       if (!($obj = $this->loadObject(true))) {
            return;
        }       
        
        $this->fields_form = array(
           // 'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Editar '),
            ),
            'input' => array(
                
                array(
                    'type' => 'html',
                    'label' => $this->l('id_sponsoredstories'),
                    'name' => 'id_sponsoredstories',
                    'html_content' => '<span style="display: inline-block; padding-top: 8px;">'.$obj->id.'</span>'
                    
                ),
    
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    
                ),

                 array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    
                ),
               
                array(
                   'type' => 'file',
                   'label' => $this->l('Picture'),
                   'name' => 'image',
                   'display_image' => true,
                   'image' => '<img src="/modules/medicalmarijuanaexchangedirectory/storyimg/'.$obj->image.'"/>'
                   
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Displayed'),
                    'name' => 'status',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                
            )
        );

        

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );
        
        

        return parent::renderForm();
    }
    
    
    
    
    
    
    public function postProcess()
    {
        if (Tools::getValue('id_sponsoredstories', 0)) {
            $this->object = $this->loadObject();
        }
        parent::postProcess();
        if (isset($_FILES) && count($_FILES)) {
            // echo "<pre>"; print_r($_FILES); echo "</pre>"; exit;
            $name = 'image';
            if ($_FILES[$name]['name'] != null) {
                $uniquename = time().'.'.pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
                $target_dir = _PS_MODULE_DIR_ . "/medicalmarijuanaexchangedirectory/storyimg/";
                move_uploaded_file($_FILES[$name]["tmp_name"],  $target_dir.$uniquename);
                $this->object->image = $uniquename;
                $this->object->title = $_POST['title'];
                $this->object->description = $_POST['description'];
                $this->object->status = $_POST['status'];
                $this->object->save();
                // echo "<pre>";
                // var_dump($this->object);
                // exit();
            }
        }
    }
}
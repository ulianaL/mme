<?php
class AdminEwalletController extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'ewallet';
        $this->className = 'Ewallet';

        $this->context = Context::getContext();
		$this->bootstrap = true;

		$this->_defaultOrderBy = 'payment_date';
		$this->_defaultOrderWay = 'ASC';
		$this->identifier = 'id';
		$this->identifier_name = 'id';
		$this->list_no_link = true;
		$this->allow_export = false;
        
		parent::__construct();

		$this->_select = 'cu.firstname, cu.lastname, cu.email, \'\' AS `payded`, \'\' AS `newtotal`';
		$this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer cu ON (cu.id_customer = a.id_customer)';

		$this->fields_list = array(
			'firstname' => array(
                'title' => $this->l('First Name'),
               'search' => false,
                'type' => 'text', 
            ),
			
			'lastname' => array(
                'title' => $this->l('Last Name'),
               'search' => false,
                'type' => 'text', 
            ),

            'email' => array(
                'title' => $this->l('Email'),
               'search' => false,
                'type' => 'text', 
            ),
            // 'total_amount' => array(
            //     'title' => $this->l('Total'),
               
            //     'type' => 'text', 
            // ),

            'newtotal' => array(
                'title' => $this->l('Total'),
               	'search' => false,
                'callback' => 'printtotal'
            ),

            'payment_date' => array(
                'title' => $this->l('Date'),
                'search' => false,
                'type' => 'text', 
            ),


            'payded' => array(
                'title' => $this->l('Pay'),
                'search' => false,
                'callback' => 'printPay'
            )
        );
    }

    public function printPay($value, $row)
    {
    	return '<a onclick="return confirm(\'Are you sure?\')" href="?controller=AdminEwallet&token='.$this->token.'&paided=1&id_customer='.$row['id_customer'].'&total='.$row['total_amount'].'" class ="payded_but">Pay</a>';
    }

    public function printTotal($value, $row)
    {

    	 $amount = number_format($row['total_amount'], 2, '.', ' ');
    	 return $amount;
    }

    public function postProcess()
    {
    	if (Tools::isSubmit('paided')) {
    		$id_customer = Tools::getValue('id_customer');
    		$total = Tools::getValue('total');
    		$total = number_format($total, 2, '.', ' ');
    		if ($id_customer) {
    			$current_date = date('d.m.Y');
    			Db::getInstance()->insert('phistory', array('id_customer' => (int)$id_customer, 'total' => $total, 'payment_date' => $current_date));

    			$date = strtotime("+14 day");
                $payment_date = date('Y-m-d', $date);
    			Db::getInstance()->update('ewallet', array('total_amount' => 0, 'payment_date' => $payment_date), 'id_customer ='.(int)$id_customer);
    		}
    	}
    }
}
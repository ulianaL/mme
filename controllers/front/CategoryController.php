<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CategoryControllerCore extends FrontController
{
    /** string Internal controller name */
    public $php_self = 'category';

    public $remove_lastes_news = true;

    /** @var Category Current category object */
    protected $category;

    /** @var bool If set to false, customer cannot view the current category. */
    public $customer_access = true;

    /** @var int Number of products in the current page. */
    protected $nbProducts;

    /** @var array Products to be displayed in the current page . */
    protected $cat_products;

    public static $_camp_reviews_was_added = false;

    /**
     * @var bool
     **/
    private $show_geo_filters = false;

    /**
     * Sets default medias for this controller
     */
    public function setMedia()
    {
        parent::setMedia();

        if (!$this->useMobileTheme()) {
            //TODO : check why cluetip css is include without js file
            $this->addCSS(array(
                _THEME_CSS_DIR_.'scenes.css'       => 'all',
                _THEME_CSS_DIR_.'category.css'     => 'all',
                _THEME_CSS_DIR_.'product_list.css' => 'all',
            ));
        }

        $scenes = Scene::getScenes($this->category->id, $this->context->language->id, true, false);
        if ($scenes && count($scenes)) {
            $this->addJS(_THEME_JS_DIR_.'scenes.js');
            $this->addJqueryPlugin(array('scrollTo', 'serialScroll'));
        }

        $this->addJS(_THEME_JS_DIR_.'category.js');
    }

    /**
     * Redirects to canonical or "Not Found" URL
     *
     * @param string $canonical_url
     */
    public function canonicalRedirection($canonical_url = '')
    {
        if (Tools::getValue('live_edit')) {
            return;
        }

        if (!Validate::isLoadedObject($this->category) || !$this->category->inShop() || !$this->category->isAssociatedToShop() || in_array($this->category->id, array(Configuration::get('PS_HOME_CATEGORY'), Configuration::get('PS_ROOT_CATEGORY')))) {
            $this->redirect_after = '404';
            $this->redirect();
        }

        if (!Tools::getValue('noredirect') && Validate::isLoadedObject($this->category)) {
            parent::canonicalRedirection($this->context->link->getCategoryLink($this->category));
        }
    }

    /**
     * Initializes controller
     *
     * @see FrontController::init()
     * @throws PrestaShopException
     */
    public function init()
    {
        // Get category ID
        $id_category = (int)Tools::getValue('id_category');
        if (!$id_category || !Validate::isUnsignedId($id_category)) {
            $this->errors[] = Tools::displayError('Missing category ID');
        }

        // Instantiate category
        $this->category = new Category($id_category, $this->context->language->id);

        if ($this->category->id == 1438) {
            $this->category->name = 'Products';
        }

        parent::init();

        // Check if the category is active and return 404 error if is disable.
        if (!$this->category->active) {
            header('HTTP/1.1 404 Not Found');
            header('Status: 404 Not Found');
        }

        // Check if category can be accessible by current customer and return 403 if not
        if (!$this->category->checkAccess($this->context->customer->id)) {
            header('HTTP/1.1 403 Forbidden');
            header('Status: 403 Forbidden');
            $this->errors[] = Tools::displayError('You do not have access to this category.');
            $this->customer_access = false;
        }
    }

    /**
     * Initializes page content variables
     */
    public function initContent()
    {

		parent::initContent();

        $geo_categories = array(1423, 1234, 1232, 1402);
        $this->show_geo_filters = (in_array((int)$this->category->id, $geo_categories) || in_array((int)$this->category->id_parent, $geo_categories));

        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_category = '.$this->category->id.' AND '._DB_PREFIX_.'banner.status = 1 AND '._DB_PREFIX_.'banner.size = 5 AND used != 0 AND (date_end >= NOW() OR IFNULL(date_end, \'0000-00-00\') = \'0000-00-00\')';
        
        $top_category_campains = Db::getInstance()->executeS($sql);
        
        $sql = 'SELECT * FROM '._DB_PREFIX_.'camp LEFT JOIN '._DB_PREFIX_.'banner ON '._DB_PREFIX_.'camp.id_banner = '._DB_PREFIX_.'banner.id_banner WHERE id_category = '.$this->category->id.' AND '._DB_PREFIX_.'banner.status = 1 AND '._DB_PREFIX_.'banner.size = 6 AND used != 0 AND (date_end >= NOW() OR IFNULL(date_end, \'0000-00-00\') = \'0000-00-00\')';
        $bottom_category_campains = Db::getInstance()->executeS($sql);

        if (count($top_category_campains) !== 0) {
            $randx =  rand(0,count($top_category_campains)-1);
            $top_category_baner = $top_category_campains[$randx];
        }else{
            $top_category_baner = '';
        }

        if (count($bottom_category_campains) !== 0) {
            $randx =  rand(0,count($bottom_category_campains)-1);
            $bottom_category_baner = $bottom_category_campains[$randx];
        }else{
            $bottom_category_baner = '';
        }
        $reviewd_camps = [];
        if (!empty($top_category_baner)) {
            if($top_category_baner['type'] == '2'){
                //if seller is see your banner....
                if((int)Context::getContext()->customer->id != $top_category_baner['customer'] ){
                    $camppppp_obj =  New Camp( (int)$top_category_baner['id_camp'] );
                    $camppppp_obj->used = $camppppp_obj->used -1;
                    $camppppp_obj->update();
                    if (!self::$_camp_reviews_was_added && !in_array($camppppp_obj->id, $reviewd_camps)) {
                        CampReview::addReview((int)$camppppp_obj->id);
                        $reviewd_camps[] = $camppppp_obj->id;
                    }
                } else {
                    if (!self::$_camp_reviews_was_added && !in_array($top_category_baner['id_camp'], $reviewd_camps)) {
                        CampReview::addReview((int)$top_category_baner['id_camp']);
                        $reviewd_camps[] = $top_category_baner['id_camp'];
                    }
                }

            }elseif($top_category_baner['type'] == '3'){
                if (!self::$_camp_reviews_was_added && !in_array($top_category_baner['id_camp'], $reviewd_camps)) {
                    CampReview::addReview((int)$top_category_baner['id_camp']);
                    $reviewd_camps[] = $top_category_baner['id_camp'];
                }
            }
        }

        if (!empty($bottom_category_baner)) {
            if($bottom_category_baner['type'] == '2'){
                //if seller is see your banner....
                if((int)Context::getContext()->customer->id != $bottom_category_baner['customer'] ){
                    $camppppp_obj =  New Camp( (int)$bottom_category_baner['id_camp'] );
                    $camppppp_obj->used = $camppppp_obj->used -1;
                    $camppppp_obj->update();
                    if (!self::$_camp_reviews_was_added && !in_array($camppppp_obj->id, $reviewd_camps)) {
                        CampReview::addReview((int)$camppppp_obj->id);
                        $reviewd_camps[] = $camppppp_obj->id;
                    }
                } else {
                    if (!self::$_camp_reviews_was_added && !in_array($bottom_category_baner['id_camp'], $reviewd_camps)) {
                        CampReview::addReview((int)$bottom_category_baner['id_camp']);
                        $reviewd_camps[] = $bottom_category_baner['id_camp'];
                    }
                }
            }elseif($bottom_category_baner['type'] == '3'){
                if (!self::$_camp_reviews_was_added && !in_array($bottom_category_baner['id_camp'], $reviewd_camps)) {
                    CampReview::addReview((int)$bottom_category_baner['id_camp']);
                    $reviewd_camps[] = $bottom_category_baner['id_camp'];
                }
            }
        }

        self::$_camp_reviews_was_added = true;
        $this->context->smarty->assign('top_category_baner', $top_category_baner);
        $this->context->smarty->assign('bottom_category_baner', $bottom_category_baner);

        $this->setTemplate(_PS_THEME_DIR_.'category.tpl');

        if (!$this->customer_access) {
            return;
        }

        if (isset($this->context->cookie->id_compare)) {
            $this->context->smarty->assign('compareProducts', CompareProduct::getCompareProducts((int)$this->context->cookie->id_compare));
        }

        // Product sort must be called before assignProductList()
        $this->productSort();

        $this->assignScenes();
        $this->assignSubcategories();
        $this->assignProductList();
		
		
		
		
		
		//
		$i = 0;
		$active_subscription = 0;

		$catproductstounset = $this->cat_products;
		unset($this->cat_products);
        unset($this->nb_products);
		$this->cat_products = array();

        //SoftSprint Uliana
        foreach ($catproductstounset as $product) {

            $customer_id = $product['id_owner'];
            $sql = "SELECT id_prem FROM pscl_prem WHERE id_customer = ".$customer_id." AND date_add <= NOW() AND date_end >= NOW() ORDER BY date_add DESC";
            $active_subscription = Db::getInstance()->getValue($sql);
            if (!$active_subscription) {

                continue;

            }else{

                if ($product['review'] == 1) {

                    $this->cat_products[$i] = $product;
                    $this->cat_products[$i]['premtype'] = $this->premtype($customer_id);
                    $i++;
                }
            }
        }

        if ((!isset($_GET['orderby']) && !isset($_GET['orderway'])) || $_GET['orderby'] == 'date_add' ) {
            
            usort ($this->cat_products, function($a, $b){
                if ($a['premtype'] == $b['premtype']) {
                    return 0;
                }
                return ($a['premtype'] > $b['premtype'] ? -1 : 1);
            });

        }
        $this->nb_products = count($this->cat_products);
        
		// foreach($catproductstounset as $proaddisallowed){
		// 	//check if customer buy store subescription

		// 	$dateaddedorder = "";
		// 	$allorder = "";
		// 	$customer_id = $proaddisallowed['id_owner'];
		// 	//directory
		// 	$allorder = Order::getIdOrderProduct($customer_id,8);
		// 	if ($allorder != 0){
		// 		$ooodet = new Order($allorder);
		// 		$statuss = $ooodet->getCurrentStateFull(1);
		// 		$dateaddedorder =  date('Y-m-d', strtotime($ooodet->date_add));
		// 		$end =  date('Y-m-d', strtotime('+1 year',strtotime($dateaddedorder)));
		// 		// order is valid???
		// 		if ($ooodet->valid == 1 && $proaddisallowed['review'] == 1){
		// 			$this->cat_products[$i] = $proaddisallowed;
		// 			$this->cat_products[$i]['proaddisallowed'] = 'testttt';
		// 			$i++;			
		// 		}else{
		// 			continue;
		// 		}
		// 	}
		// }
		//END check if customer buy store subescription
		
        // echo "<pre>"; print_r($this->cat_products); echo "</pre>"; exit;
        $this->context->smarty->assign(array(
            'category'             => $this->category,
            'description_short'    => Tools::truncateString($this->category->description, 350),
            'products'             => (isset($this->cat_products) && $this->cat_products) ? $this->cat_products : null,
            'nb_products'          => (int)$this->nb_products,
            'id_category'          => (int)$this->category->id,
            'id_category_parent'   => (int)$this->category->id_parent,
            'return_category_name' => Tools::safeOutput($this->category->name),
            'path'                 => Tools::getPath($this->category->id),
            'add_prod_display'     => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'categorySize'         => Image::getSize(ImageType::getFormatedName('category')),
            'mediumSize'           => Image::getSize(ImageType::getFormatedName('medium')),
            'thumbSceneSize'       => Image::getSize(ImageType::getFormatedName('m_scene')),
            'homeSize'             => Image::getSize(ImageType::getFormatedName('home')),
            'allow_oosp'           => (int)Configuration::get('PS_ORDER_OUT_OF_STOCK'),
            'comparator_max_item'  => (int)Configuration::get('PS_COMPARATOR_MAX_ITEM'),
            'suppliers'            => Supplier::getSuppliers(),
            'body_classes'         => array($this->php_self.'-'.$this->category->id, $this->php_self.'-'.$this->category->link_rewrite),
            'category_fetature_products' => CategoryFeatures::getProductsForCategory($this->category->id, $this->context->language->id, 8),
            'show_geo_filters'     => $this->show_geo_filters
        ));
    }

    //SoftSprint Uliana
    public function premtype($customer_id){

        $premtype = 0;
        $sql = "SELECT premtype FROM pscl_prem WHERE id_customer = ".$customer_id." AND date_add <= NOW() AND date_end >= NOW() ORDER BY date_add DESC";
        $premtype = Db::getInstance()->getValue($sql);
        
        if (!$premtype) {
                    return 0;
                }else{
                    return $premtype;
                }

    }
    /**
     * Assigns scenes template variables
     */
    protected function assignScenes()
    {
        // Scenes (could be externalised to another controller if you need them)
        $scenes = Scene::getScenes($this->category->id, $this->context->language->id, true, false);
        $this->context->smarty->assign('scenes', $scenes);

        // Scenes images formats
        if ($scenes && ($scene_image_types = ImageType::getImagesTypes('scenes'))) {
            foreach ($scene_image_types as $scene_image_type) {
                if ($scene_image_type['name'] == ImageType::getFormatedName('m_scene')) {
                    $thumb_scene_image_type = $scene_image_type;
                } elseif ($scene_image_type['name'] == ImageType::getFormatedName('scene')) {
                    $large_scene_image_type = $scene_image_type;
                }
            }

            $this->context->smarty->assign(array(
                'thumbSceneImageType' => isset($thumb_scene_image_type) ? $thumb_scene_image_type : null,
                'largeSceneImageType' => isset($large_scene_image_type) ? $large_scene_image_type : null,
            ));
        }
    }

    /**
     * Assigns subcategory templates variables
     */
    protected function assignSubcategories()
    {
        if ($this->category->id != 1438 && $sub_categories = $this->category->getSubCategories($this->context->language->id)) {
            // foreach () {

            // }
            $rows = Category::getProductsByDefautlCategories(
                array_map(function($c){return (int)$c['id_category'];}, $sub_categories)
            );
            $sub_products = array();
            foreach ($rows as $row) {
                if (!isset($sub_products[$row['id_category']])) {
                    $sub_products[$row['id_category']] = array();
                }
                if (count($sub_products[$row['id_category']]) == 4) {
                    continue;
                }
                $sub_products[$row['id_category']][] = $row;
            }
            unset($rows);
            // echo "<pre>"; print_r($sub_products); echo "</pre>"; exit;
            // echo "<pre>"; print_r($sub_categories); echo "</pre>"; exit;
            $this->context->smarty->assign(array(
                'sub_products'           => $sub_products,
                'subcategories'          => $sub_categories,
                'subcategories_nb_total' => count($sub_categories),
                'subcategories_nb_half'  => ceil(count($sub_categories) / 2)
            ));
        }
    }

    /**
     * Assigns product list template variables
     */
    public function assignProductList()
    {
        $hook_executed = false;
        Hook::exec('actionProductListOverride', array(
            'nbProducts'   => &$this->nbProducts,
            'catProducts'  => &$this->cat_products,
            'hookExecuted' => &$hook_executed,
        ));

        // The hook was not executed, standard working
        if (!$hook_executed) {
            $this->context->smarty->assign('categoryNameComplement', '');
            
			//LLL 21/11/2017
			//$this->nbProducts = $this->category->getProducts(null, null, null, $this->orderBy, $this->orderWay, true);
			$this->nbProducts = $this->category->getProductscity(0,null, null, null, $this->orderBy, $this->orderWay, true, true, false, 1, true, null, $this->show_geo_filters);
            
			
			$this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
            
			//LLL 21/11/2017
			//$this->cat_products = $this->category->getProducts($this->context->language->id, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay);
			$this->cat_products = $this->category->getProductscity(0,$this->context->language->id, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay, false, true, false, 1, true, null, $this->show_geo_filters);

        }
        // Hook executed, use the override
        else {
            // Pagination must be call after "getProducts"
            //$this->pagination($this->nbProducts);
        }
        
        $this->addColorsToProductList($this->cat_products);

        Hook::exec('actionProductListModifier', array(
            'nb_products'  => &$this->nbProducts,
            'cat_products' => &$this->cat_products,
        ));

   //      foreach ($this->cat_products as &$product) {
            
			// //LLL 21/11/2017
			// if ($this->doorderandpay(9,$product['id_owner']) == 0 ){	
			// 		$product['featurecustomer'] =0;
			//  }else{
			// 		$product['featurecustomer'] =1;
			//  }	 
			
		
			
			// if (isset($product['id_product_attribute']) && $product['id_product_attribute'] && isset($product['product_attribute_minimal_quantity'])) {
   //              $product['minimal_quantity'] = $product['product_attribute_minimal_quantity'];
   //          }
   //      }
        
			// $this->cat_products = $this->array_sort($this->cat_products, 'featurecustomer', SORT_DESC);

		
		
		
        $this->context->smarty->assign('nb_products', $this->nbProducts);
    }
	
	
	
	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	
	
	public function doorderandpay($id_product,$customer_id){
			//$customer_id = Context::getContext()->customer->id;
				//print "<pre>";
				$allorder = Order::getIdOrderProduct($customer_id,$id_product);

				$ooodet = new Order($allorder);
				// order is valid???
				if ($ooodet->valid == 1){
					//calc 365 days
					//print $ooodet->date_add;
					$now = time(); // or your date as well
					$your_date = strtotime($ooodet->date_add);
					$datediff = $now - $your_date;
					 $daysbet= floor($datediff / (60 * 60 * 24));
					// if more then 365 days 
					if($daysbet < 365){
						return 1;
					}else{
						return 0;
					}
				}else{
					return 0;
				}
			
	}
	
	
	
	
    /**
     * Returns an instance of the current category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}

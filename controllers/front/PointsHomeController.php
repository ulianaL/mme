<?php


class PointsHomeControllerCore extends FrontController
{
    /** string Internal controller name */
    public $php_self = 'pointshome';

    /** @var Category Current category object */
    protected $category;

    /** @var bool If set to false, customer cannot view the current category. */
    public $customer_access = true;

    /** @var int Number of products in the current page. */
    protected $nbProducts;

    /** @var array Products to be displayed in the current page . */
    protected $products_arr = array();

    /**
     * @var int
     **/
    private $hpoint = 0;

    /**
     * @var string
     **/
    private $hpoint_name = '';

    /**
     * Sets default medias for this controller
     */
    public function setMedia()
    {
        parent::setMedia();

        if (!$this->useMobileTheme()) {
            //TODO : check why cluetip css is include without js file
            $this->addCSS(array(
                _THEME_CSS_DIR_.'scenes.css'       => 'all',
                _THEME_CSS_DIR_.'category.css'     => 'all',
                _THEME_CSS_DIR_.'product_list.css' => 'all',
            ));
        }

        // $scenes = Scene::getScenes($this->category->id, $this->context->language->id, true, false);
        // if ($scenes && count($scenes)) {
        //     $this->addJS(_THEME_JS_DIR_.'scenes.js');
        //     $this->addJqueryPlugin(array('scrollTo', 'serialScroll'));
        // }

        $this->addJS(_THEME_JS_DIR_.'category.js');
    }

    /**
     * Initializes controller
     *
     * @see FrontController::init()
     * @throws PrestaShopException
     */
    public function init()
    {
        $cats = self::getHomeCategoriesList();
        $this->hpoint = (int)Tools::getValue('hpoint', 0);
        if (!isset($cats[$this->hpoint])) {
            header('HTTP/1.1 404 Not Found');
            header('Status: 404 Not Found');
            $this->hpoint = 0;
            $this->errors[] = Tools::displayError('Category not found');
        } else {
            $this->hpoint_name = $cats[$this->hpoint];
        }

        parent::init();
    }


    /**
     * Initializes page content variables
     */
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate(_PS_THEME_DIR_.'pointshome.tpl');

        // if (isset($this->context->cookie->id_compare)) {
        //     $this->context->smarty->assign('compareProducts', CompareProduct::getCompareProducts((int)$this->context->cookie->id_compare));
        // }

        // Product sort must be called before assignProductList()
        // $this->productSort();

        // $this->assignScenes();
        // $this->assignSubcategories();
        $this->assignProductList();

        $this->nb_products = count($this->products_arr);
        // echo "<pre>"; var_dump($this->products_arr); echo "</pre>"; exit;
        $this->context->smarty->assign(array(
            'category'             => $this->category,
            // 'description_short'    => Tools::truncateString($this->category->description, 350),
            'products'             => (isset($this->products_arr) && $this->products_arr) ? $this->products_arr : null,
            'products' . $this->hpoint             => (isset($this->products_arr) && $this->products_arr) ? $this->products_arr : null,
            'nb_products'          => (int)$this->nb_products,
            'hpoint_id'            => (int)$this->hpoint,
            'hpoint_name'            => $this->hpoint_name,
            // 'id_category_parent'   => (int)$this->category->id_parent,
            // 'return_category_name' => Tools::safeOutput($this->category->name),
            // 'path'                 => Tools::getPath($this->category->id),
            'add_prod_display'     => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'categorySize'         => Image::getSize(ImageType::getFormatedName('category')),
            'mediumSize'           => Image::getSize(ImageType::getFormatedName('medium')),
            'thumbSceneSize'       => Image::getSize(ImageType::getFormatedName('m_scene')),
            'homeSize'             => Image::getSize(ImageType::getFormatedName('home')),
            'allow_oosp'           => (int)Configuration::get('PS_ORDER_OUT_OF_STOCK'),
            'comparator_max_item'  => (int)Configuration::get('PS_COMPARATOR_MAX_ITEM'),
            // 'suppliers'            => Supplier::getSuppliers(),
            'body_classes'         => array($this->php_self.'-'.$this->hpoint, $this->php_self.'-'.$this->hpoint.'-r'),

            'meta_title' => $this->hpoint_name . ' - Medical Marijuana Exchange',
        ));
    }

    /**
     * Assigns product list template variables
     */
    public function assignProductList()
    {
        $this->products_arr = (new Category((int)Configuration::get('HOME_FEATURED_NBR'), (int)Context::getContext()->language->id))
            ->getProductshome($this->hpoint, (int)Context::getContext()->language->id, 1, 1000, null, null, false, true, true, 1000, false, null);
    }

    public static function getHomeCategoriesList()
    {
        return array(
            1 => 'Deals',
            2 => 'Deals Nearby',
            3 => 'Delivery',
            4 => 'Dispensary Storefronts',
            5 => 'Doctor',
            6 => 'Featured Brands',
            7 => 'Medical',
            8 => 'Events'
        );
    }
}
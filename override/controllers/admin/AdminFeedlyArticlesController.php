<?php

class AdminFeedlyArticlesController extends AdminController
{
    public function __construct()
    {
        $this->table = 'board_article';
        $this->className = 'BoardArticle';
        // $this->module = 'medicalmarijuanaexchangedirectory';
        $this->allow_export = true;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->_orderBy = 'date';
        $this->_orderWay = 'DESC';
        parent::__construct();
        $this->bulk_actions = array(
        'delete' => array(
            'text' => $this->l('Delete selected'),
            'confirm' => $this->l('Would you like to delete the selected items?'),
            )
        );
        
        $this->_where .= ' AND a.deleted = 0';

        $this->fields_list = array(           
            'date' => array(
                'title' => $this->l('Date'),
                'type' => 'text', 
            
            ),
            'title' => array(
                'title' => $this->l('Title'),
               
                'type' => 'text', 
            ),
            'confirm' => array(
                'title' => $this->l('Confirm'),
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'admin_category' => array(
                'title' => $this->l('Category'),
                'type' => 'text', 
            ),
        );
    }

    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        //$this->addRowAction('view');

        return parent::renderList();
    }

    public function initToolbar() {
        parent::initToolbar();

        unset( $this->toolbar_btn['new'] );
    }



    
    public function renderForm()
    {
       if (!($obj = $this->loadObject(true))) {
            return;
        }       
        if(!filter_var($obj->image, FILTER_VALIDATE_URL)){
            $obj->image = '../upload/blockblog/'.$obj->image;
        }
        $this->fields_form = array(
           // 'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Edit'),
            ),
            'input' => array(
                
                array(
                    'type' => 'html',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'html_content' => '<input type="text" name="title" value="'.$obj->title.'">
                    '  
                ),
                array(
                    'type' => 'date',
                    'label' => $this->l('Date'),
                    'name' => 'date',
                    'html_content' => '<input type="text" name="date" value="'.$obj->date.'">
                    '  
                ),
                array(
                    'type' => 'html',
                    'html_content' => '<img style="padding-top: 8px; max-height:300px;" src="'.$obj->image.'">'
                    
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Change picture'),
                    'name' => 'source',
                    'html_content' => '<div class="col-lg-6">
                    <input id="news_image" type="file" name="image" class="hide">
                    <div class="dummyfile input-group">
                        <span class="input-group-addon"><i class="icon-file"></i></span>
                        <input id="news_image-name" type="text" class="disabled" name="filename" readonly="">
                            <span class="input-group-btn">
                                <button id="news_image-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
                                    <i class="icon-folder-open"></i> Choose a file
                                </button>
                            </span>
                    </div>

                    
                        <script type="text/javascript">'."
                            $(document).ready(function(){
                                $('#news_image-selectbutton').click(function(e){
                                    $('#news_image').trigger('click');
                                });
                                $('#news_image').change(function(e){
                                    var val = $(this).val();
                                    var file = val.split(/[\/]/);
                                    $('#news_image-name').val(file[file.length-1]);
                                });
                            });
                        </script>"
                    
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Source'),
                    'name' => 'source',
                    'html_content' => '<span style="display: inline-block; padding-top: 8px;">'.$obj->source.'</span>'
                    
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Source Link'),
                    'name' => 'source_link',
                    'html_content' => '<a href="'.$obj->url.'" target="_bank" style="display: inline-block; padding-top: 8px;">'.$obj->url.'</a>'
                    
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Location'),
                    'name' => 'location',
                    'html_content' => '<span style="display: inline-block; padding-top: 8px;">'.$obj->city.', '.$obj->country.'</span>'
                    
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Categories'),
                    'name' => 'categories',
                    'html_content' => $this->categoriescallback($obj->categories),
                    
                ),
                // array(
                //     'type' => 'input',
                //     'label' => $this->l('Shop category'),
                //     'name' => 'admin_category',
                //     'value' => $obj->admin_category,
                    
                // ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Content'),
                    'name' => 'content',
                    'html_content' => '<div style="display:inline-block; padding-top: 8px; max-height:300px; overflow:auto">'.nl2br($obj->content).'</div>'
                    
                ),

                // array(
                //     'type' => 'html',
                //     'html_content' => '<div style="display:inline-block; padding-top: 8px; max-height:300px; overflow:auto">
                //     <label style="display:block"><input type="checkbox" value="1" name="show_in_news">Show in news/recent articles</label>
                //     </div>'  
                // ),

                array(
                    'type' => 'textarea',
                    'label' => $this->l('SEO Keywords'),
                    'name' => 'seo_keywords',
                    'id' => 'seo_keywords',
                    'required' => FALSE,
                    'autoload_rte' => FALSE,
                    'lang' => false,
                    'rows' => 4,
                    'cols' => 40,

                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('SEO Description'),
                    'name' => 'seo_description',
                    'id' => 'seo_description',
                    'required' => FALSE,
                    'autoload_rte' => FALSE,
                    'lang' => false,
                    'rows' => 4,
                    'cols' => 40,

                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Hashtags'),
                    'name' => 'hashtags',
                    'html_content' => '<input type="text" name="hashtags" value="'.$obj->hashtags.'">',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show in news/recent articles'),
                    'name' => 'show_in',
                    'required' => false,
                    'orderby' => 'ASC',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),

                array(
                    'type' => 'html',
                    'label' => $this->l('Select categories:'),
                    'name' => 'show_in',
                    'orderby' => 'ASC',
                    'html_content' => $this->getcat($obj->category_to_show),
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Displayed'),
                    'name' => 'confirm',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                
            )
        );

        

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );
        
        

        return parent::renderForm();
    }

    public function categoriescallback($value)
    {
        $categories = json_decode($value);
        $cat_list = '';
        if (!empty($categories)&&isset($categories)){
            
            foreach ($categories as $cat) {
                $cat_list .='<p>'.$cat.'<p>';
            }
        }
        
        return '<div style="display:inline-block; padding-top: 8px;">'.$cat_list.'</div>';
    }

    public function getcat($value)
    {
        $cat_selected = $value;
        if (!is_null($cat_selected) && $value){
            $cat_selected = json_decode($cat_selected);
        }else{
            $cat_selected = array();
        }

        $sql = 'SELECT pc.*
            FROM `'._DB_PREFIX_.'blog_category` pc
            ORDER BY pc.`time_add` DESC';
  
        $categories = Db::getInstance()->ExecuteS($sql);

        foreach($categories as $k => $_item){
            $items_data = Db::getInstance()->ExecuteS('
            SELECT pc.*
            FROM `'._DB_PREFIX_.'blog_category_data` pc
            WHERE pc.id_item = '.$_item['id'].'
            ');
            
            
            global $cookie;
            $defaultLanguage =  $cookie->id_lang;
            
            $tmp_title = '';
            $tmp_id = '';
            $tmp_link = '';
            $tmp_time_add = '';
            
            foreach ($items_data as $item_data){
                
                $title = isset($item_data['title'])?$item_data['title']:'';
                $id = isset($item_data['id_item'])?$item_data['id_item']:'';
                $time_add = isset($categories[$k]['time_add'])?$categories[$k]['time_add']:'';
                
                if(strlen($tmp_title)==0){
                    if(strlen($title)>0)
                            $tmp_title = $title; 
                }
                
                if(strlen($tmp_id)==0){
                    if(strlen($id)>0)
                            $tmp_id = $id; 
                }
                
                if(strlen($tmp_time_add)==0){
                    if(strlen($time_add)>0)
                            $tmp_time_add = $time_add; 
                }
                
                if($defaultLanguage == $item_data['id_lang']){
                    $items[$k]['title'] = $item_data['title'];
                    $items[$k]['id'] = $id;
                    $items[$k]['time_add'] = $time_add;
                }
                
            }
            
            if(@strlen($items[$k]['title'])==0)
                $items[$k]['title'] = $tmp_title;
                
            if(@strlen($items[$k]['id'])==0)
                $items[$k]['id'] = $tmp_id;
                
            if(@strlen($items[$k]['time_add'])==0)
                $items[$k]['time_add'] = $tmp_time_add;   
        }

        $items_list = '';
        function cmp($a, $b){
            return strcmp(trim($a['title']), trim($b['title']));
        }
        usort($items, "cmp");
        foreach ($items as $item) {
            if (in_array($item["id"], $cat_selected)) {
                $items_list.='<label style="width:30%"><input type="checkbox" value="'.$item["id"].'"   name="ids_categories[]" checked ="checked">'.$item["title"].'</label>';
            }else{
                $items_list.='<label style="width:30%"><input type="checkbox" value="'.$item["id"].'"   name="ids_categories[]">'.$item["title"].'</label>';
            }
        }
        return  '<div style="display:inline-block; padding-top: 8px;">'.$items_list.'</div>';
    }

    public function postProcess()
    {
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        // exit;
        if (Tools::getValue('id_board_article', 0)) {
            $this->object = $this->loadObject();
        }
        parent::postProcess();
        // echo '<pre>';
        // var_dump($this->object);exit;
        if (isset($_POST['submitAddboard_article'])) {
            if (!empty($_POST['ids_categories'])) {
                $ids_categories = json_encode($_POST['ids_categories']);
            }else{
                $ids_categories = json_encode($_POST['ids_categories']);
            }
            $this->object->show_in = $_POST['show_in'];
            $this->object->confirm = $_POST['confirm'];
            $this->object->category_to_show = $ids_categories;
            $this->object->seo_keywords = $_POST['seo_keywords'];
            $this->object->seo_description = $_POST['seo_description'];
            $this->object->hashtags = $_POST['hashtags'];
            $this->object->save();
        }
        
        if (isset($_FILES) && count($_FILES)) {
            $name = 'image';
            if ($_FILES[$name]['name'] != null) {
                $uniquename = time().'.'.pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
                $target_dir = _PS_MODULE_DIR_ . "../upload/blockblog/";
                move_uploaded_file($_FILES[$name]["tmp_name"],  $target_dir.$uniquename);
                $this->object->image = $uniquename;
                $this->object->save();
            }
        }
        if(isset($_POST['title'])){
            $this->object->title = $_POST['title'];
            $this->object->date = $_POST['date'];
            $this->object->save();
        }
    }
}

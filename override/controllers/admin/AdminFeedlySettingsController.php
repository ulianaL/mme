<?php

class AdminFeedlySettingsController extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function postProcess()
    {
        $this->display = 'edit';
        if (Tools::isSubmit('submitAddconfiguration')) {
            Configuration::updateValue('FEEDLY_BOARDS', Tools::getValue('boards'));
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminFeedlySettings', true) . '&conf=6');
            // echo "<pre>"; print_r($_POST); echo "</pre>";
            // exit;
        }

        return parent::postProcess();
    }

    public function renderForm()
    {
        $this->initToolbar();
        $this->initPageHeaderToolbar();    
    

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'label' => $this->l('Boards'),
                    'name' => 'boards',
                    'type' => 'textarea',
                    'lang' => false,
                    'required' => false,
                    'rows' => 15,
                    'desc' => $this->l('Here you can set multiple boards') . '<br/>' .
                        $this->l('Please enter the board link in the new line') . '<br/>' .
                        '<a href="/how-to-get-board-url.html" target="_blank">' . $this->l('How I can get the url?') . '</a>'
                ),
                // array(
                //     'type' => 'switch',
                //     'label' => $this->l('Active'),
                //     'name' => 'active',
                //     'required' => false,
                //     'is_bool' => true,
                //     'values' => array(
                //         array(
                //             'id' => 'active_on',
                //             'value' => 1,
                //             'label' => $this->l('Yes')
                //         ),
                //         array(
                //             'id' => 'active_off',
                //             'value' => 0,
                //             'label' => $this->l('No')
                //         )
                //     )
                // ),
                // array(
                //     'type' => 'switch',
                //     'label' => $this->l('Repeat'),
                //     'name' => 'repeat',
                //     'required' => false,
                //     'is_bool' => true,
                //     'values' => array(
                //         array(
                //             'id' => 'repeat_on',
                //             'value' => 1,
                //             'label' => $this->l('Yes')
                //         ),
                //         array(
                //             'id' => 'repeat_off',
                //             'value' => 0,
                //             'label' => $this->l('No')
                //         )
                //     ),
                //     'desc' => $this->l('The popup will display again')
                // ),               
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );

        $this->object = new StdClass;
        $this->object->id = 1;
        $this->object->boards = Configuration::get('FEEDLY_BOARDS');

        return parent::renderForm();
    }
}

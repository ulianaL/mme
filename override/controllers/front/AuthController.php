<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class AuthController extends AuthControllerCore
{
   
    /*
    * module: megarecaptcha
    * date: 2018-03-11 20:20:09
    * version: 1.0.0
    */
    protected function processSubmitLogin()
    {
        Hook::exec('actionBeforeAuthentication');
        
        
		
		
		
		
		
		
		if (Configuration::get('MEGARECAPTCHA_LOGIN_ENABLE')) {
            if (Tools::getIsset('g-recaptcha-response')) {
                        $post_data = http_build_query(
                            array(
                                'secret' => Configuration::get('MEGARECAPTCHA_SECRET'),
                                'response' => Tools::getValue('g-recaptcha-response'),
                                'remoteip' => $_SERVER['REMOTE_ADDR']
                            )
                        );
                        $opts = array('http' =>
                            array(
                                'method'  => 'POST',
                                'header'  => 'Content-type: application/x-www-form-urlencoded',
                                'content' => $post_data
                            )
                        );
                        $context2  = stream_context_create($opts);
                        $response = Tools::file_get_contents(
                            'https://www.google.com/recaptcha/api/siteverify',
                            false,
                            $context2
                        );
                        $result = json_decode($response);
                if (!$result->success) {
                            $errorfromcaptcha = 1;
                } else {
                            $errorfromcaptcha = 0;
                }
            } else {
                     $errorfromcaptcha = 1;
            }
        } else {
               $errorfromcaptcha = 0;
        }
        
      
       
			
		
		   //$_POST['bussiness'] = 1;

        if (!isset($_POST['dispensaries'])) {

            $_POST['dispensaries'] = 0;

        }
		
        $passwd = trim(Tools::getValue('passwd'));
        $_POST['passwd'] = null;
        $email = trim(Tools::getValue('email'));
        if (empty($email)) {
            $this->errors[] = Tools::displayError('An email address required.');
        } elseif (!Validate::isEmail($email)) {
            $this->errors[] = Tools::displayError('Invalid email address.');
        } elseif (empty($passwd)) {
            $this->errors[] = Tools::displayError('Password is required.');
        } elseif (!Validate::isPasswd($passwd)) {
            $this->errors[] = Tools::displayError('Invalid password.');
        } elseif ($errorfromcaptcha == 1) {
            $this->errors[] = Tools::displayError('CAPTCHA verification failed.');
				  
		} else {
			
			 
			
			
			
            $customer = new Customer();
            $authentication = $customer->getByEmail(trim($email), trim($passwd));
            if (isset($authentication->active) && !$authentication->active) {
                $this->errors[] = Tools::displayError('Your account isn\'t available at this time, please contact us');
            } elseif (!$authentication || !$customer->id) {
                $this->errors[] = Tools::displayError('Authentication failed.');
            
			} elseif ($_POST['bussiness'] == 0 && $_POST['bussiness'] != $customer->bussiness) {
                
				
				$this->errors[] = Tools::displayError('You can not login because your account is Bussiness account.');
			 
			} elseif ($_POST['dispensaries'] == 0 && $_POST['dispensaries'] != $customer->dispensaries) {
                
                
                $this->errors[] = Tools::displayError('You can not login because your account is Dispensaries account.');
             
            } elseif ($_POST['bussiness'] == 1 && $_POST['bussiness'] != $customer->bussiness) {
                
				//print "okok" ; $_POST['bussiness'];
				$this->errors[] = Tools::displayError('You can not login because your account is Normal account.');
			
			} elseif ($_POST['dispensaries'] == 1 && $_POST['dispensaries'] != $customer->dispensaries && $_POST['dispensaries'] != $customer->bussiness) {
                
                $this->errors[] = Tools::displayError('You can not login because your account is Normal account.');
            
            } elseif ($_POST['dispensaries'] == 1 && $_POST['dispensaries'] != $customer->dispensaries && $_POST['dispensaries'] == $customer->bussiness) {
                
                $this->errors[] = Tools::displayError('You can not login because your account is Bussiness account.');
            
            } else {
                $this->context->cookie->id_compare = isset($this->context->cookie->id_compare)
                ? $this->context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
                $this->context->cookie->id_customer = (int)($customer->id);
                $this->context->cookie->customer_lastname = $customer->lastname;
                $this->context->cookie->customer_firstname = $customer->firstname;
                $this->context->cookie->logged = 1;
                $customer->logged = 1;
                $this->context->cookie->is_guest = $customer->isGuest();
                $this->context->cookie->passwd = $customer->passwd;
                $this->context->cookie->email = $customer->email;
                $this->context->customer = $customer;
                if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart)
                    || Cart::getNbProducts($this->context->cookie->id_cart) == 0)
                    && $id_cart = (int)Cart::lastNoneOrderedCart($this->context->customer->id)) {
                    $this->context->cart = new Cart($id_cart);
                } else {
                    $id_carrier = (int)$this->context->cart->id_carrier;
                    $this->context->cart->id_carrier = 0;
                    $this->context->cart->setDeliveryOption(null);
                    $cufi = (int)Address::getFirstCustomerAddressId((int)($customer->id));
                    $this->context->cart->id_address_delivery = $cufi;
                    $this->context->cart->id_address_invoice = $cufi;
                }
                $this->context->cart->id_customer = (int)$customer->id;
                $this->context->cart->secure_key = $customer->secure_key;
                if ($this->ajax && isset($id_carrier) && $id_carrier && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                    $delivery_option = array($this->context->cart->id_address_delivery => $id_carrier.',');
                    $this->context->cart->setDeliveryOption($delivery_option);
                }
                $this->context->cart->save();
                $this->context->cookie->id_cart = (int)$this->context->cart->id;
                $this->context->cookie->write();
                $this->context->cart->autosetProductAddress();
                Hook::exec('actionAuthentication', array('customer' => $this->context->customer));
                CartRule::autoRemoveFromCart($this->context);
                CartRule::autoAddToCart($this->context);
                if (!$this->ajax) {
                    $back = Tools::getValue('back', 'my-account');
                    if ($back == Tools::secureReferrer($back)) {
                        Tools::redirect(html_entity_decode($back));
                    }
                    Tools::redirect('index.php?controller='
                    .(($this->authRedirection !== false) ? urlencode($this->authRedirection) : $back));
                }
            }
        }
        if ($this->ajax) {
            $return = array(
                'hasError' => !empty($this->errors),
                'errors' => $this->errors,
                'token' => Tools::getToken(false)
            );
            $this->ajaxDie(Tools::jsonEncode($return));
        } else {
            $this->context->smarty->assign('authentification_error', $this->errors);
        }
    }
    
    /*
    * module: megarecaptcha
    * date: 2018-03-11 20:20:09
    * version: 1.0.0
    */
    protected function processSubmitAccount()
    {
        if (Configuration::get('MEGARECAPTCHA_ACCOUNT_ENABLE')) {
            if (Tools::getIsset('g-recaptcha-response')) {
                        $post_data = http_build_query(
                            array(
                                'secret' => Configuration::get('MEGARECAPTCHA_SECRET'),
                                'response' => Tools::getValue('g-recaptcha-response'),
                                'remoteip' => $_SERVER['REMOTE_ADDR']
                            )
                        );
                        $opts = array('http' =>
                            array(
                                'method'  => 'POST',
                                'header'  => 'Content-type: application/x-www-form-urlencoded',
                                'content' => $post_data
                            )
                        );
                        $context2  = stream_context_create($opts);
                        $response = Tools::file_get_contents(
                            'https://www.google.com/recaptcha/api/siteverify',
                            false,
                            $context2
                        );
                        $result = json_decode($response);
                if (!$result->success) {
                            $errorfromcaptcha = 1;
                } else {
                            $errorfromcaptcha = 0;
                }
            } else {
                     $errorfromcaptcha = 1;
            }
        } else {
               $errorfromcaptcha = 0;
        }
        
        
        if ($errorfromcaptcha == 0) {
               parent::processSubmitAccount();
        } else {
               $this->errors[] = Tools::displayError('CAPTCHA verification failed.');
               parent::processSubmitAccount();
        }
    } 
}

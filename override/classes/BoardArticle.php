<?php

class BoardArticle extends ObjectModel
{
    // public $id_board_article;
    public $id;
    public $id_feedly_board;
    public $uri;
    public $uid;
    public $title;
    public $image;
    public $content;
    public $date;
    public $categories;
    public $city;
    public $country;
    public $category_to_show;
    public $show_in;
    public $url; 
    public $source;
    public $confirm; 
    public $admin_category;
    public $deleted = 0;
    public $seo_keywords;
    public $seo_description;
    public $hashtags;
    
    public static $definition = array(
        'table' => 'board_article',
        'primary' => 'id_board_article',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'id_feedly_board' =>                    array('type' => self::TYPE_INT),
            'uri' =>             array('type' => self::TYPE_STRING),
            'uid' =>             array('type' => self::TYPE_STRING),
            'title' =>             array('type' => self::TYPE_STRING),
            'image' =>             array('type' => self::TYPE_STRING),
            'content' =>             array('type' => self::TYPE_STRING),
            'date' =>             array('type' => self::TYPE_STRING),
            'categories' =>             array('type' => self::TYPE_STRING),
            'city' =>             array('type' => self::TYPE_STRING),
            'country' => array('type' => self::TYPE_STRING),
            'category_to_show' => array('type' => self::TYPE_STRING),
            'show_in' => array('type' => self::TYPE_STRING),
            'url' => array('type' => self::TYPE_STRING),
            'source' => array('type' => self::TYPE_STRING),
            'confirm' => array('type' => self::TYPE_INT),
            'admin_category' => array('type' => self::TYPE_STRING),
            'deleted' =>                    array('type' => self::TYPE_INT),
            'seo_keywords' =>             array('type' => self::TYPE_STRING),
            'seo_description' =>             array('type' => self::TYPE_STRING),
            'hashtags' =>             array('type' => self::TYPE_STRING),
        ),
        
    );

    
   

    public function __construct($id_board_article = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
        parent::__construct($id_board_article, $id_lang, $id_shop);
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        } 
        return true;
    }

    public function add($auto_date = true, $null_values = false)
    {
        return parent::add($auto_date = true, $null_values = false);
    }
    
    public function update($null_values = false)
    {
       return parent::update($null_values);
    }
    public static function checkDuplicate($uri='', $title=''){
        return Db::getInstance()->getRow('
         SELECT *
         FROM `pscl_board_article` 
         WHERE `uri` = ' . $uri . ' OR `title` = "'.$title.'"');
    }
    public static function getBoardArticle()
    { 
         return Db::getInstance()->executeS("
         SELECT *
         FROM `pscl_board_article`
         WHERE `confirm` = 1 AND deleted = 0");
    }
    public static function removeRowBoardArticle($id){
        return Db::getInstance()->getRow("
            DELETE
            FROM `pscl_board_article` 
            WHERE `id_board_article` = ".$id
        );
    }

    public static function markAsDeleted($id){
        return Db::getInstance()->execute("
            UPDATE `pscl_board_article` SET deleted = 1 
            WHERE `id_board_article` = ".$id . " LIMIT 1"
        );
    }

    public static function getInstanceByUid($uid)
    {
        return new self(
            (int)Db::getInstance()->getValue(
                "SELECT id_board_article FROM " . _DB_PREFIX_ . "board_article WHERE uid = '" . addslashes($uid) . "'"
            )
        );
    }

    public static function generateUri()
    {
        $chars = str_split('1234567890');

        while (true) {
            $uri = '5';
            for ($i = 0; $i < 6; $i++) {
                $uri .= $chars[array_rand($chars)];
            }

            $i = (int)Db::getInstance()->getValue(
                "SELECT id_board_article FROM " . _DB_PREFIX_ . "board_article WHERE uri = '" . $uri . "'"
            );
            if ($i) {
                continue;
            }

            return $uri;
        }
    }

    public static function loadFullContent($uid)
    {
        $curl = curl_init();

        // print_r($endpoint. "\n");
        // print_r(http_build_query($request_data) . "\n");

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://feedly.com/v3/entries/.mget?ck=' . time() . '000&ct=feedly.desktop&cv=31.0.1137',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => Tools::jsonEncode(array($uid)),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0',
                'Authorization: Bearer A87LNN8k8m1_1-L7qRoPq2ZmpuHKU1IIlka14lG4Ig-mc4YVjS9xxOEKfmQIu_s-WUlp7YlmSxrcs1PLjTRYf2UyUVwNWkczItvQENWUBg4UzbvhcKHdIcznO23vkszYlpZuyBXVxUQ44ztEgu0mjfQ1Erq40HhaOAw6HCn5MEAbfwooImEhlhfpvxT_T-uiN2h2EQJD9SwJTeyUsLv_DH8-WYrocVSuZYrwhkMnzKRlzA:feedly'
            )
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception(
                "Feedly request error: " . $err
            );
            return false;
        }

        return Tools::jsonDecode($response, true);
    }
}

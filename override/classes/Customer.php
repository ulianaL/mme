<?php
class Customer extends CustomerCore
{
	/*
    * module: tmsociallogin
    * date: 2017-12-12 06:27:55
    * version: 1.2.0
    */
    public function logout()
	{
		if (file_exists(dirname(__FILE__).'/../../facebook/facebook.php'))
		{
			include(dirname(__FILE__).'/../../facebook/facebook.php');
			$facebook = new Facebook(array(
				'appId'  => Configuration::get('TMSOCIALLOGIN_APPID'),
				'secret' => Configuration::get('TMSOCIALLOGIN_APPSECRET'),
			));
			$facebook->destroySession();
		}
		parent::logout();
	}
	/*
    * module: tmsociallogin
    * date: 2017-12-12 06:27:55
    * version: 1.2.0
    */
    public function mylogout()
	{
		if (file_exists(dirname(__FILE__).'/../../facebook/facebook.php'))
		{
			include(dirname(__FILE__).'/../../facebook/facebook.php');
			$facebook = new Facebook(array(
				'appId'  => Configuration::get('TMSOCIALLOGIN_APPID'),
				'secret' => Configuration::get('TMSOCIALLOGIN_APPSECRET'),
			));
			$facebook->destroySession();
		}
		parent::mylogout();
	}
}

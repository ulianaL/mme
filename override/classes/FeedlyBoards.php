<?php

class FeedlyBoards extends ObjectModel
{
    public $id;
    public $url;
    public $stream_id;
    public $title;
    public $updated = 0;
    public $date_add;
    public $date_upd;

    private $is_initialized = false;
    private $urls = array();

    public static $definition = array(
        'table' => 'feedly_board',
        'primary' => 'id_feedly_board',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'url' =>  array('type' => self::TYPE_STRING),
            'stream_id' =>  array('type' => self::TYPE_STRING),
            'title' =>  array('type' => self::TYPE_STRING),
            'updated' =>                    array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate')
        )
    );

    public function init()
    {
        $this->is_initialized = true;

        $this->urls = array_unique(array_filter(
            array_map(
                function ($u) {
                    return trim($u);
                },
                explode("\n", Configuration::get('FEEDLY_BOARDS'))
            )
        ));

        return $this;
    }

    public function loadArticles()
    {
        if (php_sapi_name() != 'cli') {
            throw new Exception('This method must be run on the command line.');
        }

        if ($this->is_initialized == false) {
            throw new Exception('The object is not initialized');
        }

        // print_r($this->getUrls());
        // var_dump(BoardArticle::generateUri()); exit;
        // print_r(BoardArticle::loadFullContent('iRKD5eAJAsxbQNwtyrieHCE5vP6GiPuDfJC2DIVDez8=_177f5491331:350f524:a423a121'));
        // echo "\n";
        // exit;

        foreach ($this->getUrls() as $url) {
            // print_r($this->getBoardUri($url) . "\n");
            $stream_id = $this->getBoardUri($url);
            if ($stream_id) {
                // get information about board
                $response = $this->request(
                    'https://feedly.com/v3/recommendations/feeds/' . urlencode($stream_id),
                    'GET',
                    array('count' => 1, 'locale' => 'en', 'ck' => time(), 'ct' => 'feedly.desktop', 'cv' => '31.0.1124')
                );
                // print_r($response);
                if ($response && isset($response['source'])) {
                    $board = self::getInstanceByStreamId($response['source']['id']);

                    if (!Validate::isLoadedObject($board)) {
                        $board->stream_id = $response['source']['id'];
                        $board->url = $url;
                        $board->date_add = date('Y-m-d H:i:s');
                        $board->title = $response['source']['title'];
                    }

                    $board->date_upd = date('Y-m-d H:i:s');
                    // check if board was updated
                    if ($board->updated < ceil($response['source']['updated'] / 1000)) {
                        // set the last updated timestamp
                        $board->updated = ceil($response['source']['updated'] / 1000);

                        $board->save();
                        // get articles of the board
                        $response = $this->request(
                            'https://feedly.com/v3/mixes/contents?' .
                                http_build_query(array('streamId' => $stream_id, 'count' => 20, 'similar' => 'true', 'findUrlDuplicates' => 'false', 'ck' => time(), 'ct' => 'feedly.desktop', 'cv' => '31.0.1146', 'ranked' => 'newest')),
                            'GET',
                            array(),
                            array('Authorization: Bearer A87LNN8k8m1_1-L7qRoPq2ZmpuHKU1IIlka14lG4Ig-mc4YVjS9xxOEKfmQIu_s-WUlp7YlmSxrcs1PLjTRYf2UyUVwNWkczItvQENWUBg4UzbvhcKHdIcznO23vkszYlpZuyBXVxUQ44ztEgu0mjfQ1Erq40HhaOAw6HCn5MEAbfwooImEhlhfpvxT_T-uiN2h2EQJD9SwJTeyUsLv_DH8-WYrocVSuZYrwhkMnzKRlzA:feedly')
                        );
                        // if ($board->title == 'cannabis') {
                        //     print_r(array_map(function($item){return $item['title'];}, $response['items'])); exit;
                        // }
                        if ($response && isset($response['items'])) {
                            foreach ($response['items'] as $item) {
                                $article = BoardArticle::getInstanceByUid($item['id']);
                                if (!Validate::isLoadedObject($article)) {
                                    $article->id_feedly_board = $board->id;
                                    $article->uid = $item['id'];
                                    $article->uri = BoardArticle::generateUri();
                                    $article->url = $item['originId'];
                                    $_url = parse_url($article->url);
                                    $article->source = $_url['host'];
                                    $article->admin_category = $item['source']['title'];
                                    $article->categories = '[]';
                                    $article->date = date('Y-m-d H:i:s', ceil($item['published'] / 1000));
                                    $article->content = isset($item['fullContent']) ? $item['fullContent'] : $item['summary']['content'];
                                    $article->image = isset($item['visual']) ? ($item['visual']['url'] == 'none' ? '' : $item['visual']['url']) : '';
                                    $article->title = $item['title'];
                                    $article->confirm = 0;

                                    $article->add();
                                }
                            }
                        }
                    } else {
                        echo "> skip \n";
                        $board->save();
                    }
                }
            }
        }
    }

    public function getUrls()
    {
        return $this->urls;
    }

    private function getBoardUri($url)
    {
        $url = str_replace(
            array(
                'https://feedly.com/i/subscription/',
                '%2F',
                '%3A'
            ),
            array(
                '',
                '/',
                ':'
            ),
            $url
        );

        return strpos($url, 'feed') === 0 ? $url : '';
    }

    /**
     * Execute api request
     *
     * @param string $method
     * @param array $request_data Request parameters
     *
     * @return array
     *
     * @throws Exception
     */
    private function request($endpoint, $method = 'GET', array $request_data = array(), array $headers = array())
    {
        $curl = curl_init();

        print_r($endpoint. "\n");
        print_r(http_build_query($request_data) . "\n");

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => http_build_query($request_data),
            CURLOPT_HTTPHEADER => array_merge(array(
                'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0'
            ), $headers)
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception(
                "Feedly request error: " . $err
            );
            return false;
        }

        return Tools::jsonDecode($response, true);
    }

    public static function getInstanceByStreamId($stream_id)
    {
        return new self(
            (int)Db::getInstance()->getValue(
                "SELECT id_feedly_board FROM " . _DB_PREFIX_ . "feedly_board WHERE stream_id = '" . addslashes($stream_id) . "'"
            )
        );
    }
}

<?php

namespace App\Controller;

use App\Component\Tools;

class MainController
{
    public function run()
    {
        $action = Tools::getValue('action', null);
        $methodName = 'process'.ucfirst($action);
        if ($action && method_exists($this, $methodName)) {
            $this->{$methodName}();
        } elseif (null !== $action) {
            $this->jsonResponse(array(
                'hasError' => true,
                'message' => 'Action not found',
                'action' => $action,
                'post' => $_POST,
                'get' => $_GET
            ));
        }
        $this->jsonResponse(array(
            'hasError' => true,
            'message' => 'Wrong request method'
        ));
    }

    /**
     * @var array
     **/
    private $_urls = [];

    public function processOpenPages()
    {
        $options = [];
        $client = new \GuzzleHttp\Client($options);

        $requests = function ($urls) {
            foreach ($urls as $url) {
                yield new \GuzzleHttp\Psr7\Request(
                    'GET',
                    $url,
                    [
                        'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.116',
                        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'Accept-Encoding' => 'gzip, deflate, br',
                        'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
                        'Cache-Control' => 'max-age=0'
                    ]
                );
            }
        };

        $this->_urls = Tools::getValue('urls', array());
        if (!is_array($this->_urls)) $this->_urls = array();

        if ($this->_urls) {
            $pool = new \GuzzleHttp\Pool($client, $requests($this->_urls), [
                'concurrency' => 5,
                'fulfilled' => function (\GuzzleHttp\Psr7\Response $response, $index) {
                    // this is delivered each successful response
                    // echo $index . ' '. $response->getStatusCode() ." " . $this->_urls[$index] . "\n";
                    $this->_urls[$index] = array(
                        'url' => $this->_urls[$index],
                        'code' => $response->getStatusCode(),
                        'content' => $response->getBody()->getContents()
                    );
                },
                'rejected' => function (\GuzzleHttp\Exception\RequestException $reason, $index) {
                    // this is delivered each failed request
                    // echo \GuzzleHttp\Psr7\str($reason->getRequest());
                    $this->_urls[$index] = array(
                        'url' => $this->_urls[$index],
                        'code' => null,
                        'content' => ''
                    );
                    // $this->_rejected_urls[$index] = ['url' => $this->_urls[$index], 'response_code' => 'uknown'];
                    if ($reason->hasResponse()) {
                        $this->_urls[$index]['code'] = $reason->getResponse()->getStatusCode();
                        $this->_urls[$index]['content'] = $reason->getResponse()->getBody()->getContents();
                        // $this->_rejected_urls[$index]['response_code'] = $reason->getResponse()->getStatusCode();
                        // if ($reason->getResponse()->getStatusCode() == 503) {
                        //     $this->blocked_urls[] = $this->_urls[$index];
                        // }
                        // echo $this->_urls[$index] .' '. $reason->getResponse()->getStatusCode() . "\n";
                        // echo \GuzzleHttp\Psr7\str($reason->getResponse());
                    } else {
                        // $this->blocked_urls[] = $this->_urls[$index];
                    }
                }
            ]);
            $promise = $pool->promise();
            $promise->wait();
        }

        $this->jsonResponse(array(
            'status' => 'ok',
            'urls' => $this->_urls
        ));
    }

    public function processInit()
    {
        $this->jsonResponse(array(
            'status' => 'ok'
        ));
    }

    protected function jsonResponse(array $data = array())
    {
        if (!headers_sent()) {
            header('Content-Type: application/json');
        }

        die(json_encode($data));
    }
}
